function out = getCoherence(A)

	A = normc(A);
	dots = A' * A;
	dotsAbs = abs(dots - diag(diag(dots)));
	out = max(max(dotsAbs));

end
