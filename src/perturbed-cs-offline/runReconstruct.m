function [rrmse, rrmseOrig] = runReconstruct(ps, T, K, KTest, s, matrixNoiseSD, measNoiseSD)
%% --------------------------------------------------------------------------------------------------------
%% Compressed Sensing with Perturbations
%% Alankar Kotwal <alankarkotwal13@gmail.com>
%% --------------------------------------------------------------------------------------------------------


%clear; clc; close all;


%% ----------------------------------------- Problem specification ----------------------------------------

%ps = 8;
n = ps^2;
%T = 2;
%K = 100;
%KTest = 1000;
D = kron(dctmtx(ps)', dctmtx(ps)');
%D = randn(n, n);
psi = zeros(n*T, n*T);
for t = 1:T
	psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
end

%r = 1;
%quantVals = [0:r:1]';

if T == 2
	perms = [5, 3; 6, 8]; 
elseif T == 4
	perms = [7, 8; 2, 8; 6, 1; 3, 5];
elseif T == 6
	perms = [6, 7; 3, 6; 6, 2; 1, 4; 8, 3; 5, 2];
end


%% ------------------------------------------- Testing variables -------------------------------------------

%s = 0.1;
%matrixNoiseSD = 0.1;
%measNoiseSD = 0.1;
epsilon = 1e-5;

runName = ['../../results/perturbed-cs-offline/' strrep(num2str(now), '.', '_') '-' num2str(n) '-' num2str(T)]; 


%% -------------------------- Initialize the true and implemented sensing matrices -------------------------

phiTrue = rand(n, 1);

%[~, idxs] = min(bsxfun(@(x, y) (x-y).^2, quantVals, phiTrue'));
%phiImp = quantVals(idxs);
%phiImp = quantizeMatrix(phiTrue, quantVals);
phiImp = phiTrue + matrixNoiseSD * norm(phiTrue) / n * randn(size(phiTrue));

deltaTrue = phiImp - phiTrue;


%% ---------------------------- Get implemented and true (designed) sensing matrix -------------------------

ATrue = getAMatrix(phiTrue, psi, perms);
AImp = getAMatrix(phiImp, psi, perms);


%% ----------------- Simulate sensing step: generate random vectors and get measurements -------------------

theta = zeros(n, T, K);
for k = 1:K
	for t = 1:T
		theta(:, t, k) = sprand(n, 1, s);
	end
end
y = AImp * reshape(theta, [n*T, K]);
y = y + measNoiseSD * norm(y) / (n * K) * randn(size(y)); 
xs = reshape(psi * reshape(theta, [n*T K]), [n, T, K]);

thetaTest = zeros(n ,T, KTest);
for k = 1:KTest
	for t = 1:T
		thetaTest(:, t, k) = sprand(n, 1, s);
	end
end
yTest = AImp * reshape(thetaTest, [n*T, KTest]);
yTest = yTest + measNoiseSD * norm(yTest) / (n * KTest) * randn(size(yTest)); 
xsTest = reshape(psi * reshape(thetaTest, [n*T KTest]), [n, T, KTest]);

%% ------------------------------------------- Reconstruct! ------------------------------------------------

disp('Optimizing matrix...');
[phi, delta] = reconstruct(y, xs, phiTrue, psi, D, perms);

disp('Optimized matrix, testing on synthetic vectors...');
ARec = getAMatrix(phi, psi, perms);
xsOut = reconstructOrig(yTest, ARec, D, epsilon);
xsOutOrig = reconstructOrig(yTest, ATrue, D, epsilon);

% Errors
rrmseDelta = norm(abs(deltaTrue - delta)) / norm(deltaTrue);
rrmse = norm(xsTest(:) - xsOut(:)) / norm(xsTest(:));
rrmseOrig = norm(xsTest(:) - xsOutOrig(:)) / norm(xsTest(:));

disp(['Delta RRMSE: ' num2str(rrmseDelta)]);
disp(['Uncalibrated RRMSE: ' num2str(rrmseOrig) ', calibrated RRMSE: ' num2str(rrmse)]);

% Error statistics
rrmses = (norms(reshape(xsOut - xsTest, [n*T, KTest])) ./ norms(reshape(xsTest, [n*T, KTest])))';
rrmsesOrig = (norms(reshape(xsOutOrig - xsTest, [n*T, KTest])) ./ norms(reshape(xsTest, [n*T, KTest])))';

%% Boxplot
%scrsz = get(groot, 'ScreenSize');
%figure('Position', scrsz);
%set(gca, 'FontSize', 50);
%hold on;
%
%bp2 = boxplot([rrmses rrmsesOrig], 'symbol', 'x', 'widths', 0.4);
%xlim auto;
%ylim auto;
%
%color = ['r', 'b'];
%h = findobj(gca,'Tag','Box');
%for j=1:length(h)
%   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
%end
%c = get(gca, 'Children');
%hleg1 = legend([c(1) c(2)], 'Calibrated', 'Uncalibrated', 'Location', 'NorthEast');
%
%ylabel('RRMSE');
%title(['RRMSE comparison: n = ' num2str(n) ', T = ' num2str(T)])
%savefig([runName '.fig']);
%
%% Save mat
%save([runName '.mat'], 'phi', 'perms', 'D', 'rrmses', 'rrmsesOrig', 'rrmseDelta', 'delta', 's', 'K', 'r', 'KTest', 'matrixNoiseSD', 'measNoiseSD');
