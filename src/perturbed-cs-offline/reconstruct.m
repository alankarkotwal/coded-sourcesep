function [phiOut, delta] = reconstruct(y, xs, phi, psi, D, perms)

	% Sizes
	n = size(phi, 1);
	ps = sqrt(n);
	T = size(perms, 1);
	K = size(y, 2);

	% Utility variables
	permsResp = zeros(n, T);
	for t = 1:T
		permsResp(:, t) = reshape(circshift(reshape((1:n)', [ps, ps]), perms(t, :)), [n 1]);
	end

	% Get ideal (specified) sensing matrix
	[ATrue, sensTrue] = getAMatrix(phi, psi, perms);

	% Get linear system for solving for delta 
	idealYs = sensTrue * reshape(xs, [n*T K]);
	rhs = y(:) - idealYs(:);
	coeffMat = zeros(n, n, K);

	for k = 1:K
		for beta = 1:n
			for t = 1:T
				coeffMat(beta, permsResp(beta, t), k) = ...
				coeffMat(beta, permsResp(beta, t), k) + xs(beta, t, k);
			end
		end
	end
	coeffMatTemp = coeffMat;
	coeffMat = reshape(permute(coeffMat, [1 3 2]), [n*K, n]);

	% Solve for delta 
	delta = coeffMat \ rhs;

	% Spit phi out
	phiOut = phi + delta;

end
