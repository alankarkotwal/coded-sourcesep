function runRunReconstruct(t)
		
ps = 8;
matrixNoiseSDs = (0.03:0.03:0.24);
K = 100;
KTest = 100;
s = 0.01;
measNoiseSD = 0.1;

runName = ['../../results/perturbed-cs-offline/' strrep(num2str(now), '.', '_') '-' num2str(ps^2) '-' num2str(t)]; 

rrmses = zeros(KTest, size(matrixNoiseSDs, 2));
rrmsesOrig = zeros(KTest, size(matrixNoiseSDs, 2));

count = 1;
for matrixNoiseSD = matrixNoiseSDs
	[rrmses(:, count), rrmsesOrig(:, count)] = runReconstruct(ps, t, K, KTest, s, matrixNoiseSD, measNoiseSD);
	count = count + 1;
end

save([runName '.mat'], 'rrmsesOrig', 'rrmses', 't', 'matrixNoiseSDs');
