T = 4;
ps = 8;
s = 0.08;

nSamp = 100;
nTestSamp = 1000;
nStarts = 100;
nSupp = 3;
eta = 1e-2;
epsil = eta;
bound = 0.2;

nIter = Inf;
repCountMax = 10;

if T == 2
	%load ../../results/descent-cacti-avg/736774_7153-64-2-10000.mat phi perms;
	%phiAvgCoh = phi;
	load ../../results/descent-cacti/736774_7098-64-2-10000 phi perms;
	phiCoh = phi;
elseif T == 4
	%load ../../results/descent-cacti-avg/736774_801-64-4-10000.mat phi perms;
	%phiAvgCoh = phi;
	load ../../results/descent-cacti/736774_7105-64-4-10000.mat phi perms;
	phiCoh = phi;
elseif T == 6
	%load ../../results/descent-cacti-avg/736775_2832-64-6-10000.mat phi perms;
	%phiAvgCoh = phi;
	load ../../results/descent-cacti/736774_7124-64-6-10000.mat phi perms;
	phiCoh = phi;
end
clear phi;

n = ps^2;
D = kron(dctmtx(8)', dctmtx(8)');
psi = kron(eye(T), D);

supps = zeros(floor(n*s), nSupp);
for supp = 1:nSupp
	temp = randperm(n);
	supps(:, supp) = temp(1:floor(n*s))';
end
supps = sort(supps);

thetas = generateSamplesSupport(n, T, nSamp, supps);

objs = zeros(nStarts, 1);
objsCoh = zeros(nStarts, 1);
phis = zeros(n, nStarts);
phiInits = zeros(n, nStarts);

for start = 1:nStarts
	disp(['*************** Start ' num2str(start) ' ***************']);
	[phis(:, start), objs(start), phiInits(:, start)] = descentRandomSearch(perms, psi, thetas, epsil, bound, nIter, repCountMax);
end
	 
[minObj, minIdx] = min(objs);
phi = phis(:, minIdx);
phiInit = phiInits(:, minIdx);

A = getAMatrix(phi, psi, perms);
ARand = getAMatrix(phiInit, psi, perms);
ACoh = getAMatrix(phiCoh, psi, perms);
%AAvgCoh = getAMatrix(phiAvgCoh, psi, perms);

testThetas = generateSamplesSupport(n, T, nTestSamp, supps);
noiseInsts = 2 * eta / sqrt(n) * (rand(n, nTestSamp) - 0.5);

desErrs = generateErrs(A, testThetas, psi, epsil, noiseInsts);
randErrs = generateErrs(ARand, testThetas, psi, epsil, noiseInsts);
cohErrs = generateErrs(ACoh, testThetas, psi, epsil, noiseInsts);
%avgCohErrs = generateErrs(AAvgCoh, testThetas, psi, epsil, noiseInsts);

runName = ['../../results/descent-cacti-mmse/' strrep(num2str(now), '.', '_') '-' num2str(n) 'x' num2str(T) '-' strrep(num2str(s), '.', '_') '-support'];
%save([runName '.mat'], 'phi', 'phiInit', 'phiCoh', 'phiAvgCoh', 'minObj', 's', 'epsil', 'desErrs', 'randErrs', 'cohErrs', 'avgCohErrs', 'perms');
save([runName '.mat'], 'phi', 'phiInit', 'phiCoh', 'minObj', 's', 'epsil', 'desErrs', 'randErrs', 'cohErrs', 'perms');
