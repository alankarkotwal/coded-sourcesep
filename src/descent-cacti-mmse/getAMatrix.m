function [A, sens] = getAMatrix(phi, psi, perms)

	n = size(phi, 1);
	ps = sqrt(n);
	T = size(perms, 1);

	sens = zeros(n, n*T);
	for t = 1:T
		thisPhi = reshape(circshift(reshape(phi, [ps, ps]), perms(t, :)), [n 1]);
		sens(:, (t-1)*n+1:t*n) = diag(thisPhi);
	end
	A = sens * psi;

end

