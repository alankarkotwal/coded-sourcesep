function [phi, obj, phiInit] = descentRandomSearch(perms, psi, thetas, epsil, bound, nIter, repCountMax) 

	n = size(thetas, 1);
	T = size(thetas, 2);
	nSamp = size(thetas, 3);

	disp('****************');
	disp('Initial objective calculation ...');
	phi = rand(n, 1);
	phiInit = phi;
	A = getAMatrix(phi, psi, perms);
	obj = objFn(A, thetas, psi, epsil);

	iter = 1;
	repCount = 0;
	while iter <= nIter 

		disp('****************');
		disp(['Iter ' num2str(iter) '...']);

		phiNew = phi + bound * randn(n, 1);
		phiNew(phiNew < 0) = 0;
		phiNew(phiNew > 1) = 1;
		ANew = getAMatrix(phiNew, psi, perms);
		objNew = objFn(ANew, thetas, psi, epsil);

		if objNew < obj
			phi = phiNew;
			obj = objNew;
			repCount = 0;
		else
			repCount = repCount + 1;
			if repCount > repCountMax
				disp(obj);
				break;
			end
		end

		iter = iter + 1;
		disp(obj);

	end

end
