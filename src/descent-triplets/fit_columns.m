function [err, coeffs] = fit_columns(A, i, j, k)

    if(max([i j k]) > size(A, 2))
        error('Fit indices are bigger than matrix size!');
    end

	Ajk = [A(:, j) A(:, k)];
	Ai = A(:, i);
    coeffs = Ajk\Ai;
	err = sum(sum((Ai - Ajk*coeffs).^2)); 

end
