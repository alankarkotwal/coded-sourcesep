function out = objDer(A, ind, coeffs)

	out = zeros(size(A));
	Ajk = [A(:, ind(2)) A(:, ind(3))];
	Ai = A(:, ind(1));

	out(:, ind(1)) = 2*(Ai - Ajk*coeffs);

	for i = 1:2

		j = ind(i+1);
		k = ind(4-i);

		Aj = A(:, j);
		Ak = A(:, k);
		
		d = [Ai A(:, j) A(:, k)];	
		d = d'*d;
		
		denom = d(2, 2) * d(3, 3) - d(2, 3)^2;
		num = d(1, 2)^2 * d(3, 3) + ...
		  d(1, 3)^2 * d(2, 2) - ...
		  2 * d(1, 2) * d(2, 3) * d(1, 3);
		denomDer = d(3, 3) * Aj - 2 * d(2, 3) * Ak;
		numDer = 2 * ((d(3, 3) * d(1, 2) - ...
				   d(1, 3) * d(2, 3)) * Ai + ...
			 d(1, 3)^2 * Aj - ...
			 d(1, 3) * d(2, 3) * Ak);
		
		out(:, ind(i+1)) = (num * denomDer - ...
				 denom * numDer) / ...
				denom;

	end
	
end
