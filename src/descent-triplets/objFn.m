function [minErr, minIndices, minCoeffs] = objFn(A)

	minErr = Inf;
	minIndices = [0 0 0];
	minCoeffs = [0; 0];

	for i = 1:size(A, 2)

		remIndices = [1:i-1 i+1:size(A, 2)];
		fitCols = combnk(remIndices, 2);

		for jk = fitCols'

			[thisErr, thisCoeffs] = fit_columns(A, i, jk(1), jk(2));

			if thisErr < minErr
				minErr = thisErr;
				minIndices = [i jk(1) jk(2)];
				minCoeffs = thisCoeffs;
			end

		end

	end

end
