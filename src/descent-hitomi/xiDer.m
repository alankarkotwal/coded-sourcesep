function out = xiDer(mu, nu, beta, gamma, delta, epsilon, phi, D)

    out = 0;

    if mu == delta
        out = out + xiDerSumTerm(phi, D, nu, gamma) * ...
                    D(epsilon, beta)^2 * phi(epsilon, mu);
    end

    if nu == delta
        out = out + xiDerSumTerm(phi, D, mu, beta) * ...
                    D(epsilon, gamma)^2 * phi(epsilon, nu);
    end
    
    out = out / xi(mu, nu, beta, gamma, phi, D);

end
