Optimizing Coherence Directly
=============================

This is a set of routines that optimize a Hitomi-style sensing matrix directly
for minimum coherence, assuming an orthogonal dictionary D. 
