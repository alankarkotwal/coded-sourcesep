function out = objDer(sgn, muMax, nuMax, betaMax, gammaMax, phi, D)

	n = size(phi, 1);
	T = size(phi, 2);

	out = zeros(n, T);

	% for mu
	for beta = 1:n
		out(beta, muMax) = sgn * normDotProductDer(muMax, nuMax, betaMax, gammaMax, muMax, beta, phi, D);
	end

	% for nu
	for gamma = 1:n
		out(gamma, nuMax) = sgn * normDotProductDer(muMax, nuMax, betaMax, gammaMax, nuMax, gamma, phi, D);
	end

end
