function out = xi(mu, nu, beta, gamma, phi, D)

	out = sqrt(sum(phi(:, mu).^2 .* D(:, beta).^2) * ...
               sum(phi(:, nu).^2 .* D(:, gamma).^2));

end
