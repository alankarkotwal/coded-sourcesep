imSize1 = 20;
imSize2 = 20;
T = 5;
D = kron(dctmtx(imSize2)', dctmtx(imSize1)');
phi = randn(size(D, 1), T);

n = imSize1 * imSize2;

delta = 0.0001;
alpha = ceil(imSize1 * imSize2 * rand - 1) + 1;
mu = ceil(T * rand - 1) + 1;

[objOrig, sgn, muMax, nuMax, betaMax, gammaMax] = objFnFast(phi, D);

phiNew = phi;
phiNew(alpha, mu) = phiNew(alpha, mu) + delta;
objFin = objFnFast(phiNew, D);

calcDer = objDer(sgn, muMax, nuMax, betaMax, gammaMax, phi, D);

digDer = (objFin - objOrig) / delta;
disp(abs(digDer - calcDer(alpha, mu))/abs(digDer));
