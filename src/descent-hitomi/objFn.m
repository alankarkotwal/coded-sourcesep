function [obj, sgn, muMax, nuMax, betaMax, gammaMax] = objFn(chis, xis)

	n = size(chis, 3);
	T = size(chis, 1);
	
	obj = 0;
	muMax = 0;
	nuMax = 0;
	betaMax = 0;
	gammaMax = 0;

	for mu = 1:T
		for nu = 1:(mu-1)
			for beta = 1:n
				for gamma = 1:n
					temp = normDotProduct(mu, nu, beta, gamma, chis, xis);
					if(abs(temp) > obj)
						obj = abs(temp);
						sgn = sign(temp);
						muMax = mu;
						nuMax = nu;
						betaMax = beta;
						gammaMax = gamma;
					end
				end
			end
		end
	end

	for mu = 1:T
		for beta = 1:n
			for gamma = 1:(beta-1)
				temp = normDotProduct(mu, mu, beta, gamma, chis, xis);
				if(abs(temp) > obj)
					obj = abs(temp);
					sgn = sign(temp);
					muMax = mu;
					nuMax = mu;
					betaMax = beta;
					gammaMax = gamma;
				end
			end
		end
	end

end
