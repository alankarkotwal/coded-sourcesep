n = 16;
nMeas = 10;
s = 0.4;
nSupps = 2;
nSamp = 1000000;
nVec = 1000;

[vecs, supps] = generateVectors(n, s, nSupps, nVec);

objs = zeros(nSamp, 1);
mats = zeros(nMeas, n, nSamp); 

parpool(20);
parfor i = 1:nSamp

	disp(['Sampling matrix ' num2str(i)]);

	thisA = normc(randn(nMeas, n));
	mats(:, :, i) = thisA;
	objs(i) = objFn(thisA, supps);

end

[obj, idx] = min(objs);
AOpt = mats(:, :, idx);

delete(gcp);

save('../../results/support/samp.mat', 'AOpt', 'obj', 'supps', 's', 'vecs');
