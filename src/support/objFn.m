function [out, suppIdx, rowIdx, colIdx] = objFn(A, D, supps)

	nSupps = size(supps, 2);
	A = A * D;

	cohs = zeros(nSupps, 1);
	inIdxs = zeros(2, nSupps);
	for i = 1:nSupps
		thisA = A(:, supps(:, i));
		[cohs(i), inIdxs(1, i), inIdxs(2, i)] = getCoherence(thisA);
	end

	[out, suppIdx] = max(cohs);
	rowIdx = inIdxs(1, suppIdx);
	colIdx = inIdxs(2, suppIdx);

end
