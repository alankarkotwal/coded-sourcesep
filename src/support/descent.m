function [A, obj, AOrig, objOrig] = descent(nMeas, D, supps, pos, step, stepFactor, iter, epsil, repCountMax, minChange, dbg)

	if nargin == 4
		step = 0.1;
		stepFactor = 0.5;
		iter = Inf;
		epsil = 0;
		repCountMax = 5;
		minChange = 0;
		dbg = 0;
	elseif nargin == 5
		stepFactor = 0.5;
		iter = Inf;
		epsil = 0;
		repCountMax = 5;
		minChange = 0;
		dbg = 0;
	elseif nargin == 6
		iter = Inf;
		epsil = 0;
		repCountMax = 5;
		minChange = 0;
		dbg = 0;
	elseif nargin == 7
		epsil = 0;
		repCountMax = 5;
		minChange = 0;
		dbg = 0;
	elseif nargin == 8
		repCountMax = 5;
		minChange = 0;
		dbg = 0;
	elseif nargin == 9
		minChange = 0;
		dbg = 0;
	elseif nargin == 10
		dbg = 0;
	end
	
	% Sizes
	n = size(D, 1);
	ps = sqrt(n);

	% Optimization variables
	if pos
		A = normc(rand(nMeas, n));
	else
		A = normc(randn(nMeas, n));
	end
	AOrig = A;
		
	% Objective function
	[obj, suppIdx, rowIdx, colIdx] = objFn(A, D, supps);
	objOrig = obj;

	count = 0;
	repCount = 0;
	iTookAStep = 1;

	if dbg disp([count obj suppIdx rowIdx colIdx]); end;
	
	while count < iter
		
		if iTookAStep
			descDir = normc(objDer(A, D, supps(:, suppIdx), rowIdx, colIdx));
		end
		
		ANew = A - step * norm(A, 'fro') / norm(descDir, 'fro') * descDir;
		if pos
			ANew(ANew < epsil) = epsil;
			ANew(ANew > 1) = 1;
		end
		ANew = normc(ANew);
		
		[objNew, suppIdxNew, rowIdxNew, colIdxNew] = objFn(ANew, D, supps);
		
		if objNew <= obj * (1 - minChange)
			A = ANew;
			obj = objNew;
			suppIdx = suppIdxNew;
			rowIdx = rowIdxNew;
			colIdx = colIdxNew;
			%step = step / stepFactor;

			count = count + 1;
			repCount = 0;
			iTookAStep = 1;
		else
			step = step * stepFactor;
			repCount = repCount + 1;
			iTookAStep = 0;
			if repCount >= repCountMax
				break;
			end
		end
	
		if dbg disp([count obj suppIdx rowIdx colIdx]); end;

	end
	
end
