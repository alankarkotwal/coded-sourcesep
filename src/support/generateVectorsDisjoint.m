function [vecs, supps] = generateVectors(n, s, nSupp, nSamp)

	nxPerSupp = floor(n * s / nSupp);

	% Generate random supports
	perm = randperm(n)';
	supps = zeros(nxPerSupp, nSupp);
	for i = 1:nSupp
		supps(:, i) = perm(nxPerSupp*(i-1)+1:nxPerSupp*i);
	end
	supps = sort(supps);

	vecs = zeros(n, nSamp);
	for i = 1:nSamp
	
		tempX = zeros(n, 1);
		tempX(supps(:, randi(nSupp))) = rand(nxPerSupp, 1);
		vecs(:, i) = tempX;

	end

end
