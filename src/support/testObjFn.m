nMeas = 32;
n = 64;
ps = sqrt(n);

A = normc(rand(nMeas, n));
D = kron(dctmtx(ps)', dctmtx(ps)');
[~, supps] = generateVectors(n, 0.2, 3, 0);

[coh, suppIdx, rowIdx, colIdx] = objFn(A, D, supps);

thisA = A(:, supps(:, suppIdx));
disp([coh, thisA(:, rowIdx)' * thisA(:, colIdx)]);

disp([getCoherence(A(:, supps(:, 1))), getCoherence(A(:, supps(:, 2))), getCoherence(A(:, supps(:, 3)))]);
disp(suppIdx);

der = objDer(A, D, supps(:, suppIdx), rowIdx, colIdx);
disp(der);
