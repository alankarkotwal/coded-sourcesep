f = 0.5;
ps = 10;
pos = 1;
testMat = 1;
nStarts = 10;
s = 0.1;
nSupp = 3;
nSamp = 100;
noiseSD = 0;

n = ps^2;
nMeas = floor(f * n);
D = kron(dctmtx(ps)', dctmtx(ps)');
D = eye(n);

[testVecs, supps] = generateVectors(n, s, nSupp, nSamp, noiseSD);

step = 1;
stepFactor = 0.5;
iter = Inf;
repCountMax = 8;
eps = 0;
minChange = 0.00001;
dbg = 0;
epsil = 1e-6;

runName = ['../../results/support/' strrep(num2str(now), '.', '_') '-' num2str(n) '-' num2str(nMeas) '-' num2str(pos) '-' num2str(nStarts)];

As = zeros(nMeas, n, nStarts);
origAs = zeros(nMeas, n, nStarts);
objs = zeros(nStarts, 1);
origObjs = zeros(nStarts, 1);

parfor i = 1:nStarts
	
	disp(['Optimizing start ' num2str(i) '...']);
	[As(:, :, i), objs(i), origAs(:, :, i), origObjs(i)] = descent(nMeas, D, supps, pos, step, stepFactor, iter, eps, repCountMax, minChange, dbg);

end

[obj, pos] = min(objs);
A = As(:, :, pos);
origA = origAs(:, :, pos);
origObj = origObjs(pos);

disp('The optimum and starting coherence values are ');
disp([obj origObj]);


if testMat

	rrmses = zeros(nSamp, 1);
	rrmsesOrig = zeros(nSamp, 1);
	
	for i = 1:nSamp
	
		disp(['Testing vector ' num2str(i) '...'])

		thisX = D * testVecs(:, i);

		y = A * thisX;
		origY = origA * thisX;
	
		xOut = solveBP(y, A, D, epsil);
		xOutOrig = solveBP(origY, origA, D, epsil);
		
		rrmses(i) = norm(xOut - thisX) / norm(thisX);
		rrmsesOrig(i) = norm(xOutOrig - thisX) / norm(thisX);

	end

	save([runName '.mat'], 'A', 'obj', 'origA', 'origObj', 'supps', 'D', 'testVecs', 'rrmses', 'rrmsesOrig');

	% Boxplot
	scrsz = get(groot, 'ScreenSize');
	figure('Position', scrsz);
	set(gca, 'FontSize', 50);
	hold on;
	
	bp2 = boxplot([rrmses rrmsesOrig], 'symbol', 'x', 'widths', 0.4);
	xlim auto;
	ylim auto;
	
	color = ['r', 'b'];
	h = findobj(gca,'Tag','Box');
	for j=1:length(h)
	   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
	end
	c = get(gca, 'Children');
	hleg1 = legend([c(1) c(2)], 'Optimized', 'Original', 'Location', 'NorthEast');
	
	ylabel('RRMSE');
	title(['RRMSE comparison']);
	savefig([runName '.fig']);

else

	save([runName '.mat'], 'A', 'obj', 'origA', 'origObj', 'supps', 'D', 'testVecs');

end
