function [out, rowIdx, colIdx] = getCoherence(M)

	M = normc(M);
	dots = M' * M;
	dots = dots - diag(diag(dots));

	[maxs, rowIdxs] = max(dots);
	[out, colIdx] = max(maxs);
	rowIdx = rowIdxs(colIdx);

end
