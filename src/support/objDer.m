function out = objDer(A, D, supp, rowIdx, colIdx)

	out = zeros(size(A));

	m = supp(rowIdx);
	n = supp(colIdx);

	rowNorm = norm(A * D(:, m));
	colNorm = norm(A * D(:, n));

	dTerm = D(:, m) * D(:, n)';
	ADTerm = A * dTerm;
	ADTermT = A * dTerm';

	out = (ADTerm + ADTermT) / (rowNorm * colNorm) - (2 * D(:, m)' * A' * A * D(:, n) * A * ...
													   (colNorm * D(:, m) * D(:, m)' + ...
													    rowNorm * D(:, n) * D(:, n)')) / ...
													  (rowNorm ^ 2 * colNorm ^ 2);

end
