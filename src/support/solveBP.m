function out = solveBP(y, A, D, epsilon)

	n = size(A, 1);
	T = size(A, 2) / n;

	cvx_begin quiet
		variable theta(n*T)
		minimize(norm(theta, 1))
		subject to
			norm(y - A * theta) <= epsilon
	cvx_end

	out = D * theta;

end
