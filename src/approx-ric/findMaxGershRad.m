function out = findMaxGershRad(A, s)

	% We assume columns of A are normalized
	nT = size(A, 2);
	combs = combnk(1:nT, s);

	out = 0;

	for i = 1:size(combs, 1)

		subsA = A(:, combs(i, :));
		dots = subsA' * subsA;
		dotsSub = dots - eye(size(dots));
	
		maxRowSum = max(sum(abs(dotsSub)));

		out = max(out, maxRowSum);

	end

end
