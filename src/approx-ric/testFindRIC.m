% Generate a random orthogonal matrix
A = rand(3);
[A, ~] = qr(A);

disp(findRIC(A, 1));
disp(findRIC(A, 2));
disp(findRIC(A, 3));

