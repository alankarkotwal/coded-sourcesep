function out = findRIC(A, s)

	A = normc(A);
	nT = size(A, 2);
	combs = combnk(1:nT, s);

	out = 0;

	for i = 1:size(combs, 1)

		subsA = A(:, combs(i, :));
		dots = subsA' * subsA;
		dotsSub = dots - eye(size(dots));

		maxEig = max(abs(eig(dotsSub)));
		out = max(out, maxEig);

	end

end
