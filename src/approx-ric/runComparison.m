function runComparison(patchSize, T, s, nIter)

	runName = input('Enter runName: ', 's');

	brauerDefs = zeros(nIter, 1);
	gershDefs = zeros(nIter, 1);
	cohDefs = zeros(nIter, 1);
	RICs = zeros(nIter, 1);

	parfor i = 1:nIter

		disp(i);

		[brDef, gershDef, cohDef, RIC] = doComparisonNonHitomi(patchSize*patchSize, T, s);
		RICs(i) = RIC;
		brauerDefs(i) = brDef;
		gershDefs(i) = gershDef;
		cohDefs(i) = cohDef;
	
	end

	h = histogram(RICs);
	title('RICs');
	xlabel('RIC');
	ylabel('Frequency');
	print('-dpng', ['../../results/approx-ric/' runName '-' num2str(patchSize) '-' num2str(T) '-RIC.png']);

	h = histogram(brauerDefs);
	title('How loose is the Brauer bound?');
	xlabel('Deficit');
	ylabel('Frequency');
	print('-dpng', ['../../results/approx-ric/' runName '-' num2str(patchSize) '-' num2str(T) '-brauerDef.png']);

	h = histogram(gershDefs);
	title('How loose is the Gershgorin bound?');
	xlabel('Deficit');
	ylabel('Frequency');
	print('-dpng', ['../../results/approx-ric/' runName '-' num2str(patchSize) '-' num2str(T) '-gershDef.png']);

	h = histogram(cohDefs);
	title('How loose is the coherence bound?');
	xlabel('Deficit');
	ylabel('Frequency');
	print('-dpng', ['../../results/approx-ric/' runName '-' num2str(patchSize) '-' num2str(T) '-cohDef.png']);

end
