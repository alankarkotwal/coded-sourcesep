function out = findCohBound(A, s)

	% We assume columns of A are normalized
	A = normc(A);
	dots = A' * A; 
	dots = abs(dots - diag(ones(size(dots, 1), 1)));
	coh = max(max(dots));

	out = coh * (s - 1);

end
