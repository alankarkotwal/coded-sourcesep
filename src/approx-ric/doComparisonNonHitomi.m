function [brDef, gershDef, cohDef, RIC] = doComparison(n, T, s)

	A = randn(n, n*T);
	A = A ./ repmat(sqrt(sum(A.^2)), size(A, 1), 1);

	RIC = findRIC(A, s);
	%disp(['The RIC is ' num2str(RIC) '.']);

	brauer = findBrauerBounds(A, s);
	%disp(['The Brauer bound is ' num2str(brauer) '.']);
	brDef = brauer - RIC;

	gersh = findMaxGershRad(A, s);
	%disp(['The maximum Gershgorin radius is ' num2str(gersh) '.']);
	gershDef = gersh - RIC;

	cohBound = findCohBound(A, s);
	%disp(['The coherence bound is ' num2str(cohBound) '.']);
	cohDef = cohBound - RIC;

end
