function out = objFn(A, xs, epsil)

	out = sum(generateErrs(A, xs, epsil, zeros(size(A, 1), size(xs, 2)))) / numel(xs);

end
