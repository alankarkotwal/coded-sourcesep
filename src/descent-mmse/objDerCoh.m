function out = objDerCoh(A, idx1, idx2)

	ai = A(:, idx1);
	aj = A(:, idx2);
	
	aii = ai' * ai;
	ajj = aj' * aj;
	aij = ai' * aj;

	out = zeros(size(A));

	out(:, idx1) = aj / sqrt(aii * ajj) - aij * ajj * ai / (2 * (aii * ajj) ^ (3/2));
	out(:, idx2) = ai / sqrt(aii * ajj) - aij * aii * aj / (2 * (aii * ajj) ^ (3/2));

end
