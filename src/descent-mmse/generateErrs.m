function errs = generateErrs(A, xs, epsil, noiseInsts)
	
	nTestSamp = size(xs, 2);

	errs = zeros(nTestSamp, 1);
	parfor samp = 1:nTestSamp
		thisX = xs(:, samp);
		thisY = A * thisX + noiseInsts(:, samp);
		errs(samp) = norm(reconstruct(thisY, A, epsil) - thisX)^2;
		%errs(samp) = (norm(reconstruct(y, A, epsil) - thisX) / ...
		%			  norm(thisX))^2;
	end

end
