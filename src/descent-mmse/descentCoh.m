function [A, obj] = descent(m, n, AInit, step, stepFactor, iter, eps, repCountMax, minChange)

	if nargin == 3
		step = 0.1;
		stepFactor = 0.5;
		iter = Inf;
		eps = 0.5;
		repCountMax = 5;
		minChange = 0.0001;
	elseif nargin == 4
		stepFactor = 0.5;
		iter = Inf;
		eps = 0.5;
		repCountMax = 5;
		minChange = 0.0001;
	elseif nargin == 5
		iter = Inf;
		eps = 0.5;
		repCountMax = 5;
		minChange = 0.0001;
	elseif nargin == 6
		eps = 0.5;
		repCountMax = 5;
		minChange = 0.0001;
	elseif nargin == 7
		repCountMax = 5;
		minChange = 0.0001;
	elseif nargin == 8
		minChange = 0.0001;
	end

	A = AInit;
	[obj, idx1, idx2] = objFnCoh(A);

	disp([0.0 obj]);

	count = 0;
	repCount = 0;
	iTookAStep = 1;
	descDir = zeros(m, n);

	while count < iter
	
		if iTookAStep
			descDir = objDerCoh(A, idx1, idx2);
		end
		
		ANew = A - step * descDir;
		%phiNew(phiNew < eps) = eps;

		[objNew, idx1New, idx2New] = objFnCoh(ANew);

		if objNew < obj - minChange
			A = ANew;
			idx1 = idx1New;
			idx2 = idx2New;
			obj = objNew;
			%step = step / stepFactor;
			count = count + 1;
			repCount = 0;
			iTookAStep = 1;
		else
			step = step * stepFactor;
			repCount = repCount + 1;
			iTookAStep = 0;
			if repCount >= repCountMax
				break;
			end
		end
	
		disp([count obj]);
	
	end
	
end

