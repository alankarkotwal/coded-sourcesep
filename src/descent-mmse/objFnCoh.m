function [out, idx1, idx2] = objFnCoh(A)

	A = normc(A);
	dots = A' * A;
	dots = abs(dots - diag(diag(dots)));

	[maxCols, maxColIdxs] = max(dots);
	[out, idx2] = max(maxCols);
	idx1 = maxColIdxs(idx2);

end
