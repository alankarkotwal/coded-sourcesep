function [A, obj, AInit] = descentRandomSearch(m, xs, epsil, bound, nIter, repCountMax) 

	n = size(xs, 1);
	nSamp = size(xs, 2);

	disp('****************');
	disp('Initial objective calculation ...');
	A = randn(m, n);
	AInit = A;
	obj = objFn(A, xs, epsil);

	iter = 1;
	repCount = 0;
	while iter <= nIter 

		disp('****************');
		disp(['Iter ' num2str(iter) '...']);

		ANew = A + bound * randn(m, n);
		objNew = objFn(ANew, xs, epsil);
		if objNew < obj
			A = ANew;
			obj = objNew;
			repCount = 0;
		else
			repCount = repCount + 1;
			if repCount > repCountMax
				disp(obj);
				break;
			end
		end

		iter = iter + 1;
		disp(obj);

	end

end
