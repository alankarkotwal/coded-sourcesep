function xs = generateSamples(n, s, nSamp, supps)
	
	xs = zeros(n, nSamp);

	if nargin == 3
		for samp = 1:nSamp
			xs(:, samp) = sprandn(n, 1, s);
		end
	else
		for samp = 1:nSamp
			thisSup = randi(size(supps, 2));
			xs(supps(:, thisSup), samp) = randn(size(supps, 1), 1);
		end
	end

end
