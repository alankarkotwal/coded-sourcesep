m = 8;
n = 50;
s = 0.08;

nSamp = 100;
nTestSamp = 1000;
nStarts = 10;
nSupp = 3;
eta = 1e-2;
epsil = eta;
bound = 0.2;

nIter = 100;
repCountMax = 8;

supps = zeros(floor(n*s), nSupp);
for supp = 1:nSupp
	temp = randperm(n);
	supps(:, supp) = temp(1:floor(n*s))';
end
xs = generateSamples(n, s, nSamp, supps);

objs = zeros(nStarts, 1);
objsCoh = zeros(nStarts, 1);
As = zeros(m, n, nStarts);
AInits = zeros(m, n, nStarts);
AsCoh = zeros(m, n, nStarts);

for start = 1:nStarts
	disp(['*************** Start ' num2str(start) ' ***************']);
	[As(:, :, start), objs(start), AInits(:, :, start)] = descentRandomSearch(m, xs, epsil, bound, nIter, repCountMax);
	[AsCoh(:, :, start), objsCoh(start)] = descentCoh(m, n, AInits(:, :, start));
end
	 
[minObj, minIdx] = min(objs);
A = As(:, :, minIdx);
AInit = AInits(:, :, minIdx);

[minObjCoh, minIdxCoh] = min(objsCoh);
ACoh = AsCoh(:, :, minIdxCoh);

%randA = randn(m, n);
ARand = AInit;

testXs = generateSamples(n, s, nTestSamp, supps);
noiseInsts = 2 * eta / sqrt(m) * (rand(m, nTestSamp) - 0.5);

desErrs = generateErrs(A, testXs, epsil, noiseInsts);
randErrs = generateErrs(ARand, testXs, epsil, noiseInsts);
cohErrs = generateErrs(ACoh, testXs, epsil, noiseInsts);

runName = ['../../results/descent-mmse/' strrep(num2str(now), '.', '_') '-' num2str(m) 'x' num2str(n) '-' strrep(num2str(s), '.', '_') '-support'];
save([runName '.mat'], 'A', 'AInit', 'ACoh', 'minObj', 'minObjCoh', 's', 'epsil', 'desErrs', 'randErrs', 'cohErrs', 'supps');
