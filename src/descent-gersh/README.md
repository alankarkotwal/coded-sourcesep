Optimizing Sensing Matrices with Gershgorin Radii
=============================

This is a set of routines that optimize a Hitomi-style sensing matrix directly
for minimizing the maximum Gershgorin radius for an appropriate matrix for an
upper bound on the RIC, assuming a dictionary D. 