imSize1 = 4;
imSize2 = 4;
T = 2;
D = kron(dctmtx(imSize2)', dctmtx(imSize1)');
s = 3;
phi = randn(size(D, 1), T);
theta = 100;
n = imSize1 * imSize2;

[chis, xis] = calcChiXi(phi, D);
softObj = objFn(chis, xis, theta, s);

sens = zeros(n, n*T);
psi = zeros(n*T, n*T);
for t = 1:T
	sens(:, (t-1)*n+1:t*n) = diag(phi(:, t));
	psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
end
A = sens * psi;
A = normc(A);

nT = size(A, 2);
combs = combnk(1:nT, s);

hardObj = 0;

for i = 1:size(combs, 1)

	subsA = A(:, combs(i, :));
	dots = subsA' * subsA;
	dotsSub = dots - eye(size(dots));

	maxRowSum = max(sum(abs(dotsSub)));

	hardObj = max(hardObj, maxRowSum);

end

disp((softObj-hardObj)/hardObj);
disp((objFnAct(phi, D, s)-hardObj)/hardObj);
