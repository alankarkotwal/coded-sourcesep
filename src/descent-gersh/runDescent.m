runName = input('Enter runName: ', 's');

n1 = 4;
n2 = 4;
T = 2;
s = 4;

nStarts = 50;
theta = 100;

step = 10;
stepFactor = 0.5;
iter = Inf;
repCountMax = 8;
eps = 0;
minChange = 0.001;

D = kron(dctmtx(n2)', dctmtx(n1)');

phis = zeros(n1*n2, T, nStarts);
objs = zeros(nStarts, 1);

parfor i = 1:nStarts
	
	disp(strcat('***** Start ', num2str(i), ' *****'));
	[thisPhi, thisObj, objAct] = descent(T, D, s, theta, step, stepFactor, iter, eps, repCountMax, minChange);

	phis(:, :, i) = thisPhi;
	objs(i) = objAct;

end

[obj, pos] = min(objs);
phi = phis(:, :, pos);

cohEst = obj;

disp('The mini-max Gershgorin radius is ');
disp(num2str(cohEst));

nowTime = now;
fileID = fopen(strcat('../../results/descent-gersh/', num2str(nowTime), '-', runName, '-output.txt'), 'w');
fprintf(fileID, '%f \n', cohEst);
for i = 1:T
	fprintf(fileID, strcat('phi_', num2str(i), ':\n'));
	fprintf(fileID, '%f \n', phi(:, i));
end
fclose(fileID);

save(strcat('../../results/descent-gersh/', num2str(nowTime), '-', runName, '.mat'));
