function [out, max] = objFn(chis, xis, theta, s)

	if nargin == 2
		theta = 25;
	end

	n = size(chis, 3);
	T = size(chis, 1);

	nT = n * T;
	combs = combnk(1:nT, s);

    max = 0;
	out = 0;
	for sInd = 1:size(combs, 1)
        for i = 1:s

            % Calculate row sums for sInd, i
            thisIMu = floor((combs(sInd, i) - 1) / n + 1);
            thisIBeta = mod(combs(sInd, i) - 1, n) + 1;
            temp = 0;
            for j = 1:s
                temp = temp + abs(normDotProduct(thisIMu, floor((combs(sInd, j) - 1) / n + 1), thisIBeta, mod(combs(sInd, j) - 1, n) + 1, chis, xis));
            end
            temp = temp - 1;

            out = out + exp(theta * temp);
            if(temp > max)
                max = abs(temp);
            end

        end
    end

	out = log(out) / theta;

end
