function out = objDer(delta, epsilon, chis, xis, xiDerSums, theta, phi, D, s)

	if nargin == 4
		theta = 25;
	end

	n = size(chis, 3);
	T = size(chis, 1);

	nT = n * T;
	combs = combnk(1:nT, s);

	out = 0;

	for sInd = 1:size(combs, 1)
        for i = 1:s

            % Calculate row sums for sInd, i
            thisIMu = floor((combs(sInd, i) - 1) / n + 1);
            thisIBeta = mod(combs(sInd, i) - 1, n) + 1;
            expTemp = 0;
            sumTemp = 0;

            for j = 1:s
                dot = normDotProduct(thisIMu, floor((combs(sInd, j) - 1) / n + 1), thisIBeta, mod(combs(sInd, j) - 1, n) + 1, chis, xis);
                expTemp= expTemp + abs(dot);
                if j ~= i
                    sumTemp = sumTemp + sign(dot) * normDotProductDer(thisIMu, floor((combs(sInd, j) - 1) / n + 1), thisIBeta, mod(combs(sInd, j) - 1, n) + 1, ...
                                                                      delta, epsilon, chis, xis, xiDerSums, phi, D);
                end
            end
            expTemp = expTemp - 1;

            out = out + exp(theta * expTemp) * sumTemp;

        end
    end

	out = out / (exp(theta * objFn(chis, xis, theta, s)));

end
