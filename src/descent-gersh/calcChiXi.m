function [chis, xis, xiDerSums] = calcChiXi(phi, D)

    n = size(phi, 1);
    T = size(phi, 2);
    
    chis = zeros(T, T, n, n);
    xis = zeros(T, T, n, n);
    xiDerSums = zeros(T, n);
    
    for mu = 1:T
        for nu = 1:T
            
            for beta = 1:n
                for gamma = 1:n
                    
                    chis(mu, nu, beta, gamma) = chi(mu, nu, beta, gamma, phi, D);
                    xis(mu, nu, beta, gamma) = xi(mu, nu, beta, gamma, phi, D);
                    
                end
            end
            
        end
    end
    
    for mu = 1:T
        for beta = 1:n
            xiDerSums(mu, beta) = xiDerSumTerm(phi, D, mu, beta);
        end
    end

end