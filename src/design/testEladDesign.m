D = kron(dctmtx(8)', dctmtx(8)');
gamma = 0.6;
p = 32;
t = 0.5;
fixed = 1;
iter = 5;

phi = eladDesign(D, p, t, gamma, iter, fixed);
