function out = shrinkfn(in, gamma, t)

	out = in;

	% Case 1
	out(abs(in) >= t) = gamma * in(abs(in) >= t);

	% Case 2
	out((abs(in) < t) & (abs(in) >= gamma * t)) = ...
	gamma * t * sign(in((abs(in) < t) & (abs(in) >= gamma * t)));

	% Case 3
	% in(abs(in) >= gamma * t) stay as they are
end
