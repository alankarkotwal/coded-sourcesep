function phi = eladDesign(D, p, t, gamma, iter, fixed)

	if nargin == 5
		fixed = 1;
	elseif nargin < 5
		error('Please check your argument size!');
	end

	n = size(D, 1);
	K = size(D, 2);
	phi = randn(p, n);

	count = 0;
	while count < iter
		
		disp(count+1);

		% Normalize and calculate Gram matrix
		dict = phi*D;
		dict = dict ./ repmat(sqrt(sum(dict .^ 2)), size(dict, 1), 1);
		gram = dict' * dict;

		% Shrink and force rank to p
		gramOut = shrinkfn(gram, gamma, t);
		[u, s, v] = svd(gramOut);
		for i = p+1:K
			s(i, i) = 0;
		end
		gramOut = u * s * v';

		% Symmetrize the matrix to get rid of rounding errors
		gramOut = (gramOut + gramOut')/2;

		% Build the square root of gramOut
		[v, d] = eig(gramOut);
		propDict = d*v';

		count = count + 1;
	end
end
