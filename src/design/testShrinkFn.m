clear; close all; clc;

h = figure;
plot(-1:0.01:1, shrinkfn(-1:0.01:1, 0.6, 0.5));
saveas(h, '~/test', 'jpg');
