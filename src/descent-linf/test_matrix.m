function errs = test_matrix(q, eps, nSamp, s)

    % Testing matrix q for sparsity level k
    [nMeas, n] = size(q); %#ok<ASGLU>
    
    errs = zeros(nSamp, 1);
   	for i = 1:nSamp
        
        disp(['Testing: ' num2str(i)]);
		thisX = sprand(n, 1, s);
        y = q * thisX + 2 * eps / sqrt(nMeas) * (rand(nMeas, 1) - 0.5); %#ok<*NASGU>
        
        cvx_begin quiet
            variable z(n)
            minimize(norm(z, 1))
            subject to
                norm(y - q*z) <= eps %#ok<*NOPRT>
        cvx_end
        
		errs(i) = norm(z - thisX);
    end
    
end
