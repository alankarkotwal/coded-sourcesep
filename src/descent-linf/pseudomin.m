clear; clc; %close all;

nMeas = 40;
n = 80;
eta = 0.5;
k = 8;
maxCount = 50;
maxIter = 1000;
nSamp = 100;
epsil = 0.0001;

q = randn(40, 80);
qInit = q;
wVal = calculate_w(q, 2*k);

count = 1;
iter = 1;
while 1
    
    disp(['Optimizing: ' num2str(count) ' ' num2str(iter)]);
    
    thisQ = q + eta*randn(size(q));
    thisWVal = calculate_w(thisQ, 2*k);
    
    if thisWVal > wVal
        wVal = thisWVal;
        q = thisQ;
        count = 1;
    else
        count = count + 1;
    end
    
    iter = iter + 1;
    if iter > maxIter || count > maxCount
        break
    end

end

nonOptErrs = test_matrix(qInit, k, nSamp, epsil);
optErrs = test_matrix(q, k, nSamp, epsil);

boxplot([nonOptErrs optErrs], [0 1]);
disp([calculate_w(qInit, 2*k) calculate_w(q, 2*k)]);