function out = calculate_obj_all(q, s, m)

    % m is the first dimension of the matrix q 
    n = size(q, 1) / m;
    q = reshape(q, [m n]);
    
    out = zeros(n, 1);
    for i = 1:n
        
        y = q(:, i);
        A = q(:, [1:i-1 i+1:n]);
        
        cvx_begin quiet
            variable lambda(n-1)
            minimize(norm(y - A*lambda))
            subject to
                norm(lambda, 1) <= s-1 %#ok<*NOPRT>
        cvx_end
        
        out(i) = - norm(y - A*lambda);
        
    end

end
