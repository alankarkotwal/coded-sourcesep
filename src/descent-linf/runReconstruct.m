function [rrmses, errorBound] = runReconstruct(nMeas, n, s, nSamp)

%nMeas = 10;
%n = 20;
%s = 0.2;
epsilRec = 1e-5;

%nSamp = 1;

Q = randn(nMeas, n);

rrmses = test_matrix(Q, epsilRec, nSamp, s);
errorBound = 2 * epsilRec * sqrt(2 * round(n * s)) / calculate_w(Q, round(2 * n * s));
