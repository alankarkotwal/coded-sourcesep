%function actminimax(nMeas, n, k)

%clear; clc; close all;
addpath('~/.export_fig/');

nMeas = 32;
factor = 2;
n = round(nMeas*factor);
k = 8;
nSamp = 500;
epsil = 1e-8;
delta = 0.0001;

poolObj = gcp;
addAttachedFiles(poolObj, '~/.cvx');

runName = ['../../results/descent-linf/' strrep(num2str(now), '.', '_') '-' ...
      num2str(nMeas) 'x' num2str(n) 'x' num2str(k) '-' num2str(nSamp) '-inf'];

q = rand(nMeas, n);
qInit = q;

q = q(:);

fun = @(x) calculate_obj_all(x, 2*k, nMeas);
cons = @(x) q_constraint(x, nMeas);

options = optimoptions('fminimax', 'Display', 'iter-detailed', ...
                       'UseParallel', 1, 'Diagnostics', 'on', ...
                       'TolFun', epsil, 'TolX', epsil, 'MaxIter', Inf);

[q, funVal] = fminimax(fun, q, [], [], [], [], [], [], cons, options);

xs = zeros(n, nSamp);

for i = 1:nSamp
    xs(:, i) = sprand(n, 1, k/n);
end

q = reshape(q, size(qInit));

nonOptErrs = test_matrix(qInit, nSamp, xs, epsil);
optErrs = test_matrix(q, nSamp, xs, epsil);

save([runName '.mat']);

scrsz = get(groot, 'ScreenSize');
figure('Position', scrsz);
boxplot([nonOptErrs optErrs], 'Labels', {'Random', 'Optimized'});
ylabel('RRMSE');
title(['Boxplot of RRMSEs, ' num2str(nMeas) ' x ' num2str(n) ' matrices, s = ' num2str(k)]);
set(gca, 'FontSize', 40);
savefig([runName '.fig']);
disp([runName '.fig']);

objRand = calculate_w(qInit, 2*k);
objOpt = calculate_w(q, 2*k);

disp([objRand objOpt]);

%end
