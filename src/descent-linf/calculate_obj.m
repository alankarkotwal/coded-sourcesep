function out = calculate_obj(q, i, lambda, m)

    % m is the first dimension of the matrix q 
    n = size(q, 1) / m;
    q = reshape(q, [m n]);
    y = q(:, i);
    A = q(:, [1:(i-1) (i+1):n]);
    out = - norm(y - A*lambda);

end