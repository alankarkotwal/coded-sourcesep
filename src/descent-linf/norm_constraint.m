function [con, out] = norm_constraint(q, m)

    q = reshape(q, [m size(q, 1)/m]);
    out = sqrt(sum(q.^2));
    out = out' - ones(size(q, 1)/m, 1);
    con = [];
    
end
