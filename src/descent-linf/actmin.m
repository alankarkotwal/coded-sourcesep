clear; clc; %close all;

nMeas = 50;
n = 100;
k = 3;
maxCount = 50;
maxIter = 1000;
nSamp = 100;
epsil = 0.0001;

q = randn(nMeas, n);
qInit = q;

q = q(:);

% for i = 1:maxIter
%     
%     [~, thisI, thisLambda] = calculate_w_vec(q, 2*k, nMeas);
%     
%     fun = @(x) calculate_obj(x, thisI, thisLambda, nMeas);
%     cons = @(x) norm_constraint(x, nMeas);
%     [q, funVal] = fmincon(fun, q, [], [], [], [], [], [], cons);
%     
%     disp([i thisI funVal]);
%     
% end

for i = 1:n
    
    [~, thisI, thisLambda] = calculate_w_vec(q, 2*k, nMeas);
    
    fun = @(x) calculate_obj(x, thisI, thisLambda, nMeas);
    cons = @(x) norm_constraint(x, nMeas);
    [q, funVal] = fmincon(fun, q, [], [], [], [], [], [], cons);
    
    disp([i thisI funVal]);
    
end
