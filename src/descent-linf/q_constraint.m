function [con, out] = q_constraint(q, m)

	[~, out_norm] = norm_constraint(q, m);
	out = [out_norm];
	%con = [-q];
	con = [];

end
