function runDescent(nMeas, n, s, pos, nStarts, nSamp)

%nMeas = 50;
%n = 100;
%s = 0.015;
%pos = 0;
%
%nStarts = 1;
%nSamp = 10;

step = 0.1;
stepFactor = 0.5;
iter = Inf;
repCountMax = 8;
epsil = 0;
epsilRec = 1e-6;
minChange = 0.001;

runName = ['../../results/descent-linf/' strrep(num2str(now), '.', '_') '-' num2str(n) '-' num2str(nMeas) '-' num2str(s) '-' num2str(pos) '-' num2str(nStarts) '-' num2str(nSamp)]; 

objs = zeros(nStarts, 1);
Qs = zeros(nMeas, n, nStarts);
origQs = zeros(nMeas, n, nStarts);

parfor i = 1:nStarts
	
	disp(strcat('***** Start ', num2str(i), ' *****'));
	[thisQ, thisOrigQ, thisObj] = descent(nMeas, n, ceil(2*s*n), pos, step, stepFactor, iter, epsil, repCountMax, minChange);

	Qs(:, :, i) = thisQ;
	origQs(:, :, i) = thisOrigQ;
	objs(i) = thisObj;

end

[obj, pos] = max(objs);
Q = Qs(:, :, pos);
origQ = origQs(:, :, pos);

save([runName '.mat'], 'n', 'nMeas', 's', 'Q', 'origQ', 'obj');

xs = zeros(n, nSamp);
for i = 1:nSamp
    xs(:, i) = sprand(n, 1, s);
end

nonOptErrs = test_matrix(origQ, xs, epsilRec);
optErrs = test_matrix(Q, xs, epsilRec);

scrsz = get(groot, 'ScreenSize');
figure('Position', scrsz);
boxplot([nonOptErrs optErrs], 'Labels', {'Random', 'Optimized'});
ylabel('RRMSE');
title(['Boxplot of RRMSEs, ' num2str(nMeas) ' x ' num2str(n) ' matrices, s = ' num2str(s)]);
set(gca, 'FontSize', 40);
savefig([runName '.fig']);
