function [Q, QInit, w] = descent(nMeas, n, s, pos, step, stepFactor, iter, epsil, repCountMax, minChange)

	if nargin == 3
		step = 0.1;
		stepFactor = 0.5;
		iter = Inf;
		epsil = 0;
		repCountMax = 5;
		minChange = 0;
	elseif nargin == 4
		stepFactor = 0.5;
		iter = Inf;
		epsil = 0;
		repCountMax = 5;
		minChange = 0;
	elseif nargin == 5
		iter = Inf;
		epsil = 0;
		repCountMax = 5;
		minChange = 0;
	elseif nargin == 6
		epsil = 0;
		repCountMax = 5;
		minChange = 0;
	elseif nargin == 7
		repCountMax = 5;
		minChange = 0;
	elseif nargin == 8
		minChange = 0;
	end
	
	if pos
		Q = normc(rand(nMeas, n));
	else
		Q = normc(randn(nMeas, n));
	end
		QInit = Q;
	
	[w, i, lambda] = calculate_w(Q, 2*s);

	disp([0.0 0]);
	disp(w);
	
	count = 0;
	repCount = 0;
	iTookAStep = 1;
	ascDir = zeros(nMeas, n);
	
	while count < iter
		
		if iTookAStep
			
			redQ = Q(:, [1:i-1 i+1:n]);
			colQ = Q(:, i);
			for k = 1:i-1
				ascDir(:, k) = 2 * lambda(k) * (redQ * lambda - colQ);
			end
			for k = i+1:n
				ascDir(:, k) = 2 * lambda(k-1) * (redQ * lambda - colQ);
			end
			ascDir(:, i) = 2 * (colQ - redQ * lambda);
			
		end
		
		QNew = Q + step * ascDir;
		if pos
			QNew(QNew < epsil) = epsil;
			QNew(QNew > 1) = 1;
		end
		QNew = normc(QNew);
		
		[wNew, iNew, lambdaNew] = calculate_w(QNew, 2*s);
		QNew = normc(QNew);
		
		if wNew >= w * (1 + minChange)
			Q = QNew;
			w = wNew;
			i = iNew;
			lambda = lambdaNew;
			%step = step / stepFactor;

			count = count + 1;
			repCount = 0;
			iTookAStep = 1;
		else
			step = step * stepFactor;
			repCount = repCount + 1;
			iTookAStep = 0;
			if repCount >= repCountMax
				break;
			end
		end
	
		disp([round(count) i]);
		disp(w);

	end
	
end
