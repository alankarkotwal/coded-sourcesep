function runRunReconstruct(nMeasIndex)

	n = 100;
	nMeass = 10:15:85;
	sparss = 0.01:0.02:0.14;
	
	nSamp = 100;
	
	runName = ['../../results/descent-linf/' strrep(num2str(now), '.', '_') '-' num2str(n) '-' num2str(nMeass(nMeasIndex)) '-' num2str(nSamp)]; 
	
	%parfor nMeas = 1:size(nMeass, 1)
	
		rrmses = zeros(nSamp, size(sparss, 2)); 
		errorBounds = zeros(size(sparss, 2), 1);
		parfor sparsIdx = 1:size(sparss, 2)
				[rrmses(:, sparsIdx), errorBounds(sparsIdx)] = runReconstruct(nMeass(nMeasIndex), n, sparss(sparsIdx), nSamp);
		end
	%end

	save([runName '-fin.mat'], 'n', 'nMeass', 'nMeasIndex', 'sparss', 'errorBounds', 'rrmses');

end
