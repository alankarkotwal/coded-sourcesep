function [minVal, iMin, lambda] = calculate_w(q, s)

    [n, maxI] = size(q); %#ok<*ASGLU>
    
    lambda = 0;
    minVal = Inf;
    
    for i = 1:maxI
       
        y = q(:, i);
        A = q(:, [1:i-1 i+1:maxI]);
        
        cvx_begin quiet
            variable thisLambda(maxI-1)
            minimize(norm(y - A*thisLambda))
            subject to
                norm(thisLambda, 1) <= s-1 %#ok<*NOPRT>
        cvx_end
        
        thisVal = norm(y - A*thisLambda);
        if thisVal < minVal
            minVal = thisVal;
            lambda = thisLambda;
            iMin = i;
        end
        
    end

end
