imSize1 = 8;
imSize2 = 7;
imSize3 = 2;

im = rand(imSize1, imSize2, imSize3);
cs = mirt_dctn(im);
f = rand(imSize1, imSize2);

phid = randn(imSize1, imSize2, imSize3);

% Conventional measurement
meas1 = reshape(phid(:, :, 1) .* im(:, :, 1) + phid(:, :, 2) .* im(:, :, 2), imSize1*imSize2, 1);
meas3 = reshape(mirt_dctn(cat(3, phid(:, :, 1) .* f, phid(:, :, 2) .* f)), imSize1*imSize2*imSize3, 1);

% DictMatrix measurement
A = DictMatrix(imSize1, imSize2, 2, phid);
meas2 = A*reshape(cs, imSize1*imSize2*imSize3, 1);
meas4 = A'*reshape(f, imSize1*imSize2, 1);

disp(norm(meas1 - meas2));
disp(norm(meas3 - meas4));