imSize = [300 300 2];

A = rand(imSize);
c1 = mirt_dctn(A);

D1 = dctmtx(imSize(1))';
D2 = kron(dctmtx(imSize(3))', dctmtx(imSize(2))');

err1 = abs(A-reshape(D1*reshape(c1, imSize(1), imSize(2)*imSize(3))*D2', imSize));
% IDCT: reshape(D1*reshape(c1, imSize(1), imSize(2)*imSize(3))*D2', imSize);
disp(sum(sum(sum(err1)))/sum(sum(sum(abs(A)))));

err2 = abs(c1-reshape(D1'*reshape(A, imSize(1), imSize(2)*imSize(3))*D2, imSize));
% DCT: reshape(D1'*reshape(A, imSize(1), imSize(2)*imSize(3))*D2, imSize);
disp(sum(sum(sum(err2)))/sum(sum(sum(abs(c1)))));
