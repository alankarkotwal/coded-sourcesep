function res = mtimes(A, x)

if A.adjoint == 0
    
    if size(x, 1) ~= A.m*A.imSize1*A.imSize2
        error('Size of x should be the same as the A.m*A.imSize1*A.imSize2 for non-adjoint A');
    end
    
    x = reshape(x, A.imSize1, A.imSize2, A.m);
    ims = mirt_idctn(x);
    res = zeros(A.imSize1, A.imSize2, 1);
    for i = 1:A.m
        res = res + A.phi(:, :, i) .* ims(:, :, i);
    end
    res = reshape(res, A.imSize1*A.imSize2, 1);
    
else 
    
    if size(x, 1) ~= A.imSize1*A.imSize2
        error('Size of x should be the same as the A.imSize1*A.imSize2 for adjoint A');
    end
	res = zeros(A.imSize1, A.imSize2, A.m);
    for i = 1:A.m
        res(:, :, i) = A.phi(:, :, i) .* reshape(x, A.imSize1, A.imSize2);
    end
    res = mirt_dctn(res);
    res = reshape(res, A.imSize1*A.imSize2*A.m, 1);
    
end

end
