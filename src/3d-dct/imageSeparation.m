% Source Separation-based Coded Video Recovery
% Alankar Kotwal <alankarkotwal13@gmail.com>
clear; close all; clc; tic;
addpath('~/.l1ls');

runName = input('Enter runName: ', 's');

% Parameters
lambda = 0.01;
tol = 1e-2;

%im1 = im2double(rgb2gray(imread('../../data/ls1.png')));
%im2 = im2double(rgb2gray(imread('../../data/ls2.jpg')));
im1 = im2double(rgb2gray(imread('../../data/c1.png')));
im2 = im2double(rgb2gray(imread('../../data/c2.png')));
%im1 = imresize(im1, 1/2);
%im2 = imresize(im2, 1/2); 
im1 = imresize(im1(:, 120:1320), 1/4);
im2 = imresize(im2(:, 120:1320), 1/4); 
imSize = size(im1);

% Measurement matrix
phi = randn(imSize(1), imSize(2), 2);
A = DictMatrix(imSize(1), imSize(2), 2, phi);
At = A';

% Get the measurement
meas = phi(:, :, 1) .* im1 + phi(:, :, 2) .* im2;
meas = reshape(meas, imSize(1)*imSize(2), 1);

%% BP machao
[cs, status] = l1_ls(A, At, imSize(1)*imSize(2), imSize(1)*imSize(2)*2, meas, lambda, tol);

cOpt = reshape(cs, imSize(1), imSize(2), 2);
output = mirt_idctn(cOpt);
output1 = output(:, :, 1);
output2 = output(:, :, 2);

%%
nowTime = now;
imwrite(im1/max(max(im1)), strcat('../../results/sourcesep/3ddct/', num2str(nowTime), '-', runName, '-ls1in.png'));
imwrite(im2/max(max(im2)), strcat('../../results/sourcesep/3ddct/', num2str(nowTime), '-', runName, '-ls2in.png'));
imwrite(output1/max(max(output1)), strcat('../../results/sourcesep/3ddct/', num2str(nowTime), '-', runName, '-ls1est.png'));
imwrite(output2/max(max(output2)), strcat('../../results/sourcesep/3ddct/', num2str(nowTime), '-', runName, '-ls2est.png'));
imwrite([im1 im2 output1 output2]/max(max([im1 im2 output1 output2])), strcat('../../results/sourcesep/3ddct/', num2str(nowTime), '-', runName, '-full.png'));

MSIE1 = sum(sum((output1-double(im1)).^2))/sum(sum(double(im1).^2));
MSIE2 = sum(sum((output2-double(im2)).^2))/sum(sum(double(im1).^2));
disp([MSIE1 MSIE2]);

%phi = [phi1*D phi2*D];
%normPhi = phi ./ repmat(sqrt(sum(phi.^2)), size(phi, 1), 1);
%cohers = normPhi' * normPhi;
%mu = max(max(abs(cohers - eye(size(cohers)))));
mu = -1;

fileID = fopen(strcat('../../results/sourcesep/3ddct/', num2str(nowTime), '-', runName, '-errImgSep.txt'), 'w');
fprintf(fileID, '%f ', [MSIE1 MSIE2 mu]);
fclose(fileID);

save(strcat('../../results/sourcesep/3ddct/', num2str(nowTime), '-', runName, '.mat'))
