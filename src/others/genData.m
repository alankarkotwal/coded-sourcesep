density = 0.2;
u = dctmtx(20);

a1 = zeros(20);
a2 = zeros(20);

a1(1, 1) = 4000;
a2(1, 1) = 5;

a1(2, 3) = 1000;
a2(1, 3) = 2;

s1 = u'*a1*u; 
s2 = u'*a2*u; 

imwrite(s1/max(max(s1)), '../data/s1.png');
imwrite(s2/max(max(s2)), '../data/s2.png');
