% Source Separation-based Coded Video Recovery
% Alankar Kotwal <alankarkotwal13@gmail.com>

clear; close all; clc; tic;

% Parameters
alpha = 0.5;
eps = 1e-5;

im1 = im2double(rgb2gray(imread('../../data/c1.png')));
im2 = im2double(rgb2gray(imread('../../data/c2.png')));
im1 = imresize(im1(:, 120:1320), 0.125);
im2 = imresize(im2(:, 120:1320), 0.125);

imSize = size(im1);
measSize = imSize - 7;

% Parameters
m = ceil(64*0.8);

% Measurement matrix
phi1 = randn(64);
phim1 = phi1(1:m, :);
phi2 = randn(64);
phim2 = phi2(1:m, :);

phim1 = phim1./repmat(sqrt(sum(phim1.^2)), m, 1);
phim2 = phim2./repmat(sqrt(sum(phim2.^2)), m, 1);

% Extract patches from the images
patches1 = zeros(64, measSize(1), measSize(2));
patches2 = zeros(64, measSize(1), measSize(2));
for i=1:measSize(1)
    for j=1:measSize(2)
        
        patches1(:, i, j) = reshape(im1(i:i+7, j:j+7), 64, 1);
        patches2(:, i, j) = reshape(im2(i:i+7, j:j+7), 64, 1);
        
    end
end

% Extract measurements using these matrices
meas = zeros(m, measSize(1), measSize(2));
for i=1:measSize(1)
    for j=1:measSize(2)
        
        meas(:, i, j) = alpha*phim1*patches1(:, i, j) + (1-alpha)*phim2*patches2(:, i, j);
        
    end
end

% Add some noise
sigma = 0.05*mean(mean(mean(abs(meas))));
meas = meas + sigma*randn(m, measSize(1), measSize(2));

% Get the DCT matrix
D = kron(dctmtx(8)', dctmtx(8)');
A1 = phim1 * D;
A2 = phim2 * D;
A = [A1 A2];

% OMP machao
op1 = zeros(imSize(1), imSize(2));
op2 = zeros(imSize(1), imSize(2));
opMask = zeros(imSize(1), imSize(2));
sqPatchErr1 = 0;
sqPatchErr2 = 0;
for i=1:measSize(1)
    for j=1:measSize(2)
        
        disp([i j]);
        
        res = meas(:, i, j);
        
        % Calculate patch from res here
        cvx_begin quiet
            variables x1Opt(64) x2Opt(64)
            minimize(norm(x1Opt, 1) + norm(x2Opt, 1) + norm(x1Opt - x2Opt))
            subject to
                norm(res-A*[x1Opt; x2Opt]) <= eps %#ok<NOPTS>
        cvx_end
        
        patch = kron(eye(2), D)*[x1Opt; x2Opt];
        patch1 = patch(1:64);
        patch2 = patch(65:128);
        
        op1(i:i+7, j:j+7) = op1(i:i+7, j:j+7) + reshape(patch1, 8, 8);
        op2(i:i+7, j:j+7) = op2(i:i+7, j:j+7) + reshape(patch2, 8, 8);
        opMask(i:i+7, j:j+7) = opMask(i:i+7, j:j+7) + ones(8);
        
        sqPatchErr1 = sqPatchErr1 + sum(sum((double(im1(i:i+7, j:j+7)) - reshape(patch1, 8, 8)).^2));
        sqPatchErr2 = sqPatchErr2 + sum(sum((double(im2(i:i+7, j:j+7)) - reshape(patch2, 8, 8)).^2));
        
    end
end

output1 = op1./opMask;
output2 = op2./opMask;

nowTime = now;
imwrite(im1/max(max(im1)), strcat('../../results/sourcesep/', num2str(nowTime), '-ls1in.png'));
imwrite(im2/max(max(im2)), strcat('../../results/sourcesep/', num2str(nowTime), '-ls2in.png'));
imwrite(output1/max(max(output1)), strcat('../../results/sourcesep/', num2str(nowTime), '-ls1est.png'));
imwrite(output2/max(max(output2)), strcat('../../results/sourcesep/', num2str(nowTime), '-ls2est.png'));

MSPE1 = sqPatchErr1/(measSize(1)*measSize(2));
MSPE2 = sqPatchErr2/(measSize(1)*measSize(2));
MSIE1 = sum(sum((output1-double(im1)).^2))/(imSize(1)*imSize(2));
MSIE2 = sum(sum((output2-double(im2)).^2))/(imSize(1)*imSize(2));
disp([MSPE1 MSPE2 MSIE1 MSIE2]);

fileID = fopen(strcat('../../results/sourcesep/', num2str(now), 'errCloseSep.txt'), 'w');
fprintf(fileID, '%f ', [MSPE1 MSPE2 MSIE1 MSIE2]);
fclose(fileID);

toc;