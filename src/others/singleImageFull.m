% Source Separation-based Coded Video Recovery
% Alankar Kotwal <alankarkotwal13@gmail.com>
clear; close all; clc; tic;

runName = input('Enter runName: ', 's');

% Parameters
eps = 1e-5;

im1 = im2double(imread('../data/s1.png'));
imSize = size(im1);

% Measurement matrix
phi1 = randn(imSize(1)*imSize(2)*0.5, imSize(1)*imSize(2));% - eye(imSize(1)*imSize(2))/2;

% Get the measurement
meas = phi1*im1(:);

% Add some noise
sigma = 0.0*mean(abs(meas));
meas = meas + sigma*randn(size(meas));

% Get the DCT matrix
D1 = dctmtx(imSize(1));
D2 = dctmtx(imSize(2));

%% BP machao
cvx_begin
    variable c1Opt(imSize)
    minimize(norm(c1Opt, 1))
    subject to
        norm(meas-phi1*reshape(D1'*c1Opt*D2, imSize(1)*imSize(2))) <= eps
cvx_end

output1 = reshape(D1'*c1Opt*D2, imSize);

nowTime = now;
imwrite(im1/max(max(im1)), strcat('../results/sourcesep/SingleFullImage/', num2str(nowTime), '-', runName, '-ls1in.png'));
imwrite(output1/max(max(output1)), strcat('../results/sourcesep/SingleFullImage/', num2str(nowTime), '-', runName, '-ls1est.png'));

MSIE1 = sum(sum((output1-double(im1)).^2))/(imSize(1)*imSize(2));
disp(MSIE1);

D = kron(dctmtx(imSize(1))', dctmtx(imSize(2))');
phi = phi1*D;
normPhi = phi ./ repmat(sqrt(sum(phi.^2)), size(phi, 1), 1);
cohers = normPhi' * normPhi;
mu = max(max(abs(cohers - eye(size(cohers)))));

fileID = fopen(strcat('../results/sourcesep/SingleFullImage/', num2str(now), '-', runName, '-err.txt'), 'w');
fprintf(fileID, '%f ', [MSIE1 mu]);
fclose(fileID);

save(strcat('../results/sourcesep/SingleFullImage/', num2str(nowTime), '-', runName, '.mat'))
