% Source Separation-based Coded Video Recovery
% Alankar Kotwal <alankarkotwal13@gmail.com>

% Parameters
alpha = 0.5;
eps = 1e-5;
patchSize = 20;
stepSize = 20;
m = ceil(patchSize*patchSize*1.0);

im1 = im2double(rgb2gray(imread('../data/c1.png')));
im2 = im2double(rgb2gray(imread('../data/c2.png')));
im1 = imresize(im1(:, 120:1320), 0.125/2);
im2 = imresize(im2(:, 120:1320), 0.125/2);

imSize = size(im1);
measSize = ceil((imSize - patchSize)/stepSize);

% Measurement matrix
phi1 = diag(rand(patchSize*patchSize, 1)) - eye(patchSize * patchSize);
phim1 = phi1(1:m, :);
phi2 = diag(rand(patchSize*patchSize, 1)) - eye(patchSize * patchSize);
phim2 = phi2(1:m, :);

% Extract patches from the images
patches1 = zeros(patchSize*patchSize, measSize(1), measSize(2));
patches2 = zeros(patchSize*patchSize, measSize(1), measSize(2));
for i=1:measSize(1)
    for j=1:measSize(2)
        
        patches1(:, i, j) = reshape(im1(1+stepSize*(i-1):stepSize*(i-1)+patchSize, 1+stepSize*(j-1):stepSize*(j-1)+patchSize), patchSize*patchSize, 1);
        patches2(:, i, j) = reshape(im2(1+stepSize*(i-1):stepSize*(i-1)+patchSize, 1+stepSize*(j-1):stepSize*(j-1)+patchSize), patchSize*patchSize, 1);
        
    end
end

% Extract measurements using these matrices
meas = zeros(m, measSize(1), measSize(2));
for i=1:measSize(1)
    for j=1:measSize(2)
        
        meas(:, i, j) = alpha*phim1*patches1(:, i, j) + (1-alpha)*phim2*patches2(:, i, j);
        
    end
end

% Add some noise
sigma = 0.05*mean(mean(mean(abs(meas))));
meas = meas + sigma*randn(m, measSize(1), measSize(2));

% Get the DCT matrix
D = kron(dctmtx(patchSize)', dctmtx(patchSize)');
A1 = phim1 * D;
A2 = phim2 * D;
A = [A1 A2];

% BP machao
op1 = zeros(imSize(1), imSize(2));
op2 = zeros(imSize(1), imSize(2));
opMask = zeros(imSize(1), imSize(2));
sqPatchErr1 = 0;
sqPatchErr2 = 0;

for i=1:measSize(1)
    for j=1:measSize(2)
        
	disp([i j]);

        res = meas(:, i, j);
        
        % Calculate patch from res here
        cvx_begin 
            variable xOpt(2*patchSize*patchSize)
            minimize(norm(xOpt, 1))
            subject to
                norm(res-A*xOpt) <= eps %#ok<NOPTS>
        cvx_end
        
        patch = kron(eye(2), D)*xOpt;
        patch1 = patch(1:patchSize*patchSize);
        patch2 = patch(patchSize*patchSize+1:2*patchSize*patchSize);
        
        op1(1+stepSize*(i-1):stepSize*(i-1)+patchSize, 1+stepSize*(j-1):stepSize*(j-1)+patchSize) = op1(1+stepSize*(i-1):stepSize*(i-1)+patchSize, 1+stepSize*(j-1):stepSize*(j-1)+patchSize) + reshape(patch1, patchSize, patchSize);
        op2(1+stepSize*(i-1):stepSize*(i-1)+patchSize, 1+stepSize*(j-1):stepSize*(j-1)+patchSize) = op2(1+stepSize*(i-1):stepSize*(i-1)+patchSize, 1+stepSize*(j-1):stepSize*(j-1)+patchSize) + reshape(patch2, patchSize, patchSize);
        opMask(1+stepSize*(i-1):stepSize*(i-1)+patchSize, 1+stepSize*(j-1):stepSize*(j-1)+patchSize) = opMask(1+stepSize*(i-1):stepSize*(i-1)+patchSize, 1+stepSize*(j-1):stepSize*(j-1)+patchSize) + ones(patchSize);
%        
%        sqPatchErr1 = sqPatchErr1 + sum(sum((double(im1(i:i+7, j:j+7)) - reshape(patch1, 8, 8)).^2));
%        sqPatchErr2 = sqPatchErr2 + sum(sum((double(im2(i:i+7, j:j+7)) - reshape(patch2, 8, 8)).^2));
    end
end

output1 = op1./opMask;
output2 = op2./opMask;

nowTime = now;
imwrite(im1/max(max(im1)), strcat('../results/sourcesep/HitomiDiffImages128/', num2str(nowTime), '-ls1in.png'));
imwrite(im2/max(max(im2)), strcat('../results/sourcesep/HitomiDiffImages128/', num2str(nowTime), '-ls2in.png'));
imwrite(output1/max(max(output1)), strcat('../results/sourcesep/HitomiDiffImages128/', num2str(nowTime), '-ls1est.png'));
imwrite(output2/max(max(output2)), strcat('../results/sourcesep/HitomiDiffImages128/', num2str(nowTime), '-ls2est.png'));

MSPE1 = sqPatchErr1/(measSize(1)*measSize(2));
MSPE2 = sqPatchErr2/(measSize(1)*measSize(2));
MSIE1 = sum(sum((output1-double(im1)).^2))/(imSize(1)*imSize(2));
MSIE2 = sum(sum((output2-double(im2)).^2))/(imSize(1)*imSize(2));
disp([MSPE1 MSPE2 MSIE1 MSIE2]);

fileID = fopen(strcat('../results/sourcesep/HitomiDiffImages128/', num2str(now), '-errReflSep.txt'), 'w');
fprintf(fileID, '%f ', [MSPE1 MSPE2 MSIE1 MSIE2]);
fclose(fileID);

save(strcat('../results/sourcesep/HitomiDiffImages128/', num2str(nowTime), '.mat'))
