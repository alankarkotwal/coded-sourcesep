% Source Separation-based Coded Video Recovery
% Alankar Kotwal <alankarkotwal13@gmail.com>

clear; close all; clc; tic;

% Parameters
alpha = 0.5;
eps = 1e-3;
sigma1 = 1;
sigma2 = 1;

im1 = imresize(im2double(rgb2gray(imread('../../data/ls1.png'))), 0.25);
im2 = imresize(im2double(rgb2gray(imread('../../data/ls2.jpg'))), 0.25);
colNos = reshape(repmat(linspace(1, 8, 8), 8, 1), 64, 1);
rowNos = reshape(repmat(linspace(1, 8, 8)', 1, 8), 64, 1);

kernel1 = zeros(64, 64);
kernel2 = zeros(64, 64);
for i=1:64
    for j=1:64
        kernel1(i, j) = exp(-((colNos(i)-colNos(j)).^2 + (rowNos(i)-rowNos(j)).^2)/(2*sigma1^2));
        kernel2(i, j) = exp(-((colNos(i)-colNos(j)).^2 + (rowNos(i)-rowNos(j)).^2)/(2*sigma2^2));
    end
end
kernel1 = kernel1 ./ repmat(sum(kernel1, 2), 1, 64);
kernel2 = kernel2 ./ repmat(sum(kernel2, 2), 1, 64);

imSize = size(im1);
measSize = imSize - 7;

% Parameters
m = ceil(64*1.0);

% Measurement matrix
phi1 = randn(64);
phim1 = phi1(1:m, :);
phi2 = randn(64);
phim2 = phi2(1:m, :);

phim1 = phim1./repmat(sqrt(sum(phim1.^2)), m, 1);
phim2 = phim2./repmat(sqrt(sum(phim2.^2)), m, 1);

% Extract patches from the images
patches1 = zeros(64, measSize(1), measSize(2));
patches2 = zeros(64, measSize(1), measSize(2));
for i=1:measSize(1)
    for j=1:measSize(2)
        
        patch1 = reshape(im1(i:i+7, j:j+7), 64, 1);
        patch2 = reshape(im2(i:i+7, j:j+7), 64, 1);
        
        patches1(:, i, j) = (alpha*patch1 + (1-alpha)*kernel2*patch2);
        patches2(:, i, j) = (alpha*patch2 + (1-alpha)*kernel1*patch1); 
        
    end
end

% Extract measurements using these matrices
meas1 = zeros(m, measSize(1), measSize(2));
meas2 = zeros(m, measSize(1), measSize(2));
for i=1:measSize(1)
    for j=1:measSize(2)
        
        meas1(:, i, j) = alpha*phim1*patches1(:, i, j) + (1-alpha)*phim2*patches2(:, i, j);
        meas2(:, i, j) = alpha*phim2*patches2(:, i, j) + (1-alpha)*phim1*patches1(:, i, j);
        
    end
end

% Add some noise
meas1 = meas1 + 0.05*mean(mean(mean(abs(meas1))))*randn(m, measSize(1), measSize(2));
meas2 = meas2 + 0.05*mean(mean(mean(abs(meas2))))*randn(m, measSize(1), measSize(2));

% Get the DCT matrix
D = kron(dctmtx(8)', dctmtx(8)');
A1 = phim1 * D;
A2 = phim2 * D;
A = [phim1*D, phim1*kernel2*D; phim2*kernel1*D, phim2*D];

% OMP machao
in1 = zeros(imSize(1), imSize(2));
in2 = zeros(imSize(1), imSize(2));
op1 = zeros(imSize(1), imSize(2));
op2 = zeros(imSize(1), imSize(2));
opMask = zeros(imSize(1), imSize(2));
sqPatchErr1 = 0;
sqPatchErr2 = 0;
for i=1:measSize(1)
    for j=1:measSize(2)
        
        disp([i j]);
        
        res = [meas1(:, i, j); meas2(:, i, j)];
        
        % Calculate patch from res here
        cvx_begin
            variable xOpt(128)
            minimize(norm(xOpt, 1))
            subject to
                norm(res-A*xOpt) <= eps %#ok<NOPTS>
        cvx_end
        
        patch = kron(eye(2), D)*xOpt;
        patch1 = patch(1:64);
        patch2 = patch(65:128);
        
        in1(i:i+7, j:j+7) = in1(i:i+7, j:j+7) + reshape(patches1(:, i, j), 8, 8);
        in2(i:i+7, j:j+7) = in2(i:i+7, j:j+7) + reshape(patches2(:, i, j), 8, 8);
        
        op1(i:i+7, j:j+7) = op1(i:i+7, j:j+7) + reshape(patch1, 8, 8);
        op2(i:i+7, j:j+7) = op2(i:i+7, j:j+7) + reshape(patch2, 8, 8);
        opMask(i:i+7, j:j+7) = opMask(i:i+7, j:j+7) + ones(8);
        
        sqPatchErr1 = sqPatchErr1 + sum(sum((double(im1(i:i+7, j:j+7)) - reshape(patch1, 8, 8)).^2));
        sqPatchErr2 = sqPatchErr2 + sum(sum((double(im2(i:i+7, j:j+7)) - reshape(patch2, 8, 8)).^2));
        
    end
end

output1 = op1./opMask;
output2 = op2./opMask;
input1 = in1./opMask;
input2 = in2./opMask;

nowTime = now;
imwrite(im1/max(max(input1)), strcat('../../results/sourcesep/ReflSeparation', num2str(nowTime), '-ls1inComb.png'));
imwrite(im2/max(max(input2)), strcat('../../results/sourcesep/ReflSeparation', num2str(nowTime), '-ls2inComb.png'));
imwrite(im1/max(max(im1)), strcat('../../results/sourcesep/ReflSeparation', num2str(nowTime), '-ls1in.png'));
imwrite(im2/max(max(im2)), strcat('../../results/sourcesep/ReflSeparation', num2str(nowTime), '-ls2in.png'));
imwrite(output1/max(max(output1)), strcat('../../results/sourcesep/ReflSeparation', num2str(nowTime), '-ls1est.png'));
imwrite(output2/max(max(output2)), strcat('../../results/sourcesep/ReflSeparation', num2str(nowTime), '-ls2est.png'));

MSPE1 = sqPatchErr1/(measSize(1)*measSize(2));
MSPE2 = sqPatchErr2/(measSize(1)*measSize(2));
MSIE1 = sum(sum((output1-double(im1)).^2))/(imSize(1)*imSize(2));
MSIE2 = sum(sum((output2-double(im2)).^2))/(imSize(1)*imSize(2));
disp([MSPE1 MSPE2 MSIE1 MSIE2]);

fileID = fopen(strcat('../../results/sourcesep/ReflSeparation', num2str(now), 'errReflSep.txt'), 'w');
fprintf(fileID, '%f ', [MSPE1 MSPE2 MSIE1 MSIE2]);
fclose(fileID);

toc;