% Source Separation-based Coded Video Recovery
% Alankar Kotwal <alankarkotwal13@gmail.com>

clear; close all; clc; tic;

% Parameters
eps = 1e-5;

% im1 = imresize(im2double(rgb2gray(imread('../data/c1.png'))), 0.125);
% im2 = imresize(im2double(rgb2gray(imread('../data/c2.png'))), 0.125);
im1 = im2double(rgb2gray(imread('../data/c1.png')));
im2 = im2double(rgb2gray(imread('../data/c2.png')));
im1 = imresize(im1(:, 120:1020), 0.0625);
im2 = imresize(im2(:, 120:1020), 0.0625);
imSize = size(im1);

% Measurement matrix
phi1 = randn(imSize(1)*imSize(2), imSize(1)*imSize(2));
phi2 = randn(imSize(1)*imSize(2), imSize(1)*imSize(2));

% Get the measurement
meas = phi1*imresize(im1, [imSize(1)*imSize(2) 1]) + phi2*imresize(im2, [imSize(1)*imSize(2) 1]);

% Add some noise
sigma = 0.05*mean(abs(meas));
meas = meas + sigma*randn(size(meas));

% Get the DCT matrix
phi = [phi1 phi2];

%% BP machao
cvx_begin
    variable x1(imSize)
    variable x2(imSize)
    expression i1(imSize)
    expression i2(imSize)
    minimize(norm(x1, 1) + norm(x2, 1))
    subject to
	i1 = x1
	i2 = x2
        norm(meas-phi*[imresize(i1, [imSize(1)*imSize(2) 1]); imresize(i2, [imSize(1)*imSize(2) 1])]) <= eps %#ok<NOPTS>
cvx_end
%
%output1 = reshape(D*xOpt(1:(imSize(1)*imSize(2))), imSize);
%output2 = reshape(D*xOpt((imSize(1)*imSize(2)+1):end), imSize);
%
%%%
%nowTime = now;
%imwrite(im1/max(max(im1)), strcat('../results/sourcesep/HitomiDiffImages128Full/', num2str(nowTime), '-ls1in.png'));
%imwrite(im2/max(max(im2)), strcat('../results/sourcesep/HitomiDiffImages128Full/', num2str(nowTime), '-ls2in.png'));
%imwrite(output1/max(max(output1)), strcat('../results/sourcesep/HitomiDiffImages128Full/', num2str(nowTime), '-ls1est.png'));
%imwrite(output2/max(max(output2)), strcat('../results/sourcesep/HitomiDiffImages128Full/', num2str(nowTime), '-ls2est.png'));
%
%MSIE1 = sum(sum((output1-double(im1)).^2))/(imSize(1)*imSize(2));
%MSIE2 = sum(sum((output2-double(im2)).^2))/(imSize(1)*imSize(2));
%disp([MSIE1 MSIE2]);
%
%fileID = fopen(strcat('../results/sourcesep/HitomiDiffImages128Full/', num2str(now), '-errReflSep.txt'), 'w');
%fprintf(fileID, '%f ', [MSIE1 MSIE2]);
%fclose(fileID);
%
%save(strcat('../results/sourcesep/HitomiDiffImages128Full/', num2str(nowTime), '.mat'))
