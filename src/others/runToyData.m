% Source Separation-based Coded Video Recovery
% Alankar Kotwal <alankarkotwal13@gmail.com>

function err = runToyData(l, s, n, m, eps)
 
	% % Parameters
	% l = 100;	    % Size of x
	% s = 0.1;	    % Sparsity in x
	% n = 0.4;	    % Fraction of signal measured
	% m = 2;		% Number of co-added signals
	% eps = 1e-5;	% Error for CVX

	% Matrices
	U = dctmtx(l);
	
	% Measurement
	phi = randn(round(n*l), l, m); 
	
	% Generate data
	x = zeros(l, m);
	y = zeros(round(n*l), 1);
	for i=1:m
		x(:, i) = sprand(l, 1, s);
		y = y + phi(:, :, i)*x(:, i);
	end
	
	% Matrices for recovery
	fullA = reshape(phi, round(l*n), l*m) * kron(eye(m), U);
	
	% Optimization
	cvx_begin quiet
		variable xOpt(m*l)
		minimize(norm(xOpt, 1))
		subject to
			norm(y-fullA*xOpt) <= eps %#ok<NOPRT>
	cvx_end
	
	% Validation
	origSignal = kron(eye(m), U) * reshape(x, l*m, 1);
	estSignal = kron(eye(m), U) * xOpt;
	err = sqrt(sum((origSignal - estSignal) .^ 2)/(sum(origSignal .^ 2)));

end
