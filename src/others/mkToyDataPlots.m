% Source Separation-based Coded Video Recovery
% Alankar Kotwal <alankarkotwal13@gmail.com>

clear; close all; clc;
scrsz = get(groot,'ScreenSize');
fig = figure('Position', scrsz);
drawnow;

nIter = 10;		% Iterations per point 

spMin = 0.05;		% Minimum sparsity
spMax = 1.00; 		% Maximum sparsity
spStep = 0.05;		% Sparsity step

nMin = 0.05;		% Minimum measurement fraction
nMax = 1.00;		% Maximum measurement fraction
nStep = 0.05;		% Measurement fraction step
 
% nIter = 1;		% Iterations per point 
% 
% spMin = 0.1;		% Minimum sparsity
% spMax = 1.00; 		% Maximum sparsity
% spStep = 0.2;		% Sparsity step
% 
% nMin = 0.1;		% Minimum measurement fraction
% nMax = 1.00;		% Maximum measurement fraction
% nStep = 0.2;		% Measurement fraction step

l = 100;		% Size of x
m = 2;			% Number of co-added signals
eps = 1e-5;		% Error for CVX

spVec = spMin:spStep:spMax;
nVec = nMin:nStep:nMax;

errs = zeros(size(spVec, 2), size(nVec, 2));

for i=1:size(spVec, 2)
	for j=1:size(nVec, 2)

		err = 0;
        for k = 1:nIter
            disp([i j k]);
			err = err + runToyData(l, spVec(i), nVec(j), m, eps);
        end
        errs(i, j) = err;
        
	end
end

imshow(1-errs/max(max(errs)), 'InitialMagnification', 'fit');
set(gca, 'XTickLabel', spVec);
set(gca, 'YTickLabel', nVec);
colorbar;

saveas(fig, '../../results/sourcesep/errorMap.png');disp(max(max(errs)));
fileID = fopen('../../results/sourcesep/maxErr.txt','w');
fprintf(fileID, '%s', max(max(errs)));
fclose(fileID);