% Source Separation-based Coded Video Recovery
% Alankar Kotwal <alankarkotwal13@gmail.com>

clear; close all; clc; tic;

% Parameters
alpha = 0.5;
eps = 1e-5;
sigma1 = 3;
sigma2 = 3;

im1 = imresize(im2double(rgb2gray(imread('../../data/ls1.png'))), 0.25);
im2 = imresize(im2double(rgb2gray(imread('../../data/ls2.jpg'))), 0.25);

phi1 = randn(size(im1));
phi2 = randn(size(im2));
phim1 = phi1(1:m, :);
phim2 = phi2(1:m, :);

