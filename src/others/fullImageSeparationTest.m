% Source Separation-based Coded Video Recovery
% Alankar Kotwal <alankarkotwal13@gmail.com>
clear; close all; clc; tic;

runName = input('Enter runName: ', 's');

% Parameters
eps = 1e-5;

im1 = im2double(imread('../data/s1.png'));
im2 = im2double(imread('../data/s2.png'));
imSize = size(im1);

% Measurement matrix
phi1 = randn(imSize(1)*imSize(2));% - eye(imSize(1)*imSize(2))/2;
phi2 = randn(imSize(1)*imSize(2));% - eye(imSize(1)*imSize(2))/2;

% Get the measurement
meas = phi1*im1(:) + phi2*im2(:);

% Add some noise
sigma = 0.05*mean(abs(meas));
meas = meas + sigma*randn(size(meas));

% Get the DCT matrix
D1 = dctmtx(imSize(1));
D2 = dctmtx(imSize(2));

%% BP machao
cvx_begin
    variable c1Opt(imSize)
    variable c2Opt(imSize)
    minimize(norm(c1Opt, 1) + norm(c2Opt, 1))
    subject to
        norm(meas-phi1*reshape(D1'*c1Opt*D2, size(meas))-phi2*reshape(D1'*c2Opt*D2, size(meas))) <= eps
cvx_end

output1 = reshape(D1'*c1Opt*D2, imSize);
output2 = reshape(D1'*c2Opt*D2, imSize);

%%
nowTime = now;
imwrite(im1/max(max(im1)), strcat('../results/sourcesep/TestFullImage/', num2str(nowTime), '-', runName, '-ls1in.png'));
imwrite(im2/max(max(im2)), strcat('../results/sourcesep/TestFullImage/', num2str(nowTime), '-', runName, '-ls2in.png'));
imwrite(output1/max(max(output1)), strcat('../results/sourcesep/TestFullImage/', num2str(nowTime), '-', runName, '-ls1est.png'));
imwrite(output2/max(max(output2)), strcat('../results/sourcesep/TestFullImage/', num2str(nowTime), '-', runName, '-ls2est.png'));
imwrite([im1 im2 output1 output2]/max(max([im1 im2 output1 output2])), strcat('../results/sourcesep/TestFullImage/', num2str(nowTime), '-', runName, '-full.png'));

MSIE1 = sum(sum((output1-double(im1)).^2))/sum(sum(double(im1).^2));
MSIE2 = sum(sum((output2-double(im2)).^2))/sum(sum(double(im1).^2));
disp([MSIE1 MSIE2]);

phi = [phi1 phi2];
normPhi = phi ./ repmat(sqrt(sum(phi.^2)), size(phi, 1), 1);
cohers = normPhi' * normPhi;
mu = max(max(abs(cohers - eye(size(cohers)))));

fileID = fopen(strcat('../results/sourcesep/BigPicture/', num2str(now), '-', runName, '-errReflSep.txt'), 'w');
fprintf(fileID, '%f ', [MSIE1 MSIE2 mu]);
fclose(fileID);

save(strcat('../results/sourcesep/TestFullImage/', num2str(nowTime), '-', runName, '.mat'))
