% Source Separation-based Coded Video Recovery
% Alankar Kotwal <alankarkotwal13@gmail.com>

clear; close all; clc; tic;

% Parameters
eps = 1e-5;

% im1 = imresize(im2double(rgb2gray(imread('../data/c1.png'))), 0.125);
% im2 = imresize(im2double(rgb2gray(imread('../data/c2.png'))), 0.125);
im1 = im2double(rgb2gray(imread('../data/c1.png')));
im2 = im2double(rgb2gray(imread('../data/c2.png')));
im1 = imresize(im1(:, 120:1020), 0.0625);
im2 = imresize(im2(:, 120:1020), 0.0625);
imSize = size(im1);

% Measurement matrix
phi1 = randn(imSize(1)*imSize(2));% - eye(imSize(1)*imSize(2))/2;
phi2 = randn(imSize(1)*imSize(2));% - eye(imSize(1)*imSize(2))/2;

% Get the measurement
meas = phi1*im1(:) + phi2*im2(:);

% Add some noise
sigma = 0.05*mean(abs(meas));
meas = meas + sigma*randn(size(meas));

% Get the DCT matrix
D1 = dctmtx(imSize(1));
D2 = dctmtx(imSize(2));

%% BP machao
cvx_begin
    variable c1Opt(imSize)
    variable c2Opt(imSize)
    minimize(norm(c1Opt, 1) + norm(c2Opt, 1))
    subject to
        norm(meas-phi1*reshape(D1'*c1Opt*D2, size(meas))-phi2*reshape(D1'*c2Opt*D2, size(meas))) <= eps
cvx_end

output1 = reshape(D1'*c1Opt*D2, imSize);
output2 = reshape(D1'*c2Opt*D2, imSize);

%%
nowTime = now;
imwrite(im1/max(max(im1)), strcat('../results/sourcesep/TestFullImage/', num2str(nowTime), '-ls1in.png'));
imwrite(im2/max(max(im2)), strcat('../results/sourcesep/TestFullImage/', num2str(nowTime), '-ls2in.png'));
imwrite(output1/max(max(output1)), strcat('../results/sourcesep/TestFullImage/', num2str(nowTime), '-ls1est.png'));
imwrite(output2/max(max(output2)), strcat('../results/sourcesep/TestFullImage/', num2str(nowTime), '-ls2est.png'));

MSIE1 = sum(sum((output1-double(im1)).^2))/(imSize(1)*imSize(2));
MSIE2 = sum(sum((output2-double(im2)).^2))/(imSize(1)*imSize(2));
disp([MSIE1 MSIE2]);

fileID = fopen(strcat('../results/sourcesep/TestFullImage/', num2str(now), '-errReflSep.txt'), 'w');
fprintf(fileID, '%f ', [MSIE1 MSIE2]);
fclose(fileID);

save(strcat('../results/sourcesep/TestFullImage/', num2str(nowTime), '.mat'))
