function compareEigen(T)

if T == 2
	load ../../results/hitomi-eigen/736446.0127-64x2x50Parallel.mat;
elseif T == 3
	load ../../results/hitomi-eigen/736492.8975-64x3x50Parallel.mat;
elseif T == 4
	load ../../results/hitomi-eigen/736493.5732-64x4x50Parallel.mat;
elseif T == 5
	load ../../results/hitomi-eigen/736494.4713-64x5x50Parallel.mat;
elseif T == 6
	load ../../results/hitomi-eigen/736496.9756-64x6x50Parallel.mat;
end

nSamp = 1000;
spars = (0.05:0.05:0.3)';
epsilon = 1e-5;

n = size(phi, 1);
T = size(phi, 2);
ps = sqrt(n);

runName = ['../../results/hitomi-eigen/' strrep(num2str(now), '.', '_') '-' num2str(n) '-' num2str(T) '-' num2str(nSamp)]; 

phiOrig = rand(size(phi));
sens = zeros(n, n*T);
sensOrig = zeros(n, n*T);
psi = zeros(n*T, n*T);
for t = 1:T
	sens(:, (t-1)*n+1:t*n) = diag(phi(:, t));
	sensOrig(:, (t-1)*n+1:t*n) = diag(phiOrig(:, t));
	psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
end
A = sens * psi;
AOrig = sensOrig * psi;

rrmse = zeros(nSamp, size(spars, 1));
rrmseOrig = zeros(nSamp, size(spars, 1));
eigvals = zeros(nSamp, size(spars, 1));
eigvalsOrig = zeros(nSamp, size(spars, 1));

for spar = 1:size(spars, 1)
	for samp = 1:nSamp

		disp([spars(spar) samp]);

		theta = zeros(n, T);
		for t = 1:T
			theta(:, t) = sprand(n, 1, spars(spar));
		end
		theta = theta(:);

		y = A * theta;
		yOrig = AOrig * theta;
		
		inpFrames = reshape(psi * theta, [n T]);
		outFrames = reconstruct(y, phi, D, epsilon);
		outFramesOrig = reconstruct(yOrig, phiOrig, D, epsilon);
		
		rrmse(samp, spar) = sqrt(norm(outFrames(:) - inpFrames(:))^2 / norm(inpFrames(:))^2);
		rrmseOrig(samp, spar) = sqrt(norm(outFramesOrig(:) - inpFrames(:))^2 / norm(inpFrames(:))^2);
        
		thisSuppA = A(:, theta ~= 0);
		thisSuppAOrig = AOrig(:, theta ~= 0);
		
		thisSuppA = thisSuppA' * thisSuppA;
		thisSuppAOrig = thisSuppAOrig' * thisSuppAOrig;
		
		thisSuppA = thisSuppA - diag(diag(thisSuppA));
		thisSuppAOrig = thisSuppAOrig - diag(diag(thisSuppAOrig));
		
		eigvals(samp, spar) = max(abs(eigs(thisSuppA)));
		eigvalsOrig(samp, spar) = max(abs(eigs(thisSuppAOrig)));
        
	end
end

save(runName);

sparsVec = (1:size(spars, 1))';

scrsz = get(groot, 'ScreenSize');
figure('Position', scrsz);
set(gca, 'FontSize', 50);
hold on;

bp1 = boxplot(rrmse, spars, 'Positions', sparsVec+0.25, 'symbol', '', 'widths', 0.4);
bp2 = boxplot(rrmseOrig, spars, 'Positions', sparsVec-0.25, 'symbol', '', 'widths', 0.4);
xlim([0 size(spars, 1)+1]);
ylim auto;

color = [repmat('b',1,size(spars, 1)), repmat('r', 1, size(spars, 1))];
h = findobj(gca,'Tag','Box');
for j=1:length(h)
   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
end
c = get(gca, 'Children');
hleg1 = legend([c(1) c(size(spars, 1)+1)], 'Optimized', 'Original', 'Location', 'NorthEast');

ylabel('RRMSE');
title(['RRMSE comparison: n = ' num2str(n) ', T = ' num2str(T)]);
set(gca, 'FontSize', 40);
savefig([runName '.fig']);
disp([runName '.fig']);

scrsz = get(groot, 'ScreenSize');
figure('Position', scrsz);
set(gca, 'FontSize', 50);
hold on;

bp3 = boxplot(eigvals, spars, 'Positions', sparsVec+0.25, 'symbol', '', 'widths', 0.4);
bp4 = boxplot(eigvalsOrig, spars, 'Positions', sparsVec-0.25, 'symbol', '', 'widths', 0.4);
xlim([0 size(spars, 1)+1]);
ylim auto;

color = [repmat('b',1,size(spars, 1)), repmat('r', 1, size(spars, 1))];
h = findobj(gca,'Tag','Box');
for j=1:length(h)
   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
end
c = get(gca, 'Children');
hleg1 = legend([c(1) c(size(spars, 1)+1)], 'Optimized', 'Original', 'Location', 'NorthEast');

ylabel('Maximum eigenvalue of AT * A - I');
title(['Eigenvalue comparison: n = ' num2str(n) ', T = ' num2str(T)]);
set(gca, 'FontSize', 40);
savefig([runName '-eig.fig']);
disp([runName '-eig.fig']);

end
