function out = chiDer(mu, nu, beta, gamma, delta, epsilon, phi, D)

	out = D(epsilon, beta) * D(epsilon, gamma) * ...
		  (double(delta == mu) * phi(epsilon, nu) + ...
		   double(delta == nu) * phi(epsilon, mu));

end
