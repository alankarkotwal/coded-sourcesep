function out = chi(mu, nu, beta, gamma, phi, D)

	out = sum((phi(:, mu) .* phi(:, nu)) .* ...
			   D(:, beta) .* D(:, gamma));

end
