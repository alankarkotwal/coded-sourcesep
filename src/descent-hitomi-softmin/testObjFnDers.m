imSize1 = 8;
imSize2 = 8;
T = 2;
D = kron(dctmtx(imSize2)', dctmtx(imSize1)');
phi = randn(size(D, 1), T);

theta = 100;
n = imSize1 * imSize2;

delta = 0.0001;
alpha = ceil(imSize1 * imSize2 * rand - 1) + 1;
mu = ceil(T * rand - 1) + 1;

[chis, xis, xiDerSums] = calcChiXi(phi, D);

objOrig = objFn(chis, xis, theta);

phiNew = phi;
phiNew(alpha, mu) = phiNew(alpha, mu) + delta;
[chisNew, xisNew, xiDerSumsNew] = calcChiXi(phiNew, D);
objFin = objFn(chisNew, xisNew, theta);

digDer = (objFin - objOrig) / delta;
disp(abs(digDer - objDer(mu, alpha, chis, xis, xiDerSums, theta, phi, D))/abs(digDer))