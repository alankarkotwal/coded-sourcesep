function [out, max] = objFn(chis, xis, theta)

	if nargin == 2
		theta = 25;
	end

	n = size(chis, 3);
	T = size(chis, 1);
    
    max = 0;

	out = 0;
    for mu = 1:T
        for nu = 1:(mu-1)
            for beta = 1:n
                for gamma = 1:n
                    temp = normDotProduct(mu, nu, beta, gamma, chis, xis);
					out = out + exp(theta * temp^2);
                    if(abs(temp) > max)
                        max = abs(temp);
                    end
                end
            end
        end
    end

	for mu = 1:T
		for beta = 1:n
			for gamma = 1:(beta-1)
                temp = normDotProduct(mu, mu, beta, gamma, chis, xis);
				out = out + exp(theta * temp^2);
                if(abs(temp) > max)
                        max = abs(temp);
                end
			end
		end
	end
	
	out = log(out) / theta;

end
