imSize1 = 8;
imSize2 = 8;
T = 2;
D = kron(dctmtx(imSize2)', dctmtx(imSize1)');
phi = randn(size(D, 1), T);
theta = 100;
n = imSize1 * imSize2;

[chis, xis] = calcChiXi(phi, D);
softObj = objFn(chis, xis, theta);

sens = zeros(n, n*T);
psi = zeros(n*T, n*T);
for t = 1:T
	sens(:, (t-1)*n+1:t*n) = diag(phi(:, t));
	psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
end
A = sens * psi;
normA = A ./ repmat(sqrt(sum(A.^2)), size(A, 1), 1);
dots = normA'*normA;
mu = max(max(abs(dots - eye(size(dots)))));

disp((sqrt(softObj)-mu)/mu);
