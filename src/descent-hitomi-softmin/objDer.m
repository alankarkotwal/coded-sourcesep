function out = objDer(delta, epsilon, chis, xis, xiDerSums, theta, phi, D)

	if nargin == 4
		theta = 25;
	end

	n = size(chis, 3);
	T = size(chis, 1);

	out = 0;
	for mu = 1:T
		for nu = 1:(mu-1)
			for beta = 1:n
				for gamma = 1:n
                    temp = normDotProduct(mu, nu, beta, gamma, chis, xis);
					out = out + exp(theta * temp^2) * ...
											2 * theta * temp * ...
								normDotProductDer(mu, nu, beta, gamma, delta, epsilon, chis, xis, xiDerSums, phi, D);
				end
			end
		end
	end

	for mu = 1:T
		for beta = 1:n
			for gamma = 1:(beta-1)
                temp = normDotProduct(mu, mu, beta, gamma, chis, xis);
				out = out + exp(theta * temp^2) * ...
										2 * theta * temp * ...
							normDotProductDer(mu, mu, beta, gamma, delta, epsilon, chis, xis, xiDerSums, phi, D);
			end
		end
	end
	
	out = out / (theta * exp(theta * objFn(chis, xis, theta)));

end
