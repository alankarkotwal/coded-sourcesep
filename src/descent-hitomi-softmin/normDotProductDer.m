function out = normDotProductDer(mu, nu, beta, gamma, delta, epsilon, chis, xis, xiDerSums, phi, D)

	out = (xis(mu, nu, beta, gamma) * chiDer(mu, nu, beta, gamma, delta, epsilon, phi, D) - ...
	       chis(mu, nu, beta, gamma) * xiDer(mu, nu, beta, gamma, delta, epsilon, phi, D, xis, xiDerSums)) / ...
	      xis(mu, nu, beta, gamma)^2;

end
