function [phi, obj] = descentPlot(T, D, theta, step, stepFactor, iter, eps, repCountMax)

	n = size(D, 1);

	if nargin == 3
		step = 0.1;
		stepFactor = 0.5;
		iter = 100;
		eps = 0.5;
		repCountMax = 5;
	elseif nargin == 4
		stepFactor = 0.5;
		iter = 100;
		eps = 0.5;
		repCountMax = 5;
	elseif nargin == 5
		iter = 100;
		eps = 0.5;
		repCountMax = 5;
	elseif nargin == 6
		eps = 0.5;
		repCountMax = 5;
	elseif nargin == 7
		repCountMax = 5;
	end

	phi = rand(n, T);
	[chis, xis] = calcChiXi(phi, D);
	objOld = objFn(chis, xis, theta);

	disp([0.0 sqrt(objOld)]);

	count = 0;
	repCount = 0;
	
	cohs = [sqrt(objOld)];

	while count < iter

		descDir = zeros(n, T);
	
		for delta = 1:T
			for epsilon = 1:n
				descDir(epsilon, delta) = objDer(delta, epsilon, chis, xis, theta, phi, D);
			end
		end
		phiNew = phi - step * descDir;
		phiNew(phiNew < eps) = eps;

		[chisNew, xisNew] = calcChiXi(phiNew, D);
		objNew = objFn(chisNew, xisNew, theta);

		if objNew < objOld - 0.0001
			phi = phiNew;
			objOld = objNew;
			%step = step / stepFactor;
			chis = chisNew;
			xis = xisNew;

			count = count + 1;
			repCount = 0;

			cohs = [cohs; sqrt(objOld)];
		else
			step = step * stepFactor;
			repCount = repCount + 1;
			if repCount >= repCountMax
				break
			end
		end
	
		disp([round(count) sqrt(objOld)]);
	
	end
	
	obj = objOld;
	
	f = figure;
	plot(1:size(cohs, 1), cohs, 'LineWidth', 10);
	set(gca, 'FontSize', 25);
	xlabel('Iteration Number');
	ylabel('Coherence');
	title('Coherence Evolution Across Iterations')
	saveas(f, '../../results/descent-hitomi-softmin/plots/descPlot.png');

end
