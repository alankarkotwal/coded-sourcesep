runName = input('Enter runName: ', 's');

n1 = 8;
n2 = 8;
T = 6;
D = kron(dctmtx(n2)', dctmtx(n1)');

nStarts = 50;
theta = 100;

step = 10;
stepFactor = 0.5;
iter = Inf;
repCountMax = 8;
eps = 0;
minChange = 0.001;

phis = zeros(n1*n2, T, nStarts);
objs = zeros(nStarts, 1);

parfor i = 1:nStarts
	
	disp(strcat('***** Start ', num2str(i), ' *****'));
	[thisPhi, thisObj] = descent(T, D, theta, step, stepFactor, iter, eps, repCountMax, minChange);

	phis(:, :, i) = thisPhi;
	objs(i) = thisObj;

end

[obj, pos] = min(objs);
phi = phis(:, :, pos);

cohEst = sqrt(obj);

disp('The minimum coherence value is ');
disp(num2str(cohEst));

nowTime = now;
fileID = fopen(strcat('../../results/descent-hitomi-softmin/', num2str(nowTime), '-', runName, '-output.txt'), 'w');
fprintf(fileID, '%f \n', cohEst);
for i = 1:T
	fprintf(fileID, strcat('phi_', num2str(i), ':\n'));
	fprintf(fileID, '%f \n', phi(:, i));
end
fclose(fileID);

save(strcat('../../results/descent-hitomi-softmin/', num2str(nowTime), '-', runName, '.mat'));
