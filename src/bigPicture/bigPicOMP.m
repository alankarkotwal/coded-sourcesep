% Source Separation-based Coded Video Recovery
% Alankar Kotwal <alankarkotwal13@gmail.com>
clear; close all; clc; tic;

runName = input('Enter runName: ', 's');

im1 = im2double(imread('../data/s1.png'));
im2 = im2double(imread('../data/s2.png'));
im1 = im2double(rgb2gray(imread('../data/c1.png')));
im2 = im2double(rgb2gray(imread('../data/c2.png')));
im1 = imresize(im1(:, 120:1320), 2/90);
im2 = imresize(im2(:, 120:1320), 2/90); 
imSize = size(im1);

% Measurement matrix
phi1 = diag(rand(imSize(1)*imSize(2), 1));% - eye(imSize(1)*imSize(2))/2;
phi2 = diag(rand(imSize(1)*imSize(2), 1));% - eye(imSize(1)*imSize(2))/2;

% Get the measurement
meas = phi1*im1(:) + phi2*im2(:);

% Add some noise
sigma = 0.05*mean(abs(meas));
meas = meas + 2*sigma*rand(size(meas)) - sigma;
eps = size(meas, 1)*sigma*sigma/3;

% Get the DCT matrix
D = kron(dctmtx(imSize(1))', dctmtx(imSize(2))');

%% OMP machao
res = meas;

suppSet = zeros(1, imSize(1)*imSize(2));
suppSetIdx = 1;
A = [phi1*D phi2*D];

count = 0;

while(sum(res .* res) > eps)
	
	disp(['Finished ' num2str(count) '/' num2str(imSize(1)*imSize(2))]);

	coeffs = abs(sum(A .* repmat(res, 1, 2*imSize(1)*imSize(2)))./sum(A .* A));
	[~, idx] = max(coeffs);
	
	if(isempty(find(suppSet == idx, 1)))
		suppSet(suppSetIdx) = idx;
		suppSetIdx = suppSetIdx + 1;
	end

	Ai = zeros(imSize(1)*imSize(2), suppSetIdx-1);
	for k=1:suppSetIdx-1
		Ai(:, k) = A(:, suppSet(k));
	end
	
	si = Ai\meas;
	
	res = meas;
	for k=1:suppSetIdx-1
		res = res - si(k)*Ai(:, k);
	end
	
	count = count + 1;
end

output1 = zeros(imSize(1)*imSize(2), 1);
output2 = zeros(imSize(1)*imSize(2), 1);
for k=1:suppSetIdx - 1
	if suppSet(k) < imSize(1)*imSize(2) + 1
		output1 = output1 + si(k)*D(:, suppSet(k));
	else
		output2 = output2 + si(k)*D(:, suppSet(k)-imSize(1)*imSize(2));
	end
end 

output1 = zeros(imSize(1)*imSize(2), 1);
output2 = zeros(imSize(1)*imSize(2), 1);
for k=1:suppSetIdx - 1
	if suppSet(k) < imSize(1)*imSize(2) + 1
		output1 = output1 + si(k)*D(:, suppSet(k));
	else
		output2 = output2 + si(k)*D(:, suppSet(k)-imSize(1)*imSize(2));
	end
end 

output1 = reshape(output1, size(im1));
output2 = reshape(output2, size(im2));

%%
nowTime = now;
imwrite(im1/max(max(im1)), strcat('../results/sourcesep/BigPicture/', runName, '-ls1in.png'));
imwrite(im2/max(max(im2)), strcat('../results/sourcesep/BigPicture/', runName, '-ls2in.png'));
imwrite(output1/max(max(output1)), strcat('../results/sourcesep/BigPicture/', runName, '-ls1est.png'));
imwrite(output2/max(max(output2)), strcat('../results/sourcesep/BigPicture/', runName, '-ls2est.png'));
imwrite([im1 im2 output1 output2]/max(max([im1 im2 output1 output2])), strcat('../results/sourcesep/BigPicture/', runName, '-full.png'));

MSIE1 = sum(sum((output1-double(im1)).^2))/sum(sum(double(im1).^2));
MSIE2 = sum(sum((output2-double(im2)).^2))/sum(sum(double(im1).^2));
disp([MSIE1 MSIE2]);

phi = [phi1 phi2];
normPhi = phi ./ repmat(sqrt(sum(phi.^2)), size(phi, 1), 1);
cohers = normPhi' * normPhi;
mu = max(max(abs(cohers - eye(size(cohers)))));

fileID = fopen(strcat('../results/sourcesep/BigPicture/', runName, '-errReflSep.txt'), 'w');
fprintf(fileID, '%f ', [MSIE1 MSIE2 mu]);
fclose(fileID);

save(strcat('../results/sourcesep/BigPicture/', runName, '.mat'))
