function out = normDotProductDer(mu, nu, beta, gamma, delta, epsilon, phi, D)

	thisXi = xi(mu, nu, beta, gamma, phi, D);
	out = (thisXi * chiDer(mu, nu, beta, gamma, delta, epsilon, phi, D) - ...
	       chi(mu, nu, beta, gamma, phi, D) * xiDer(mu, nu, beta, gamma, delta, epsilon, phi, D)) / ...
	      thisXi^2;

end
