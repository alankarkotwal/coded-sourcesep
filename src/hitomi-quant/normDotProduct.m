function out = normDotProduct(mu, nu, beta, gamma, chis, xis)

	out = chis(mu, nu, beta, gamma) / xis(mu, nu, beta, gamma);

end