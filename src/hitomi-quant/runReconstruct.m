function runReconstruct(T, spacing)

if T == 2
	load ../../results/hitomi-eigen/736446.0127-64x2x50Parallel.mat;
elseif T == 3
	load ../../results/hitomi-eigen/736492.8975-64x3x50Parallel.mat;
elseif T == 4
	load ../../results/hitomi-eigen/736493.5732-64x4x50Parallel.mat;
elseif T == 5
	load ../../results/hitomi-eigen/736494.4713-64x5x50Parallel.mat;
elseif T == 6
	load ../../results/hitomi-eigen/736496.9756-64x6x50Parallel.mat;
end

nSamp = 100;
spars = (0.01:0.02:0.14)';
epsilon = 1e-5;
%spacing = 0.2;
quantVals = [0:spacing:1]';

n = size(phi, 1);
T = size(phi, 2);
ps = sqrt(n);
D = kron(dctmtx(ps)', dctmtx(ps)');

runName = ['../../results/hitomi-quant/' strrep(num2str(now), '.', '_') '-' num2str(n) '-' num2str(T) '-' num2str(spacing) '-' num2str(nSamp)]; 
phiQuant = quantizeMatrix(phi, quantVals);

sens = zeros(n, n*T);
sensQuant = zeros(n, n*T);
psi = zeros(n*T, n*T);
for t = 1:T
	sens(:, (t-1)*n+1:t*n) = diag(phi(:, t));
	sensQuant(:, (t-1)*n+1:t*n) = diag(phiQuant(:, t));
	psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
end
A = sens * psi;
AQuant = sensQuant * psi;

rrmse = zeros(nSamp, size(spars, 1));
rrmseQuant = zeros(nSamp, size(spars, 1));

for spar = 1:size(spars, 1)
	parfor samp = 1:nSamp

		disp([spars(spar) samp]);

		theta = zeros(n, T);
		for t = 1:T
			theta(:, t) = sprand(n, 1, spars(spar));
		end
		theta = theta(:);

		y = A * theta;
		yQuant = AQuant * theta;

		y = y + 2 * epsilon / ps * (rand(n, 1) - 0.5);
		yQuant = yQuant + 2 * epsilon / ps * (rand(n, 1) - 0.5);
		
		inpFrames = reshape(psi * theta, [n T]);
		outFrames = reconstruct(y, phi, D, epsilon);
		outFramesQuant = reconstruct(yQuant, phiQuant, D, epsilon);
		
		rrmse(samp, spar) = sqrt(norm(outFrames(:) - inpFrames(:))^2 / norm(inpFrames(:))^2);
		rrmseQuant(samp, spar) = sqrt(norm(outFramesQuant(:) - inpFrames(:))^2 / norm(inpFrames(:))^2);
	end
end

save(runName);

sparsVec = (1:size(spars, 1))';

scrsz = get(groot, 'ScreenSize');
figure('Position', scrsz);
set(gca, 'FontSize', 50);
hold on;

bp1 = boxplot(rrmse, spars, 'Positions', sparsVec+0.25, 'widths', 0.4);
bp2 = boxplot(rrmseQuant, spars, 'Positions', sparsVec-0.25, 'widths', 0.4);
xlim([0 size(spars, 1)+1]);
ylim auto;

color = [repmat('b',1,size(spars, 1)), repmat('r', 1, size(spars, 1))];
h = findobj(gca,'Tag','Box');
for j=1:length(h)
   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
end
c = get(gca, 'Children');
hleg1 = legend([c(1) c(size(spars, 1)+1)], 'Original', 'Quantized', 'Location', 'NorthEast');

xlabel('Sparsity');
ylabel('RRMSE');
title(['RRMSE comparison: n = ' num2str(n) ', T = ' num2str(T)]);
set(gca, 'FontSize', 40);
savefig([runName '.fig']);
disp([runName '.fig']);

