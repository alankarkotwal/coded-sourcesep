function [phi, obj] = descent(T, D, step, stepFactor, iter, eps, repCountMax, minChange)

	n = size(D, 1);

	if nargin == 3
		step = 0.1;
		stepFactor = 0.5;
		iter = 100;
		eps = 0.5;
		repCountMax = 5;
		minChange = 0.0001;
	elseif nargin == 4
		stepFactor = 0.5;
		iter = 100;
		eps = 0.5;
		repCountMax = 5;
		minChange = 0.0001;
	elseif nargin == 5
		iter = 100;
		eps = 0.5;
		repCountMax = 5;
		minChange = 0.0001;
	elseif nargin == 6
		eps = 0.5;
		repCountMax = 5;
		minChange = 0.0001;
	elseif nargin == 7
		repCountMax = 5;
		minChange = 0.0001;
	elseif nargin == 8
		minChange = 0.0001;
	end

	phi = rand(n, T);
	[objOld, sgn, muMax, nuMax, betaMax, gammaMax] = objFnFast(phi, D);

	disp([0.0 objOld]);

	count = 0;
	repCount = 0;
	iTookAStep = 1;
	descDir = zeros(n, T);
	while count < iter
	
		if iTookAStep
			descDir = objDer(sgn, muMax, nuMax, betaMax, gammaMax, phi, D);
		end
		
		phiNew = phi - step * descDir;
		phiNew(phiNew < eps) = eps;

		[objNew, sgnNew, muMaxNew, nuMaxNew, betaMaxNew, gammaMaxNew] = objFnFast(phiNew, D);

		if objNew < objOld - minChange
			phi = phiNew;
			objOld = objNew;
			sgn = sgnNew;
			muMax = muMaxNew;
			nuMax = nuMaxNew;
			betaMax = betaMaxNew;
			gammaMax = gammaMaxNew;
			%step = step / stepFactor;

			count = count + 1;
			repCount = 0;
			iTookAStep = 1;
		else
			step = step * stepFactor;
			repCount = repCount + 1;
			iTookAStep = 0;
			if repCount >= repCountMax
				break
			end
		end
	
		disp([count objOld]);
	
	end
	
	obj = objOld;

end
