function out = quantizeMatrix(in, vals)

	out = vals(knnsearch(vals, in(:)));
	out = reshape(out, size(in));

end
