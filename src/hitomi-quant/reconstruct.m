function out = reconstruct(y, phi, D, epsilon)

	n = size(phi, 1);
	T = size(phi, 2);
	ps = sqrt(n);

	sens = zeros(n, n*T);
	psi = zeros(n*T, n*T);
	for t = 1:T
		sens(:, (t-1)*n+1:t*n) = diag(phi(:, t));
		psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
	end
	A = sens * psi;

	cvx_begin quiet
		variable theta(n*T)
		minimize(norm(theta, 1))
		subject to
			norm(y - A * theta) <= epsilon
	cvx_end

	theta = reshape(theta, [n, T]);
	out = D * theta;

end
