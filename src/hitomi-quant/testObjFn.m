imSize1 = 4;
imSize2 = 4;
T = 5;
D = kron(dctmtx(imSize2)', dctmtx(imSize1)');
phi = randn(size(D, 1), T);
n = imSize1 * imSize2;

[chis, xis] = calcChiXi(phi, D);
[obj, sgn, muMax, nuMax, betaMax, gammaMax] = objFn(chis, xis);

sens = zeros(n, n*T);
psi = zeros(n*T, n*T);
for t = 1:T
	sens(:, (t-1)*n+1:t*n) = diag(phi(:, t));
	psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
end
A = sens * psi;
normA = A ./ repmat(sqrt(sum(A.^2)), size(A, 1), 1);
dots = normA'*normA;
mu = max(max(abs(dots - eye(size(dots)))));

[objFast, sgnFast, muMaxFast, nuMaxFast, betaMaxFast, gammaMaxFast] = objFnFast(phi, D);

disp((obj-mu)/mu);
disp((objFast-mu)/mu);

disp(sgnFast == sgn);
disp([muMax nuMax muMaxFast nuMaxFast]);
disp([betaMax gammaMax betaMaxFast gammaMaxFast]);
