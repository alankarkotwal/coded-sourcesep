function out = xiDerSumTerm(phi, D, mu, beta)

        out = sum(phi(:, mu).^2 .* D(:, beta).^2);

end