function out = getRIC(A, s)

	A = normc(A);
	n = size(A, 2);
	combs = combnk(1:n, s);

	out = 0;

	eigs = zeros(size(combs, 1), 1);
	parfor i = 1:size(combs, 1)

		subsA = A(:, combs(i, :));
		dots = subsA' * subsA;
		dotsSub = dots - diag((diag(dots)));

		eigs(i) = max(abs(eig(dotsSub)));

	end

	out = max(eigs);

end
