function [errs, xs, xOuts, ys] = generateSamples(A, s, nSamp, epsilon)

	nMeas = size(A, 1);
	n = size(A, 2);

	xs = zeros(n, nSamp);
	ys = zeros(nMeas, nSamp);
	xOuts = zeros(n, nSamp);
	parfor samp = 1:nSamp
	    disp(['Working on sample ' num2str(samp)]);
	    xs(:, samp) = sprand(n, 1, s/n);
	    ys(:, samp) = A * xs(:, samp) + 2 * epsilon / sqrt(nMeas) * (rand(nMeas, 1) - 0.5);
	    xOuts(:, samp) = reconstruct(ys(:, samp), A, epsilon);
	end

	errs = norms(xOuts - xs)';

end
