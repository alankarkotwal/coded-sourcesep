function out = reconstruct(y, A, epsil)

	cvx_begin quiet
		variable out(size(A, 2))
		minimize(norm(out, 1))
		subject to
			norm(y - A * out) <= epsil
	cvx_end

end
