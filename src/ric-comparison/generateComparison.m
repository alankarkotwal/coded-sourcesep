function runGenerateComparison(nMeas, n, s)

	%clear; clc; close all;
	
	nSamp = 100;
	%nMeas = 70;
	%n = 250;
	epsilon = 1e-5;
	
	A = normc(randn(nMeas, n));
	ricA = getRIC(A, s);
	if ricA > 0.307
		disp(['RIC condition not satisfied! RIC = ' num2str(ricA)]);
		return;
	else
		disp(['RIC condition satisfied! RIC = ' num2str(ricA)]);
	end
	
	errs = generateSamples(A, s, nSamp, epsilon);
	ricErrBound = epsilon / (0.307 - ricA);

	relativeDiffs = (ricErrBound - errs) ./ errs;
	
	runName = ['../../results/ric-comparison/' strrep(num2str(now), '.', '_') '-' num2str(nMeas) 'x' num2str(n) '-' num2str(s) '-' num2str(nSamp)];
	
	save([runName '.mat'], 'A', 's', 'ricA', 'ricErrBound', 'relativeDiffs');

end
