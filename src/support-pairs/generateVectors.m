function [vecs, supps] = generateVectors(n, s, nSupp, nSamp, noiseSD)

	nxPerSupp = ceil(n * s);

	% Generate random supports
	supps = zeros(nxPerSupp, nSupp);
	for i = 1:nSupp
		perm = randperm(n);
		supps(:, i) = perm(1:nxPerSupp);
	end
	supps = sort(supps);

	vecs = zeros(n, nSamp);
	for i = 1:nSamp
	
		tempX = zeros(n, 1);
		tempX(supps(:, randi(nSupp))) = rand(nxPerSupp, 1);
		vecs(:, i) = tempX + noiseSD * randn(n, 1);

	end

end
