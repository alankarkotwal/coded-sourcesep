function frames = demosaic(snap, phis, patchSize, eps, mod)

	% We assume the snapshot is created with the Hitomi model, that is,
	% as a linear coded combination of input frames
	% Further, paches are square
	if size(phis, 1) ~= size(snap, 1) || size(phis, 2) ~= size(snap, 2)
		error('Code and snapshot sizes do not match!');
	end

	D = kron(dctmtx(patchSize)', dctmtx(patchSize)');
	imSize = size(snap);
	T = size(phis, 3);

	frames = zeros(imSize(1), imSize(2), T);
	opMask = zeros(imSize(1), imSize(2));

	if strcmp(mod, 'ovp')
		step = 2;
	elseif strcmp(mod, 'novp')
		step = patchSize;
	else
		error('Mode error!');
	end

	for i = 1:step:imSize(1)-patchSize+1
	    for j = 1:step:imSize(2)-patchSize+1
	        
	        disp([i j]);
	        
	        res = reshape(snap(i:i+patchSize-1, j:j+patchSize-1), patchSize*patchSize, 1);
		phi = phis(i:i+patchSize-1, j:j+patchSize-1, :);

		A = zeros(patchSize*patchSize, patchSize*patchSize*T);
		for k = 1:T
			A(:, (k-1)*patchSize*patchSize+1:k*patchSize*patchSize) = diag(reshape(phi(:, :, k), patchSize*patchSize, 1)) * D;
		end
	        
	        % Calculate patch from res here
	        cvx_begin quiet
	            variable c(patchSize*patchSize*T)
	            minimize(norm(c, 1))
	            subject to
	                norm(res-A*c) <= eps
	        cvx_end
	        
		c = reshape(c, patchSize*patchSize, T);

		for k = 1:T
	        	frames(i:i+patchSize-1, j:j+patchSize-1, k) = frames(i:i+patchSize-1, j:j+patchSize-1, k) + reshape(D*c(:, k), patchSize, patchSize);
		end
	        opMask(i:i+7, j:j+7) = opMask(i:i+7, j:j+7) + ones(8);
	        
	    end
	end
	
	opMask = repmat(opMask, 1, 1, T);
	frames = frames ./ opMask;	
	frames(find(isnan(frames))) = 0;

end
