function mu = getCoherence(phi, D)

	n = size(phi, 1);
	T = size(phi, 2);

	sens = zeros(n, n*T);
	psi = zeros(n*T, n*T);
	for t = 1:T
		sens(:, (t-1)*n+1:t*n) = diag(phi(:, t));
		psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
	end
	A = sens * psi;
	normA = A ./ repmat(sqrt(sum(A.^2)), size(A, 1), 1);
	dots = normA'*normA;
	mu = max(max(abs(dots - eye(size(dots)))));

end
