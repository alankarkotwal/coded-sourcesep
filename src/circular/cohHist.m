clear; clc; close all;

load ../../results/descent-circular/good/736507.287-64x2x50Parallel.mat;
%load ../../results/descent/good/736446.0127-64x2x50Parallel.mat;

runName = input('Enter runName: ', 's');

T = size(phi, 2);
patchSize = 8;

D = kron(dctmtx(patchSize)', dctmtx(patchSize)');
cohs = zeros(patchSize);
%phi = rand(size(phi));

for i = 1:patchSize
	for j = 1:patchSize
	
		thisPhi = circshift(reshape(phi, patchSize, patchSize, T), [i j 0]);
		cohs(i, j) = getCoherence(reshape(thisPhi, patchSize*patchSize, T), D);

	end
end

scrsz = get(groot,'ScreenSize');
fig = figure('Position', scrsz);
h = histogram(cohs);
title('Circularly shifted histograms');
xlabel('Coherence value');
ylabel('Frequency');
set(gca, 'FontSize', 40);
print('-depsc', ['../../papers_write/icassp1/pics/descent-circular' runName '-cohHist.eps']);
