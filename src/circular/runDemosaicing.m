% Source Separation-based Coded Video Recovery
% Alankar Kotwal <alankarkotwal13@gmail.com>

function runDemosaicing(isrand, isovp)
%clear; close all; clc; tic;

% Parameters
patchSize = 8;
eps = 1e-5;

% Run settings
T = 3;
%isrand = 0;
%isovp = 0;


% Load matrices
load ../../results/descent/good/736492.8975-64x3x50Parallel.mat;
%load ../../results/descent-circular/good/736546.1679-64x3x1.mat

% Form runName
if isrand
	runName = 'DemosiacRandom';
else
	runName = 'DemosaicDesigned';
end

if isovp
	runName = [runName 'Ovp'];
else
	runName = [runName 'NoOvp'];
end

% Load data
image = '../../data/kodim03.png';
imTemp = im2double(imread(image));
ims = zeros([round(0.75*size(imTemp, 1)) round(0.75*size(imTemp, 2)) T]);
for i = 1:T
	ims(:, :, i) = imresize(imTemp(:, :, i), 0.75);
	%ims(:, :, i) = imTemp(:, :, i);
end


% Generate sensing matrices
phiPatch = zeros(patchSize, patchSize, 2);
for i = 1:T
	phiPatch(:, :, i) = reshape(phi(:, i), patchSize, patchSize);
end
rep1 = ceil(size(ims, 1) / patchSize);
rep2 = ceil(size(ims, 2) / patchSize);
phis = repmat(phiPatch, rep1, rep2, 1);
phis = phis(1:size(ims, 1), 1:size(ims, 2), :);

if isrand
	phis = rand(size(ims, 1), size(ims, 2), size(ims, 3));
end
% Generate coded snapshot
snap = sum(ims .* phis, 3); 

% Solve Basis Pursuit
if isovp
	outs = processSnapshot(snap, phis, patchSize, eps, 'ovp');
else
	outs = processSnapshot(snap, phis, patchSize, eps, 'novp');
end

% Write results
nowTime = now;
imwrite(ims, strcat('../../results/sourcesep/Demosiacing/', num2str(nowTime), '-', runName, '-in.png'));
imwrite(outs, strcat('../../results/sourcesep/Demosiacing/', num2str(nowTime), '-', runName, '-out.png'));

err = sqrt(sum(sum(sum((ims - outs) .^ 2)))/sum(sum(sum(ims .^ 2))));
disp(['The RMS error per channel is ' num2str(err/3) '.']);

fileID = fopen(strcat('../../results/sourcesep/Demosiacing/', num2str(now), '-', runName, '-err.txt'), 'w');
fprintf(fileID, '%f', err);
fclose(fileID);

end
