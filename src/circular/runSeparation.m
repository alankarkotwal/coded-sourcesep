% Source Separation-based Coded Video Recovery
% Alankar Kotwal <alankarkotwal13@gmail.com>

function runSeparation(T, isrand, isovp)
%clear; close all; clc; tic;

% Parameters
patchSize = 4;
eps = 1e-5;

% Run settings
%T = 5;
%isrand = 0;
%isovp = 0;


% Load matrices
if T == 2
	%load ../../results/descent/good/736446.0127-64x2x50Parallel.mat;
	%load ../../results/descent-circular/good/736507.287-64x2x50Parallel.mat;
	load ../../results/descent-gersh/good/736543.8837-16x2x4.mat;
elseif T == 3
	load ../../results/descent/good/736492.8975-64x3x50Parallel.mat;
elseif T == 4
	load ../../results/descent/good/736493.5732-64x4x50Parallel.mat;
elseif T == 5
	load ../../results/descent/good/736494.4713-64x5x50Parallel.mat;
elseif T == 6
	load ../../results/descent/good/736496.9756-64x6x50Parallel.mat;
end

% Form runName
if isrand
	runName = 'Random';
else
	runName = 'Designed';
end

if isovp
	runName = [runName 'Ovp'];
else
	runName = [runName 'NoOvp'];
end

runName = [runName '-' num2str(T)]

% Load data
images = cellstr(['../../data/c1.png'; '../../data/c2.png'; '../../data/c3.png'; '../../data/c4.png'; '../../data/c5.png'; '../../data/c6.png']);
imTemp = imread(images{1});
ims = zeros([448 544 T]);
for i = 1:T
	imTemp = im2double(rgb2gray(imread(images{i})));
	imTemp = imresize(imTemp(:, 170:1270), 0.5);
	ims(:, :, i) = imTemp(1:448, 1:544);
end


% Generate sensing matrices
phiPatch = zeros(patchSize, patchSize, 2);
for i = 1:T
	phiPatch(:, :, i) = reshape(phi(:, i), patchSize, patchSize);
end
rep1 = ceil(size(ims, 1) / patchSize);
rep2 = ceil(size(ims, 2) / patchSize);
phis = repmat(phiPatch, rep1, rep2, 1);
phis = phis(1:size(ims, 1), 1:size(ims, 2), :);

if isrand
	phis = rand(size(ims, 1), size(ims, 2), size(ims, 3));
end
% Generate coded snapshot
snap = sum(ims .* phis, 3); 

% Solve Basis Pursuit
if isovp
	outs = processSnapshot(snap, phis, patchSize, eps, 'ovp');
else
	outs = processSnapshot(snap, phis, patchSize, eps, 'novp');
end

% Write results
nowTime = now;
for i = 1:T
	imwrite(ims(:, :, i)/max(max(ims(:, :, 1))), strcat('../../results/sourcesep/HitomiTiled/GershOpt/', num2str(nowTime), '-', runName, '-', num2str(i), '-in.png'));
	imwrite(outs(:, :, i)/max(max(outs(:, :, 1))), strcat('../../results/sourcesep/HitomiTiled/GershOpt/', num2str(nowTime), '-', runName, '-', num2str(i), '-out.png'));
end

err = sqrt(sum(sum(sum((ims - outs) .^ 2)))/sum(sum(sum(ims .^ 2))));
disp(['The RMS error is ' num2str(err) '.']);

fileID = fopen(strcat('../../results/sourcesep/HitomiTiled/GershOpt/', num2str(now), '-', runName, '-err.txt'), 'w');
fprintf(fileID, '%f', err);
fclose(fileID);

end
