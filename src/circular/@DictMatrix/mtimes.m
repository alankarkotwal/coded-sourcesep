function res = mtimes(A, x)

if A.adjoint == 0
    
    if size(x, 1) ~= A.m*A.imSize1*A.imSize2
        error('Size of x should be the same as the A.m*A.imSize1*A.imSize2 for non-adjoint A');
    end
    
    x = reshape(x, A.imSize1*A.imSize2, A.m);
    res = zeros(A.imSize1*A.imSize2, 1);
    for i = 1:A.m
        res = res + A.phi(:, i).*reshape(idct2(reshape(x(:, i), A.imSize1, A.imSize2)), A.imSize1*A.imSize2, 1);
    end
    
else 
    
    if size(x, 1) ~= A.imSize1*A.imSize2
        error('Size of x should be the same as the A.imSize1*A.imSize2 for adjoint A');
    end
	res = zeros(A.m*A.imSize1*A.imSize2, 1);
    n = A.imSize1*A.imSize2;
    for i = 1:A.m
        res((i-1)*n+1:n*i) = reshape(dct2(reshape(A.phi(:, i) .* x, A.imSize1, A.imSize2)), A.imSize1*A.imSize2, 1);
    end
    
end

end