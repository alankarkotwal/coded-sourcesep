function xs = generateSamples(n, T, nSamp, supps)

	nSupp = size(supps, 2);

	xs = zeros(n, T, nSamp);
	for samp = 1:nSamp
		for t = 1:T
			xs(supps(:, randi(nSupp)), t, samp) = rand(size(supps, 1), 1);
		end
	end

end
