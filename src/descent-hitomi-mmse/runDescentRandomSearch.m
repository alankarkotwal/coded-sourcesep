T = 2;
ps = 8;
s = 0.2;

nSamp = 250;
nTestSamp = 1000;
nStarts = 10;
nSupp = 3;
eta = 1e-2;
epsil = eta;
bound = 0.2;

nIter = Inf;
repCountMax = 10;

if T == 2
	load ../../results/hitomi-eigen/736440.8135-64x2x10Parallel.mat phi;
	phiCoh = phi;
elseif T == 4
	load ../../results/hitomi-eigen/736493.5732-64x4x50Parallel.mat phi;
	phiCoh = phi;
elseif T == 6
	load ../../results/hitomi-eigen/736496.9756-64x6x50Parallel.mat phi;
	phiCoh = phi;
end
clear phi;

n = ps^2;
D = kron(dctmtx(8)', dctmtx(8)');
psi = kron(eye(T), D);

thetas = generateSamples(n, T, nSamp, s);

objs = zeros(nStarts, 1);
objsCoh = zeros(nStarts, 1);
phis = zeros(n, T, nStarts);
phiInits = zeros(n, T, nStarts);

for start = 1:nStarts
	disp(['*************** Start ' num2str(start) ' ***************']);
	[phis(:, :, start), objs(start), phiInits(:, :, start)] = descentRandomSearch(psi, thetas, epsil, bound, nIter, repCountMax);
end
	 
[minObj, minIdx] = min(objs);
phi = phis(:, :, minIdx);
phiInit = phiInits(:,:, minIdx);

A = getAMatrix(phi, psi);
ARand = getAMatrix(phiInit, psi);
ACoh = getAMatrix(phiCoh, psi);

testThetas = generateSamples(n, T, nTestSamp, s);
noiseInsts = 2 * eta / sqrt(n) * (rand(n, nTestSamp) - 0.5);

desErrs = generateErrs(A, testThetas, psi, epsil, noiseInsts);
randErrs = generateErrs(ARand, testThetas, psi, epsil, noiseInsts);
cohErrs = generateErrs(ACoh, testThetas, psi, epsil, noiseInsts);

runName = ['../../results/descent-hitomi-mmse/' strrep(num2str(now), '.', '_') '-' num2str(n) 'x' num2str(T) '-' strrep(num2str(s), '.', '_')];
save([runName '.mat'], 'phi', 'phiInit', 'phiCoh', 'minObj', 's', 'epsil', 'desErrs', 'randErrs', 'cohErrs');
