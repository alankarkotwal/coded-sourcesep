function [A, sens] = getAMatrix(phi, psi)

	n = size(phi, 1);
	ps = sqrt(n);
	T = size(phi, 2);

	sens = zeros(n, n*T);
	for t = 1:T
		thisPhi = phi(:, t);
		sens(:, (t-1)*n+1:t*n) = diag(thisPhi);
	end
	A = sens * psi;

end

