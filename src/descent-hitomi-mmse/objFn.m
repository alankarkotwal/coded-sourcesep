function out = objFn(A, thetas, psi, epsil)

	out = sum(generateErrs(A, thetas, psi, epsil, zeros(size(A, 1), size(thetas, 3)))) / numel(thetas);

end
