function errs = generateErrs(A, thetas, psi, epsil, noiseInsts)
	
	n = size(thetas, 1);
	T = size(thetas, 2);
	nTestSamp = size(thetas, 3);

	errs = zeros(nTestSamp, 1);
	parfor samp = 1:nTestSamp
		thisX = psi * reshape(thetas(:, :, samp), [n*T, 1]);
		thisY = A * reshape(thetas(:, :, samp), [n*T, 1]) + noiseInsts(:, samp);
		errs(samp) = norm(psi * reconstruct(thisY, A, epsil) - thisX)^2;
		%errs(samp) = (norm(reconstruct(y, A, epsil) - thisX) / ...
		%			  norm(thisX))^2;
	end

end
