function xs = generateSamples(n, T, nSamp, s)

	xs = zeros(n, T, nSamp);
	for samp = 1:nSamp
		for t = 1:T
			xs(:, t, samp) = sprand(n, 1, s);
		end
	end

end
