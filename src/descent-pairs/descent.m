function [A, AInit, obj] = descent(nMeas, n, nonneg, step, stepFactor, iter, epsil, repCountMax, minChange, AInit)

	if nargin == 2
		nonneg = 1;
		step = 0.1;
		stepFactor = 0.5;
		iter = Inf;
		epsil = 0;
		repCountMax = 5;
		minChange = 0.001;
	elseif nargin == 3
		step = 0.1;
		stepFactor = 0.5;
		iter = Inf;
		epsil = 0;
		repCountMax = 5;
		minChange = 0.001;
	elseif nargin == 4
		stepFactor = 0.5;
		iter = Inf;
		epsil = 0;
		repCountMax = 5;
		minChange = 0.001;
	elseif nargin == 5
		iter = Inf;
		epsil = 0;
		repCountMax = 5;
		minChange = 0.001;
	elseif nargin == 6
		epsil = 0;
		repCountMax = 5;
		minChange = 0.001;
	elseif nargin == 7
		repCountMax = 5;
		minChange = 0.001;
	elseif nargin == 8
		minChange = 0.001;
	end

	if nargin < 10 
		if nonneg == 1
			AInit = normc(rand(nMeas, n));
		else
			AInit = normc(randn(nMeas, n));
		end
	end

	A = normc(AInit);
	
	[obj, maxIndices] = objFn(A);

	disp([0.0 obj]);
	
	count = 0;
	repCount = 0;
	iTookAStep = 1;
	ascDir = zeros(nMeas, n);
	
	while count < iter
		
		if iTookAStep
			ascDir = objDer(A, maxIndices);
		end
		 
		ANew = A + step * ascDir;
		if nonneg == 1
			ANew(ANew < epsil) = epsil;
		end
		ANew = normc(ANew);
		
		[objNew, maxIndNew] = objFn(ANew);
		
		if objNew >= obj + minChange
			A = ANew;
			obj = objNew;
			maxIndices = maxIndNew;
			step = step / stepFactor;

			count = count + 1;
			repCount = 0;
			iTookAStep = 1;
		else
			step = step * stepFactor;
			repCount = repCount + 1;
			iTookAStep = 0;
			if repCount >= repCountMax
				break
			end
		end
	
		disp([round(count) obj maxIndices repCount]);

	end
	
end
