function out = objDer(A, ind)

	out = zeros(size(A));
	Ai = A(:, ind(1));
	Aj = A(:, ind(2));

	ii = norm(Ai);
	jj = norm(Aj);
	ij = dot(Ai, Aj);

	out(:, ind(1)) = 2 * (Ai - Aj * ij/jj);	
	out(:, ind(2)) = 2 * (Aj * ij^2 - Ai * ij/jj) / jj^2;

end
