function [minErr minIndices] = objFn(A)

	minErr = Inf;
	minIndices = [0 0];
	dots = A'*A;

	for i = 1:size(A, 2)
		for j = 1:i-1

			thisErr = dots(i, i) - dots(i, j)^2/dots(j, j);

			if thisErr < minErr
				minErr = thisErr;
				minIndices = [i j];
			end

		end
	end


end
