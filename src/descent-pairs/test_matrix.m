function errs = test_matrix(q, nSamp, xs, eps)

    % Testing matrix q for sparsity level k
    [~, n] = size(q); %#ok<ASGLU>
    
    errs = zeros(nSamp, 1);
    for i = 1:nSamp
        
        y = q * xs(:, i); %#ok<*NASGU>
        
        cvx_begin quiet
            variable z(n)
            minimize(norm(z, 1))
            subject to
                norm(y - q*z) <= eps %#ok<*NOPRT>
        cvx_end
        
        %errs(i) = sqrt(sum((z-xs(:, i)).^2) / sum(xs(:, i).^2));
        errs(i) = sqrt(max(abs(z-xs(:, i))) / max(abs(xs(:, i))));
        disp(['Testing: ' num2str(i)]);
    end
    
end
