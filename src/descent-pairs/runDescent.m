runName = input('Enter runName: ', 's');

nMeas = 50;
n = 100;

nStarts = 1000;

nonneg = 1;
step = 0.1;
stepFactor = 0.25;
iter = Inf;
repCountMax = 10;
epsil = 0;
minChange = 0.0001;

As = zeros(nMeas, n, nStarts);
objs = zeros(nStarts, 1);

if nonneg
	AInits = rand(nMeas, n, nStarts);
else
	AInits = randn(nMeas, n, nStarts);
end

parfor i = 1:nStarts
	
	disp(strcat('***** Start ', num2str(i), ' *****'));
	[thisA, ~, thisObj] = descent(nMeas, n, nonneg, step, stepFactor, iter, epsil, repCountMax, minChange, AInits(:, :, i));

	As(:, :, i) = thisA;
	objs(i) = thisObj;

end

[obj, ind] = max(objs);
A = As(:, :, ind);

disp('The objective value is ');
disp(num2str(obj));

nowTime = now;
fileID = fopen(strcat('../../results/descent-pairs/', num2str(nowTime), '-', num2str(nMeas), 'x', num2str(n), '-', num2str(nonneg), '-', num2str(nStarts), '-', runName, '-output.txt'), 'w');
fprintf(fileID, '%f \n', obj);
fprintf(fileID, strcat('A:\n'));
fprintf(fileID, '%f \n', A(:));
fclose(fileID);

save(strcat('../../results/descent-pairs/', num2str(nowTime), '-', num2str(nMeas), 'x', num2str(n), '-', num2str(nonneg), '-', num2str(nStarts), '-', runName, '.mat'), 'A', 'obj');
