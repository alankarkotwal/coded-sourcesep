function xs = reconstructOrig(y, A, D, epsilon)
	
	n = size(y, 1);
	T = size(A, 2) / n;
	K = size(y, 2);

	xs = zeros(n, T, K);
	parfor k = 1:K
		disp(['Solving original problem for k = ' num2str(k)])
		xs(:, :, k) = solveBP(y(:, k), A, D, epsilon);
	end

end
