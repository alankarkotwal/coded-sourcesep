nSamp = 1000;

spars = zeros(nSamp, 1);
sparsOrig = zeros(nSamp, 1);

for i = 1:nSamp

	runReconstruct;
	sparsOrig(i) = norm(theta(:), 1)/(K*n*T);
	spars(i) = norm(thetaRec(:), 1)/(K*n*T);

end

save('spars.mat', 'sparsOrig', 'spars');
