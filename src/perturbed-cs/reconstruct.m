function [xsOpt, deltaOpt] = reconstruct(y, phi, psi, D, perms, r, epsilon, nStarts, nIter, minChange)

	% Sizes
	n = size(phi, 1);
	ps = sqrt(n);
	T = size(perms, 1);
	K = size(y, 2);

	% Utility variables
	permsResp = zeros(n, T);
	for t = 1:T
		permsResp(:, t) = reshape(circshift(reshape((1:n)', [ps, ps]), perms(t, :)), [n 1]);
	end

	% Get ideal (specified) sensing matrix
	[ATrue, sensTrue] = getAMatrix(phi, psi, perms);

	% Begin optimization to select delta leading to sparsest solution
	spars = Inf;
	deltaOpt = zeros(n, 1);
	xsOpt = zeros(n, T, K);

	for start = 1:nStarts
		% Optimization variables
		delta = r * zeros(n, 1);
		deltaOld = delta;

		xs = zeros(n, T, K);
		xsOld = xs;

		% Alternately optimize delta and xs
		iter = 0;
		change = Inf;

		while iter < nIter && change > minChange

			% Get the current sensing matrix
			AEst = getAMatrix(phi + delta, psi, perms);

			% Solve BP individually for all k
			parfor k = 1:K
				xs(:, :, k) = solveBP(y(:, k), AEst, D, epsilon);
			end

			% Get linear system for solving for delta 
			idealYs = sensTrue * reshape(xs, [n*T K]);
			rhs = y(:) - idealYs(:);
			coeffMat = zeros(n, n, K);

			for k = 1:K
				for beta = 1:n
					for t = 1:T
						coeffMat(beta, permsResp(beta, t), k) = ...
						coeffMat(beta, permsResp(beta, t), k) + xs(beta, t, k);
					end
				end
			end
			coeffMat = reshape(permute(coeffMat, [1 3 2]), [n*K, n]);

			% Solve for delta 
			delta = coeffMat \ rhs;
			delta(delta > r/2) = r/2;
			delta(delta < -r/2) = -r/2;
			
			% Convergence and iteration updates
			change = norm(xs(:) - xsOld(:)) / norm(xsOld(:));
			deltaOld = delta;
			xsOld = xs;
			iter = iter + 1;
			disp('*****');
			disp([start iter change]);

		end

		thetaRec = psi' * reshape(xs, [n*T K]);
		thisSpars = norm(thetaRec(:), 1)/(K*n*T);
		if thisSpars < spars
			spars = thisSpars;
			deltaOpt = delta;
			xsOpt = xs;
		end
		
	end
end
