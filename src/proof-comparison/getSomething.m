function out = getSomething(hs, h0s, nzIndices, A)

	out = zeros(size(hs, 2), 1);
	for i = 1:size(hs, 2)
		
		nonZeros = nzIndices(:, i);
		X = find(nzIndices(:, i) == 1);
		XC = find(nzIndices(:, i) == 0);
		for j = 1:size(X, 1)
			for k = 1:size(XC, 1)
				out(i) = out(i) + h0s(X(j), i) * hs(XC(k), i) * A(:, X(j))' * A(:, XC(k));
			end
		end
	end

end
