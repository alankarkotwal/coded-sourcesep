function runGenerateComparisonCacti(T)

	%clear; clc; close all;
	
	nSamp = 100;
	ps = 8;
	%T = 2;
	epsilon = 1e-5;
	
	n = ps^2;
	D = kron(dctmtx(ps)', dctmtx(ps)');
	psi = kron(eye(T), D);
	
	phi = rand(n, T);
	sens = zeros(n, n*T);
	for t = 1:T
		sens(:, (t-1)*n+1:t*n) = diag(phi(:, t));
	end
	A = normc(sens * psi);
	
	cohA = getCoherence(A);
	nx = floor((1+1/cohA)/2);
	
	xs = zeros(n*T, nSamp);
	ys = zeros(n, nSamp);
	xOuts = zeros(n*T, nSamp);
	parfor samp = 1:nSamp
	    disp(['Working on sample ' num2str(samp)]);
	    xs(:, samp) = sprand(n*T, 1, nx/(n*T));
		ys(:, samp) = A * xs(:, samp) + 2 * epsilon / sqrt(n) * (rand(n, 1) - 0.5);;
	    xOuts(:, samp) = reconstruct(ys(:, samp), A, epsilon);
	end
	
	[diffs4n4, diffs4n5, diffs4n6, diffs4n7, diffs4n9, diffs10n10, diffs10n11, diffs10n13, diffsGnG1, diffsGnG2, finBound, finBoundRel] = generateComparison(A, xs, xOuts, epsilon);
	
	runName = ['../../results/proof-comparison/' strrep(num2str(now), '.', '_') '-' num2str(n) 'x' num2str(T) '-' num2str(nSamp) '-hitomi'];
	
	%saveBoxplot(diffs4n4, 'diffs4n4', [runName '-diffs4n4.fig']);
	%saveBoxplot(diffs4n5, 'diffs4n5', [runName '-diffs4n5.fig']);
	%saveBoxplot(diffs4n6, 'diffs4n6', [runName '-diffs4n6.fig']);
	%saveBoxplot(diffs4n7, 'diffs4n7', [runName '-diffs4n7.fig']);
	%saveBoxplot(diffs4n9, 'diffs4n9', [runName '-diffs4n9.fig']);
	%saveBoxplot(diffs10n10, 'diffs10n10', [runName '-diffs10n10.fig']);
	%saveBoxplot(diffs10n11, 'diffs10n11', [runName '-diffs10n11.fig']);
	%saveBoxplot(diffs10n13, 'diffs10n13', [runName '-diffs10n13.fig']);
	%saveBoxplot(diffsGnG1, 'diffsGnG1', [runName '-diffsGnG1.fig']);
	%saveBoxplot(diffsGnG1, 'diffsGnG2', [runName '-diffsGnG2.fig']);
	
	save([runName '.mat'], 'A', 'diffs4n4', 'diffs4n5', 'diffs4n6', 'diffs4n7', 'diffs4n9', 'diffs10n10', 'diffs10n11', 'diffs10n13', 'diffsGnG1', 'diffsGnG2', 'finBound', 'finBoundRel');

end
