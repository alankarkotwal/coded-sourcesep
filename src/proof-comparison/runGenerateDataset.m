clc; clear; close all;

nSamp = 1000;
nMeas = 10;
n = 20;
s = 0.1;
epsilon = 1e-5;

A = randn(nMeas, n);

generateDataset(A, s, nSamp, epsilon);
