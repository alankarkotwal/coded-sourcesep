function out = reconstruct(y, A, epsilon)

	cvx_begin quiet
		variable out(size(A, 2))
		minimize(norm(out, 1))
		subject to
			norm(y - A * out) <= epsilon
	cvx_end

end
