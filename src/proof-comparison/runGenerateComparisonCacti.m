function runGenerateComparisonCacti(T, avg)

	%clear; clc; close all;
	phi = [];
	perms = [];
	
	nSamp = 100;
	ps = 8;
	%T = 6;
	epsilon = 1e-5;
	%perms = zeros(T, 2);
	%for t = 1:T
	%	perms(t, :) = [randi(8) randi(8)];
	%end
	%disp('Perms:');
	%disp(perms);
	%
	%keyboard;
	if avg
		if T == 2
			load ../../results/descent-cacti-avg/736774_7153-64-2-10000.mat phi perms;
		elseif T == 4
			load ../../results/descent-cacti-avg/736774_801-64-4-10000.mat phi perms;
		elseif T == 6
			load ../../results/descent-cacti-avg/736775_2832-64-6-10000.mat phi perms;
		end
	else
		if T == 2
			load ../../results/descent-cacti/736774_7098-64-2-10000 phi perms;
		elseif T == 4
			load ../../results/descent-cacti/736774_7105-64-4-10000.mat phi perms;
		elseif T == 6
			load ../../results/descent-cacti/736774_7124-64-6-10000.mat phi perms;
		end
	end
	%if T == 2
	%	perms = [3, 1; 5, 5];
	%elseif T == 3
	%	perms = [7, 7; 4, 4; 7, 3];
	%elseif T == 4
	%	perms = [1, 2; 6, 6; 7, 3; 2, 7];
	%elseif T == 5
	%	perms = [7, 6; 4, 8; 4, 2; 6, 5; 2, 3];
	%elseif T == 6
	%	perms = [1, 8; 8, 7; 6, 1; 4, 6; 7, 8; 5, 3];
	%end
	
	n = ps^2;
	D = kron(dctmtx(ps)', dctmtx(ps)');
	psi = kron(eye(T), D);
	
	%phi = rand(n, 1);
	sens = zeros(n, n*T);
	for t = 1:T
		sens(:, (t-1)*n+1:t*n) = diag(reshape(circshift(reshape(phi, [ps, ps]), perms(t, :)), [n 1]));
	end
	A = normc(sens * psi);
	
	cohA = getCoherence(A);
	nx = floor((1+1/cohA)/2);
	
	xs = zeros(n*T, nSamp);
	ys = zeros(n, nSamp);
	xOuts = zeros(n*T, nSamp);
	parfor samp = 1:nSamp
	    disp(['Working on sample ' num2str(samp)]);
	    xs(:, samp) = sprand(n*T, 1, nx/(n*T));
		ys(:, samp) = A * xs(:, samp) + 2 * epsilon / sqrt(n) * (rand(n, 1) - 0.5);;
	    xOuts(:, samp) = reconstruct(ys(:, samp), A, epsilon);
	end
	
	[diffs4n4, diffs4n5, diffs4n6, diffs4n7, diffs4n9, diffs10n10, diffs10n11, diffs10n13, diffsGnG1, diffsGnG2, finBound, finBoundRel] = generateComparison(A, xs, xOuts, epsilon);
	
	if avg
		runName = ['../../results/proof-comparison/' strrep(num2str(now), '.', '_') '-' num2str(n) 'x' num2str(T) '-' num2str(nSamp) '-cacti-des-avg-randn-vec-fin'];
	else
		runName = ['../../results/proof-comparison/' strrep(num2str(now), '.', '_') '-' num2str(n) 'x' num2str(T) '-' num2str(nSamp) '-cacti-des-randn-vec-fin'];
	end
	
	%saveBoxplot(diffs4n4, 'diffs4n4', [runName '-diffs4n4.fig']);
	%saveBoxplot(diffs4n5, 'diffs4n5', [runName '-diffs4n5.fig']);
	%saveBoxplot(diffs4n6, 'diffs4n6', [runName '-diffs4n6.fig']);
	%saveBoxplot(diffs4n7, 'diffs4n7', [runName '-diffs4n7.fig']);
	%saveBoxplot(diffs4n9, 'diffs4n9', [runName '-diffs4n9.fig']);
	%saveBoxplot(diffs10n10, 'diffs10n10', [runName '-diffs10n10.fig']);
	%saveBoxplot(diffs10n11, 'diffs10n11', [runName '-diffs10n11.fig']);
	%saveBoxplot(diffs10n13, 'diffs10n13', [runName '-diffs10n13.fig']);
	%saveBoxplot(diffsGnG1, 'diffsGnG1', [runName '-diffsGnG1.fig']);
	%saveBoxplot(diffsGnG1, 'diffsGnG2', [runName '-diffsGnG2.fig']);
	
	save([runName '.mat'], 'A', 'diffs4n4', 'diffs4n5', 'diffs4n6', 'diffs4n7', 'diffs4n9', 'diffs10n10', 'diffs10n11', 'diffs10n13', 'diffsGnG1', 'diffsGnG2', 'finBound', 'finBoundRel', 'perms');

end
