function generateDataset(A, s, nSamp, epsilon)
	
	[nMeas, n] = size(A);

	xs = zeros(n, nSamp);
	ys = zeros(nMeas, nSamp);
	xOuts = zeros(n, nSamp);
	parfor samp = 1:nSamp
		disp(['Working on sample ' num2str(samp)]);
		xs(:, samp) = sprand(n, 1, s);
		ys(:, samp) = A * xs(:, samp);
		xOuts(:, samp) = reconstruct(ys(:, samp), A, epsilon);
	end

	runName = ['../../results/proof-comparison/' strrep(num2str(now), '.', '_') '-' num2str(nMeas) '-' num2str(n) '-' num2str(s) '-' num2str(nSamp)]; 
	save([runName '-dataset.mat'], 'A', 'xs', 'ys', 'xOuts');

end
