function [diffs4n4, diffs4n5, diffs4n6, diffs4n7, diffs4n9, diffs10n10, diffs10n11, diffs10n13, diffsGnG1, diffsGnG2, finBound, finBoundRel] = generateComparison(A, xs, xOuts, epsilon)

	hs = xOuts - xs;
	h0s = hs;
	h0s(xs == 0) = 0;

	terms4Vals = terms4(A, hs, h0s);
	diffs4n4 = (terms4Vals - terms4(A, h0s, h0s) + terms4(A, hs - h0s, h0s)) ./ terms4Vals;

	cohA = getCoherence(A);
	normH0s = norms(h0s)';
	normH0s1 = norms(h0s, 1)';
	nzIndices = (xs ~= 0);
	nonZeros = sum(nzIndices)';
	rhs1 = (1 - cohA * (nonZeros - 1));
	rhs2 = (1 - cohA * (2 * nonZeros - 1));
    
	diffs4n5 = (terms4Vals - rhs1 .* (normH0s .^ 2) + getSomething(hs, h0s, nzIndices, A)) ./ terms4Vals;

	diffs4n6 = (terms4Vals - rhs1 .* (normH0s .^ 2) + cohA * normH0s1 .* norms(hs - h0s, 1)') ./ terms4Vals;

	diffs4n7 = (terms4Vals - rhs1 .* (normH0s .^ 2) + cohA * normH0s1 .^ 2) ./ terms4Vals;

	diffs4n9 = (terms4Vals - rhs2 .* (normH0s .^ 2)) ./ terms4Vals;

	diffs10n10 = (terms4Vals ./ (rhs2 .* normH0s) - normH0s) ./ normH0s;

	diffs10n11 = ((norms(A * hs) .* norms(A * h0s))' ./ (rhs2 .* normH0s) - normH0s) ./ normH0s;

	diffs10n13 = (2 * epsilon * sqrt(1 + cohA * (nonZeros - 1)) ./ rhs2 - normH0s) ./ normH0s;

	normAh0s = norms(A * h0s)';
	diffsGnG1 = (normAh0s .^ 2 - (1 - cohA * (nonZeros - 1)) .* (normH0s .^ 2)) ./ normAh0s .^ 2;
	diffsGnG2 = ((1 + cohA * (nonZeros - 1)) .* (normH0s .^ 2) - normAh0s .^ 2) ./ normAh0s .^ 2;

	finBoundTmp = epsilon * (1 - cohA * (2 * nonZeros - 1) + 2 * (sqrt(cohA * nonZeros)) .* ...
														  sqrt(1 + cohA * (nonZeros - 1))) ./ ...
						 (sqrt(1 + cohA) * (1 - cohA * (2 * nonZeros - 1)));
	finBound = finBoundTmp - norms(hs)';
	finBoundRel = finBound ./ norms(hs)';

end
