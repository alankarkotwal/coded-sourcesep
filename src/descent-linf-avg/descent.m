function [Q, QInit, obj] = descent(nMeas, n, s, pos, step, stepFactor, iter, epsil, repCountMax, minChange)

    if nargin == 3
		step = 0.1;
		stepFactor = 0.5;
		iter = Inf;
		epsil = 0;
		repCountMax = 5;
        minChange = 0;
	elseif nargin == 4
		stepFactor = 0.5;
		iter = Inf;
		epsil = 0;
		repCountMax = 5;
        minChange = 0;
	elseif nargin == 5
		iter = Inf;
		epsil = 0;
		repCountMax = 5;
        minChange = 0;
	elseif nargin == 6
		epsil = 0;
		repCountMax = 5;
        minChange = 0;
	elseif nargin == 7
		repCountMax = 5;
        minChange = 0;
	elseif nargin == 8
        minChange = 0;
    end
    
	if pos
		Q = normc(rand(nMeas, n));
	else
    	Q = normc(randn(nMeas, n));
    end
	QInit = Q;
    
    [obj, terms, lambdas] = objFn(Q, 2*s);

    disp([0.0 0]);
	disp(obj);
    
    count = 0;
    repCount = 0;
    iTookAStep = 1;
    ascDir = zeros(nMeas, n);
    
    while count < iter
        
        if iTookAStep
			ascDir = objDer(Q, 2*s, terms, lambdas);
        end
        
        QNew = Q + step * ascDir;
		if pos
        	QNew(QNew < epsil) = epsil;
			QNew(QNew > 1) = 1;
		end
        QNew = normc(QNew);
        
        [objNew, termsNew, lambdasNew] = objFn(QNew, 2*s);
        
        if objNew >= obj * (1 + minChange)
			Q = QNew;
			obj = objNew;
			terms = termsNew;
			lambdas = lambdasNew;
			%step = step / stepFactor;

			count = count + 1;
			repCount = 0;
            iTookAStep = 1;
        else
			step = step * stepFactor;
			repCount = repCount + 1;
            iTookAStep = 0;
            if repCount >= repCountMax
				break;
            end
        end
	
		disp([count repCount]);
		disp(obj);

    end
    
end
