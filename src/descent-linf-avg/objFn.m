function [out, terms, lambdas] = objFn(q, s)

    [~, n] = size(q); %#ok<*ASGLU>
    
    lambdas = zeros(n - 1, n); 
	terms = zeros(n, 1);
	out = 0;
	
	for i = 1:n
       
        y = q(:, i);
        A = q(:, [1:i-1 i+1:n]);
        
        cvx_begin quiet
            variable thisLambda(n-1)
            minimize(norm(y - A*thisLambda))
            subject to
                norm(thisLambda, 1) <= s-1 %#ok<*NOPRT>
        cvx_end
        
        thisVal = norm(y - A*thisLambda);
		lambdas(:, i) = thisLambda;
		terms(i) = thisVal;
		out = out + thisVal ^ 2;
        
    end

end
