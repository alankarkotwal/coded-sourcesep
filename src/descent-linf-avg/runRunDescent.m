clc; clear; close all;

n = 100;
nMeass = 25:15:85;
sparss = 0.01:0.02:0.1;
poss = 0:1:1;

nStarts = 12;
nSamp = 1000;

for nMeas = 1:size(nMeass, 1)
	for spars = sparss
		for pos = poss
	
			disp([nMeass(nMeas) spars pos]);
			runDescent(nMeass(nMeas), n, spars, pos, nStarts, nSamp);

		end
	end
end
