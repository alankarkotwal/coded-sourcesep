function out = objDer(Q, s, terms, lambdas)

	[nMeas, n] = size(Q);

	out = zeros(nMeas, n);
	for i = 1:n

		redQ = Q(:, [1:i-1 i+1:n]);
		colQ = Q(:, i);
		for k = 1:i-1
			out(:, k) = out(:, k) + 4 * terms(i) * lambdas(k, i) * (redQ * lambdas(:, i) - colQ);
		end
		for k = i+1:n
			out(:, k) = out(:, k) + 4 * terms(i) * lambdas(k-1, i) * (redQ * lambdas(:, i) - colQ);
		end
		out(:, i) = out(:, i) + 4 * terms(i) * (colQ - redQ * lambdas(:, i));
		
 	end

end
