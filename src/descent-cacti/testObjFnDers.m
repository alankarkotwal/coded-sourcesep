ps = 8;
n = ps^2;
D = kron(dctmtx(ps)', dctmtx(ps)');
phi = randn(n, 1);
perms = [0 0; 1 2; 2 6]; 


delta = 0.0001;
alpha = ceil(n * rand - 1) + 1;

[objOrig, sgn, muMax, nuMax, betaMax, gammaMax] = objFn(phi, D, perms);

phiNew = phi;
phiNew(alpha) = phiNew(alpha) + delta;
objFin = objFn(phiNew, D, perms);

calcDer = objDer(sgn, muMax, nuMax, betaMax, gammaMax, phi, D, perms);

digDer = (objFin - objOrig) / delta;
disp(abs(digDer - calcDer(alpha))/abs(digDer));
