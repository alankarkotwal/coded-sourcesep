function out = reconstruct(y, phi, D, perms, epsilon)

	T = size(perms, 1);
	n = size(phi, 1);
	ps = sqrt(n);

	sens = zeros(n, n*T);
	psi = zeros(n*T, n*T);
	for t = 1:T
		thisPhi = reshape(circshift(reshape(phi, [ps, ps]), perms(t, :)), [n 1]);
		sens(:, (t-1)*n+1:t*n) = diag(thisPhi);
		psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
	end
	A = sens * psi;

	cvx_begin quiet
		variable theta(n*T)
		minimize(norm(theta, 1))
		subject to
			norm(y - A * theta) <= epsilon
	cvx_end

	theta = reshape(theta, [n, T]);
	out = D * theta;

end
