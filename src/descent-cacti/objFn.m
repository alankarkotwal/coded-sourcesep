function [coh, sgn, muMax, nuMax, betaMax, gammaMax] = objFn(phi, D, perms)

	T = size(perms, 1);
	n = size(phi, 1);
	ps = sqrt(n);

	sens = zeros(n, n*T);
	psi = zeros(n*T, n*T);
	for t = 1:T
		thisPhi = reshape(circshift(reshape(phi, [ps, ps]), perms(t, :)), [n 1]);
		sens(:, (t-1)*n+1:t*n) = diag(thisPhi);
		psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
	end
	A = sens * psi;
	normA = A ./ repmat(sqrt(sum(A.^2)), size(A, 1), 1);
	dots = normA'*normA - eye(n*T, n*T);
	dotsAbs = abs(dots);

	[colMax, colMaxIdx] = max(dotsAbs);
	[rowMax, rowMaxIdx] = max(colMax);
	maxIdx = [rowMaxIdx colMaxIdx(rowMaxIdx)];

	coh = abs(rowMax);
	sgn = sign(dots(maxIdx(1), maxIdx(2)));

	muMax = ceil(maxIdx(1) / n);
	nuMax = ceil(maxIdx(2) / n);
	betaMax = maxIdx(1) - n * (muMax-1);
	gammaMax = maxIdx(2) - n * (nuMax-1);

end
