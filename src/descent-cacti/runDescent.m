ps = 8;
T = 6;
n = ps^2;
D = kron(dctmtx(ps)', dctmtx(ps)');

perms = zeros(T, 2);
for t = 1:T
	perms(t, :) = [randi(8) randi(8)];
end
disp('Perms:');
disp(perms);

nStarts = 10000;

step = 10;
stepFactor = 0.5;
iter = 50;
repCountMax = 8;
eps = 0;
minChange = 0.001;

runName = ['../../results/descent-cacti/' strrep(num2str(now), '.', '_') '-' num2str(n) '-' num2str(T) '-' num2str(nStarts)];

phis = zeros(n, nStarts);
origPhis = zeros(n, nStarts);
objs = zeros(nStarts, 1);
cohDecs = zeros(iter, nStarts);

parfor i = 1:nStarts
	
	disp(strcat('***** Start ', num2str(i), ' *****'));
	[thisPhi, thisObj, thisOrigPhi, thisDecs] = descent(perms, D, step, stepFactor, iter, eps, repCountMax, minChange);

	phis(:, i) = thisPhi;
	origPhis(:, i) = thisOrigPhi;
	objs(i) = thisObj;
	cohDecs(:, i) = thisDecs;

end

[obj, pos] = min(objs);
phi = phis(:, pos);
origPhi = origPhis(:, pos);

disp('The minimum coherence value is ');
disp(num2str(obj));

save([runName '.mat'], 'phi', 'obj', 'origPhi', 'perms', 'D');
