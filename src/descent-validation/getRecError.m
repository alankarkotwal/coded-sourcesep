function out = getRecError(s, phi, D, nIter, lambda)

	errs = zeros(nIter, 1);

	n = size(phi, 1);
	T = size(phi, 2);

	parfor i = 1:nIter
		
		% Generate T random s-sparse signals.
		coeffs = zeros(n, T);
		meas = zeros(n, 1);

		for j = 1:T
			coeffs(:, j) = full(sprand(n, 1, s));

			% Get the measurement using phi and D.
			meas = meas + phi(:, j) .* (D * coeffs(:, j)); 
		end

		% Solve the compressed sensing problem.
		A = DictMatrix(sqrt(n), sqrt(n), T, phi);
		[cs, status] = l1_ls(A, A', n, n*T, meas, lambda);
		coeffsOut = reshape(cs, n, T);

		inImages = zeros(n, T);
		outImages = zeros(n, T);

		% Get errors.
		for j = 1:T
			inImages(:, j) = D * coeffs(:, j);
			outImages(:, j) = D * coeffsOut(:, j);
		end

		errs(i) = sum(sum((inImages - outImages).^2))/sum(sum(inImages.^2));
		
	end

	out = mean(errs);

end
