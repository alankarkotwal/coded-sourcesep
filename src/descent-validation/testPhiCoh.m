load phiOpt;

D = kron(dctmtx(sqrt(n))', dctmtx(sqrt(n))');

disp('Coherence of optimized phi');
disp(getCoherence(phi, D));

phi = rand(n, T);

disp('Coherence of random phi');
disp(getCoherence(phi, D));
