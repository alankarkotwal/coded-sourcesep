close all;
clear;

runName = input('Enter runName: ', 's');

load phiOpt;

spars = (0.01:0.01:0.5)';
nIter = 50;
lambda = 0.001;

n = size(phi, 1);
T = size(phi, 2);
phiRand = rand(n, T);
D = kron(dctmtx(8)', dctmtx(8)');

errs = zeros(size(spars, 1), 2);

for i = 1:size(spars, 1)

	errs(i, 1) = getRecError(spars(i), phi, D, nIter, lambda);
	errs(i, 2) = getRecError(spars(i), phiRand, D, nIter, lambda);

end

cohOpt = getCoherence(phi, D);
cohRand = getCoherence(phiRand, D);

f = figure;
plot(spars, errs(:, 1)); hold on; plot(spars, errs(:, 2)); hold off;
legend('Optimized', 'Random');
xlabel('Signal sparsity');
ylabel('Reconstruction error');
title('Sparsity testing for optimized matrices');

nowTime = now;
saveas(f, strcat('../../results/descent/validation/', num2str(nowTime), '-', runName, '-errs.png'));

close all;
save(strcat('../../results/descent/validation/', num2str(nowTime), '-', runName, '.mat'))
