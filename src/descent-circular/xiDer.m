function out = xiDer(mu, nu, beta, gamma, delta, epsilon, phi, D, xis, xiDerSums)

    out = 0;

    if mu == delta
        out = out + xiDerSums(nu, gamma) * ...
                    D(epsilon, beta)^2 * phi(epsilon, mu);
    end

    if nu == delta
        out = out + xiDerSums(mu, beta) * ...
                    D(epsilon, gamma)^2 * phi(epsilon, nu);
    end
    
    out = out / xis(mu, nu, beta, gamma);

end
