Optimizing Coherence Directly
=============================

This is a set of routines that optimize a Hitomi-style sensing matrix directly
for minimum coherence in all its circular permutations, assuming a dictionary D. 
