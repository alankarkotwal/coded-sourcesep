function [phi, obj] = descent(T, D, theta, step, stepFactor, iter, eps, repCountMax, minChange)

    n = size(D, 1);

    if nargin == 3
        step = 0.1;
        stepFactor = 0.5;
        iter = 100;
        eps = 0.5;
        repCountMax = 5;
        minChange = 0.0001;
    elseif nargin == 4
        stepFactor = 0.5;
        iter = 100;
        eps = 0.5;
        repCountMax = 5;
        minChange = 0.0001;
    elseif nargin == 5
        iter = 100;
        eps = 0.5;
        repCountMax = 5;
        minChange = 0.0001;
    elseif nargin == 6
        eps = 0.5;
        repCountMax = 5;
        minChange = 0.0001;
    elseif nargin == 7
        repCountMax = 5;
        minChange = 0.0001;
    elseif nargin == 8
        minChange = 0.0001;
    end

    phi = rand(n, T);
    [chis, xis, xiDerSums] = calcChiXi(phi, D);
    objOld = objFn(chis, xis, theta);
    objAct = objFnAct(phi, D);

    disp([0.0 sqrt(objOld) objAct]);

    count = 0;
    repCount = 0;
    iTookAStep = 1;
    descDir = zeros(n, T);
    
    while count < iter
    
        if iTookAStep
            for delta = 1:T
                for epsilon = 1:n
                    descDir(epsilon, delta) = objDer(delta, epsilon, chis, xis, xiDerSums, theta, phi, D);
                end
            end
        end
        
        phiNew = phi - step * descDir;
        phiNew(phiNew < eps) = eps;

        [chisNew, xisNew, xiDerSumsNew] = calcChiXi(phiNew, D);
        objNew = objFn(chisNew, xisNew, theta);

        if objNew < objOld - minChange
            phi = phiNew;
            objOld = objNew;
            objAct = objFnAct(phi, D);
            %step = step / stepFactor;
            chis = chisNew;
            xis = xisNew;
            xiDerSums = xiDerSumsNew;

            count = count + 1;
            repCount = 0;
            iTookAStep = 1;
        else
            step = step * stepFactor;
            repCount = repCount + 1;
            iTookAStep = 0;
            if repCount >= repCountMax
                break
            end
        end
    
        disp([round(count) sqrt(objOld) objAct]);
    
    end
    
    obj = objOld;

end
