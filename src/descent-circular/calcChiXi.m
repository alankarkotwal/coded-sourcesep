function [chis, xis, xiDerSums] = calcChiXi(phi, D)

    n = size(phi, 1);
    T = size(phi, 2);

    chis = zeros(T, T, n, n, sqrt(n), sqrt(n));
    xis = zeros(T, T, n, n, sqrt(n), sqrt(n));
    xiDerSums = zeros(T, n, sqrt(n), sqrt(n));
    
    for psi = 1:sqrt(n)
        for ups = 1:sqrt(n)

            thisPhi = zeros(n, T);
            for i = 1:T
                phiTemp = reshape(phi(:, i), sqrt(n), sqrt(n));
                phiTemp = circshift(phiTemp, [psi ups]);
                thisPhi(:, i) = reshape(phiTemp, n, 1);
            end

            for mu = 1:T
                for beta = 1:n
                    
                    xiDerSums(mu, beta, psi, ups) = xiDerSumTerm(thisPhi, D, mu, beta);
                    
                    for nu = 1:T
                        for gamma = 1:n
                    
                            chis(mu, nu, beta, gamma, psi, ups) = chi(mu, nu, beta, gamma, thisPhi, D);
                            xis(mu, nu, beta, gamma, psi, ups) = xi(mu, nu, beta, gamma, thisPhi, D);
                        
                        end
                    end

                end
            end
            
        end
    end
    
end
