function out = objDer(delta, epsilon, chis, xis, xiDerSums, theta, phi, D)

    if nargin == 4
        theta = 25;
    end

    n = size(chis, 3);
    T = size(chis, 1);
    nArr = (1:n)';

    out = 0;

    for psi = 1:sqrt(n)
        for ups = 1:sqrt(n)

            thisChi = chis(:, :, :, :, psi, ups);
            thisXi = xis(:, :, :, :, psi, ups);
            thisXiDerSum = xiDerSums(:, :, psi, ups);

            thisPhi = zeros(n, T);
            thisNArr = zeros(n, T);
            for i = 1:T
                phiTemp = reshape(phi(:, i), sqrt(n), sqrt(n));
                phiTemp = circshift(phiTemp, [psi ups]);
                thisPhi(:, i) = reshape(phiTemp, n, 1);
            end
            nArrTemp = reshape(nArr, sqrt(n), sqrt(n));
            nArrTemp = circshift(nArrTemp, [psi ups]);
            thisNArr = reshape(nArrTemp, n, 1);

            for mu = 1:T
                for nu = 1:(mu-1)
                    for beta = 1:n
                        for gamma = 1:n
                            temp = normDotProduct(mu, nu, beta, gamma, thisChi, thisXi);
                            out = out + exp(theta * temp^2) * ...
                                        2 * theta * temp * ...
                                        normDotProductDer(mu, nu, beta, gamma, delta, ...
                                                          find(thisNArr == epsilon), ...
                                                          thisChi, thisXi, thisXiDerSum, ...
                                                          thisPhi, D);
                        end
                    end
                end
            end

            for mu = 1:T
                for beta = 1:n
                    for gamma = 1:(beta-1)
                        temp = normDotProduct(mu, mu, beta, gamma, thisChi, thisXi);
                        out = out + exp(theta * temp^2) * ...
                                    2 * theta * temp * ...
                                    normDotProductDer(mu, mu, beta, gamma, delta, ... 
                                                      find(thisNArr == epsilon), ...
                                                      thisChi, thisXi, thisXiDerSum, ...
                                                      thisPhi, D);
                    end
                end
            end

        end
    end
    
    out = out / (theta * exp(theta * objFn(chis, xis, theta)));

end
