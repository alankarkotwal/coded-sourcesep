function out = objFn(chis, xis, theta)

    if nargin == 2
        theta = 25;
    end

    n = size(chis, 3);
    T = size(chis, 1);
    
    out = 0;

    for psi = 1:sqrt(n)
        for ups = 1:sqrt(n)

            thisChi = chis(:, :, :, :, psi, ups);
            thisXi = xis(:, :, :, :, psi, ups);

            for mu = 1:T
                for nu = 1:(mu-1)
                    for beta = 1:n
                        for gamma = 1:n
                            temp = normDotProduct(mu, nu, beta, gamma, thisChi, thisXi);
                            out = out + exp(theta * temp^2);
                        end
                    end
                end
            end

            for mu = 1:T
                for beta = 1:n
                    for gamma = 1:(beta-1)
                        temp = normDotProduct(mu, mu, beta, gamma, thisChi, thisXi);
                        out = out + exp(theta * temp^2);
                    end
                end
            end

        end
    end

    out = log(out) / theta;

end
