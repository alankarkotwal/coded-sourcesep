imSize1 = 8;
imSize2 = 8;
T = 2;
D = kron(dctmtx(imSize2)', dctmtx(imSize1)');
phi = rand(size(D, 1), T);
theta = 500;
n = imSize1 * imSize2;

[chis, xis] = calcChiXi(phi, D);
softObj = objFn(chis, xis, theta);

hardObj = 0;
for zeta = 1:imSize1
	for ups = 1:imSize2

		thisPhi = zeros(n, T);
		for i = 1:T
		    phiTemp = reshape(phi(:, i), sqrt(n), sqrt(n));
		    phiTemp = circshift(phiTemp, [zeta ups]);
		    thisPhi(:, i) = reshape(phiTemp, n, 1);
		end

		sens = zeros(n, n*T);
		psi = zeros(n*T, n*T);
		for t = 1:T
			sens(:, (t-1)*n+1:t*n) = diag(thisPhi(:, t));
			psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
		end
		A = sens * psi;
		normA = A ./ repmat(sqrt(sum(A.^2)), size(A, 1), 1);
		dots = normA'*normA;
		mu = max(max(abs(dots - eye(size(dots)))));
	
		if mu > hardObj
			hardObj = mu;
		end
	
	end
end

disp((sqrt(softObj)-mu)/mu);
