function out = objFnAct(phi, D)

    n = size(phi, 1);
    T = size(phi, 2);

    hardObj = 0;
    for zeta = 1:sqrt(n)
    	for ups = 1:sqrt(n)
    
    		thisPhi = zeros(n, T);
    		for i = 1:T
    		    phiTemp = reshape(phi(:, i), sqrt(n), sqrt(n));
    		    phiTemp = circshift(phiTemp, [zeta ups]);
    		    thisPhi(:, i) = reshape(phiTemp, n, 1);
    		end
    
    		sens = zeros(n, n*T);
    		psi = zeros(n*T, n*T);
    		for t = 1:T
    			sens(:, (t-1)*n+1:t*n) = diag(thisPhi(:, t));
    			psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
    		end
    		A = sens * psi;
    		normA = A ./ repmat(sqrt(sum(A.^2)), size(A, 1), 1);
    		dots = normA'*normA;
    		mu = max(max(abs(dots - eye(size(dots)))));
    	
    		if mu > hardObj
    			hardObj = mu;
    		end
    	
    	end
    end

    out = hardObj;

end
