% Source Separation-based Coded Video Recovery
% Alankar Kotwal <alankarkotwal13@gmail.com>
clear; close all; clc; tic;

runName = input('Enter runName: ', 's');

% Parameters
lambda = 0.001;
tol = 1e-3;

%im1 = im2double(imread('../../data/s1.png'));
%im2 = im2double(imread('../../data/s2.png'));
im1 = im2double(rgb2gray(imread('../../data/c1.png')));
im2 = im2double(rgb2gray(imread('../../data/c2.png')));
im3 = im2double(rgb2gray(imread('../../data/c3.png')));
im1 = imresize(im1(:, 120:1320), 1/4);
im2 = imresize(im2(:, 120:1320), 1/4); 
im3 = imresize(im3(:, 120:1320), 1/4); 
imSize = size(im1);
im1d = reshape(im1, imSize(1)*imSize(2), 1);
im2d = reshape(im2, imSize(1)*imSize(2), 1);
im3d = reshape(im3, imSize(1)*imSize(2), 1);

% Measurement matrix
phi1d = rand(imSize(1)*imSize(2), 1);
phi2d = rand(imSize(1)*imSize(2), 1);
phi3d = rand(imSize(1)*imSize(2), 1);
A = DictMatrix(imSize(1), imSize(2), 3, [phi1d phi2d phi3d]);
At = A';

% Get the measurement
meas = phi1d .* im1d + phi2d .* im2d + phi3d .* im3d;

% Add some noise
sigma = 0.0*mean(abs(meas));
meas = meas + 2*sigma*rand(size(meas)) - sigma;
eps = size(meas, 1)*sigma*sigma/3;

%% BP machao
[cs, status] = l1_ls(A, At, imSize(1)*imSize(2), imSize(1)*imSize(2)*3, meas, lambda, tol);

c1Opt = cs(1:imSize(1)*imSize(2));
c2Opt = cs(imSize(1)*imSize(2)+1:2*imSize(1)*imSize(2));
c3Opt = cs(2*imSize(1)*imSize(2)+1:end);
output1 = idct2(reshape(c1Opt, imSize));
output2 = idct2(reshape(c2Opt, imSize));
output3 = idct2(reshape(c3Opt, imSize));

%%
nowTime = now;
imwrite(im1/max(max(im1)), strcat('../../results/sourcesep/L1LS/', num2str(nowTime), '-', runName, '-ls1in.png'));
imwrite(im2/max(max(im2)), strcat('../../results/sourcesep/L1LS/', num2str(nowTime), '-', runName, '-ls2in.png'));
imwrite(im3/max(max(im3)), strcat('../../results/sourcesep/L1LS/', num2str(nowTime), '-', runName, '-ls3in.png'));
imwrite(output1/max(max(output1)), strcat('../../results/sourcesep/L1LS/', num2str(nowTime), '-', runName, '-ls1est.png'));
imwrite(output2/max(max(output2)), strcat('../../results/sourcesep/L1LS/', num2str(nowTime), '-', runName, '-ls2est.png'));
imwrite(output3/max(max(output3)), strcat('../../results/sourcesep/L1LS/', num2str(nowTime), '-', runName, '-ls3est.png'));
imwrite([im1 im2 im3 output1 output2 output3]/max(max([im1 im2 im3 output1 output2 output3])), strcat('../../results/sourcesep/L1LS/', num2str(nowTime), '-', runName, '-full.png'));

MSIE1 = sum(sum((output1-double(im1)).^2))/sum(sum(double(im1).^2));
MSIE2 = sum(sum((output2-double(im2)).^2))/sum(sum(double(im2).^2));
MSIE3 = sum(sum((output3-double(im3)).^2))/sum(sum(double(im3).^2));
disp([MSIE1 MSIE2 MSIE3]);

%phi = [phi1*D phi2*D];
%normPhi = phi ./ repmat(sqrt(sum(phi.^2)), size(phi, 1), 1);
%cohers = normPhi' * normPhi;
%mu = max(max(abs(cohers - eye(size(cohers)))));
mu = -1;

fileID = fopen(strcat('../../results/sourcesep/L1LS/', num2str(nowTime), '-', runName, '-errReflSep.txt'), 'w');
fprintf(fileID, '%f ', [MSIE1 MSIE2 MSIE3 mu]);
fclose(fileID);

save(strcat('../../results/sourcesep/L1LS/', num2str(nowTime), '-', runName, '.mat'))
