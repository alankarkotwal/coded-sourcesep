imSize1 = 8;
imSize2 = 7;

im1 = rand(imSize1, imSize2);
im2 = rand(imSize1, imSize2);
c1 = reshape(dct2(im1), imSize1*imSize2, 1);
c2 = reshape(dct2(im1), imSize1*imSize2, 1);
% c1 = rand(imSize1*imSize2, 1);
% c2 = rand(imSize1*imSize2, 1);
c = [c1; c2];
f = rand(imSize1*imSize2, 1);

phi1d = randn(imSize1*imSize2, 1);
phi2d = randn(imSize1*imSize2, 1);
phi1 = diag(phi1d);
phi2 = diag(phi2d);

% Conventional measurement
D = kron(dctmtx(imSize2)', dctmtx(imSize1)');
meas1 = [phi1*D phi2*D]*c;
meas3 = [phi1*D phi2*D]'*f;

% DictMatrix measurement
A = DictMatrix(imSize1, imSize2, 2, [phi1d phi2d]);
meas2 = A*c;
meas4 = A'*f;

disp(norm(meas1 - meas2));
disp(norm(meas3 - meas4));