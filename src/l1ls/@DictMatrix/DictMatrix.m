function res = DictMatrix(imSize1, imSize2, m, phi)

res.adjoint = 0;
res.imSize1 = imSize1;
res.imSize2 = imSize2;
res.m = m;
res.phi = phi;

res = class(res, 'DictMatrix');
