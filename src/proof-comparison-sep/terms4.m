function out = terms4(A, hs, h0s)
	
	out = zeros(size(hs, 2), 1);
	for i = 1:size(hs, 2)
		out(i) = abs(hs(:, i)' * (A)' * A * h0s(:, i));
	end

end
