function saveBoxplot(data, plotTitle, filename)

    scrsz = get(groot, 'ScreenSize');
    figure('Position', scrsz);
    set(gca, 'FontSize', 50);
    hold on;

    boxplot(data, 'symbol', '', 'widths', 0.4);

    title(plotTitle);
    set(gca, 'FontSize', 40);
    savefig(filename);
    disp(filename);

end