clear; clc; close all;

nSamp = 1000;
ps = 8;
epsilon = 1e-5;

n = ps^2;
D = kron(dctmtx(ps)', dctmtx(ps)');
psi = kron(eye(2), D);

phi = rand(n, 2);

A = diag(phi(:, 1)) * D;
B = diag(phi(:, 2)) * D;
D = [A B];
cohA = getCoherence(A);
cohB = getCoherence(B);
cohD = getCoherence(D);
cohM = max(max(abs(A' * B)));
cohMax = max(cohA, cohB);
nw = floor(max((1 + cohD)/(2 * cohD), 2 * (1 + cohMax) / (cohMax + 2 * cohD + sqrt(cohMax ^ 2 + cohM ^ 2))));
nwOld = floor((1 + cohD)/(2 * cohD));

if nw > nwOld
	disp('Success!');
end

%xs = zeros(n*T, nSamp);
%ys = zeros(n, nSamp);
%xOuts = zeros(n*T, nSamp);
%parfor samp = 1:nSamp
%    disp(['Working on sample ' num2str(samp)]);
%    xs(:, samp) = sprand(n*T, 1, nx/(n*T));
%    ys(:, samp) = A * xs(:, samp);
%    xOuts(:, samp) = reconstruct(ys(:, samp), A, epsilon);
%end
%
%[diffs4n4, diffs4n5, diffs4n6, diffs4n7, diffs4n9, diffs10n10, diffs10n11, diffs10n13, diffsGnG1, diffsGnG2] = generateComparison(A, xs, xOuts, epsilon);
%
%runName = ['../../results/proof-comparison/' strrep(num2str(now), '.', '_') '-' num2str(n) 'x' num2str(T) '-' num2str(nSamp) '-hitomi'];
%
%saveBoxplot(diffs4n4, 'diffs4n4', [runName '-diffs4n4.fig']);
%saveBoxplot(diffs4n5, 'diffs4n5', [runName '-diffs4n5.fig']);
%saveBoxplot(diffs4n6, 'diffs4n6', [runName '-diffs4n6.fig']);
%saveBoxplot(diffs4n7, 'diffs4n7', [runName '-diffs4n7.fig']);
%saveBoxplot(diffs4n9, 'diffs4n9', [runName '-diffs4n9.fig']);
%saveBoxplot(diffs10n10, 'diffs10n10', [runName '-diffs10n10.fig']);
%saveBoxplot(diffs10n11, 'diffs10n11', [runName '-diffs10n11.fig']);
%saveBoxplot(diffs10n13, 'diffs10n13', [runName '-diffs10n13.fig']);
%saveBoxplot(diffsGnG1, 'diffsGnG1', [runName '-diffsGnG1.fig']);
%saveBoxplot(diffsGnG1, 'diffsGnG2', [runName '-diffsGnG2.fig']);
