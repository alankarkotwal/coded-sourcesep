clear; clc; close all;

nSamp = 1000;
nMeas = 498;
n = 500;
epsilon = 1e-5;

A = normc(randn(nMeas/2, n));
B = normc(randn(nMeas/2, n));
D = [A B];
cohA = getCoherence(A);
cohB = getCoherence(B);
cohD = getCoherence(D);
cohM = max(max(abs(A' * B)));
cohMax = max(cohA, cohB);
nw = floor(max((1 + cohD)/(2 * cohD), 2 * (1 + cohMax) / (cohMax + 2 * cohD + sqrt(cohMax ^ 2 + cohM ^ 2))));
nwOld = floor((1 + cohD)/(2 * cohD));

if nw > nwOld
	disp('Success!');
end

%xs = zeros(n, nSamp);
%ys = zeros(nMeas, nSamp);
%xOuts = zeros(n, nSamp);
%parfor samp = 1:nSamp
%    disp(['Working on sample ' num2str(samp)]);
%    xs(:, samp) = sprand(n, 1, nx/n);
%    ys(:, samp) = A * xs(:, samp);
%    xOuts(:, samp) = reconstruct(ys(:, samp), A, epsilon);
%end
%
%[diffs4n4, diffs4n5, diffs4n6, diffs4n7, diffs4n9, diffs10n10, diffs10n11, diffs10n13, diffsGnG1, diffsGnG2] = generateComparison(A, xs, xOuts, epsilon);
%
%runName = ['../../results/proof-comparison/' strrep(num2str(now), '.', '_') '-' num2str(nMeas) 'x' num2str(n) '-' num2str(nSamp)];
%
%saveBoxplot(diffs4n4, 'diffs4n4', [runName '-diffs4n4.fig']);
%saveBoxplot(diffs4n5, 'diffs4n5', [runName '-diffs4n5.fig']);
%saveBoxplot(diffs4n6, 'diffs4n6', [runName '-diffs4n6.fig']);
%saveBoxplot(diffs4n7, 'diffs4n7', [runName '-diffs4n7.fig']);
%saveBoxplot(diffs4n9, 'diffs4n9', [runName '-diffs4n9.fig']);
%saveBoxplot(diffs10n10, 'diffs10n10', [runName '-diffs10n10.fig']);
%saveBoxplot(diffs10n11, 'diffs10n11', [runName '-diffs10n11.fig']);
%saveBoxplot(diffs10n13, 'diffs10n13', [runName '-diffs10n13.fig']);
%saveBoxplot(diffsGnG1, 'diffsGnG1', [runName '-diffsGnG1.fig']);
%saveBoxplot(diffsGnG1, 'diffsGnG2', [runName '-diffsGnG2.fig']);
