clear; close all; clc

ps = 8;
T = 2;
n = ps^2;
D = kron(dctmtx(ps)', dctmtx(ps)');
spacing = 0.5;
quantVals = [0:spacing:1]';

perms = zeros(T, 2);
for t = 1:T
	perms(t, :) = [randi(8) randi(8)];
end

phi = rand(n, 1);
quantPhi = quantizeMatrix(phi, quantVals);

obj = objFn(phi, D, perms);
objQuant = objFn(quantPhi, D, perms);

epsilon = 1e-5;
nSamp = 100;
spars = 0.016; %(0.05:0.05:0.3)';
scale = 1/32;

runName = ['../../results/cacti-quant/' strrep(num2str(now), '.', '_') '-' num2str(n) '-' num2str(T) '-' num2str(spacing) '-' num2str(nSamp) '-real']; 

imgSize = size(imresize(rgb2gray(imread('../../data/c1.png')), 1/scale));
inp = zeros([imgSize T]);
for t = 1:T
	inp(:, :, t) = imresize(im2double(rgb2gray(imread(['../../data/c' num2str(t) '.png']))), 1/scale);
end

runName = ['../../results/descent-cacti/' strrep(num2str(now), '.', '_') '-real-' num2str(n) '-' num2str(T)]; 

sens = zeros(n, n*T);
psi = zeros(n*T, n*T);
for t = 1:T
	thisPhi = reshape(circshift(reshape(phi, [ps, ps]), perms(t, :)), [n 1]);
	thisPhiQuant = reshape(circshift(reshape(quantPhi, [ps, ps]), perms(t, :)), [n 1]);
	sens(:, (t-1)*n+1:t*n) = diag(thisPhi);
	sensQuant(:, (t-1)*n+1:t*n) = diag(thisPhiQuant);
	psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
end
A = sens * psi;
AQuant = sensQuant * psi;

rrmse = zeros(size(inp(:, :, 1), 1)-ps+1, size(inp(:, :, 1), 2)-ps+1);
rrmseQuant = zeros(size(inp(:, :, 1), 1)-ps+1, size(inp(:, :, 1), 2)-ps+1);

for i = 1:size(inp(:, :, 1), 1)-ps+1
	for j = 1:size(inp(:, :, 1), 2)-ps+1
	
		disp([num2str(i) '/' num2str(size(inp(:, :, 1), 1)-ps+1) ', ' num2str(j) '/' num2str(size(inp(:, :, 1), 2)-ps+1)]);

		thisX = zeros(n*T, 1);
		for t = 1:T
			thisPatch = imresize(inp(i:i+ps-1, j:j+ps-1), [n 1]);
			thisX((T-1)*n+1:T*n) = thisPatch(:);
		end

		thisY = sens * thisX;
		thisYQuant = sensQuant * thisX;

		outXRec = reconstruct(thisY, phi, D, perms, epsilon);
		outXRecQuant = reconstruct(thisYQuant, quantPhi, D, perms, epsilon);

		rrmse(i, j) = sqrt(norm(outXRec(:) - thisX)^2 / norm(thisX)^2);
		rrmseQuant(i, j) = sqrt(norm(outXRecQuant(:) - thisX)^2 / norm(thisX)^2);
	end
end

scrsz = get(groot, 'ScreenSize');
figure('Position', scrsz);
set(gca, 'FontSize', 50);
hold on;

bp1 = boxplot([rrmse(:) rrmseQuant(:)], {'Original' 'Quantized'}, 'Positions', [1 5], 'symbol', '', 'widths', 0.4);
xlim auto;
ylim auto;

ylabel('RRMSE');
title(['RRMSE comparison: n = ' num2str(n) ', T = ' num2str(T)]);
set(gca, 'FontSize', 40);
savefig([runName '.fig']);
disp([runName '.fig']);
disp([obj objQuant]);
