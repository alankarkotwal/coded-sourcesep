clear; close all; clc

load ../../results/descent-cacti/736744.7304-64x2x100000-parallel.mat;
%load ../../results/descent-cacti/736746.2202-64x5x1000000-parallel.mat;

ps = sqrt(size(D, 1));
n = ps^2;
T = size(perms, 1);
D = kron(dctmtx(ps)', dctmtx(ps)');

runName = ['../../results/descent-cacti/' strrep(num2str(now), '.', '_') '-cohPlot-' num2str(n) '-' num2str(T)]; 

origPhi = rand(n, 1);

sens = zeros(n, n*T);
psi = zeros(n*T, n*T);
for t = 1:T
	thisPhi = reshape(circshift(reshape(phi, [ps, ps]), perms(t, :)), [n 1]);
	thisPhiOrig = reshape(circshift(reshape(origPhi, [ps, ps]), perms(t, :)), [n 1]);
	sens(:, (t-1)*n+1:t*n) = diag(thisPhi);
	sensOrig(:, (t-1)*n+1:t*n) = diag(thisPhiOrig);
	psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
end
A = normc(sens * psi);
AOrig = normc(sensOrig * psi);

gram = A' * A - eye(n*T);
gramOrig = AOrig' * AOrig - eye(n*T);

gram = gram - diag(diag(gram));
gramOrig = gramOrig - diag(diag(gramOrig));

gram(gram == 0) = [];
gramOrig(gramOrig == 0) = [];

scrsz = get(groot, 'ScreenSize');
figure('Position', scrsz);
set(gca, 'FontSize', 50);
hold on;

h = histogram(abs(gram));
title('Optimized matrix coherence distribution');
xlabel('Coherence bins');
ylabel('Number');
xlim([0 1]);
ylim([0 6000]);
savefig([runName '-optimized.fig']);

scrsz = get(groot, 'ScreenSize');
figure('Position', scrsz);
set(gca, 'FontSize', 50);
hold on;

hOrig = histogram(abs(gramOrig));
title('Non-optimized matrix coherence distribution');
xlabel('Coherence bins');
ylabel('Number');
xlim([0 1]);
ylim([0 6000]);
savefig([runName '-nonoptimized.fig']);
