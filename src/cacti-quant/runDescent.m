runName = input('Enter runName: ', 's');

ps = 8;
T = 2;
n = ps^2;
D = kron(dctmtx(ps)', dctmtx(ps)');

perms = zeros(T, 2);
for t = 1:T
	perms(t, :) = [randi(8) randi(8)];
end

nStarts = 1;

step = 10;
stepFactor = 0.5;
iter = 50;
repCountMax = 8;
eps = 0;
minChange = 0.001;

phis = zeros(n, nStarts);
origPhis = zeros(n, nStarts);
objs = zeros(nStarts, 1);
cohDecs = zeros(iter, nStarts);

parfor i = 1:nStarts
	
	disp(strcat('***** Start ', num2str(i), ' *****'));
	[thisPhi, thisObj, thisOrigPhi, thisDecs] = descent(perms, D, step, stepFactor, iter, eps, repCountMax, minChange);

	phis(:, i) = thisPhi;
	origPhis(:, i) = thisOrigPhi;
	objs(i) = thisObj;
	cohDecs(:, i) = thisDecs;

end

[obj, pos] = min(objs);
phi = phis(:, pos);
origPhi = origPhis(:, pos);

disp('The minimum coherence value is ');
disp(num2str(obj));

nowTime = now;
fileID = fopen(strcat('../../results/descent-cacti/', num2str(nowTime), '-', runName, '-output.txt'), 'w');
fprintf(fileID, '%f \n', obj);
fprintf(fileID, '%f \n', phi);
fclose(fileID);

save(strcat('../../results/descent-cacti/', num2str(nowTime), '-', runName, '.mat'), 'phi', 'obj', 'origPhi', 'perms', 'D');
