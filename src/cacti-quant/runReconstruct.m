function runRecontruct(type, spacing)
%clear; close all; clc

perms = [];
phi = [];
origPhi = [];
D = [];

if type == 2
	load ../../results/descent-cacti/736774_7098-64-2-10000.mat;
elseif type == 4
	load ../../results/descent-cacti/736774_7105-64-4-10000.mat;
elseif type == 6
	load ../../results/descent-cacti/736774_7124-64-6-10000.mat;
end

epsilon = 1e-5;
nSamp = 100;
spars = (0.01:0.02:0.14)';

ps = sqrt(size(D, 1));
n = ps^2;
T = size(perms, 1);

%spacing = 0.2;
quantVals = [0:spacing:1]';

quantPhi = quantizeMatrix(phi, quantVals);

obj = objFn(phi, D, perms);
objQuant = objFn(quantPhi, D, perms);

runName = ['../../results/cacti-quant/' strrep(num2str(now), '.', '_') '-' num2str(n) '-' num2str(T) '-' num2str(spacing) '-' num2str(nSamp)]; 

sens = zeros(n, n*T);
sensQuant = zeros(n, n*T);
psi = zeros(n*T, n*T);
for t = 1:T
	thisPhi = reshape(circshift(reshape(phi, [ps, ps]), perms(t, :)), [n 1]);
	thisPhiQuant = reshape(circshift(reshape(quantPhi, [ps, ps]), perms(t, :)), [n 1]);
	sens(:, (t-1)*n+1:t*n) = diag(thisPhi);
	sensQuant(:, (t-1)*n+1:t*n) = diag(thisPhiQuant);
	psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
end
A = sens * psi;
AQuant = sensQuant * psi;

rrmse = zeros(nSamp, size(spars, 1));
rrmseQuant = zeros(nSamp, size(spars, 1));

for spar = 1:size(spars, 1)
	parfor samp = 1:nSamp
	
		disp([spar samp]);

		theta = zeros(n, T);
		for t = 1:T
			theta(:, t) = sprand(n, 1, spars(spar));
		end
		theta = theta(:);

		y = A * theta;
		yQuant = AQuant * theta;

		y = y + 2 * epsilon / ps * (rand(n, 1) - 0.5);
		yQuant = yQuant + 2 * epsilon / ps * (rand(n, 1) - 0.5);
		
		inpFrames = reshape(psi * theta, [n T]);
		outFrames = reconstruct(y, phi, D, perms, epsilon);
		outFramesQuant = reconstruct(yQuant, quantPhi, D, perms, epsilon);
		
		rrmse(samp, spar) = norm(outFrames(:) - inpFrames(:)) / norm(inpFrames(:));
		rrmseQuant(samp, spar) = norm(outFramesQuant(:) - inpFrames(:)) / norm(inpFrames(:));
	end
end

%save(runName);

sparsVec = (1:size(spars, 1))';

scrsz = get(groot, 'ScreenSize');
figure('Position', scrsz);
set(gca, 'FontSize', 50);
hold on;

bp1 = boxplot(rrmse, spars, 'Positions', sparsVec+0.25, 'widths', 0.4);
bp2 = boxplot(rrmseQuant, spars, 'Positions', sparsVec-0.25, 'widths', 0.4);
xlim([0 size(spars, 1)+1]);
ylim auto;

color = [repmat('b',1,size(spars, 1)), repmat('r', 1, size(spars, 1))];
h = findobj(gca,'Tag','Box');
for j=1:length(h)
   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
end
c = get(gca, 'Children');
hleg1 = legend([c(1) c(size(spars, 1)+1)], 'Continuous', 'Quantized', 'Location', 'NorthEast');

xlabel('Sparsity');
ylabel('RRMSE');
title(['RRMSE comparison: n = ' num2str(n) ', T = ' num2str(T)]);
set(gca, 'FontSize', 40);
savefig([runName '-fin.fig']);
disp([runName '-fin.fig']);
disp([obj objQuant]);
