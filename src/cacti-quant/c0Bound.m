function c0 = c0Bound(mu, nx, epsilon)

	c0 = [0 1];
	if(nx > 0.5*(1+1/mu))
		c0(2) = 0;
	end

	num = 1 - mu * (2 * nx - 1) + 2 * sqrt(mu * nx) * ...
		  sqrt(1 + mu * (nx - 1));
	den = sqrt(1 + mu) * (1 - mu * (2 * nx - 1));

	c0(1) = epsilon * num / den;
end
