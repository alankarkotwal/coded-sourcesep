function out = quantizeMatrix(in, vals)

	out = vals(knnsearch(vals, in));

end
