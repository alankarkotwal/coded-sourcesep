function out = objDer(sgn, muMax, nuMax, betaMax, gammaMax, phi, D, perms)

	n = size(phi, 1);
	ps = sqrt(n);

	out = zeros(n, 1);

	phiMu = reshape(circshift(reshape(phi, [ps, ps]), perms(muMax, :)), [n 1]);
	phiNu = reshape(circshift(reshape(phi, [ps, ps]), perms(nuMax, :)), [n 1]);

	chi = sum((phiMu .* phiNu) .* ...
			  (D(:, betaMax) .* D(:, gammaMax)));
	xi = sqrt(sum(phiMu.^2 .* D(:, betaMax).^2) * ...
			  sum(phiNu.^2 .* D(:, gammaMax).^2));

	for epsilon=1:n
		
		alphaMu = ps * mod(ceil(epsilon / ps) + perms(muMax, 2) - 1, ps) + ...
				  mod(epsilon - ps * (ceil(epsilon / ps) - 1) + perms(muMax, 1) - 1, ps) + 1;
		alphaNu = ps * mod(ceil(epsilon / ps) + perms(nuMax, 2) - 1, ps) + ...
				  mod(epsilon - ps * (ceil(epsilon / ps) - 1) + perms(nuMax, 1) - 1, ps) + 1;

		chiDer = D(alphaMu, betaMax) * D(alphaMu, gammaMax) * phiNu(alphaMu) + ...
				 D(alphaNu, betaMax) * D(alphaNu, gammaMax) * phiMu(alphaNu);

		xiDer = 2 * D(alphaMu, betaMax)^2 * phiMu(alphaMu) * ...
				sum((phiNu .^ 2) .* (D(:, gammaMax) .^ 2)) + ...
				2 * D(alphaNu, betaMax)^2 * phiMu(alphaNu) * ...
				sum((phiMu .^ 2) .* (D(:, betaMax) .^ 2));

		out(epsilon) = (xi * chiDer - chi * xiDer) / xi ^ 2;

	end

	out = sgn * out;

end
