clear; clc; close all;

nSamp = 1000;
ks = (1:5:50)';
nK = size(ks, 1);
epsil = 1e-8;

posTrip = load('../../results/descent-triplets/50x100x1.mat', 'A');
norTrip = load('../../results/descent-triplets/50x100x0.mat', 'A');
posPair = load('../../results/descent-pairs/50x100x1.mat', 'A');
norPair = load('../../results/descent-pairs/50x100x0.mat', 'A');

[nMeas, n] = size(posTrip.A);

runName = ['../../results/descent-comparison/' strrep(num2str(now), '.', '_') '-' ...
      num2str(nMeas) 'x' num2str(n) 'x' num2str(nK) '-' num2str(nSamp)];

xs = zeros(n, nSamp, nK);

for j = 1:size(ks, 1)
	for i = 1:nSamp
    	xs(:, i, j) = sprand(n, 1, ks(j)/n);
	end
end

mats = zeros([nMeas n 4]);
mats(:, :, 1) = posTrip.A;
mats(:, :, 2) = norTrip.A;
mats(:, :, 3) = posPair.A;
mats(:, :, 4) = norPair.A; 

errs = zeros(nSamp, nK, 4);

parfor i=1:4
    errs(:, :, i) = test_matrix(mats(:, :, i), xs, epsil);
end

posTripErrs = errs(:, :, 1);
norTripErrs = errs(:, :, 2);
posPairErrs = errs(:, :, 3);
norPairErrs = errs(:, :, 4);

ksPos = 1:nK;

scrsz = get(groot, 'ScreenSize');
figure('Position', scrsz);
set(gca, 'FontSize', 50);
hold on;

bp1 = boxplot(posTripErrs, ks, 'Positions', ksPos+0.25, 'symbol', '', 'widths', 0.4);
bp2 = boxplot(posPairErrs, ks, 'Positions', ksPos-0.25, 'symbol', '', 'widths', 0.4);
xlim([0 nK+1]);
ylim auto;

color = [repmat('b',1,nK), repmat('r', 1, nK)];
h = findobj(gca,'Tag','Box');
for j=1:length(h)
   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
end
c = get(gca, 'Children');
hleg1 = legend([c(1) c(nK+1)], 'Triplets', 'Pairs', 'Location', 'NorthEast');

ylabel('RRMSE');
title(['RRMSEs, non-negative ' num2str(nMeas) ' x ' num2str(n) ' matrices']);
set(gca, 'FontSize', 40);
savefig([runName '-pos.fig']);
disp([runName '-pos.fig']);

figure('Position', scrsz);
set(gca, 'FontSize', 50);
hold on;

bp3 = boxplot(norTripErrs, ks, 'Positions', ksPos+0.25, 'symbol', '', 'widths', 0.4);
bp4 = boxplot(norPairErrs, ks, 'Positions', ksPos-0.25, 'symbol', '', 'widths', 0.4);
xlim([0 nK+1]);
ylim auto;

color = [repmat('b',1,nK), repmat('r', 1, nK)];
h = findobj(gca,'Tag','Box');
for j=1:length(h)
   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
end
c = get(gca, 'Children');
hleg2 = legend([c(1) c(nK+1)], 'Triplets', 'Pairs', 'Location', 'NorthEast');

ylabel('RRMSE');
title(['RRMSEs, general ' num2str(nMeas) ' x ' num2str(n) ' matrices']);
set(gca, 'FontSize', 40);
savefig([runName '-nor.fig']);
disp([runName '-nor.fig']);
