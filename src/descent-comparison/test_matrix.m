function errs = test_matrix(q, xs, eps)

    % Testing matrix q for sparsity level k
    [~, n] = size(q); %#ok<ASGLU>
	[~, nSamp, nK] = size(xs);
    
    errs = zeros(nSamp, nK);
	for j = 1:nK
		for i = 1:nSamp
			
			y = q * xs(:, i, j); %#ok<*NASGU>
			
			cvx_begin quiet
				variable z(n)
				minimize(norm(z, 1))
				subject to
					norm(y - q*z) <= eps %#ok<*NOPRT>
			cvx_end
			
			errs(i, j) = sqrt(sum((z-xs(:, i, j)).^2) / sum(xs(:, i, j).^2));
			%errs(i, j) = sqrt(max(abs(z-xs(:, i))) / max(abs(xs(:, i, j))));
			disp(['Testing: ' num2str(i)]);
		end
	end
    
end
