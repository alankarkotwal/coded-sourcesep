function runRecontruct(type)
%clear; close all; clc

perms = [];
phi = [];
origPhi = [];
D = [];

if type == 2
	load ../../results/descent-cacti-avg/736774_7153-64-2-10000.mat;
elseif type == 4
	load ../../results/descent-cacti-avg/736774_801-64-4-10000.mat;
elseif type == 6
	load ../../results/descent-cacti-avg/736775_2832-64-6-10000.mat;
end

epsilon = 1e-5;
nSamp = 1000;
spars = (0.05:0.05:0.3)';

ps = sqrt(size(D, 1));
n = ps^2;
T = size(perms, 1);

runName = ['../../results/descent-cacti-avg/' strrep(num2str(now), '.', '_') '-' num2str(n) '-' num2str(T) '-' num2str(nSamp)]; 

sens = zeros(n, n*T);
sensOrig = zeros(n, n*T);
psi = zeros(n*T, n*T);
for t = 1:T
	thisPhi = reshape(circshift(reshape(phi, [ps, ps]), perms(t, :)), [n 1]);
	thisPhiOrig = reshape(circshift(reshape(origPhi, [ps, ps]), perms(t, :)), [n 1]);
	sens(:, (t-1)*n+1:t*n) = diag(thisPhi);
	sensOrig(:, (t-1)*n+1:t*n) = diag(thisPhiOrig);
	psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
end
A = sens * psi;
AOrig = sensOrig * psi;

rrmse = zeros(nSamp, size(spars, 1));
rrmseOrig = zeros(nSamp, size(spars, 1));
eigvals = zeros(nSamp, size(spars, 1));
eigvalsOrig = zeros(nSamp, size(spars, 1));

for spar = 1:size(spars, 1)
	parfor samp = 1:nSamp

		disp([spars(spar) samp]);

		theta = zeros(n, T);
		for t = 1:T
			theta(:, t) = sprand(n, 1, spars(spar));
		end
		theta = theta(:);

		y = A * theta;
		yOrig = AOrig * theta;
		
		inpFrames = reshape(psi * theta, [n T]);
		outFrames = reconstruct(y, phi, D, perms, epsilon);
		outFramesOrig = reconstruct(yOrig, origPhi, D, perms, epsilon);
		
		rrmse(samp, spar) = norm(outFrames(:) - inpFrames(:)) / norm(inpFrames(:));
		rrmseOrig(samp, spar) = norm(outFramesOrig(:) - inpFrames(:)) / norm(inpFrames(:));
        
		thisSuppA = A(:, theta ~= 0);
		thisSuppAOrig = AOrig(:, theta ~= 0);
		
		thisSuppA = thisSuppA' * thisSuppA;
		thisSuppAOrig = thisSuppAOrig' * thisSuppAOrig;
		
		thisSuppA = thisSuppA - diag(diag(thisSuppA));
		thisSuppAOrig = thisSuppAOrig - diag(diag(thisSuppAOrig));
		
		eigvals(samp, spar) = max(abs(eig(thisSuppA)));
		eigvalsOrig(samp, spar) = max(abs(eig(thisSuppAOrig)));
        
	end
end

save(runName);

sparsVec = (1:size(spars, 1))';

scrsz = get(groot, 'ScreenSize');
figure('Position', scrsz);
set(gca, 'FontSize', 50);
hold on;

bp1 = boxplot(rrmse, spars, 'Positions', sparsVec+0.25, 'symbol', '', 'widths', 0.4);
bp2 = boxplot(rrmseOrig, spars, 'Positions', sparsVec-0.25, 'symbol', '', 'widths', 0.4);
xlim([0 size(spars, 1)+1]);
ylim auto;

color = [repmat('b',1,size(spars, 1)), repmat('r', 1, size(spars, 1))];
h = findobj(gca,'Tag','Box');
for j=1:length(h)
   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
end
c = get(gca, 'Children');
hleg1 = legend([c(1) c(size(spars, 1)+1)], 'Optimized', 'Original', 'Location', 'NorthEast');

ylabel('RRMSE');
title(['RRMSE comparison: n = ' num2str(n) ', T = ' num2str(T)]);
set(gca, 'FontSize', 40);
savefig([runName '-fin.fig']);
disp([runName '-fin.fig']);

scrsz = get(groot, 'ScreenSize');
figure('Position', scrsz);
set(gca, 'FontSize', 50);
hold on;

bp3 = boxplot(eigvals, spars, 'Positions', sparsVec+0.25, 'symbol', '', 'widths', 0.4);
bp4 = boxplot(eigvalsOrig, spars, 'Positions', sparsVec-0.25, 'symbol', '', 'widths', 0.4);
xlim([0 size(spars, 1)+1]);
ylim auto;

color = [repmat('b',1,size(spars, 1)), repmat('r', 1, size(spars, 1))];
h = findobj(gca,'Tag','Box');
for j=1:length(h)
   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
end
c = get(gca, 'Children');
hleg1 = legend([c(1) c(size(spars, 1)+1)], 'Optimized', 'Original', 'Location', 'NorthEast');

ylabel('Maximum eigenvalue of AT * A - I');
title(['Eigenvalue comparison: n = ' num2str(n) ', T = ' num2str(T)]);
set(gca, 'FontSize', 40);
savefig([runName '-eig-fin.fig']);
disp([runName '-eig-fin.fig']);
