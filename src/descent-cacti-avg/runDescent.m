ps = 8;
T = 6;
n = ps^2;
D = kron(dctmtx(ps)', dctmtx(ps)');

perms = zeros(T, 2);
for t = 1:T
	perms(t, :) = [randi(8) randi(8)];
end
disp('Perms:');
disp(perms);

nStarts = 10000;

step = 1;
stepFactor = 0.5;
iter = Inf;
repCountMax = 8;
eps = 0;
minChange = 0.0001;

runName = ['../../results/descent-cacti-avg/' strrep(num2str(now), '.', '_') '-' num2str(n) '-' num2str(T) '-' num2str(nStarts)];

phis = zeros(n, nStarts);
origPhis = zeros(n, nStarts);
objs = zeros(nStarts, 1);

parfor i = 1:nStarts
	
	disp(strcat('***** Start ', num2str(i), ' *****'));
	[thisPhi, thisObj, thisOrigPhi] = descent(perms, D, step, stepFactor, iter, eps, repCountMax, minChange);

	phis(:, i) = thisPhi;
	origPhis(:, i) = thisOrigPhi;
	objs(i) = thisObj;

end

[obj, pos] = min(objs);
phi = phis(:, pos);
origPhi = origPhis(:, pos);

nCohs = n * T * (n * T - 1)/2;

disp('The minimum average coherence value is ');
disp(num2str(sqrt(obj/nCohs)));

save([runName '.mat'], 'phi', 'obj', 'origPhi', 'perms', 'D');
