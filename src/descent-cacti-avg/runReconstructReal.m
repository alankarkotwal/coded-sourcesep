clear; close all; clc

%load ../../results/descent-cacti-avg/736774_7153-64-2-10000.mat;
%load ../../results/descent-cacti-avg/736774_801-64-4-10000.mat;
load ../../results/descent-cacti-avg/736775_2832-64-6-10000.mat;

epsilon = 1e-5;
scale = 16;

ps = sqrt(size(D, 1));
n = ps^2;
T = size(perms, 1);
D = kron(dctmtx(ps)', dctmtx(ps)');

imgSize = size(imresize(rgb2gray(imread('../../data/c1.png')), 1/scale));
inp = zeros([imgSize T]);
for t = 1:T
	inp(:, :, t) = imresize(im2double(rgb2gray(imread(['../../data/c' num2str(t) '.png']))), 1/scale);
end

runName = ['../../results/descent-cacti-avg/' strrep(num2str(now), '.', '_') '-real-' num2str(n) '-' num2str(T)]; 

sens = zeros(n, n*T);
psi = zeros(n*T, n*T);
for t = 1:T
	thisPhi = reshape(circshift(reshape(phi, [ps, ps]), perms(t, :)), [n 1]);
	thisPhiOrig = reshape(circshift(reshape(origPhi, [ps, ps]), perms(t, :)), [n 1]);
	sens(:, (t-1)*n+1:t*n) = diag(thisPhi);
	sensOrig(:, (t-1)*n+1:t*n) = diag(thisPhiOrig);
	psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
end
A = sens * psi;
AOrig = sensOrig * psi;

rrmse = zeros(size(inp(:, :, 1), 1)-ps+1, size(inp(:, :, 1), 2)-ps+1);
rrmseOrig = zeros(size(inp(:, :, 1), 1)-ps+1, size(inp(:, :, 1), 2)-ps+1);

for i = 1:size(inp(:, :, 1), 1)-ps+1
	for j = 1:size(inp(:, :, 1), 2)-ps+1
	
		disp([num2str(i) '/' num2str(size(inp(:, :, 1), 1)-ps+1) ', ' num2str(j) '/' num2str(size(inp(:, :, 1), 2)-ps+1)]);

		thisX = zeros(n*T, 1);
		for t = 1:T
			thisPatch = imresize(inp(i:i+ps-1, j:j+ps-1), [n 1]);
			thisX((T-1)*n+1:T*n) = thisPatch(:);
		end

		thisY = sens * thisX;
		thisYOrig = sensOrig * thisX;

		outXRec = reconstruct(thisY, phi, D, perms, epsilon);
		outXRecOrig = reconstruct(thisYOrig, origPhi, D, perms, epsilon);

		rrmse(i, j) = sqrt(norm(outXRec(:) - thisX)^2 / norm(thisX)^2);
		rrmseOrig(i, j) = sqrt(norm(outXRecOrig(:) - thisX)^2 / norm(thisX)^2);
	end
end

scrsz = get(groot, 'ScreenSize');
figure('Position', scrsz);
set(gca, 'FontSize', 50);
hold on;

bp1 = boxplot([rrmse(:) rrmseOrig(:)], {'Designed' 'Original'}, 'Positions', [1 5], 'symbol', '', 'widths', 0.4);
xlim auto;
ylim auto;

%color = [repmat('b',1,size(spars, 1)), repmat('r', 1, size(spars, 1))];
%h = findobj(gca,'Tag','Box');
%for j=1:length(h)
%   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
%end
%c = get(gca, 'Children');
%hleg1 = legend([c(1) c(size(spars, 1)+1)], 'Optimized', 'Original', 'Location', 'NorthEast');

ylabel('RRMSE');
title(['RRMSE comparison: n = ' num2str(n) ', T = ' num2str(T)]);
set(gca, 'FontSize', 40);
savefig([runName '.fig']);
disp([runName '.fig']);
