function [phi, objOld, origPhi] = descent(perms, D, step, stepFactor, iter, eps, repCountMax, minChange)

	n = size(D, 1);
	T = size(perms, 1);

	if nargin == 3
		step = 0.1;
		stepFactor = 0.5;
		iter = 100;
		eps = 0.5;
		repCountMax = 5;
		minChange = 0.0001;
	elseif nargin == 4
		stepFactor = 0.5;
		iter = 100;
		eps = 0.5;
		repCountMax = 5;
		minChange = 0.0001;
	elseif nargin == 5
		iter = 100;
		eps = 0.5;
		repCountMax = 5;
		minChange = 0.0001;
	elseif nargin == 6
		eps = 0.5;
		repCountMax = 5;
		minChange = 0.0001;
	elseif nargin == 7
		repCountMax = 5;
		minChange = 0.0001;
	elseif nargin == 8
		minChange = 0.0001;
	end
	
	nCohs = n * T * (n * T - 1)/2;

	phi = rand(n, 1);
	origPhi = phi;
	objOld = objFn(phi, D, perms);

	disp([0.0 sqrt(objOld/nCohs)]);

	count = 0;
	repCount = 0;
	iTookAStep = 1;
	descDir = zeros(n, 1);

	while count < iter
	
		if iTookAStep
			descDir = objDer(phi, D, perms);
		end
		
		phiNew = phi - step * descDir;
		phiNew(phiNew < eps) = eps;
		phiNew(phiNew > 1) = 1;

		objNew = objFn(phiNew, D, perms);

		if objNew < objOld * (1 - minChange)
			phi = phiNew;
			objOld = objNew;
			%step = step / stepFactor;
			count = count + 1;
			repCount = 0;
			iTookAStep = 1;
		else
			step = step * stepFactor;
			repCount = repCount + 1;
			iTookAStep = 0;
			if repCount >= repCountMax
				break;
			end
		end
	
		disp([count sqrt(objOld/nCohs)]);
	
	end
	
end
