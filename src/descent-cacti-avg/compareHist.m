clc; close all;

T = size(perms, 1);
n = size(phi, 1);
ps = sqrt(n);

sens = zeros(n, n*T);
psi = zeros(n*T, n*T);
for t = 1:T
    thisPhi = reshape(circshift(reshape(phi, [ps, ps]), perms(t, :)), [n 1]);
    sens(:, (t-1)*n+1:t*n) = diag(thisPhi);
    psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
end
A = sens * psi;
normA = A ./ repmat(sqrt(sum(A.^2)), size(A, 1), 1);
dots = normA'*normA;
dotsAbs = abs(dots - diag(diag(dots)));
dotsAbs = dotsAbs(:);
dotsAbs(dotsAbs == 0) = [];

origSens = zeros(n, n*T);
for t = 1:T
    thisOrigPhi = reshape(circshift(reshape(origPhi, [ps, ps]), perms(t, :)), [n 1]);
    origSens(:, (t-1)*n+1:t*n) = diag(thisOrigPhi);
end
A = origSens * psi;
normA = A ./ repmat(sqrt(sum(A.^2)), size(A, 1), 1);
dots = normA'*normA;
dotsAbsOrig = abs(dots - diag(diag(dots)));
dotsAbsOrig = dotsAbsOrig(:);
dotsAbsOrig(dotsAbsOrig == 0) = [];

figure; histogram(dotsAbs); xlim([0 1]); ylim([0 20000]);
xlabel('Dot product value'); ylabel('Frequency'); title('Optimized dot products');

figure; histogram(dotsAbsOrig); xlim([0 1]); ylim([0 20000]);
xlabel('Dot product value'); ylabel('Frequency'); title('Original dot products');