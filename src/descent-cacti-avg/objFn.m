function obj = objFn(phi, D, perms)

	T = size(perms, 1);
	n = size(phi, 1);
	ps = sqrt(n);

	sens = zeros(n, n*T);
	psi = zeros(n*T, n*T);
	for t = 1:T
		thisPhi = reshape(circshift(reshape(phi, [ps, ps]), perms(t, :)), [n 1]);
		sens(:, (t-1)*n+1:t*n) = diag(thisPhi);
		psi((t-1)*n+1:t*n, (t-1)*n+1:t*n) = D;
	end
	A = sens * psi;
	normA = A ./ repmat(sqrt(sum(A.^2)), size(A, 1), 1);
	dots = normA'*normA - eye(n*T, n*T);
	dotsAbs = abs(dots);

	obj = sum(sum(dotsAbs .^ 2))/2;	
	

end
