function out = objDer(phi, D, perms)

	n = size(phi, 1);
	T = size(perms, 1);
	ps = sqrt(n);

	out = zeros(n, 1);

	for mu = 1:T
		for nu = 1:(mu-1)
			for beta = 1:n
				for gamma = 1:n

					phiMu = reshape(circshift(reshape(phi, [ps, ps]), perms(mu, :)), [n 1]);
					phiNu = reshape(circshift(reshape(phi, [ps, ps]), perms(nu, :)), [n 1]);

					chi = sum((phiMu .* phiNu) .* ...
							  (D(:, beta) .* D(:, gamma)));
					xi = sqrt(sum((phiMu .^ 2) .* (D(:, beta) .^ 2)) * ...
							  sum((phiNu .^ 2) .* (D(:, gamma) .^ 2)));

					for epsilon=1:n
						
						alphaMu = ps * mod(ceil(epsilon / ps) + perms(mu, 2) - 1, ps) + ...
								  mod(epsilon - ps * (ceil(epsilon / ps) - 1) + perms(mu, 1) - 1, ps) + 1;
						alphaNu = ps * mod(ceil(epsilon / ps) + perms(nu, 2) - 1, ps) + ...
								  mod(epsilon - ps * (ceil(epsilon / ps) - 1) + perms(nu, 1) - 1, ps) + 1;
						
						chiDer = D(alphaMu, beta) * D(alphaMu, gamma) * phiNu(alphaMu) + ...
								 D(alphaNu, beta) * D(alphaNu, gamma) * phiMu(alphaNu);

						xiDer = (D(alphaMu, beta) ^ 2 * phiMu(alphaMu) * ...
								 sum((phiNu .^ 2) .* (D(:, gamma) .^ 2)) + ...
								 D(alphaNu, gamma) ^ 2 * phiNu(alphaNu) * ...
								 sum((phiMu .^ 2) .* (D(:, beta) .^ 2))) / xi;

						out(epsilon) = out(epsilon) + chi/xi * (xi * chiDer - chi * xiDer) / xi ^ 2;

					end

				end
			end
		end
	end

	for mu = 1:T
		for beta = 1:n
			for gamma = 1:(beta-1)

				phiMu = reshape(circshift(reshape(phi, [ps, ps]), perms(mu, :)), [n 1]);

				chi = sum((phiMu .^ 2) .* ...
						  (D(:, beta) .* D(:, gamma)));
				xi = sqrt(sum(phiMu.^2 .* D(:, beta).^2) * ...
						  sum(phiMu.^2 .* D(:, gamma).^2));

				for epsilon=1:n
					
					alphaMu = ps * mod(ceil(epsilon / ps) + perms(mu, 2) - 1, ps) + ...
							  mod(epsilon - ps * (ceil(epsilon / ps) - 1) + perms(mu, 1) - 1, ps) + 1;
					
					chiDer = 2 * D(alphaMu, beta) * D(alphaMu, gamma) * phiMu(alphaMu);

					xiDer = (D(alphaMu, beta)^2 * phiMu(alphaMu) * ...
							 sum((phiMu .^ 2) .* (D(:, gamma) .^ 2)) + ...
							 D(alphaMu, gamma)^2 * phiMu(alphaMu) * ...
							 sum((phiMu .^ 2) .* (D(:, beta) .^ 2))) / xi;

					out(epsilon) = out(epsilon) + (xi * chiDer - chi * xiDer) / xi ^ 2;

				end

			end
		end
	end

end
