%% --------------------------------------------------------------------------------------------------------
%% Compressed Sensing with Perturbations
%% Alankar Kotwal <alankarkotwal13@gmail.com>
%% --------------------------------------------------------------------------------------------------------


%clear; clc; close all;


%% -------------------------------------------- Hyperparameters -------------------------------------------
lam1 = 1;
lam2 = 1;

%% ----------------------------------------- Problem specification ----------------------------------------

ps = 8;
n = ps^2;
T = 2;
K = 1;
D = kron(dctmtx(ps)', dctmtx(ps)');
psi = kron(eye(T), D);

r = 0.2;
quantVals = [0:r:1]';

perms = [0 0; 4 3];


%% ------------------------------------------- Testing variables -------------------------------------------

s = 0.1;
step = 0.1;
stepFactor = 0.5;
repCountMax = 5;
nIter = Inf;
minChange = 1e-5;
epsilon = 1e-10;

runName = ['../../results/perturbed-cs-bayes/' strrep(num2str(now), '.', '_') '-' num2str(n) '-' num2str(T)]; 


%% -------------------------- Initialize the true and implemented sensing matrices -------------------------

phiTrue = rand(n, 1);
phiImp = quantizeMatrix(phiTrue, quantVals);
deltaTrue = phiImp - phiTrue;


%% ---------------------------- Get implemented and true (designed) sensing matrix -------------------------

ATrue = getAMatrix(phiTrue, psi, perms);
AImp = getAMatrix(phiImp, psi, perms);


%% ----------------- Simulate sensing step: generate random vectors and get measurements -------------------

theta = zeros(n, T, K);
for k = 1:K
	for t = 1:T
		theta(:, t, k) = sprand(n, 1, s);
	end
end
y = AImp * reshape(theta, [n*T, K]);
xs = reshape(psi * reshape(theta, [n*T K]), [n, T, K]);


%% ------------------------------------------- Reconstruct! ------------------------------------------------

[xsOut, delta] = reconstruct(y, phiTrue, D, perms, r, lam1, lam2, epsilon, step, stepFactor, repCountMax, nIter, minChange);
xsOutOrig = reconstructOrig(y, ATrue, D, epsilon);
xsOutCheck = reconstructOrig(y, AImp, D, epsilon);

% Errors
rrmses = norms(xs - xsOut) ./ norms(xs);
rrmsesOrig = norms(xs - xsOutOrig) ./ norms(xs);
rrmsesCheck = norms(xs - xsOutCheck) ./ norms(xs);
rrmseDelta = norm(abs(deltaTrue - delta)) / norm(deltaTrue);

%disp([rrmseDelta]);
%disp([max(abs(rrmses(:))) max(abs(rrmsesOrig(:))) max(abs(rrmsesCheck(:)))]);

% Boxplot

% Save mat
%save([runName '.mat'], 'phi', 'perms', 'D', 'rrmses', 'rrmsesOrig', 'rrmseDelta', 'rrmseDeltaOrig', 'xs', 'xsOut', 'delta');
