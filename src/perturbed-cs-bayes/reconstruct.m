function [xs, delta] = reconstruct(y, phi, D, perms, r, lam1, lam2, epsilon, step, stepFactor, repCountMax, nIter, minChange)

	% Sizes
	n = size(phi, 1);
	ps = sqrt(n);
	T = size(perms, 1);
	K = size(y, 2);

	% Utility variables
	permsResp = zeros(n, T);
	for t = 1:T
		permsResp(:, t) = reshape(circshift(reshape((1:n)', [ps, ps]), perms(t, :)), [n, 1]);
	end

	delta = zeros(n, 1);
	theta = zeros(n, T, K);

	% Get the current sensing matrix
	psi = kron(eye(T), D);
	A = getAMatrix(phi+delta, psi, perms);

	% Objective function
	obj = objFn(y, A, theta, delta, lam1, lam2);
	
	% Jointly optimize delta and xs
    count = 0;
    repCount = 0;
    iTookAStep = 1;
	thetaGrad = zeros(size(theta));
	deltaGrad = zeros(size(delta));

    while count < nIter

		if iTookAStep

			thetaGrad = objDerTheta(y, A, theta, lam1);
			deltaGrad = objDerDelta(y, A, D, delta, permsResp, theta, lam1);
			disp([norm(thetaGrad(:)) norm(deltaGrad)])

		end

		% Take a step
		thetaNew = theta - step * thetaGrad;
		
		deltaNew = delta - step * deltaGrad;
		deltaNew(deltaNew > r) = r;
		deltaNew(deltaNew < -r) = -r;

		ANew = getAMatrix(phi + deltaNew, psi, perms);
		objNew = objFn(y, ANew, thetaNew, deltaNew, lam1, lam2);
		
		disp([obj objNew]);

        if objNew <= obj * (1 - minChange)

			theta = thetaNew;
			delta = deltaNew;
			obj = objNew;

			%step = step / stepFactor;
			count = count + 1;
			repCount = 0;
            iTookAStep = 1;
        
		else
		
			step = step * stepFactor;
			repCount = repCount + 1;
            iTookAStep = 0;
            if repCount >= repCountMax
				break;
            end

        end

	end

	xs = reshape(D * reshape(theta, [n T * K]), [n T K]);
	
end
