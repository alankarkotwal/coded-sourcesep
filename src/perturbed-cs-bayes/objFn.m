function out = objFn(y, A, theta, delta, lam1, lam2)

	[n K] = size(y);
	T = size(theta, 2);
	yRec = A * reshape(theta, [n*T K]);

	out = norm(y(:) - yRec(:)) ^ 2 + lam1 * norm(theta(:), 1) + lam2 * norm(delta) ^ 2;

end
