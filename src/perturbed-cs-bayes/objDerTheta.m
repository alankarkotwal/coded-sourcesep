function out = objDerTheta(y, A, theta, lam1)

	[n K] = size(y);
	T = size(theta, 2);

	rems = y - A * reshape(theta, [n*T K]);
	out = 2 * reshape(A' * rems, size(theta)) + lam1 * sign(theta);

end
