function xs = reconstructOrig(y, A, D, epsilon)
	
	n = size(y, 1);
	T = size(A, 2) / n;
	K = size(y, 2);

	xs = zeros(n, T, K);
	for k = 1:K
		xs(:, :, k) = solveBP(y(:, k), A, D, epsilon);
	end

end
