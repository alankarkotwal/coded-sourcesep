function out = objDerDelta(y, A, D, delta, permsResp, theta, lam1)
	
	[n K] = size(y);
	T = size(theta, 2);

	out = zeros(n, 1);

	for k = 1:K

		thetaVec = reshape(theta(:, :, k), [n*T, 1]);
		rems = y(:, k) - A * thetaVec;
		x = reshape(kron(eye(T), D) * thetaVec, [n, T]);

		coeffMat = zeros(n, n);
		for t = 1:T
			for i = 1:n
				coeffMat(i, permsResp(i, t)) = ...
				coeffMat(i, permsResp(i, t)) + x(permsResp(i, t), t);
			end
		end

		out = out - 2 * coeffMat * rems; 
	
	end

end
