Coded Source Separation for Compressed Video Recovery
=====================================================

Alankar Kotwal | 12D070010   
Electrical Engineering, IIT Bombay   
   
Compressed sensing has been explored as an alternative (usually, faster) way of sampling continuous–time signals. Its success with still images has inspired efforts to apply it to video. Indeed, [1] achieves compression across time by combining frames into coded snapshots while sensing and separating them with a pre–trained over–complete dictionary. This works well, but needs a dictionary at the same frame–rate and time–smoothness as the video.   
   
We try relaxing this constraint using a source–separation approach to this problem [2], where precise error bounds on the recovery of the images have been derived, with possible improvement using the techniques in [3]. Each of the coded snapshots is treated as a mixture of sources, each sparse in some basis. We experimented with basis pursuit recovery with Gaussian–random sensing matrices, getting excellent results with no visible ghosting for both similar and radically different images. Unfortunately, the same experiment with the more realizable [0, 1]–uniformly–random sensing matrices does not work, because they do not have the nice incoherence properties of Gaussian–random matrices, which are sufficient conditions for accurate or near-accurate recovery as derived in [2]. We aim to design such sensing matrices with low mutual coherence, making them ideal for compressed video. This has already been done [4, 5], but without the non–negativity and diagonal–matrix constraints in the model of [1].   
   
Solved well, this will find applications in multi–spectral imaging, image demosaicing, fast video sensing and the general problem of coded source separation.   
   
[1] Y. Hitomi, J. Gu, M. Gupta, T. Mitsunaga, and S. Nayar, “Video from a Single Coded Exposure Photograph using a Learned Over-Complete Dictionary,” in IEEE International Conference on Computer Vision (ICCV), Nov 2011.   
[2] C. Studer and R. G. Baraniuk, “Stable restoration and separation of approximately sparse signals,” Applied and Computational Harmonic Analysis, vol. 37, no. 1, pp. 12 – 35, 2014.   
[3] T. T. Cai, L. Wang, and G. Xu, “New Bounds for Restricted Isometry Constants,” IEEE Transactions on Information Theory (IEEE-TIT), vol. 56, no. 9, pp. 4388 – 4398, 2010.   
[4] J. M. Duarte-Carvajalino and G. Sapiro, “Learning to Sense Sparse Signals: Simultaneous Sensing Matrix and Sparsifying Dictionary Optimization,” IEEE Transactions on Image Processing (IEEE-TIP), vol. 18, no. 7, pp. 1395 – 1408, 2009.   
[5] M. Elad, “Optimized Projections for Compressed Sensing,” IEEE Transactions on Signal Processing (IEEE-TSP), vol. 55, no. 12, pp. 5696 – 5702, 2006.