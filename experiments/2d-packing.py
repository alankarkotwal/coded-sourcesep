import numpy as np
from matplotlib import pyplot as plt
import pdb

nIter = 100000
nVec = 4
minCoh = 1000
minList = []

for i in range(nIter):
	angles = np.random.uniform(-np.pi,np.pi, nVec)
	diffs = np.asarray([])
	for k in range(nVec):
		for j in range(k+1, nVec):
			diffs = np.append(diffs, angles[k]-angles[j])
	coh = np.max(np.abs(np.cos(diffs)))

	if coh < minCoh:
		minCoh = coh
		minList = angles

print minCoh
print 180/np.pi*minList

xs = np.cos(minList)
ys = np.sin(minList)
xos = np.zeros((3, 1))
yos = np.zeros((3, 1))
f = plt.figure()
plt.quiver(xos, yos, xs, ys)
plt.show()
