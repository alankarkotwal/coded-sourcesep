clear; close all; clc

nSamp = 1000000;
maxK = 10;
diffs = zeros(nSamp, 1);

for i = 1:nSamp

	mu = rand;
	k = floor(0.5 * (1 + 1/mu));
	
	barBound = (1 - mu * (2 * k - 1) + 2 * sqrt(mu * k) * ...
					   sqrt(1 + mu * (k - 1))) / ...
		   (sqrt(1 + mu) * (1 - mu * (2 * k - 1)));

	nehBound = 2 * sqrt(2 * k) / (1 - mu ^ 2);

	diffs(i) = barBound - nehBound;

end

disp([max(max(diffs)) min(min(diffs))])
