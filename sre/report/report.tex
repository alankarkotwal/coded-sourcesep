\documentclass[10pt,final,conference,a4paper]{IEEEtran}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{hyperref}
\usepackage{formatfig}
%\usepackage{breqn}

\title{Coded Source Separation for Compressed Video}

\author{\IEEEauthorblockN{Alankar Kotwal $\|$ 12D070010}
\IEEEauthorblockA{Department of Electrical Engineering \\
Indian Institute of Technology, Bombay \\
Email: \texttt{\href{mailto:alankar.kotwal@iitb.ac.in}{alankar.kotwal@iitb.ac.in}}}}

\begin{document}
\maketitle

\begin{abstract}
Traditional approaches to visual compressed sensing have focused on single image sensing, with compression across space, as opposed to video sensing with compression across time. Recent work~\cite{CAVE_0311} tries to achieve the latter with a learned 3--D dictionary. However, this approach assumes smoothness of pixel values in video frames across time and a fixed frame--rate. We attempt to solve two problems: relaxing this assumption with a source--separation approach in the same hardware framework and optimizing sensing matrix required for the source--separation approach to potentially achieve reconstructions better than those achievable with random matrices.
\end{abstract}
\begin{IEEEkeywords}
video compressed sensing, time compression, source separation, coherence, gradient descent.
\end{IEEEkeywords}

\section{Introduction}
Compressed sensing has been explored as an alternative (usually, faster) way of sampling continuous--time signals. Its success with still images has inspired efforts to apply it to video. Indeed,~\cite{CAVE_0311} achieves compression across time by combining frames into coded snapshots while sensing and separating them with a pre--trained over--complete dictionary. This works well, but needs a dictionary at the same frame--rate and time--smoothness as the video. 

We try relaxing this constraint using a source--separation approach to this problem~\cite{Studer201412}, where precise error bounds on the recovery of the images have been derived, with possible improvement using the techniques in~\cite{Cai2010}. Each of the coded snapshots is treated as a mixture of sources, each sparse in some basis. We experimented with basis pursuit recovery with Gaussian--random sensing matrices, getting excellent results with no visible ghosting for both similar and radically different images. Unfortunately, the same experiment with the more realizable $[0, 1]$--uniformly--random sensing matrices does not work as well, because they do not have the nice incoherence properties of Gaussian--random matrices, which are sufficient conditions for accurate or near-accurate recovery as derived in \cite{Studer201412}.
We aim to design such sensing matrices with low mutual coherence, making them ideal for compressed video. This has already been done~\cite{Duarte200907, Elad200610}, but without the non--negativity and diagonal--matrix constraints in the model of~\cite{CAVE_0311}.

Solved well, this will find applications in multi--spectral imaging, image demosaicing, fast video sensing and the general problem of coded source separation.

\section{The Context of the Problem}
The ability of standard recovery algorithms to reconstruct a compressed--sensed signal accurately depends on two main factors: (i)~sparsity in a basis and (ii)~incoherent sampling. 
%In a way, then, the distinguishing feature of any compressed sampling system is its choice of sensing matrix and sparsifying basis. 
The choice of a sparsifying basis is made from prior knowledge about the signal. Most natural images are sparse in bases like the Discrete Cosine Transform (DCT). A given class of signals may be sparsified by learning a dictionary (possibly over--complete) on them.
% if they are not adequately sparsely representable in any fixed basis.
%The choice of the basis is important because
Guarantees on reconstruction of signals (see Eq.~\ref{eq:cohGuarantee}) involve the sparsity of the signal on the chosen basis.

Sensing matrices need to be chosen so that they capture information about the projection of the signal on all the atoms of the sparsifying basis, and hence needs to be `incoherent' with the basis. However, there also exist hardware constraints on the choice of a sensing matrix: most cameras have constraints on manipulating pixel-wise exposures. Existing CMOS sensors (which are used in most ordinary cameras) cannot sense linear combinations of pixel values in real time \cite{CAVE_0311}; hence, practical sensing matrices using these sensors have to be non--negative diagonal matrices. The problem is, then, restricted to sensing linear combinations of pixel values across time (as opposed to linear combinations across space, which most still image sensing systems do).

\subsection{Previous Work: Video Compressed Sensing}
\cite{CAVE_0311}, where a system for capturing and reconstructing compressed video has been designed, makes a diagonal choice for the sensing matrix. $T$ vectorized input frames $\{\mathbf{X}_i\}_{i=1}^{T}$ are sensed so that the vectorized output $\mathbf{Y}$ appears to be a coded combination (dictated by the `sensing matrices' $\mathbf{\Phi}_i$) of the inputs. The sensing framework is
\begin{equation}
\mathbf{Y} = \sum_{i=1}^{T} \mathbf{\Phi}_i \mathbf{X}_i
\label{eq:hitSensing}
\end{equation}

The sparsifying basis here is a 3--D dictionary learned on video patches. Given this dictionary, called $\mathbf{D}$, any given signal $\mathbf{X}$, and in particular, its frames $\{\mathbf{X}_i\}_{i=1}^{T}$ can be approximately reconstructed as a sum of its projections $\mathbb{\alpha}_j$ on the $K$ atoms in $\mathbf{D}$:
\begin{equation}
\mathbf{X}_i = \sum_{j=1}^{K} \mathbf{D}_{ji} \mathbb{\alpha}_{j}
\label{eq:hitDict}
\end{equation}
where $\mathbf{D}_{ji}$ is the $i^\text{th}$ frame in the $j^\text{th}$ 3--D dictionary atom $\mathbf{D}_{ji}$. From the measurements and the dictionary, the input images are recovered (Fig.~\ref{fig:hitResults}) solving the following optimization problem:
\begin{equation}
\min_{\mathbb{\alpha}} \|\mathbb{\alpha}\|_0 \text{ subject to } \left\| \mathbf{Y} - \sum_{i=1}^{T} \sum_{j=1}^{K} \mathbf{D}_{ji} \mathbb{\alpha}_{j} \right\|_2 \leq \epsilon
\label{eq:hitOpt}
\end{equation}

This problem can be approximately solved with sparse recovery techniques like orthogonal matching pursuit \cite{Cai2011}.

\begin{figure}
\includegraphics[scale=0.3]{pics/hitresults}
\caption{Results from \cite{CAVE_0311}}
\label{fig:hitResults}
\end{figure}

The drawback here, though, is that the 3--D dictionary imposes a smoothness assumption on the scene. Since a linear combination of dictionary atoms cannot `speed' an atom up, the typical speeds of objects moving in the video must be roughly the same as the dictionary. Also, because of the nature of the training data, the dictionary fails to sparsely represent sudden scene changes caused by, say, lighting or occlusion.

Other techniques like \cite{VR201104} exploit additional structure within the signal, like periodicity, rigid motion or analytical motion models and cannot be used in the general video sensing case.

\subsection{Previous Work: Sensing Matrix Optimization}
The second choice in a compressed sensing system is the sensing matrix $\mathbf{\Phi}$. Given a sparsifying basis $\mathbf{\Psi}$, it is necessary to construct an `optimal' sensing matrix. It has been shown~\cite{Studer201412} that if the sparsity of a signal in a basis, given by the $l_0$ norm of its coefficient vector $\mathbb{\alpha}$ in the dictionary $\mathbf{D} = \mathbf{\Phi} \mathbf{\Psi}$ satisfies
\begin{equation}
\|\mathbb{\alpha}\|_{0} \leq \frac{1}{2}	\left( 1 + \frac{1}{\mu{\left(\mathbf{D}\right)}} \right)
\label{eq:cohGuarantee}
\end{equation}
and the compressed measurement yields $\mathbf{Y}$, then the optimization problem 
\begin{equation}
\min_{\mathbb{\alpha}} \|\mathbb{\alpha}\|_0 \text{ subject to } \mathbf{Y} = \mathbf{\Phi} \mathbf{\Psi} \mathbf{\alpha}
\label{eq:l0Opt}
\end{equation}
necessarily yields the true coefficient vector $\mathbb{\alpha}$.

The function $\mu{\left(\mathbf{D}\right)}$ of the dictionary $\mathbf{D}$ is called the coherence of the dictionary $\mathbf{D}$. Defining the $j^\text{th}$ 2--normalized column of $\mathbf{D}$ to be $\mathbf{\bar{d}}_j$, this quantity is given by
\begin{equation}
\mu{\left(\mathbf{D}\right)} = \max_{i \neq j} |\left< \mathbf{\bar{d}}_i, \mathbf{\bar{d}}_j \right>|
\label{eq:cohDefn}
\end{equation}
Clearly, the guarantee on recovery would apply to `more' signals (greater allowed values of  $\|\mathbb{\alpha}\|_0$, so less sparse signals are allowed) if the value of $\mu{\left(\mathbf{D}\right)}$ is small. Most approaches to sensing matrix optimization, thus, focus on finding a sensing matrix (and sometimes, jointly finding a sensing matrix and sparsifying basis) such that $\mu{\left(\mathbf{D}\right)}$ is minimized.

\subsubsection{Minimization via Gram Matrix}
One way to look at the coherence is~\cite{Elad200610} to look at the absolute maximum non--diagonal element of $\mathbf{G} = \mathbf{D}^T \mathbf{D}$.
% Since $\mathbf{D} is overcomplete, this is a low--rank matrix.
The goal is to reduce the magnitudes of the non--diagonal elements. \cite{Elad200610} tries to minimize the following function, with a parameter $t$:
\begin{equation}
\mu_t{\left(\mathbf{D}\right)} = \frac{\sum_{i \neq j} \left(|g_{ij}| > t\right) |g_{ij}| }{\sum_{i \neq j} \left(|g_{ij}| > t\right)}
\label{eq:tAvgCoh}
\end{equation}
This is an absolute average of off--diagonal Gram matrix entries above $t$. To achieve this, \cite{Elad200610} processes the entries of the Gram matrix by a `shrinking' function, forces the shrunk Gram matrix to be low--rank to get a `new' Gram matrix, and builds the square root of the this matrix to obtain the updated dictionary.

However, this method gives no guarantees on whether the actual maximum value decreases or not (notice the method minimizes the \textit{average} value of off--diagonal elements above $t$). Also, the square--root step involves an assumption that the input matrix is positive semi--definite, which is not always the case. When it is not, one needs to force the offending eigenvalues to zero. Guarantees on whether coherence decreases across these iterations don't exist.

\subsubsection{Minimization via Rank--1 Approximation}
An equivalent way to look at the problem is making the columns of $\mathbf{D}$ as `orthogonal' to each other as possible. This implies that the Gram matrix $\mathbf{G}$ should be as close to the identity matrix as possible. \cite{Duarte200907} solves the problem of estimating $\mathbf{\Phi}$ given $\mathbf{\Psi}$ this way (\cite{Duarte200907} also solves the problem of estimating both jointly from sample signals, but that is not applicable in the general video scenario). Knowing that we need $\mathbf{G} = \mathbf{\Psi}^T \mathbf{\Phi}^T \mathbf{\Phi}\mathbf{\Psi} \approx \mathbf{I}$, $\mathbf{\Psi} \mathbf{\Psi}^T \mathbf{\Phi}^T \mathbf{\Phi} \mathbf{\Psi} \mathbf{\Psi}^T \approx \mathbf{\Psi}\mathbf{\Psi}^T$. With $\mathbf{\Psi}\mathbf{\Psi}^T = \mathbf{V} \mathbf{\Lambda} \mathbf{V}^T$ and $\mathbf{\Phi V} = \mathbf{\Gamma}$, we need $\mathbf{\Lambda} \mathbf{\Gamma}^T \mathbf{\Gamma} \mathbf{\Lambda} \approx \mathbf{\Lambda}$. So we solve
\begin{equation}
\min_{\mathbf{\Gamma}} \left\|\mathbf{\Lambda} \mathbf{\Gamma}^T \mathbf{\Gamma} \mathbf{\Lambda} - \mathbf{\Lambda} \right\|_F
\label{eq:rankApproxOpt}
\end{equation}
This can be written as 
\begin{equation}
\min_{\mathbf{\Gamma}} \left\| \mathbf{\Lambda} - \sum_{i} \mathbf{\nu}_i \mathbf{\nu}_i^T \right\|_F = \min_{\mathbf{\Gamma}} \left\| \mathbf{\Lambda} - \sum_{i, i \neq j} \mathbf{\nu}_i \mathbf{\nu}_i^T - \mathbf{\nu}_j \mathbf{\nu}_j^T \right\|_F
\label{eq:rankApproxSimp}
\end{equation}
where $\mathbf{\nu}_i$ is the $i^\text{th}$ column of $\mathbf{\Lambda \Gamma}^T$. This, however, is a rank--1 approximation problem which can be solved non--iteratively with the singular value decomposition of $\mathbf{\Lambda} - \sum_{i, i \neq j} \mathbf{\nu}_i \mathbf{\nu}_i^T$. We do this by initializing $\mathbf{\Lambda \Gamma}^T$ to a random matrix and successively optimizing for all $j$. This in turn yields $\mathbf{\Gamma}$, and therefore $\mathbf{\Phi}$.

\section{Our Approach: Video Compressed Sensing}
We propose to use a recovery method different from the one used in \cite{CAVE_0311}, within the same acquisition framework. Thus, our signals are still acquired according to Eq.~\ref{eq:hitSensing}. However, the choice of the sparsifying basis is different: we use a DCT basis $\mathbf{D}$ to model each frame in the input data. Thus,
\begin{align}
\mathbf{Y} &= \begin{pmatrix}
\mathbf{\Phi}_1 & \hdots & \mathbf{\Phi}_T
\end{pmatrix}
\begin{pmatrix}
\mathbf{D \alpha}_1 &
\hdots &
\mathbf{D \alpha}_T
\end{pmatrix}^T \\
&= \begin{pmatrix}
\mathbf{\Phi}_1 \mathbf{D} & \hdots & \mathbf{\Phi}_T \mathbf{D}
\end{pmatrix}
\begin{pmatrix}
\mathbf{\alpha}_1 &
\hdots &
\mathbf{\alpha}_T
\end{pmatrix}^T
\label{eq:sourceSepModel}
\end{align}
Thus, the effective dictionary matrix in our case is
\begin{equation}
\mathbf{\Psi} = \begin{pmatrix}
\mathbf{\Phi}_1 \mathbf{D} & \mathbf{\Phi}_2 \mathbf{D} & \hdots & \mathbf{\Phi}_T \mathbf{D}
\end{pmatrix}
\end{equation}

Given a measurement $\mathbf{Y}$, we recover the input $\{\mathbf{X}_i\}_{i=1}^{T}$ through the DCT coefficients $\mathbf{\alpha}$ by solving the optimization problem
\begin{equation}
\min_{\mathbb{\alpha}} \|\mathbb{\alpha}\|_1 \text{ subject to } \mathbf{Y} = \mathbf{\Psi} \mathbf{\alpha},\ \mathbf{\alpha} = \begin{pmatrix}
\mathbf{\alpha}_1 &
\mathbf{\alpha}_2 &
\hdots &
\mathbf{\alpha}_T
\end{pmatrix}^T
\label{eq:sourceSepOpt}
\end{equation}
In our implementation we used the \texttt{l1\_ls}~\cite{Koh07l1ls} solver for solving the convex optimization problem
\begin{equation}
\min_{\mathbb{\alpha}} \|\mathbb{\alpha}\|_1 + \lambda \left\| \mathbf{Y} - \mathbf{\Psi} \mathbf{\alpha} \right\|_2,\ \mathbf{\alpha} = \begin{pmatrix}
\mathbf{\alpha}_1 &
\mathbf{\alpha}_2 &
\hdots &
\mathbf{\alpha}_T
\end{pmatrix}^T
\label{eq:sourceSepOptRel}
\end{equation}
that is equivalent to the problem in Eq.~\ref{eq:sourceSepOpt} \cite{Foucart2013}.

\section{Results: Video Compressed Sensing}
We start with testing the proposed framework on two synthetic images that are known to have very low sparsity. These are $20 \times 20$ images, with only 3 out of the 400 DCT coefficients set to non--zero values. The results, with RRMSE errors of the order of $10^{-5}$, for these are shown in Fig.~\ref{fig:syn}. The results are similar for Gaussian sensing matrices and $[0, 1]$--uniformly random matrices.
\begin{figure}[!th]
\fourTwoAcross{pics/syn1in}{pics/syn2in}{pics/syn1est}{pics/syn2est}
\caption{Synthetic image results: (a),~(b)~Input images, (c),~(d)~Estimates}
\label{fig:syn}
\end{figure}

Next, we test on two video frames that are very similar, with Gaussian random matrices. The RRMSE errors are around 0.0019 for each image. The results are shown in Fig.~\ref{fig:realGauss}.
\begin{figure}
\fourTwoAcross{pics/realGauss1in}{pics/realGauss2in}{pics/realGauss1est}{pics/realGauss2est}
\caption{Real images, Gaussian matrices: (a),~(b)~Input images, (c),~(d)~Estimates}
\label{fig:realGauss}
\end{figure}

Next, with $[0, 1]$--uniformly random diagonal matrices, the RRMSE errors are around 0.0036 for each image. The results are shown in Fig.~\ref{fig:realUnif}.
\begin{figure}
\fourTwoAcross{pics/realUnif1in}{pics/realUnif2in}{pics/realUnif1est}{pics/realUnif2est}
\caption{Real images, uniform matrices: (a),~(b)~Input images, (c),~(d)~Estimates}
\label{fig:realUnif}
\end{figure}
Seeing the above results, one notices that there is very little to no ghosting, that is, appearance of features from one image into the other, in the output images even when the images are very close to each other. This is a very desirable property in any algorithm that separates images from compressed video.

To evaluate how this works for multiple images, we try separating three images with uniform matrices. See Fig.~\ref{fig:threeUnif}. Here, we notice ghosting happening in the third frame. However, with better--designed sensing matrices, one can think of getting rid of this effect. The RRMSE errors here are worse, around 0.005 for each image.
\begin{figure}[!h]
\sixThreeAcross{pics/threeImage1in}{pics/threeImage2in}{pics/threeImage3in}{pics/threeImage1est}{pics/threeImage2est}{pics/threeImage3est}{}
\caption{Separating three images, uniform matrices. Up:~Input images, Down:~Estimates}
\label{fig:threeUnif}
\end{figure}

To simulate sudden changes, we run the optimization with two very different input images. We can separate these well, as is shown in Fig.~\ref{fig:realSuddenChangeUnif}.
\begin{figure}
\fourTwoAcross{pics/suddenChange1in}{pics/suddenChange2in}{pics/suddenChange1est}{pics/suddenChange2est}
\caption{Real images with sudden change, uniform matrices: (a),~(b)~Input images, (c),~(d)~Estimates}
\label{fig:realSuddenChangeUnif}
\end{figure}

\section{Our Approach: Sensing Matrix Optimization}
Unlike previous work, we aim to optimize the sensing matrix for coherence directly. As a start, we try to optimize with gradient descent. This requires us to compute analytical expressions for the coherence and its derivatives with respect to the diagonals of the matrices $\mathbf{\Phi}_t$. We will call the index varying from $1$ to $T$ as $\mu$ or $\nu$, and the index varying from $1$ to $n$ as $\alpha$, $\beta$ or $\gamma$. The $\mu^\text{th}$ block of $\mathbf{\Phi}$ is thus $\mathbf{\Phi}_\mu$. Let the $\beta^\text{th}$ diagonal element of $\mathbf{\Phi}_\mu$ be $\phi_{\mu\beta}$. Define the $\alpha^\text{th}$ column of $\mathbf{D}^T$ to be $\mathbf{d}_\alpha$. Thus, the Gram matrix $\mathbf{\tilde{M}} = \Psi^T \Phi^T \Phi \Psi$ has the block structure
\begin{align}
\tilde{\mathbf{M}}_{\mu \nu} &= \mathbf{D}^T \mathbf{\Phi}_\mu^T \mathbf{\Phi}_\nu \mathbf{D} \\
%&= \begin{pmatrix}
%\mathbf{d}_1 & \mathbf{d}_2 & \hdots & \mathbf{d}_n
%\end{pmatrix}
%\begin{pmatrix}
%\phi_{\mu 1} \phi_{\nu 1} & \hdots & 0 \\
%\vdots & \ddots & \vdots \\
%0 & \hdots & \phi_{\mu n} \phi_{\nu n}
%\end{pmatrix}
%\begin{pmatrix}
%\mathbf{d}_1^T \\
%\mathbf{d}_2^T \\
%\vdots \\
%\mathbf{d}_n^T
%\end{pmatrix} \\
%&= \begin{pmatrix}
%\mathbf{d}_1 & \mathbf{d}_2 & \hdots & \mathbf{d}_n
%\end{pmatrix}
%\begin{pmatrix}
%\phi_{\mu 1} \phi_{\nu 1} \mathbf{d}_1^T \\
%\phi_{\mu 2} \phi_{\nu 2} \mathbf{d}_2^T \\
%\vdots \\
%\phi_{\mu n} \phi_{\nu n} \mathbf{d}_n^T
%\end{pmatrix} \\
&= \sum_{\alpha = 1}^{n} \phi_{\mu \alpha} \phi_{\nu \alpha} \mathbf{d}_\alpha \mathbf{d}_\alpha^T
\label{eq:mTilde}
\end{align}
%
Normalization of columns of the Gram matrix needs addition over blocks of additions of squared elements along block columns. In other words, if $\xi_{\nu \gamma}$ is the norm of the $\gamma^\text{th}$ column in the $\nu^\text{th}$ column block, we have
\begin{align}
\xi_{\nu \gamma}^2 &= \sum_{\mu = 1}^{T} \left(\mathbf{\tilde{M}}_{\mu \nu}^T \mathbf{\tilde{M}}_{\mu \nu} \right)_{\gamma \gamma}  \\
&= \sum_{\mu = 1}^{T} \left[ \sum_{\alpha = 1}^{n} \sum_{\delta = 1}^{n} \phi_{\mu \alpha} \phi_{\nu \alpha} \phi_{\mu \delta} \phi_{\nu \delta} \mathbf{d}_\alpha \mathbf{d}_\alpha^T \mathbf{d}_\delta \mathbf{d}_\delta^T \right]_{\gamma \gamma}
\label{eq:normalization}
\end{align}
%
The chosen DCT basis is an orthogonal basis, and hence this sum simplifies. Defining the $\beta^\text{th}$ element of $\mathbf{d}_\alpha$ to be $d_\alpha(\beta)$, this yields the $\beta\gamma^\text{th}$ element of $\mathbf{M}_{\mu \nu}$:
\begin{align}
M_{\mu \nu}(\beta\gamma) &= \frac{\sum_{\alpha = 1}^{n} \phi_{\mu \alpha} \phi_{\nu \alpha} d_\alpha (\beta) d_\alpha (\gamma)}{\sqrt{\sum_{\mu = 1}^{T} \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 \phi_{\nu \alpha}^2 d_\alpha^2(\gamma)}}
\label{eq:normGramEntries}
\end{align}

We need the maximum absolute off--diagonal value in this matrix. We take a soft approximation to it, parameterized by $\theta$, to get the squared coherence $\mathcal{C}(\mathbf{\Phi})$:
\begin{multline}
\mathcal{C} = \frac{1}{\theta} \log \left[ \sum_{\mu=1}^{T} \sum_{\beta=1}^{n} \left( \sum_{\nu=1}^{\mu-1} \sum_{\gamma=1}^{n} e^{\theta M_{\mu \nu}^2(\beta\gamma)} +
%\right. \right. \\ \left. \left.
\sum_{\gamma=1}^{\beta - 1} e^{\theta M_{\mu \mu}^2(\beta\gamma)} \right) \right]
\label{eq:cohSquareSoftMax}
\end{multline}

In the above, the first term corresponds to all ($\mu > \nu$) blocks that are `below' the block diagonal. Here, we consider all terms in the given block for the maximum. The second term corresponds to ($\mu = \nu$) blocks on the block diagonal. Here, we consider only consider ($\beta > \gamma$) below-diagonal elements for the maximum. Next, defining the numerator of the expression for $M_{\mu \nu}(\beta\gamma)$ as $\chi_{\mu \nu}(\beta\gamma)$ and $\uparrow_{\mu \epsilon}$ as the Kronecker delta function:
\begin{align}
\frac{d\chi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} &= d_\epsilon (\beta) d_\epsilon (\gamma) \left( \phi_{\mu \epsilon} \uparrow_{\nu \delta} + \uparrow_{\mu \delta} \phi_{\nu \epsilon} \right) \\
\frac{d\xi_{\nu \gamma}}{d\phi_{\delta \epsilon}} &= \frac{\phi_{\nu \epsilon} d_\epsilon^2(\gamma)}{\xi_{\nu \gamma}} \left( \phi_{\delta \epsilon} \phi_{\nu \epsilon} + \uparrow_{\nu \delta} \sum_{\mu = 1}^{T} \phi_{\mu \epsilon}^2 \right)
\label{eq:chiXiDers}
\end{align}

This allows us to calculate the derivatives of $M_{\mu \nu}(\beta\gamma)$ with respect to $\phi_{\delta \epsilon}$, and therefore, the derivatives of the squared soft-max function $\mathcal{C}(\mathbf{\Phi})$. Using these, we do gradient descent with adaptive step--size and use a multi--start strategy to combat the non--convexity of the problem.

\section{Results: Sensing Matrix Optimization}
As of now, gradient descent has been implemented, and test runs show that the method indeed decreases coherence, to a minimum of 0.5 yet. However, it is very apparent that the problem is highly non--convex, and every run of the optimization has us stuck in a new local minimum. However, testing all parts of the code hasn't been finished yet, so we do not quote results for this part.

\section{Conclusion and Future Work}
The image results from the source--separation algorithm give us confidence about the fact that this method works. The current failures of the method point towards designing better sensing matrices. The immediate task, thus, is to get results from gradient descent. Once (if) gradient descent gives satisfactory results, we need fit our sensing matrix constraints into the problem formulation of~\cite{Duarte200907} and design optimal sensing matrices through that framework. Matrices designed that way will be compared to the outputs from gradient descent. Once we get our optimal matrices, they will be tested in our source--separation framework, first on synthetic data which is exactly sparse and then on real video data. Results from the video case will be compared to the method in~\cite{CAVE_0311} to see if the source--separation approach indeed performs better.

\noindent Source--separation and gradient descent live in the Bitbucket repository at \href{https://bitbucket.org/alankarkotwal/coded-sourcesep}{\texttt{alankarkotwal/coded-sourcesep}}. Some other code used in the start of the project lives in \href{https://bitbucket.org/alankarkotwal/compressed-segmentation}{\texttt{alankarkotwal/compressed-segmentation}} (access on request).

\bibliographystyle{ieeetr}
\nocite{*}
\bibliography{ref}
\end{document}