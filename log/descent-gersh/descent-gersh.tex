\documentclass[11pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{hyperref}

\title{Optimizing Sensing Matrices via Gershgorin Radii}
\author{Alankar Kotwal}

\renewcommand{\exp}[1]{e^{#1}}

\begin{document}
\maketitle

\section{Definitions}
Let $\Phi$ be a sensing matrix which is a concatenation of $T$ diagonal matrices along the columns:
\begin{align*}
\Phi = \begin{pmatrix}
\phi_1 & \phi_2 & \hdots & \phi_T
\end{pmatrix}
\end{align*}

\noindent We will call the index varying from $1$ to $T$ as $\mu$ or $\nu$, and the index varying from $1$ to $n$ as $\alpha$, $\beta$ or $\gamma$. The $\mu^\text{th}$ block of $\Phi$ is thus $\phi_\mu$. Let the $\beta^\text{th}$ diagonal element of $\phi_\mu$ be $\phi_{\mu\beta}$ \\

\noindent Let $\Psi$ be the concatenation of the DCT basis $D$ along the diagonal. Thus,
\begin{align*}
\Psi = \begin{pmatrix}
D & 0 & \hdots & 0 \\
0 & D & \hdots & 0 \\
\vdots & \vdots & \ddots & \vdots \\
0 & 0 & \hdots & D
\end{pmatrix}
\end{align*}

\noindent Define the $\alpha^\text{th}$ column of $D^T$ to be $d_\alpha$.

\section{Computing Maximum Gershgorin Radius}

\noindent The effective dictionary $\Phi \Psi$ is
\begin{align*}
\Phi \Psi = \begin{pmatrix}
\phi_1 D & \phi_2 D & \hdots & \phi_T D
\end{pmatrix}
\end{align*}

\noindent Thus, the Gram matrix $\tilde{M} = \Psi^T \Phi^T \Phi \Psi$ has the block structure
\begin{align*}
\tilde{M}_{\mu \nu} &= D^T \phi_\mu^T \phi_\nu D \\
&= D^T \phi_\mu \phi_\nu D \\
&= \begin{pmatrix}
d_1 & d_2 & \hdots & d_n
\end{pmatrix}
\begin{pmatrix}
\phi_{\mu 1} \phi_{\nu 1} & 0 & \hdots & 0 \\
0 & \phi_{\mu 2} \phi_{\nu 2} & \hdots & 0 \\
\vdots & \vdots & \ddots & \vdots \\
0 & 0 & \hdots & \phi_{\mu n} \phi_{\nu n}
\end{pmatrix}
\begin{pmatrix}
d_1^T \\
d_2^T \\
\vdots \\
d_n^T
\end{pmatrix} \\
&= \begin{pmatrix}
d_1 & d_2 & \hdots & d_n
\end{pmatrix}
\begin{pmatrix}
\phi_{\mu 1} \phi_{\nu 1} d_1^T \\
\phi_{\mu 2} \phi_{\nu 2} d_2^T \\
\vdots \\
\phi_{\mu n} \phi_{\nu n} d_n^T
\end{pmatrix} \\
&= \sum_{\alpha = 1}^{n} \phi_{\mu \alpha} \phi_{\nu \alpha} d_\alpha d_\alpha^T
\end{align*}

\noindent The $\beta\gamma^\text{th}$ element of $\tilde{M}_{\mu \nu}$ is
\begin{align*}
\tilde{M}_{\mu \nu}(\beta\gamma) &= \sum_{\alpha = 1}^{n} \phi_{\mu \alpha} \phi_{\nu \alpha} d_\alpha (\beta) d_\alpha (\gamma)
\end{align*}

\noindent Now we need to normalize the columns of $\Phi \Psi$. Squared column norms are diagonal elements of $\tilde{M}_{\mu \nu}$. So the product of the squared norms of the $\beta^\text{th}$ column of the $\mu^\text{th}$ block and the $\gamma^\text{th}$ column of the $\nu^\text{th}$ block is
\begin{align*}
\xi_{\mu \nu}^2 (\beta \gamma) &= \left( \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right) \left( \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) \right)
\end{align*}

\noindent Let the normalized Gram matrix be $M$. Thus, following the same conventions as above, 
\begin{align*}
M_{\mu \nu}(\beta\gamma) &= \frac{\sum_{\alpha = 1}^{n} \phi_{\mu \alpha} \phi_{\nu \alpha} d_\alpha (\beta) d_\alpha (\gamma)}{\sqrt{\left( \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right) \left( \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) \right)}}
\end{align*}

\noindent Now, given a particular $s$--cardinality subset $S$ of indices from 1 to $nT$, we want to evaluate dot products of (normalized) columns of $\Phi \Psi$. Let us call the sensing matrix with normalized columns $A$. Restricting this to the columns specified by $S$ reduces us to $A_S$. Note that 
\begin{align*}
\left[ A_S^T A_S - I \right]_{ij} &= \left[ A^T A - I \right]_{S_i S_j} \\
&= M_{\mu \nu} \left( \beta \gamma \right) - \mathbf{1}_{S_i = S_j}
\end{align*}
where we calculate the $\mu$, $\nu$, $\beta$, $\gamma$ arguments for the $M$ by the appropriate column number: $\mu^S_{i} = \text{floor}(S_i/n)$ and $\beta^S_{i} = S_i \text{ mod } n$. Call $M_{\mu^S_i \mu^S_j} \left( \beta^S_i \beta^S_j \right)$ as $\omega^S_{ij}$. This is symmetric in the arguments $i$ and $j$. \\

\noindent We now want to calculate row absolute sums for the matrix $A_S^T A_S - I$. Since by definition $M_{\mu \mu} (\beta \beta) = 1$,
\begin{align*}
\sum_{j} |\omega^S_{ij} - \mathbf{1}_{S_i = S_j}| = \sum_{j \neq i} |\omega^S_{ij}|
\end{align*}

\noindent Finally, using the square soft-max function, we get the maximum row absolute sum, the Gershgorin radius and our objective function $\mathcal{C}$ to be
\begin{align*}
\mathcal{C}\left( \Phi \right) = \frac{1}{\theta} \log \left[ \sum_S \sum_i \exp{\theta \sum_{j \neq i} |\omega^S_{ij}|} \right]
\end{align*}

\newpage
\section{Derivatives of $\mathcal{C}$}
We note that the $\mathcal{C}$ computed in the section above is a function of $\Phi$. We differentiate $\mathcal{C}$ with respect to $\phi_{\delta \epsilon}$.
\begin{align*}
\frac{d\mathcal{C}(\Phi)}{d\phi_{\delta \epsilon}} = \frac{1}{\exp{\theta \mathcal{C}(\Phi)}} \left[ \sum_S \sum_i \left[ \exp{\theta \sum_{j \neq i} |\omega^S_{ij}|} \sum_{j \neq i} \left[ \text{sign}\left( \omega^S_{ij} \right) \frac{d\omega^S_{ij}}{d \phi_{\delta \epsilon}} \right] \right] \right]
\end{align*}

\noindent Next, we calculate the derivatives in the above equation, ${dM_{\mu \nu}(\beta\gamma)}/{d\phi_{\delta \epsilon}}$ (assume correct definitions for the $\mu$, $\nu$, $\beta$ and $\gamma$ as above). Define the numerator of the expression for $M_{\mu \nu}(\beta\gamma)$ as $\chi_{\mu \nu}(\beta\gamma)$, and thus, $M_{\mu \nu}(\beta\gamma) = \chi_{\mu \nu}(\beta\gamma)/\xi_{\mu \nu}(\beta\gamma)$. Clearly,
\begin{align*}
\frac{dM_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} = \frac{\xi_{\mu \nu}(\beta\gamma) \frac{d\chi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} - \chi_{\mu \nu}(\beta\gamma)\frac{d\xi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}}}{\xi_{\mu \nu}(\beta\gamma)^2}
\end{align*}

\noindent Next,
\begin{align*}
\frac{d\chi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} &= \frac{d}{d\phi_{\delta \epsilon}} \sum_{\alpha = 1}^{n} \phi_{\mu \alpha} \phi_{\nu \alpha} d_\alpha (\beta) d_\alpha (\gamma) \\
&= \sum_{\alpha = 1}^{n} d_\alpha (\beta) d_\alpha (\gamma) \frac{d}{d\phi_{\delta \epsilon}} \left( \phi_{\mu \alpha} \phi_{\nu \alpha} \right)
\end{align*}

\noindent Notice that a term in the above summation can be non--zero only if $\alpha = \epsilon$. Thus,
\begin{align*}
\frac{d\chi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} &= d_\epsilon (\beta) d_\epsilon (\gamma) \frac{d}{d\phi_{\delta \epsilon}} \left( \phi_{\mu \epsilon} \phi_{\nu \epsilon} \right) \\
&= d_\epsilon (\beta) d_\epsilon (\gamma) \left( \phi_{\mu \epsilon} \frac{d\phi_{\nu \epsilon}}{d\phi_{\delta \epsilon}} + \frac{d\phi_{\mu \epsilon}}{d\phi_{\delta \epsilon}} \phi_{\nu \epsilon} \right)
\end{align*}

\noindent Now, notice that ${d\phi_{\mu \epsilon}}/{d\phi_{\delta \epsilon}}$ is non--zero only if $\mu = \epsilon$. Denote by $\uparrow_{\mu \epsilon}$ the Kronecker delta function, which is 1 only if $\mu = \epsilon$, 0 otherwise. Then,
\begin{align*}
\frac{d\chi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} &= d_\epsilon (\beta) d_\epsilon (\gamma) \left( \phi_{\mu \epsilon} \uparrow_{\nu \delta} + \uparrow_{\mu \delta} \phi_{\nu \epsilon} \right)
\end{align*}

\noindent Next, 
\begin{align*}
\frac{d\xi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} &= \frac{d}{d\phi_{\delta \epsilon}} \sqrt{\left( \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right) \left( \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) \right)} \\
&= \frac{1}{2 \xi_{\mu \nu}(\beta\gamma)} \frac{d}{d\phi_{\delta \epsilon}} \left( \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) \right) \\
&= \frac{1}{2 \xi_{\mu \nu}(\beta\gamma)} \left[ \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \frac{d}{d\phi_{\delta \epsilon}} \left( \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) \right) + \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) \frac{d}{d\phi_{\delta \epsilon}} \left( \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right) \right] \\
\end{align*}

\noindent Again, a term one of the above summations is non--zero only if $\alpha$ or $\tau$ is the same as $\epsilon$. Thus,
\begin{align*}
\frac{d}{d\phi_{\delta \epsilon}} \left( \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right) &= 2 \phi_{\mu \epsilon} d_\epsilon^2(\beta) \uparrow_{\mu \delta}
\end{align*}

\noindent Thus, 
\begin{align*}
\frac{d\xi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} &= \frac{1}{2 \xi_{\mu \nu}(\beta \gamma)} \left[ 2 \phi_{\mu \epsilon} d_\epsilon^2(\beta) \uparrow_{\mu \delta} \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) + 2 \phi_{\nu \epsilon} d_\epsilon^2(\gamma) \uparrow_{\nu \delta} \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right] \\
&= \frac{1}{\xi_{\mu \nu}(\beta \gamma)} \left[ \phi_{\mu \epsilon} d_\epsilon^2(\beta) \uparrow_{\mu \delta} \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) + \phi_{\nu \epsilon} d_\epsilon^2(\gamma) \uparrow_{\nu \delta} \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right]
\end{align*}

\noindent This completes the calculation of derivatives.

\section{Implementation}
The implementation will live in the \texttt{src/descent-gersh} directory in the Bitbucket repository \href{https://bitbucket.org/alankarkotwal/coded-sourcesep}{here}. All function names are self--explanatory.

\end{document}