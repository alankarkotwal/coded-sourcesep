\documentclass[11pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath,amsfonts,amssymb,mathtools}

\title{Derivation of a Gini-index-based bound for compressed sensing}
\author{Alankar Kotwal}

\setlength{\parindent}{0pt}
\newcommand{\norm}[2]{\|#1\|_#2}
\newcommand{\oversymbol}[2]{\stackrel{\mathclap{\normalfont\mbox{#1}}}{#2}}

\begin{document}
\maketitle

\section{The bound}
Consider the general compressed sensing scenario
\begin{equation}
y = Ax + w
\label{eq:compSens}
\end{equation}
We wish to derive an error bound based on the Gini Index $s(x)$ for sparse recovery. Assume that the vector $x$ is $s$-sparse, that is, $\|x\|_0 = s$. Then, with the basis pursuit solver 
\begin{equation}
\hat{x} = \arg \min_{\|y - Az\|_\diamond \leq \epsilon} \|z\|_1
\label{eq:basisPursuit}
\end{equation}

Let $h = \hat{x} - x$ and $\text{supp}(x) = S$. Then
\begin{align}
\norm{x+h}{1} &\leq \norm{x}{1} \\
\implies \sum_{i} |x_i + h_i| &\leq \sum_{i} |x_i| \\
\implies \sum_{i \in S} |x_i + h_i| + \sum_{i \in S^c} |h_i| &\leq \sum_{i \in S} |x_i| \\
\implies \sum_{i \in S} |x_i| - \sum_{i \in S} |h_i| + \sum_{i \in S^c} |h_i| &\leq \sum_{i \in S} |x_i| \\
\implies \sum_{i \in S^c} |h_i| &\leq \sum_{i \in S} |h_i| \\
\implies \norm{h_{S^c}}{1} &\leq \norm{h_S}{1} \\
\implies \norm{h}{1} &\leq 2\norm{h_S}{1}
\label{eq:l1normRes}
\end{align}

Next, we have 
\begin{align}
\norm{Ah}{\diamond} &\leq \norm{y-Ax}{\diamond} + \norm{y-A\hat{x}}{\diamond} \\
&\leq \norm{w}{\diamond} + \epsilon \\
&\leq 2\epsilon
\label{eq:smallAh}
\end{align}

Therefore
\begin{align}
1 &\leq \frac{2\epsilon}{\norm{Ah}{\diamond}} \\
\implies \norm{h}{*} &\leq \frac{2\epsilon}{\frac{\norm{Ah}{\diamond}}{\norm{h}{*}}}
\label{eq:boundForOneH}
\end{align}

For an upper bound in general, we must minimize the denominator over all possible $h$. Now, the Gini index $s(x)$ of the $N$-dimensional $x$ is
\begin{align}
s(h) &= 1 - 2\sum_{i=1}^{N} \frac{\bar{h}_k}{\norm{h}{1}} \left( \frac{N-k+1/2}{N}\right) \\
&= 1 - 2\sum_{i=1}^{N} \frac{\bar{h}_k}{\norm{h}{1}} \left( \frac{N+1/2}{N}\right) + 2 \sum_{i=1}^{N} \frac{\bar{h}_k}{\norm{h}{1}} \left( \frac{k}{N}\right) \\
&= 1 - 2 \left( 1 + \frac{1}{2N} \right) + 2 \sum_{i=1}^{N} \frac{\bar{h}_k}{\norm{h}{1}} \left( \frac{k}{N}\right) \\
&= 2 \sum_{i=1}^{N} \frac{\bar{h}_k}{\norm{h}{1}} \left( \frac{k}{N}\right) - 1 - \frac{1}{N}
\label{eq:gini}
\end{align}
where $\bar{h}$ is the vector obtained on taking the absolute values of the elements of $h$ and arranging them in an increasing order. Therefore, $s(x) \geq s$ implies
\begin{align}
2 \sum_{i=1}^{N} \frac{\bar{h}_k}{\norm{h}{1}} \left( \frac{k}{N}\right) - 1 - \frac{1}{N} &\geq s \\
\implies \bar{s}(h) = \sum_{i=1}^{N} \frac{k\bar{h}_k}{N\norm{h}{1}} &\geq 2(s + 1 + 1/N) \\
&= \bar{s}
\end{align}

Clearly, $\bar{s}(h)$ is an increasing function of $s(h)$. A sparse vector has a large value of $s(h)$. We therefore need to find a lower bound on how sparse a measurement vector can be: so, we need to find an $h$-independent lower bound on $\bar{s}(h)$. \\

Let the vector of numbers from 1 to $N$, $[1, 2, ..., N]^T$, be $n$. We then have
\begin{align}
\bar{s}(h) &= \frac{n^T \bar{h}}{N\norm{h}{1}} \\
&\oversymbol{(a)}{\geq} \frac{n^T \bar{h}}{2N\norm{h_S}{1}} \\
&\oversymbol{(b)}{=} \frac{\bar{n}^T \tilde{h}}{2N\norm{h_S}{1}} \\
&\oversymbol{(c)}{\geq} \frac{\bar{n}_S^T \tilde{h}_S}{2N\norm{h_S}{1}} \\
&\oversymbol{(d)}{\geq} \frac{\mathbf{1}^T \tilde{h}_S}{2N\norm{h_S}{1}} \\
&= \frac{1}{2N}
\end{align}
where (a) comes from Eq.~\ref{eq:l1normRes}, (b) rearranges the elements of $n$ to place $k$ at the position of the $k^\text{th}$ absolute largest element of $h$, thus keeping $n^T \bar{h}=\bar{n}^T h$, and $\tilde{h}$ is the vector of absolute values of elements of $h$. (c) restricts the support of the vectors in the numerator to $S$, which decreases the dot product because all elements in the dot product are positive, and (d) replaces the elements of $\bar{n}_S$ by ones. This can possibly be improved my somehow using the fact that each number from 1 to $N$ can appear only once in $\bar{n}_S$. \\

The final bound, then, is
\begin{align}
1 &\leq \frac{2\epsilon}{\norm{Ah}{\diamond}} \\
\implies \norm{h}{*} &\leq \frac{2\epsilon}{\psi_{*\diamond}(A)}
\label{eq:finalBound}
\end{align}
where
\begin{equation}
\psi_{*\diamond}(A) = \min_{\bar{s}(h) \geq 1/2N} \frac{\norm{Ah}{\diamond}}{\norm{h}{*}}
\label{eq:psiDef}
\end{equation}

\section{Calculating $\psi_{*\diamond}(A)$}
\begin{align}
\psi_{*\diamond}(A) &= \min_{\bar{s}(h) \geq 1/2N} \frac{\norm{Ah}{\diamond}}{\norm{h}{*}} \\
&= \min_{\sum_{i=1}^{N} \frac{k\bar{h}_k}{N\norm{h}{1}} \geq 1/2N} \frac{\norm{Ah}{\diamond}}{\norm{h}{*}}
\end{align}
To relax the ordering imposed by the presence of $\bar{h}$, we notice that arranging the elements of $\tilde{h}$ is equivalent to permuting the columns of $A$: we therefore split the minimization into two pieces: one assuming that the elements of $h$ are already arranged in an absolutely increasing manner, and another that permutes the columns of A:
\begin{align}
\psi_{*\diamond}(A) &= \min_{\zeta \in \text{perm}(1, N)} \min_{\sum_{i=1}^{N} \frac{kh_k}{N\norm{h}{1}} \geq 1/2N,\ |h_1| \leq |h_2| \leq ... \leq |h_N|} \frac{\norm{A^\zeta h}{\diamond}}{\norm{h}{*}}
\end{align}
This is combinatorial in the number of columns of $A$. Is it NP-hard to compute? I don't know, but I sure feel so.

\end{document}