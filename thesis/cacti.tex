\chapter{Optimizing for the CACTI Camera} \label{cacti}

\lettrine[lines=2]{\scalebox{2}{I}}{nspired} by the events of the previous chapter, we turn to another compressive camera, the coded aperture compressive temporal imaging system introduced in \cite{Llull13a, Llull13b}, to attempt to design masks better than random. The results of this study constitute the beginning of an important lesson in compressed sensing. 

\section{Introducing the CACTI camera}

The principal idea behind the design of the CACTI camera, in the words of the authors of \cite{Llull13a} is using "mechanical translation of a coded aperture for code division multiple access (CDMA) compression of video". The setup is shown in Fig. 1 of \cite{Llull13a}. 

%\begin{figure}[!h]
%\centering
%\includegraphics[scale=0.3]{pics/cactiSetup}
%\caption{The sensing scheme used in the CACTI camera}
%\label{fig:cactiSetup}
%\end{figure}

Linear coded combinations appear here as well, allowing us to use our previous framework in reconstructing and in optimizing codes. To recapitulate, $T$ vectorized input frames $\{x_i\}_{i=1}^{T}$ are combined so that the vectorized output $y$ appears as a coded combination (dictated by the `sensing matrices' $\phi_i$) of the inputs. However, this time, the sensing matrices $\phi_i$ are not independent across $i$: the mechanical translation amounts to a fixed circular shift in the elements of $\phi_i$, dictated by the set mechanical translations. The sensing framework here is, with $\phi_i$ being the $i^\text{th}$ circular shift, 
\begin{equation}
y = \sum_{i=1}^{T} \phi_i x_i
\label{eq:cactiSensing}
\end{equation}

Again, the sensing framework constrains the $\phi_i$ to be a diagonal matrix, with the code elements on the diagonal. To recover the input $\{x_i\}_{i=1}^{T}$ through the DCT coefficients $\alpha$ given a measurement $y$, as before, we solve the optimization problem
\begin{align}
\min_{\alpha} \|\alpha\|_1 \text{ subject to } y &= \Phi \Psi \mathbf{\alpha}, \\
\alpha &= \begin{pmatrix}
\alpha_1 &
\alpha_2 &
\hdots &
\alpha_T
\end{pmatrix}^T, \\ 
\Phi &= \begin{pmatrix}
\phi_{11} & 0 & \hdots & 0 & \hdots & \hdots & \phi_{T1} & 0 & \hdots & 0 \\
0 & \phi_{12} & \hdots & 0 & \hdots & \hdots & 0 & \phi_{T2} & \hdots & 0 \\
\vdots & \vdots & \ddots & \vdots & \ddots & \ddots & \vdots & \vdots & \ddots & \vdots \\
0 & 0 & \hdots & \phi_{1n} & \hdots & \hdots & 0 & 0 & \hdots & \phi_{Tn}
\end{pmatrix}, \\
\Psi &= \begin{pmatrix}
D & 0 & \hdots & 0 \\
0 & D & \hdots & 0 \\
\vdots & \vdots & \ddots & \vdots \\
0 & 0 & \hdots & D
\end{pmatrix}
\label{eq:cactiSourceSepOpt}
\end{align}
Again, we use the \texttt{CVX}~\cite{cvx} solver for solving the convex optimization problem in Eq.~\ref{eq:cactiSourceSepOpt}.

\section{Optimizing codes}
We follow a policy similar to the one in the previous chapter for optimizing codes. To minimize coherence, we write down the expression for the coherence of the joint dictionary $\Phi \Psi$:
\begin{align}
\Phi \Psi = \begin{pmatrix}
\phi_1 D & \phi_2 D & \hdots & \phi_T D
\end{pmatrix}
\end{align}

It is important, here, to note where each $\phi_{ij}$ came from. For this purpose, let us define a vector $\phi$ of code elements with elements $\phi^k$ for $k$ going from 1 to $n$. The $i^\text{th}$ permutation, therefore, takes $\phi$ to $\phi_i$. Clearly, if we apply the permutation $i$ to the vector $(1\ 2\ 3\ \hdots\ n)^T$ and call the resultant vector $p_i$, the $j^\text{th}$ element of $p_i$ will denote which element of $\phi$ got circularly shifted to $\phi_{ij}$. With this definition, therefore, we have $\phi_{ij} = \phi^{p_{ij}}$. 

In a similar way to the previous chapter, following the same definition, we write the normalized dot product between the $\beta^\text{th}$ column of the $\mu^\text{th}$ block and the $\gamma^\text{th}$ column of the $\nu^\text{th}$ block as
\begin{align}
M_{\mu \nu}(\beta\gamma) &= \frac{\sum_{\alpha = 1}^{n} \phi_{\mu \alpha} \phi_{\nu \alpha} d_\alpha (\beta) d_\alpha (\gamma)}{\sqrt{\left( \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right) \left( \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) \right)}} \\
&= \frac{\sum_{\alpha = 1}^{n} \phi^{p_{\mu \alpha}} \phi^{p_{\nu \alpha}} d_\alpha (\beta) d_\alpha (\gamma)}{\sqrt{\left( \sum_{\alpha = 1}^{n} \phi^{p_{\mu \alpha}2} d^2_\alpha (\beta) \right) \left( \sum_{\tau = 1}^{n} \phi^{p_{\nu \tau}2} d^2_\tau (\gamma) \right)}}
\end{align}

With the numerator of the above expression renamed to $\chi_{\mu \nu}(\beta\gamma)$ and the denominator renamed to $xi_{\mu \nu}(\beta\gamma)$, we write, 
\begin{align}
\frac{d\chi_{\mu \nu}(\beta\gamma)}{d\phi^{p_{\delta \epsilon}}} &= d_\epsilon (\beta) d_\epsilon (\gamma) \left( \phi_{\mu \epsilon} \uparrow_{\nu \delta} + \uparrow_{\mu \delta} \phi_{\nu \epsilon} \right) \\
&= d_\epsilon (\beta) d_\epsilon (\gamma) \left( \phi^{p_{\mu \epsilon}} \uparrow_{\nu \delta} + \uparrow_{\mu \delta} \phi^{p_{\nu \epsilon}} \right)
\end{align}
\begin{align}
\frac{d\xi_{\mu \nu}(\beta\gamma)}{d\phi^{p_{\delta \epsilon}}} &= \frac{1}{\xi_{\mu \nu}(\beta \gamma)} \left[ \phi_{\mu \epsilon} d_\epsilon^2(\beta) \uparrow_{\mu \delta} \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) + \phi_{\nu \epsilon} d_\epsilon^2(\gamma) \uparrow_{\nu \delta} \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right] \\
&= \frac{1}{\xi_{\mu \nu}(\beta \gamma)} \left[ \phi^{p_{\mu \epsilon}} d_\epsilon^2(\beta) \uparrow_{\mu \delta} \sum_{\tau = 1}^{n} \phi^{p_{\nu \tau}2} d^2_\tau (\gamma) + \phi^{p_{\nu \epsilon}} d_\epsilon^2(\gamma) \uparrow_{\nu \delta} \sum_{\alpha = 1}^{n} \phi^{p_{\mu \alpha}2} d^2_\alpha (\beta) \right]
\end{align}

As before, we do gradient descent with adaptive step-size and use a multi-start strategy to combat the non-convexity of the problem.

\section{Experiments and results}

\subsection{Simulated data}
Here, we experiment with toy data where we can precisely control the sparsity of the input signals. Specifically, assuming a set of mechanical translations, we randomly generate $T$ $s$-sparse (in 2D DCT) $8 \times 8$ signals $\{x_i\}_{i=1}^{T}$, combine them using random and designed matrices and add noise bounded in norm to $\epsilon=10^{-5}$ to get $y$. Average RRMSE errors on doing this over a set of 100 vectors, as a function of signal sparsity and compression level $T$ are shown in Figs. \ref{fig:cactiRRMSE2}, \ref{fig:cactiRRMSE4}, and \ref{fig:cactiRRMSE6}.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{pics/cacti/736852_6681-64-2-100-fin}
\caption{Average RRMSE as a function of sparsity for $8 \times 8$ signals, sparse in 2D DCT, combined with $T = 2$. Permutations: [5, 3; 6, 8]. Errors for low sparsity are near-zero.}
\label{fig:cactiRRMSE2}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{pics/cacti/736852_6689-64-4-100-fin}
\caption{Average RRMSE as a function of sparsity for $8 \times 8$ signals, sparse in 2D DCT, combined with $T = 4$. Permutations: [7, 8; 2, 8; 6, 1; 3, 5]}
\label{fig:cactiRRMSE4}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{pics/cacti/736852_6699-64-6-100-fin}
\caption{Average RRMSE as a function of sparsity for $8 \times 8$ signals, sparse in 2D DCT, combined with $T = 6$. Permutations: [6, 7; 3, 6; 6, 2; 1, 4; 8, 3; 5, 2]}
\label{fig:cactiRRMSE6}
\end{figure}

These figures don't tell a very happy story: the optimization technique completely fails to produce any statistically significant improvement in the error over random matrices. The coherence decreases are significant: in the best case, the coherence for $T = 2$ decreases from 0.7911 to 0.3462, for $T = 4$ decreases from 0.7921 to 0.4952, and for $T = 6$ decreases from 0.9156 to 0.5430. 

What causes the algorithm to fail? The underlying assumption in this method is that the bound that RIC establishes on the maximum recovery error surface plotted against the space of sensing matrices behaves close to the actual maximum error surface (coherence further loosens up the bound). However, this might not be the case: the looser the bound gets, the more freedom the error surface has to deviate from the behavior of the bound. Then, minimizing the maximum in the bound may not correspond to minimizing the maximum in the actual error surface.

To quantify this concept, note that the $s^\text{th}$ RIC of a matrix is the following:
\begin{equation}
\delta_s = \max_{\mathcal{S} \in \{1\ ...\ n\}, k} |\lambda_k(A_\mathcal{S}^T A_\mathcal{S} - I)|
\end{equation}
We therefore plot, for the entire dataset of vectors we used to make the RRMSE plot, the absolute maximum eigenvalue of $A_\mathcal{S}^T A_\mathcal{S} - I$, where $\mathcal{S}$ is the support of the vector, for both random $A$ of the form imposed by CACTI, and designed $A$. The error in reconstructing this particular vector is bounded tighter than coherence by this absolute maximum eigenvalue. This gives us a handle on how well minimizing coherence minimizes this eigenvalue across supports, and therefore how much we lose by relaxing the RIC to coherence.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{pics/cacti/736852_6681-64-2-100-eig-fin}
\caption{Absolute maximum restricted eigenvalue boxplot as a function of sparsity for $8 \times 8$ signals, sparse in 2D DCT, with $T = 2$. Permutations: [5, 3; 6, 8]}
\label{fig:cactiEigen2}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{pics/cacti/736852_6689-64-4-100-eig-fin}
\caption{Absolute maximum restricted eigenvalue boxplot as a function of sparsity for $8 \times 8$ signals, sparse in 2D DCT, with $T = 4$. Permutations: [7, 8; 2, 8; 6, 1; 3, 5]}
\label{fig:cactiEigen4}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{pics/cacti/736852_6699-64-6-100-eig-fin}
\caption{Absolute maximum restricted eigenvalue boxplot as a function of sparsity for $8 \times 8$ signals, sparse in 2D DCT, with $T = 6$. Permutations: [6, 7; 3, 6; 6, 2; 1, 4; 8, 3; 5, 2]}
\label{fig:cactiEigen6}
\end{figure}

The $T = 2$ case is surprising: decreasing coherence over random seems to increase the values of the absolute maximum eigenvalues, which goes directly against the assumption involved in minimizing coherence. The $T = 4$ and $T = 6$ cases behave better in terms of eigenvalues, though their performance in terms of RRMSE error isn't very good. These findings point to the fact that the problem lies not only in the relaxation of the RIC to the coherence, but also in the RIC bound itself. 

A tempting thought, at this juncture, is to maximize some average of the off-diagonal dot products of columns in the effective dictionary, instead of the coherence, which is the maximum of these. This is because the coherence bound on the RIC relaxes the sum of $k-1$ off-diagonal elements to $k-1$ times the maximum, which is the coherence. Designing matrices optimizing square of these off-diagonal elements and performing simulated data experiments similar to the above produce similar RRMSE behavior: the matrices designed this way are no better than matrices designed using just coherence, and certainly no better than random matrices. 

This warrants a more detailed empirical understanding of error bounds in compressed sensing, focusing on how bounds evolve across inequalities that give rise to them. This will be the subject of the next chapter.
