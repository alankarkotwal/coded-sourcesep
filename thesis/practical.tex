\chapter{Practical Constraints in Compressed Sensing Design} \label{practical}

\lettrine[lines=2]{\scalebox{2}{O}}{ptimization} methods for matrices, whether successful or not, must consider the important issue of perturbations in sensing matrices. These arise from designed matrices being floating point numbers of high precision, while only a finite set of measurement gains might be allowed by hardware. These kinds of effects are best modeled by quantization. Physical manufacturing and aging can cause continuously varying noise in the quantized mask. We present here an analysis of effects of these perturbations. Though admittedly simple, it must be stressed that evaluating designed matrices for stability under perturbations is an important step.

\section{Quantization errors}
Quantization is a major effect in both the cameras we considered. In the camera in \cite{Hitomi2011}, the quantization arises from the fact that exposure times, which are used to generate the codes that linearly combine input frames, can take values that are integer multiples of the time resolution of the camera shutter. In the CACTI camera \cite{Llull13a}, the mask is realized as a transparency with space-varying transmission. Manufacturing such transmission is, again, limited to the precision of the instrument that manufactures the mask. In other systems like the Rice single pixel camera \cite{Duarte2008}, compressive sampling is realized by turning on and off mirrors, which correspond to a binary encoding. We test the sensitivity of matrices to perturbation by quantizing the mask values and comparing reconstruction performance (RRMSE error on a toy dataset) to what we achieve with exact values. Conditional on the matrix performing similarly under quantization, we can use offline calibration as outlined below to mitigate quantization well along with noise. 

\subsection{Effects on coded source separation}
We generate $8 \times 8$ random vectors sparse in the 2D DCT domain and pass them through both the quantized and original sensing matrices. We then solve basis pursuit to obtain reconstructions using the respective matrices, from which we obtain relative errors. A comparison of errors given by a matrix and its quantized version is shown in the figures below, for quantization levels of 0.2 and 1. Noise bounded in norm at $\epsilon = 10^{-5}$ was added.

\begin{figure}
\centering
\includegraphics[scale=0.3]{pics/hitomi-quant/736852_7427-64-2-0_2-100}
\caption{Comparison between RRMSEs with original and quantized matrices as a function of sparsity on $8 \times 8$ vectors sparse in 2D DCT, combined with $T = 2$ in the coded source separation scenario. Code elements rounded off to the nearest 0.2}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.3]{pics/hitomi-quant/736852_7443-64-4-0_2-100}
\caption{Comparison between RRMSEs with original and quantized matrices as a function of sparsity on $8 \times 8$ vectors sparse in 2D DCT, combined with $T = 4$ in the coded source separation scenario. Code elements rounded off to the nearest 0.2}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.3]{pics/hitomi-quant/736852_7461-64-6-0_2-100}
\caption{Comparison between RRMSEs with original and quantized matrices as a function of sparsity on $8 \times 8$ vectors sparse in 2D DCT, combined with $T = 6$ in the coded source separation scenario. Code elements rounded off to the nearest 0.2}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.3]{pics/hitomi-quant/736852_7419-64-2-1-100}
\caption{Comparison between RRMSEs with original and quantized matrices as a function of sparsity on $8 \times 8$ vectors sparse in 2D DCT, combined with $T = 2$ in the coded source separation scenario. Code elements rounded off to binary}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.3]{pics/hitomi-quant/736852_7435-64-4-1-100}
\caption{Comparison between RRMSEs with original and quantized matrices as a function of sparsity on $8 \times 8$ vectors sparse in 2D DCT, combined with $T = 4$ in the coded source separation scenario. Code elements rounded off to binary}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.3]{pics/hitomi-quant/736852_7452-64-6-1-100}
\caption{Comparison between RRMSEs with original and quantized matrices as a function of sparsity on $8 \times 8$ vectors sparse in 2D DCT, combined with $T = 6$ in the coded source separation scenario. Code elements rounded off to binary}
\end{figure}

The fact that RRMSEs don't deviate much from designed matrix RRMSE values is en encouraging result. It means that under quantization noise, matrices behave similar to what the original matrices used to behave like, and hence offline calibration can be used to correct for and obtain exact values of code elements. 

\subsection{Effects on the CACTI camera}
Repeating the experiment on the CACTI camera, we replicate the results obtained in the section above. Offline calibration can, therefore, also be used in the CACTI camera for accurate recovery of input signals.

\begin{figure}
\centering
\includegraphics[scale=0.3]{pics/cacti-quant/736852_691-64-2-0_2-100-fin}
\caption{Comparison between RRMSEs with original and quantized matrices as a function of sparsity on $8 \times 8$ vectors sparse in 2D DCT, combined with $T = 2$. Code elements rounded off to the nearest 0.2}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.3]{pics/cacti-quant/736852_6927-64-4-0_2-100-fin}
\caption{Comparison between RRMSEs with original and quantized matrices as a function of sparsity on $8 \times 8$ vectors sparse in 2D DCT, combined with $T = 4$. Code elements rounded off to the nearest 0.2}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.3]{pics/cacti-quant/736852_6946-64-6-0_2-100-fin}
\caption{Comparison between RRMSEs with original and quantized matrices as a function of sparsity on $8 \times 8$ vectors sparse in 2D DCT, combined with $T = 6$. Code elements rounded off to the nearest 0.2}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.3]{pics/cacti-quant/736852_6877-64-2-1-100-fin}
\caption{Comparison between RRMSEs with original and quantized matrices as a function of sparsity on $8 \times 8$ vectors sparse in 2D DCT, combined with $T = 2$. Code elements rounded off to binary}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.3]{pics/cacti-quant/736852_6918-64-4-1-100-fin}
\caption{Comparison between RRMSEs with original and quantized matrices as a function of sparsity on $8 \times 8$ vectors sparse in 2D DCT, combined with $T = 4$. Code elements rounded off to binary}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.3]{pics/cacti-quant/736852_6936-64-6-1-100-fin}
\caption{Comparison between RRMSEs with original and quantized matrices as a function of sparsity on $8 \times 8$ vectors sparse in 2D DCT, combined with $T = 6$. Code elements rounded off to binary}
\end{figure}

\section{Calibration}
Continuously varying noise introduced in the sensing matrix needs to be corrected for to achieve accurate reconstruction. We consider calibrating the sensing matrices for the CACTI camera. This can be attempted either online or offline, as described in the sections below.

To introduce some notation, we consider the general source separation model, with $y$ being a compressive measurement, and $\{x_i\}^{i=1}_T$ being input frames. Let $\phi$ be the original codes, which on degradation, go to $\tilde{\phi}$. Consistent with our original definitions, we let the $j^\text{th}$ element of $\phi$ be $\phi^j$. Let the noise introduced by the degradation be $\delta^i = \tilde{\phi^i} - \phi$. Given this, the sensing model is
\begin{align}
y &= \begin{pmatrix}
\tilde{\phi}_1 & \tilde{\phi}_2 & \hdots & \tilde{\phi}_T
\end{pmatrix}
\begin{pmatrix}
x_1 &
x_2 &
\hdots &
x_T
\end{pmatrix}^T \label{eq:prSourceSep} \\
&= \begin{pmatrix}
\tilde{\phi_1} & \tilde{\phi}_2 & \hdots & \tilde{\phi}_T
\end{pmatrix}
\begin{pmatrix}
D \alpha_1 &
D \alpha_2 &
\hdots &
D \alpha_T
\end{pmatrix}
\label{eq:prSensingModel}
\end{align}
However, in contrast to the earlier case, we do not know $\tilde{\phi}$, just $\phi$. The reconstruction technique we would have used without considering noise effects would have been
\begin{equation}
\min_{\alpha} \|\alpha\|_1 \text{ subject to } y = \Phi \Psi \mathbf{\alpha},\ \alpha = \begin{pmatrix}
\alpha_1 &
\alpha_2 &
\hdots &
\alpha_T
\end{pmatrix}^T
\label{eq:prBadSourceSepOpt}
\end{equation}
Considering noise effects, with an estimate $\delta$, the improved reconstruction technique is
\begin{equation}
\min_{\alpha} \|\alpha\|_1 \text{ subject to } y = \left( \Phi + \Delta \right) \Psi \mathbf{\alpha},\ \alpha = \begin{pmatrix}
\alpha_1 &
\alpha_2 &
\hdots &
\alpha_T
\end{pmatrix}^T
\label{eq:prSourceSepOpt}
\end{equation}
where $\Delta$ is defined appropriately.

\subsection{Online calibration}
Online calibration entails a joint estimation of $\delta$ and $\{x_i\}_{i=1}^T$ on the go as measurements come in. Multiple sets $\{x_i^k\}_{i=1,\ k=1}^{i=T,\ k=K}$ can also be used to achieve better estimation of $\delta$. A tempting method to try out is an alternating minimization. Setting an initial value for $\delta$, one can solve Eq. \ref{eq:prSourceSepOpt} individually for all $K$ sets to get an estimate for the input frames. Refining the estimate for $\delta$, then, reduces to finding those $\delta$ consistent with the input frames. This is an overcomplete linear system in $\delta$, yielding $n$ equations for each set of measurements. There are $n$ variables involved, and one can do a pseudoinverse solution. The magnitudes of the elements of $\delta$ are typically small, and can be clipped at appropriate values. The linear system is detailed in Eq. \ref{eq:recoveringDelta}.

The problem with this method is that because the effective dictionary is overcomplete, a fit is always possible for any given measurement. The alternating optimization provides no mechanism to ensure that the resulting $x$s are the sparsest among the possibilities as well. This points to selecting the sparsest input signals as well as the smallest error deviations. One can, therefore, solve the joint optimization problem
\begin{equation}
\left( \{x_i^k\}_{i=1,\ k=1}^{i=T,\ k=K},\ \delta \right) = \arg \min_{\{\tilde{x}_i^k\}_{i=1,\ k=1}^{i=T,\ k=K},\ \tilde{\delta}}\ \sum_{k=1}^{K} \left[ \left( y^k - \Phi \Psi \mathbf{\alpha}^k \right) + \lambda_1 \left\| \mathbf{\alpha}^k \right\|_1 \right] + \lambda_2 \left\| \delta \right\|_2 
\end{equation}

This joint optimization framework, however, needs a tuning of the parameters $\lambda_1$ and $\lambda_2$. We do not attempt the joint optimization in this work.

\subsection{Offline calibration}
Offline calibration entails estimation of $\delta$ from a given set of ground truth signals that are propagated through the forward model of the system to generate measurements. The method is exactly the one used to refine the estimate of $\delta$ in the second step of the alternating optimization above. To detail, we have in terms of the elementwise product $\cdot$ 
\begin{align}
y^k &= \sum_{t=1}^T \tilde{\phi}_t \cdot x^k_t \\
    &= \sum_{t=1}^T \tilde{\phi}^{p_t} \cdot x^k_t \\
    &= \sum_{t=1}^T \left( \phi^{p_t} + \delta^{p_t} \right) \cdot x^k_t \\
    &\implies y^k - \phi^{p_t} \cdot x^k_t = \delta^{p_t} \cdot x^k_t 
\label{eq:recoveringDelta}
\end{align}
where $\delta^p_t$ and $\phi^p_t$ are the appropriate circularly shifted versions of $\delta$ and $\phi$. Eq. \ref{eq:recoveringDelta} constitute $n$ equations -- one for each element of $y$ -- in $n$ variables -- the entries of $\delta$. Stacking these equations over all $k$, one gets $nK$ equations in $n$ variables, which can be solved using a pseudoinverse.

\subsection{Calibration results}
We do not attempt the joint optimization in this work. Instead, we perform offline calibration for the CACTI camera to demonstrate that the simple method above produces results much better than the ones we get without considering the effects of noise. 

We generate $K = 100$ random input signals $x^k$ for $T = 2, 4, 6$, and generate the compressive measurement $y^k$ for each vector using Eq. \ref{eq:prSensingModel}. These are then used to calibrate by finding the values of elements of $\delta$. Finally, a validation set of 1000 vectors is used to test the improvement in quality due to calibration, by finding their recovery error using both calibrated and uncalibrated matrices. This is repeated across various amounts of noise added to $\phi$.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{pics/perturbed/736850_6488-64-2}
\caption{Average RRMSE for calibrated and uncalibrated matrices as a function of matrix noise standard deviation for $8 \times 8$ signals, sparse in 2D DCT, combined with $T = 2$. Permutations: [5, 3; 6, 8]}
\label{fig:cactiRRMSEPer2}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{pics/perturbed/736850_6607-64-4}
\caption{Average RRMSE for calibrated and uncalibrated matrices as a function of matrix noise standard deviation for $8 \times 8$ signals, sparse in 2D DCT, combined with $T = 4$. Permutations: [7, 8; 2, 8; 6, 1; 3, 5]}
\label{fig:cactiRRMSEPer4}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{pics/perturbed/736850_6694-64-6}
\caption{Average RRMSE for calibrated and uncalibrated matrices as a function of matrix noise standard deviation for $8 \times 8$ signals, sparse in 2D DCT, combined with $T = 6$. Permutations: [6, 7; 3, 6; 6, 2; 1, 4; 8, 3; 5, 2]}
\label{fig:cactiRRMSEPer6}
\end{figure}
