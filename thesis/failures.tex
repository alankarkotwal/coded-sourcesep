\newcommand{\abs}[1]{\left|#1\right|}
\newcommand{\normtwo}[1]{\left\|#1\right\|_2}
\newcommand{\normone}[1]{\left\|#1\right\|_1}
\newcommand{\nx}{n_x}
\newcommand{\setX}{\mathcal{X}}

\chapter{Limitations of Coherence-based Bounds} \label{failures}

\lettrine[lines=2]{\scalebox{2}{T}}{he} surprising failure of coherence minimization in improving recovery performance for the kind of matrices needed by the CACTI camera challenges the coherence as a basic element in understanding compressed sensing recovery. The badness, we saw in the last chapter, however, stems not only from coherence but also from the steps in which the bound is derived. It makes sense, therefore, to examine a recovery guarantee proof step by step and analyze, empirically, how looseness propagates across chains of inequalities leading to the bound. 

\section{The bound}
The bound we choose to examine is the one proved in for recovery of nearly sparse vectors using basis pursuit denoising. Let the compressed sensing scenario be $z = Ax + n$, with an overcomplete $m \times n$-sized $A$, $n \times 1$-sized $x$ and $m \times 1$-sized $z$. Let $\mu$ denote the coherence of $A$. Suppose we recover $\hat{x}$ by solving the basis pursuit denoising problem.
\begin{equation}
\hat{x} = \arg \min_{\tilde{x}} \|\tilde{x}\|_1 \text{ such that } \|z - A\tilde{x}\|_2 < \epsilon
\label{eq:basisPursuit}
\end{equation}

Given a particular $n_x < n$, define $\mathcal{X}$ as the set of indices of the $n_x$ absolute greatest entries of x. Define the best $n_x$-sparse approximation to $x$, $x_\mathcal{X}$, by setting the $x$ values at indices not in $\mathcal{X}$ to zero. Then, if $$n_x < \frac{1}{2} \left( 1 + \frac{1}{\mu} \right)$$
and $\|n\|_2 < \eta$, we have the upper bound
\begin{equation}
\|x - \hat{x}\|_2 \leq C_0 (\epsilon + \eta) + C_1 \|x - x_\mathcal{X}\|_1 
\end{equation}

We will quote some relevant steps from the proof of the bound, following \cite{Studer201412} here. Let $h = \hat{x} - x$. Construct $h_0$ by setting elements of $h$ at indices not in $\mathcal{X}$ to zero. Let $e_0 = 2 \|x - x_\mathcal{X}\|_1$. Then, using the above definitions,
\begin{align}
\|x\|_1  \geq \|\hat{x}\|_1 &= \|\hat{x}_\mathcal{X}\|_1 + \|\hat{x}_{\mathcal{X}^C}\|_1 = \|x_\mathcal{X} + h_0\|_1 + \|h - h_0 + x_{\mathcal{X}^C}\|_1 \\
&\geq \|x_\mathcal{X}\|_1 - \|h_0\|_1 + \|h-h_0\|_1 - \|x_{\mathcal{X}^C}\|_1 \\
&\implies \|h-h_0\|_1 \leq 2 \|x_{\mathcal{X}^C}\|_1 + \|h_0\|_1 \\ 
&\implies \|h-h_0\|_1 \leq \|h_0\|_1 + e_0 \label{eq:coneconstraint}\\
&\implies \|h\|_1  \leq 2 \|h_0\|_1 + e_0 
\end{align}
%
where the last step follows from the reverse triangle inequality. Furthermore, 
\begin{align}
\|Ah\|_2 &= \|A\hat{x} - y - (Ax - y)\|_2 \\
&\leq \|A \hat{x} - y\|_2 + \|Ax - y\|_2 \\
&\leq \eta + \epsilon \label{eq:tubeconstraint}
\end{align}
%
An application of the Ger\v{s}gorin disk theorem to $\|Ah_0\|^2$ gives, since $h_0$ is perfectly sparse
\begin{align}
(1 - \mu (n_x - 1)) \|h_0\|_2^2 \leq \|Ah_0\|_2^2 \leq (1 + \mu (n_x - 1)) \|h_0\|_2^2
\label{eq:gersgorindiscthm}
\end{align}
%
Next,
\begin{align}
\abs{h^T A^T A h_0} & \geq \abs{h_0^T A^T A h_0} - \abs{(h-h_0)^T A^T A h_0} \label{eq:diffs4n4} \\
& \geq \left(1-\mu(\nx-1)\right)\normtwo{h_0}^2 - \abs{\sum_{k\in\setX}\sum_{l\in\setX^c} [h^T_0]_k a^T_k a_l [h]_l} \label{eq:errbound0} \\
& \geq \left(1-\mu(\nx-1)\right)\normtwo{h_0}^2 - \mu\normone{h_0}\normone{h-h_0} \label{eq:errbound1} \\
& \geq \left(1-\mu(\nx-1)\right)\normtwo{h_0}^2 - \mu\normone{h_0}\left(\normone{h_0} + e_0\right) \label{eq:errbound2}\\
& \geq \left(1-\mu(\nx-1)\right)\normtwo{h_0}^2 - \mu \nx \normtwo{h_0}^2 - \mu\sqrt{\nx}\normtwo{h_0}e_0 \label{eq:errbound3} \\
& = \left(1-\mu(2\nx-1)\right)\normtwo{h_0}^2 - \mu\sqrt{\nx}\normtwo{h_0}e_0, \label{eq:errbound4}
\end{align}
Eq. \ref{eq:errbound0} is a result of the Ger\v{s}gorin bound in Eq. \ref{eq:gersgorindiscthm}, Eq. \ref{eq:errbound1} arises from $\abs{a_k^Ta_l} \leq \mu$,  $\forall\ k\neq l$ and Eq. \ref{eq:errbound2} comes from the condition in Eq. \ref{eq:coneconstraint}. Eq. \ref{eq:errbound3} is an application of the Cauchy-Schwarz inequality. Next,
\begin{align}
\normtwo{h_0} &\leq \frac{\abs{h^TA^TAh_0} + \mu\sqrt{\nx}\normtwo{h_0}e_0}{\left(1-\mu(2\nx-1)\right)\normtwo{h_0}} \label{eq:errbound5} \\
& \leq \frac{\normtwo{Ah}\normtwo{Ah_0}+\mu\sqrt{\nx}\normtwo{h_0}e_0}{\left(1-\mu(2\nx-1)\right)\normtwo {h_0}} \label{eq:errbound6} \\
& \leq \frac{(\epsilon+\eta) \sqrt{1+\mu(\nx-1)}\normtwo{h_0} + \mu\sqrt{\nx}\normtwo{h_0}e_0}{\left(1-\mu(2\nx-1)\right)\normtwo{h_0}} \label{eq:errbound7} \\
& = \frac{ (\epsilon+\eta) \sqrt{1+\mu(\nx-1)}+\mu\sqrt{\nx}e_0}{ 1-\mu(2\nx-1)} \label{eq:errbound7b}
\end{align}

The proof further goes on to bound $\normtwo{h}$. The rest of the proof, however, is an application of the bound yet derived, and we will study the looseness of just this part of the proof. For completeness, we quote the bound here.
\begin{align}
\|h\|_2\ \leq\ &\frac{1 - \mu (2 n_x - 1) + \sqrt{\mu n_x} \sqrt{1 + \mu (n_x - 1)}}{\sqrt{1 + \mu (2 n_x - 1)}}\ (\epsilon + \eta)\ + \label{eq:finalBound} \\
&\frac{2\sqrt{\mu + \mu^2}}{1 - \mu(2 n_x - 1)}\ \|x - x_\mathcal{X}\|_1 \notag
\end{align}

\section{Empirical analysis of the bound}
We will concentrate on the simplest case, where the vector $x$ is exactly sparse, and set $n_x = \|x\|_0$. $\|x\|_0$ is set as the maximum $l_0$ norm that the bound allows, which is the greatest integer below $0.5(1 + 1/\mu)$. The definitions then reduce $e_0$ to 0. We generate a set of sparse vectors of a size suitable to be sensed with a selected sensing matrix, add noise bounded in norm by $\eta = \epsilon =10^{-5}$, and plot a boxplot of the relative difference between the left and right hand sides in selected inequalities in the proof above.

The relative differences we choose to show are due to, in order,
\begin{enumerate}
\item The triangle inequality in Eq. \ref{eq:diffs4n4}, with respect to the left hand side of Eq. \ref{eq:diffs4n4}  
\item The Ger\v{s}gorin bound and triangle inequality in Eq. \ref{eq:errbound0}, with respect to the left hand side of Eq. \ref{eq:diffs4n4}
\item Replacing dot products with their maximum, coherence, in Eq. \ref{eq:errbound1}, with respect to the left hand side of Eq. \ref{eq:diffs4n4} \label{chk:step3}
\item The application of Eq. \ref{eq:coneconstraint} in Eq. \ref{eq:errbound2}, with respect to the left hand side of Eq. \ref{eq:diffs4n4} \label{chk:step4}
\item The bound relating the $l_1$ and $l_2$ norms in Eq. \ref{eq:errbound4}, with respect to the left hand side of Eq. \ref{eq:diffs4n4} \label{chk:step5}
\item The rearrangement of Eq. \ref{eq:errbound5}, with respect to the left hand side of Eq. \ref{eq:errbound5} \label{chk:step6}
\item The Cauchy-Swartz inequality in Eq. \ref{eq:errbound6}, with respect to the left hand side of Eq. \ref{eq:errbound4} \label{chk:step7}
\item The application of Eq. \ref{eq:coneconstraint} in Eq. \ref{eq:errbound7b}, with respect to the left hand side of Eq. \ref{eq:errbound4} \label{chk:step8}
\item The leftmost side of the Ger\v{s}gorin bound in Eq. \ref{eq:gersgorindiscthm}, with respect to $\|Ah_0\|_2^2$ \label{chk:stepGnG1} 
\item The rightmost side of the Ger\v{s}gorin bound in Eq. \ref{eq:gersgorindiscthm}, with respect to $\|Ah_0\|_2^2$ \label{chk:stepGnG2} 
\item The actual RRMSE error for the simulated vector in question, which is the left hand side of Eq. \ref{eq:finalBound}, and the bound predicted by Eq. \ref{eq:finalBound}, with respect to the actual RRMSE.
\end{enumerate}

\subsection{General sensing matrices}
Here, to allow a high $\|x\|_0$, we first choose a random $499 \times 500$ matrix drawn from a Gaussian distribution. Also as a benchmark for a compressive scenario, we test with $250 \times 500$, $167 \times 500$, $125 \times 500$ and $50 \times 500$ matrices corresponding to 50\%, 33\%, 25\% and 10\% sparsity. All this is done at a reconstruction tolerance of $\epsilon = 10^{-6}$.

Figs. \ref{fig:genBoundComp2}, \ref{fig:genBoundComp3}, \ref{fig:genBoundComp4} and \ref{fig:genBoundComp5} show the relative differences for these scenarios. As is immediately noticed, the differences in the first two cases are shockingly high: the bound is off by two orders of magnitude compared to the actual error surface. The other three are off by a relative error well above 1.  

Consistent among all these figures is, however, the presence of jumps between relative differences across steps. There appear jumps in the transition from step \ref{chk:step3} to step \ref{chk:step4}, and from step \ref{chk:step7} to step \ref{chk:step8} (the baseline for comparison changes between step \ref{chk:step5} and step \ref{chk:step6}, so the jump here isn't significant. Also, these two steps are rearrangements of each other, so there's no loss happening in between). The fact that the Ger\v{s}gorin disk theorem does not cause much trouble is an effect of the fact that the matrix was drawn from a Gaussian random distribution, because Gaussian matrices are known to have low RIC and coherence values \cite{Foucart2013}.

We will contrast this behavior to what happens with matrices of the kind we encounter in practical signal processing scenarios further.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736852_8417-499x500-100}
\caption{Relative difference between inequalities in the error bound above for a $499 \times 500$ matrix drawn from a standard normal distribution}
\label{fig:genBoundComp1}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736852_8342-250x500-100}
\caption{Relative difference between inequalities in the error bound above for a $250 \times 500$ matrix drawn from a standard normal distribution}
\label{fig:genBoundComp2}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736852_8329-167x500-100}
\caption{Relative difference between inequalities in the error bound above for a $167 \times 500$ matrix drawn from a standard normal distribution}
\label{fig:genBoundComp3}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736852_8325-128x500-100}
\caption{Relative difference between inequalities in the error bound above for a $125 \times 500$ matrix drawn from a standard normal distribution}
\label{fig:genBoundComp4}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736852_8319-50x500-100}
\caption{Relative difference between inequalities in the error bound above for a $50 \times 500$ matrix drawn from a standard normal distribution}
\label{fig:genBoundComp5}
\end{figure}

\subsection{In coded source separation}
The maximum sparsity coded source separation affords, in most cases, is 1, because of the $n \times nT$ size of the matrix, and because typical matrices where the mask values are drawn from positive uniform distributions have coherence values around 0.8 to 0.9. Nevertheless, we repeat the same process as above, expecting the bound to work a little better in light of this low sparsity.

In contrast to expectations, the same steps show significant jumps in all the steps the general sensing matrices show in. The bound does not deprecate as badly as in the general sensing matrix case, just by one order of magnitude, but offers enough room for failure nevertheless.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736852_8479-64x2-100-hitomi}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ random positive codes in the coded source separation framework for $T = 2$}
\label{fig:hitomiBoundComp2}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736852_8479-64x3-100-hitomi}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ random positive codes in the coded source separation framework for $T = 3$}
\label{fig:hitomiBoundComp3}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736852_8479-64x4-100-hitomi}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ random positive codes in the coded source separation framework for $T = 4$}
\label{fig:hitomiBoundComp4}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736852_848-64x5-100-hitomi}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ random positive codes in the coded source separation framework for $T = 5$}
\label{fig:hitomiBoundComp5}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736852_848-64x6-100-hitomi}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ random positive codes in the coded source separation framework for $T = 6$}
\label{fig:hitomiBoundComp6}
\end{figure}

\subsection{In the CACTI camera}
Similarly, we test the matrices we use in the CACTI camera to for looseness in bound. Similar behavior as in the coded source separation case is observed, with a similar (more or less) deprecation. On the same lines, we plot looseness evolution for matrices designed for the CACTI camera in Chapter \ref{cacti}, both using coherence and using the sum of squares of off-diagonal dot products of columns of the effective dictionary. 

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736852_8518-64x2-100-cacti}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ random positive codes in the CACTI camera for $T = 2$. Permutations: [3, 1; 5, 5]}
\label{fig:cactiBoundComp2}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736852_8518-64x3-100-cacti}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ random positive codes in the CACTI camera for $T = 3$. Permutations: [7, 7; 4, 4; 7, 3]}
\label{fig:cactiBoundComp3}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736852_8518-64x4-100-cacti}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ random positive codes in the CACTI camera for $T = 4$. Permutations: [1, 2; 6, 6; 7, 3; 2, 7]}
\label{fig:cactiBoundComp4}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736852_8519-64x5-100-cacti}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ random positive codes in the CACTI camera for $T = 5$. Permutations: [7, 6; 4, 8; 4, 2; 6, 5; 2, 3]}
\label{fig:cactiBoundComp5}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736852_8519-64x6-100-cacti}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ random positive codes in the CACTI camera for $T = 6$. Permutations: [1, 8; 8, 7; 6, 1; 4, 6; 7, 8; 5, 3]}
\label{fig:cactiBoundComp6}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736856_6676-64x2-100-cacti-des}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ designed positive codes in the CACTI camera for $T = 2$. Permutations: [5, 3; 6, 8]}
\label{fig:cactiDesBoundComp4}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736856_6682-64x4-100-cacti-des}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ designed positive codes in the CACTI camera for $T = 4$. Permutations: [7, 8; 2, 8; 6, 1; 3, 5]}
\label{fig:cactiDesBoundComp5}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736856_6684-64x6-100-cacti-des}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ designed positive codes in the CACTI camera for $T = 6$. Permutations: [6, 7; 3, 6; 6, 2; 1, 4; 8, 3; 5, 2]}
\label{fig:cactiDesBoundComp6}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736856_668-64x2-100-cacti-des-avg}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ average-designed positive codes in the CACTI camera for $T = 2$. Permutations: [8, 4; 7, 2]}
\label{fig:cactiAvgDesBoundComp4}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736856_6683-64x4-100-cacti-des-avg}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ average-designed positive codes in the CACTI camera for $T = 4$. Permutations: [3, 1; 1, 7; 6, 3; 8, 1]}
\label{fig:cactiAvgDesBoundComp5}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{sims/proof/736856_6685-64x6-100-cacti-des-avg}
\caption{Relative difference between inequalities in the error bound above for $8 \times 8$ average-designed positive codes in the CACTI camera for $T = 6$. Permutations: [7, 3; 5, 6; 8, 8; 5, 2; 2, 3; 7, 3]}
\label{fig:cactiAvgDesBoundComp6}
\end{figure}

\section{Discussion}
While the figures above do not explain why coherence succeeds in the coded source separation case but doesn't in the CACTI case, the fact that there are common steps that cause significant looseness of bound is a big takeaway. These steps can be isolated and the precise inequalities causing problems can be pointed to. In our case, these are the ones leading to step \ref{chk:step4} from step \ref{chk:step3}, and from step \ref{chk:step7} to step \ref{chk:step8}. We will examine these inequalities in some detail now.

The transition from step \ref{chk:step3} to step \ref{chk:step4} involves the use of the constraint in Eq. \ref{eq:coneconstraint}. Further, going from step \ref{chk:step7} to step \ref{chk:step8} involves the constraint in Eq. \ref{eq:tubeconstraint}, and the right side of the Ger\v{s}gorin inequality in Eq. \ref{eq:gersgorindiscthm}. The culprits here, therefore, are Eq. \ref{eq:tubeconstraint}, Eq. \ref{eq:coneconstraint} and the right side of Eq. \ref{eq:gersgorindiscthm}. 

However, these are fundamental constraints that come from the problem itself. For instance, the constraint in Eq. \ref{eq:tubeconstraint} comes from the nature of the noise. However, as we saw empirically in our situation, there are hardly any (in our dataset, none) vectors that meet this bound. It can be inferred, therefore, that at the cost of accommodating a set of rare worst case vectors, optimizing worst case bounds does not do well on the average case signal.

At this point, it is worthwhile to stop to consider the array of design schemes based on coherence. These methods (\cite{Elad200610, Duarte200907, Mordechay2014, Obermeier17, Bouchhima15, Abolghasemi10, Pereira14, Parada17}) attempt to design matrices with an extremely loose bound. The sheer number of these methods very emphatically states the popularity of the coherence as a measure of matrix goodness. Why these methods are successful is unclear at this moment, given the results in this chapter. Better, tighter bounds on sparse recovery will possibly make these methods more effective at optimizing matrices in their particular applications.

A full analysis of average case error bounds, however, requires estimating a posterior on the space of input vectors as well as assuming a specific statistical model for noise, and involves an intractable quantity to calculate, as stated in \cite{Carson2012}. Such an analysis is beyond the scope of this thesis. A sampling-based approach seems to be a direction in which to proceed for any tractable sensing matrix design considering the average case.
