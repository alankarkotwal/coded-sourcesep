\chapter{Et tu, RIC?} \label{chp:ric}
The error bound introduced by \cite{Tang2015}, quoted in Eq.~\ref{eq:linfErrorBound} has been interpreted as a compromise between coherence and the RIC. Coherence penalizes the dot products of normalized column pairs, and therefore the projections of columns on each other (see Eq.~\ref{eq:cohDefn}). This qualitatively expresses the ability of one column to approximate the other, or in other words, how well a $1$-sparse combination of $n-1$ of columns from $A$ can represent the remaining column. Looking at Eq.~\ref{eq:omegaExpr}, one realizes that the linear combination being penalized here is not just $1$-sparse: all linear combinations of $n-1$ columns with a coefficient vector $\lambda$ are penalized for how well they can represent the $n^\text{th}$ column, as long as $\|\lambda\|_1 \leq s$. While no theoretical claims can be made about which bound is better, a looseness analysis of the RIC bound seems to be a fitting final section of this paper.

The RIC-based bound error bound in \cite{Cai2010} states that if $\delta_k < 0.307$
\begin{equation}
    \|\hat{x} - x\|_2 \leq \frac{\epsilon}{0.307-\delta_k}
    \label{eq:ricErrorBound}
\end{equation}
Though the RIC is intractable to compute, it is computable for small sparsity levels for reasonably sized matrices. We, therefore, calculate the relative difference between the left and right hand sides of the bound in Eq.~\ref{eq:ricErrorBound} with respect to the left hand side, which is the actual error between the actual vector and the reconstruction. We randomly generate $m \times 550$ matrices for $m$ = 275, 367, 412 and 549 (Figs. \ref{fig:ricRRMSE1}, \ref{fig:ricRRMSE2}, \ref{fig:ricRRMSE3} and \ref{fig:ricRRMSE4} respectively), and random positive $k = 2$-sparse $550 \times 1$ vectors. These numbers are selected so that the RIC condition $\delta_k \leq 0.307$ is satisfied. Then, reconstructing using the basis pursuit solver in Eq.~\ref{eq:basisPursuit}, we calculate the $l_2$ error between the original and reconstructed vectors. We also perform the same analysis on a $k = 3$-sparse vector for a $549 \times 500$ matrix (Fig.~\ref{fig:ricRRMSE5}), since the matrix instance we chose permits the RIC condition to hold.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{pics/ric/736864_081-275x550-2-100}
\caption{Relative difference between reconstruction error and the error bound in Eq.~\ref{eq:ricErrorBound} for sparse $550 \times 1$ signals with $k = 2$, sensed with a $275 \times 550$ Gaussian random matrix}
\label{fig:ricRRMSE1}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{pics/ric/736864_0829-367x550-2-100}
\caption{Relative difference between reconstruction error and the error bound in Eq.~\ref{eq:ricErrorBound} for sparse $550 \times 1$ signals with $k = 2$, sensed with a $367 \times 550$ Gaussian random matrix}
\label{fig:ricRRMSE2}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{pics/ric/736864_0846-412x550-2-100}
\caption{Relative difference between reconstruction error and the error bound in Eq.~\ref{eq:ricErrorBound} for sparse $550 \times 1$ signals with $k = 2$, sensed with a $412 \times 550$ Gaussian random matrix}
\label{fig:ricRRMSE3}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{pics/ric/736864_0885-549x550-2-100}
\caption{Relative difference between reconstruction error and the error bound in Eq.~\ref{eq:ricErrorBound} sparse $550 \times 1$ signals with $k = 2$, sensed with a $549 \times 550$ Gaussian random matrix}
\label{fig:ricRRMSE4}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.3]{pics/ric/736864_0986-549x550-3-100}
\caption{Relative difference between reconstruction error and the error bound in Eq.~\ref{eq:ricErrorBound} for sparse $550 \times 1$ signals with $k = 3$, sensed with a $549 \times 550$ Gaussian random matrix}
\label{fig:ricRRMSE5}
\end{figure}

The values of looseness of bound in the RIC case are not close to zero either. The RIC, therefore, does not establish a tight bound on the recovery error. The problem optimizing with coherence now, we establish empirically, is twofold: the coherence establishes a loose bound on the RIC, and the RIC establishes a loose bound on the recovery error. 
