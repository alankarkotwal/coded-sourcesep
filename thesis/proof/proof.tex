\documentclass{article}
\usepackage{amsmath,amsfonts,amssymb}

\newcommand{\abs}[1]{\left|#1\right|}
\newcommand{\normtwo}[1]{\left\|#1\right\|_2}
\newcommand{\normone}[1]{\left\|#1\right\|_1}
\newcommand{\nx}{n_x}
\newcommand{\setX}{\mathcal{X}}

\begin{document}

The bound we choose to examine is the one proved in for recovery of nearly sparse vectors using basis pursuit denoising. Let the compressed sensing scenario be $z = Ax + n$, with an overcomplete $m \times n$-sized $A$, $n \times 1$-sized $x$ and $m \times 1$-sized $z$. Let $\mu$ denote the coherence of $A$. Suppose we recover $\hat{x}$ by solving the basis pursuit denoising problem.
\begin{equation}
\hat{x} = \arg \min_{\tilde{x}} \|\tilde{x}\|_1 \text{ such that } \|z - A\tilde{x}\|_2 < \epsilon
\end{equation}

Given a particular $n_x < n$, define $\mathcal{X}$ as the set of indices of the $n_x$ absolute greatest entries of x. Define the best $n_x$-sparse approximation to $x$, $x_\mathcal{X}$, by setting the $x$ values at indices not in $\mathcal{X}$ to zero.

Then, if $$n_x < \frac{1}{2} \left( 1 + \frac{1}{\mu} \right)$$
and $\|n\|_2 < \eta$, we have the upper bound
\begin{equation}
\|x - \hat{x}\|_2 \leq C_0 (\epsilon + \eta) + C_1 \|x - x_\mathcal{X}\|_1 
\end{equation}

The proof of the bound proceeds in the following manner. Let $h = \hat{x} - x$. Construct $h_0$ by setting elements of $h$ at indices not in $\mathcal{X}$ to zero. Let $e_0 = 2 \|x - x_\mathcal{X}\|_1$. Then, using the above definitions,
\begin{align}
\|x\|_1  \geq \|\hat{x}\|_1 &= \|\hat{x}_\mathcal{X}\|_1 + \|\hat{x}_{\mathcal{X}^C}\|_1 = \|x_\mathcal{X} + h_0\|_1 + \|h - h_0 + x_{\mathcal{X}^C}\|_1 \\
&\geq \|x_\mathcal{X}\|_1 - \|h_0\|_1 + \|h-h_0\|_1 - \|x_{\mathcal{X}^C}\|_1 \\
&\implies \|h-h_0\|_1 \leq 2 \|x_{\mathcal{X}^C}\|_1 + \|h_0\|_1 \\ 
&\implies \|h-h_0\|_1 \leq \|h_0\|_1 + e_0 \\
&\implies \|h\|_1  \leq 2 \|h_0\|_1 + e_0
\end{align}
where the last step follows from the reverse triangle inequality.

Furthermore, 
\begin{align}
\|Ah\|_2 &= \|A\hat{x} - y - (Ax - y)\|_2 \\
&\leq \|A \hat{x} - y\|_2 + \|Ax - y\|_2 \\
&\leq \eta + \epsilon
\end{align}

An application of the Ger\v{s}gorin disk theorem to $\|Ah_0\|^2$ gives, since $h_0$ is perfectly sparse
\begin{align}
(1 - \mu (n_x - 1)) \|h_0\|_2^2 \leq \|Ah_0\|_2^2 \leq (1 + \mu (n_x - 1)) \|h_0\|_2^2
\end{align}

Next,
\begin{align}
\abs{h^T A^T A h_0} & \geq \abs{h_0^T A^T A h_0} - \abs{(h-h_0)^T A^T A h_0} \\
& \geq \left(1-\mu(\nx-1)\right)\normtwo{h_0}^2 - \abs{\sum_{k\in\setX}\sum_{l\in\setX^c} [h^T_0]_k a^T_k a_l [h]_l} \label{eq:errbound0} \\
& \geq \left(1-\mu(\nx-1)\right)\normtwo{h_0}^2 - \mu\normone{h_0}\normone{h-h_0} \label{eq:errbound1} \\
& \geq \left(1-\mu(\nx-1)\right)\normtwo{h_0}^2 - \mu\normone{h_0}\left(\normone{h_0} + e_0\right) \label{eq:errbound2}\\
& \geq \left(1-\mu(\nx-1)\right)\normtwo{h_0}^2 - \mu \nx \normtwo{h_0}^2 - \mu\sqrt{\nx}\normtwo{h_0}e_0 \label{eq:errbound3} \\
& = \left(1-\mu(2\nx-1)\right)\normtwo{h_0}^2 - \mu\sqrt{\nx}\normtwo{h_0}e_0, \label{eq:errbound4}
\end{align}
%where \fref{eq:errbound0} follows from~\fref{eq:gersgorindiscthm}, \fref{eq:errbound1} is a consequence of $\abs{a_k^Ta_l} \leq \mu$,  $\forall k\neql$, \fref{eq:errbound2} results from the cone constraint~\fref{eq:coneconstraint}, and \fref{eq:errbound3} from the Cauchy-Schwarz inequality. 
%
%We emphasize that \fref{eq:errbound4} is crucial, since it  determines the recovery condition for BPDN. 
%
%In particular, if the first RHS term in~\fref{eq:errbound4} satisfies $(1-\mu(2\nx-1))>0$ and $h_0\neq\bZero_{\inputdimA\times1}$, then the error $\normtwo{h_0}$ is bounded from above as follows:
\begin{align}
\normtwo{h_0} &\leq \frac{\abs{h^TA^TAh_0} + \mu\sqrt{\nx}\normtwo{h_0}e_0}{\left(1-\mu(2\nx-1)\right)\normtwo{h_0}} \label{eq:errbound5} \\
& \leq \frac{\normtwo{Ah}\normtwo{Ah_0}+\mu\sqrt{\nx}\normtwo{h_0}e_0}{\left(1-\mu(2\nx-1)\right)\normtwo {h_0}} \label{eq:errbound6} \\
& \leq \frac{(\varepsilon+\eta) \sqrt{1+\mu(\nx-1)}\normtwo{h_0} + \mu\sqrt{\nx}\normtwo{h_0}e_0}{\left(1-\mu(2\nx-1)\right)\normtwo{h_0}} \label{eq:errbound7} \\
& = \frac{ (\varepsilon+\eta) \sqrt{1+\mu(\nx-1)}+\mu\sqrt{\nx}e_0}{ 1-\mu(2\nx-1)}.\label{eq:errbound7b}
\end{align}
%Here, \fref{eq:errbound5} is a consequence of \fref{eq:errbound4},  \fref{eq:errbound6} follows from the Cauchy-Schwarz inequality, and 
%\fref{eq:errbound7} results from the tube constraint~\fref{eq:tubeconstraint} and the RIP~\fref{eq:gersgorindiscthm}. 
%
%The case $h_0=\bZero_{\inputdimA\times1}$ is trivial as it implies~$\normtwo{h_0}=0$.

%\begin{align}
%\normtwo{Ah}^2 & = h^TA^TAh = \sum_{k,l} [h^T]_k a^T_ka_l [h]_l \notag \\
%& = \sum_{k}\normtwo{a_k}^2\abs{[h]_k}^2 + \sum_{k,l,k\neql} [h^T]_k a^T_ka_l [h]_l \notag \\
%& \geq \normtwo{h}^2 - \mu \sum_{k,l,k\neql}\abs{[h^T]_k[h]_l} \label{eq:finbound1}\\
%& = \normtwo{h}^2 + \mu \sum_{k}\abs{[h]_k}^2 - \mu\sum_{k,l} \abs{[h^T]_k[h]_l} \notag \\
%& = (1+\mu)\normtwo{h}^2 - \mu\normone{h}^2, \label{eq:finbound2}
%\end{align}
%where \fref{eq:finbound1} follows from $\normtwo{a_k}=1$, $\forall k$, and $\abs{a_k^Ta_l} \leq \mu$, $\forall k\neq l$. 
%%
%With \fref{eq:finbound2}, the recovery error can be bounded as
%\begin{align}
%\normtwo{h}^2 & \leq \frac{\normtwo{Ah}^2 + \mu\normone{h}^2}{1+\mu} \leq \frac{(\varepsilon+\eta)^2 + \mu\left(2\normone{h_0} + e_0\right)^2}{1+\mu}, \label{eq:finbound3}
%\end{align}
%where \fref{eq:coneconstraintonh} is used to arrive at \fref{eq:finbound3}.
%%
%By taking the square root of \fref{eq:finbound3} and applying the Cauchy-Schwarz inequality, we arrive at the following bound:
%\begin{align}
%\normtwo{h} & \leq \frac{\sqrt{(\varepsilon+\eta)^2 + \mu\left(2\normone{h_0} + e_0\right)^2}}{\sqrt{1+\mu}} \notag \\
%& \leq \frac{(\varepsilon+\eta) + \sqrt{\mu}\left(2\normone{h_0} + e_0\right)}{\sqrt{1+\mu}}.\label{eq:finbound4}
%\end{align}
%%
%Finally, using $\normone{h_0}\leq\sqrt{\nx}\normtwo{h_0}$ with the bound in \fref{eq:errbound7b} followed by algebraic simplifications yields
%\begin{align*}
%\normtwo{h}  \leq\,\, &\frac{(\varepsilon+\eta)+ \sqrt{\mu}\left(2\sqrt{\nx}\normtwo{h_0} + e_0\right)}{\sqrt{1+\mu}}  \\
% \leq\,\, &(\varepsilon+\eta)\frac{ 1-\mu(2\nx-1) +  2 \sqrt{\mu\nx}  \sqrt{1+\mu(\nx-1)}   }{\sqrt{1+\mu}\left(1-\mu(2\nx-1) \right)} \\
%& \!\!+ e_0 \frac{ \sqrt{\mu+\mu^2} }{ \left(1-\mu(2\nx-1) \right)}  = C_0 (\eta+\varepsilon) + C_1 \normone{\inputvec-\inputvec_\setX},
%\end{align*}
%which concludes the proof. 

\begin{equation}
\left( \{x_i^k\}_{i=1,\ k=1}^{i=T,\ k=K},\ \delta \right) = \arg \min_{\{\tilde{x}_i^k\}_{i=1,\ k=1}^{i=T,\ k=K},\ \tilde{\delta}}\ \sum_{k=1}^{K} \left[ \left( y^k - \Phi \Psi \mathbf{\alpha}^k \right) + \left\| \mathbf{\alpha}^k \right\|_1 \right] + \left\| \delta \right\|_2 
\end{equation}
\end{document}
