\contentsline {chapter}{Abstract}{iii}{chapter*.2}
\contentsline {chapter}{List of Figures}{ix}{chapter*.4}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Preliminaries}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Compressed sensing}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Motivation}{5}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}General framework}{6}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Reconstruction methods}{7}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Theoretical guarantees}{9}{subsection.2.1.4}
\contentsline {subsubsection}{$l_0$ optimization}{9}{section*.9}
\contentsline {subsubsection}{$l_1$ optimization}{9}{section*.10}
\contentsline {section}{\numberline {2.2}Source separation}{10}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}The framework}{10}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Theoretical guarantees}{11}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Optimizing compressed sensing -- previous work}{11}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Coherence minimization via the Gram matrix}{11}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Coherence Minimization via rank-1 approximation}{13}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Information-theoretic methods}{13}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Other methods and applications}{14}{subsection.2.3.4}
\contentsline {chapter}{\numberline {3}Optimizing Coded Source Separation}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Sensing framework}{15}{section.3.1}
\contentsline {section}{\numberline {3.2}Our approach to reconstruction}{17}{section.3.2}
\contentsline {section}{\numberline {3.3}Optimizing codes for source separation}{17}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Track I: Direct coherence minimization}{17}{subsection.3.3.1}
\contentsline {subsubsection}{Calculation of coherence derivatives}{18}{section*.13}
\contentsline {subsubsection}{Time complexity and the need for something more}{19}{section*.14}
\contentsline {subsection}{\numberline {3.3.2}Track II: Including circular shifts}{19}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Track III: Optimizing bounds tighter than coherence}{21}{subsection.3.3.3}
\contentsline {subsubsection}{Ger\v {s}gorin radii}{21}{section*.16}
\contentsline {subsubsection}{Brauer ellipse bounds}{22}{section*.17}
\contentsline {section}{\numberline {3.4}Experiments and results}{22}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Validating our framework}{22}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Demosaicing}{25}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Coherence minimization}{27}{subsection.3.4.3}
\contentsline {subsubsection}{Circularly-symmetric coherence minimization}{29}{section*.32}
\contentsline {chapter}{\numberline {4}Optimizing for the CACTI Camera}{35}{chapter.4}
\contentsline {section}{\numberline {4.1}Introducing the CACTI camera}{35}{section.4.1}
\contentsline {section}{\numberline {4.2}Optimizing codes}{36}{section.4.2}
\contentsline {section}{\numberline {4.3}Experiments and results}{37}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Simulated data}{37}{subsection.4.3.1}
\contentsline {chapter}{\numberline {5}Limitations of Coherence-based Bounds}{43}{chapter.5}
\contentsline {section}{\numberline {5.1}The bound}{43}{section.5.1}
\contentsline {section}{\numberline {5.2}Empirical analysis of the bound}{45}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}General sensing matrices}{46}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}In coded source separation}{47}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}In the CACTI camera}{48}{subsection.5.2.3}
\contentsline {section}{\numberline {5.3}Discussion}{51}{section.5.3}
\contentsline {chapter}{\numberline {6}Alternate Compressed Sensing Bounds}{59}{chapter.6}
\contentsline {section}{\numberline {6.1}A new bound}{59}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}An $l_\infty $ error-based sparsity criterion}{59}{subsection.6.1.1}
\contentsline {section}{\numberline {6.2}Is this bound feasible to optimize on?}{60}{section.6.2}
\contentsline {chapter}{\numberline {7}Et tu, RIC?}{65}{chapter.7}
\contentsline {chapter}{\numberline {8}The Average Case: A Proof of Concept}{69}{chapter.8}
\contentsline {section}{\numberline {8.1}General sensing matrices}{70}{section.8.1}
\contentsline {section}{\numberline {8.2}In the CACTI camera}{72}{section.8.2}
\contentsline {chapter}{\numberline {9}Practical Constraints in Compressed Sensing Design}{75}{chapter.9}
\contentsline {section}{\numberline {9.1}Quantization errors}{75}{section.9.1}
\contentsline {subsection}{\numberline {9.1.1}Effects on coded source separation}{76}{subsection.9.1.1}
\contentsline {subsection}{\numberline {9.1.2}Effects on the CACTI camera}{79}{subsection.9.1.2}
\contentsline {section}{\numberline {9.2}Calibration}{79}{section.9.2}
\contentsline {subsection}{\numberline {9.2.1}Online calibration}{83}{subsection.9.2.1}
\contentsline {subsection}{\numberline {9.2.2}Offline calibration}{84}{subsection.9.2.2}
\contentsline {subsection}{\numberline {9.2.3}Calibration results}{84}{subsection.9.2.3}
\contentsline {chapter}{\numberline {10}Conclusion and Future Work}{87}{chapter.10}
\contentsline {section}{\numberline {10.1}Takeaways}{87}{section.10.1}
\contentsline {section}{\numberline {10.2}Future work}{88}{section.10.2}
\contentsline {chapter}{Appendices}{91}{section*.96}
\contentsline {chapter}{\numberline {A}Derivation of coherence expressions}{93}{Appendix.1.A}
\contentsline {chapter}{\numberline {B}Derivation of coherence derivatives}{95}{Appendix.1.B}
