\chapter{Preliminaries} \label{preliminaries}

\section{Compressed sensing}

\subsection{Motivation}
One of the fundamental and preliminary problems in the typical signal processing pipeline is the discrete representation of continuous-time signals. A general continuous-time signal has infinite degrees of freedom -- at each point on the domain of the function, we are free to choose any value in the range of the function. A discrete representation, therefore, does not preserve all the information in the signal. However, we cannot use continuous information -- such a representation would take up infinite space and computation time.

However, most signals we find in everyday life aren't completely random. There is often an underlying structure to them, and we don't need all the infinite degrees to represent the signal. For instance, a fundamental result, the Nyquist-Shannon sampling theorem, says that if the signal is bandlimited (limited in frequency in the spectral domain), a discrete representation spaced at half the minimum period in the spectrum of the signal uniquely determines the signal. For a general bandlimited signal, it can be shown that we can't do any better.

Natural signals, however, have more structure than bandlimitedness. Natural images, for instance, are known to be sparse in spectral domains like the discrete Fourier and cosine transforms. Among the (bounded) set of frequencies in these signals, only few have any significant contribution to the signal energy. The image in Fig.~\ref{fig:sparsityImg}, for instance, has a DCT spectrum shown in Fig.~\ref{fig:sparsityEx}. Note that among the $4 \times 10^4$ coefficients plotted, only a few are non-zero.

This seems to suggest that we can get away by sensing only those components that contribute any significant energy and still achieve a good representation of these signals and thus, beat the sampling theorem by exploiting structure.

\begin{figure}
	\centering
	\includegraphics{pics/sparsityImg}
	\caption{Example image for the sparsity analysis in Fig.~\ref{fig:sparsityEx}}
	\label{fig:sparsityImg}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{pics/sparsityEx}
	\caption{Plot of DCT coefficients for the image in Fig.~\ref{fig:sparsityImg}}
	\label{fig:sparsityEx}
\end{figure}

\subsection{General framework}
We, then, must equip ourselves to sample only some coefficients of that spectral domain that sparsifies a given signal. Since the spectral transforms are linear functions of the input signal, it is sufficient to consider linear combinations of signal elements.

Thus, our general sensing framework, for obtaining the measurement $y$ from the inherent signal $x$, given a sensing matrix (that dictates the above linear combinations) $\phi$ is
\begin{equation}
	y = \phi x
	\label{eq:csMeasEqn}
\end{equation}

The $\phi$ here is a short, fat matrix because the number of elements in $y$ is less than the number of elements in $x$ due to the compressive nature of the measurement. Now, if the (general) basis $\psi$ sparsifies the signal $x$, we write
\begin{equation}
	y = \phi \psi c \\
	= A c
	\label{eq:csMainEqn}
\end{equation}
where $c$ is the vector of coefficients of the signal $x$ in the basis $\psi$.

\subsection{Reconstruction methods}
The goal, then, is to reconstruct the signal $x$ (equivalently, $c$) from the compressive measurement $y$. We formulate the problem as follows: we want the `sparsest' (in $\psi$) $x$ that satisfies the measurement equation. The definition of sparsity in this context is usually taken to be the $l_0$ norm of the vector $x$.

Thus the optimization problem that faces us is
\begin{equation}
	\min_{c} \|c\|_0\text{ such that }y=Ac
	\label{eq:csL0Rec}
\end{equation}

This optimization problem, however, can be shown \cite{Foucart2013} to be combinatorial in $c$ -- there's no polynomial time solution to this problem. However, greedy methods can be used to select the support of the vector $c$ and then estimate the coefficients in the support. Examples of such methods are matching pursuit [Alg.~\ref{alg:mp}] and orthogonal matching pursuit [Alg.~\ref{alg:omp}] \cite{Cai2011} These algorithms are summarized in brief for reference:

\begin{algorithm}[]
    \KwData{Signal: $\mathcal{Y}(x)$, dictionary $\mathcal{D}$}
    \KwResult{List of coefficients: $\left(a_n, f_{n}(x)\right)$.}
    Initialization\: \\
    $R_1(x)\,\leftarrow\,\mathcal{Y}(x)$ \\
    $n\,\leftarrow\,1;$\\
    \While{$\|R_n(x)\| < \mathrm{threshold}$}{
        $f_{n}(x) \leftarrow \text{arg} \max_{f_i(x) \in \mathcal{D}} \|R_n(x) - f_i(x)\|$ \\
        $a_n \leftarrow \|R_n(x) - f_i(x)\|\;$ \\
        $R_{n+1}(x) \leftarrow R_n(x) - a_n f_n(x)$ \\
        $n \leftarrow n+1$
    }
    \caption{Matching Pursuit}
    \label{alg:mp}
\end{algorithm}

\begin{algorithm}[]
    \KwData{Signal: $\mathcal{Y}(x)$, dictionary $\mathcal{D}$}
    \KwResult{List of coefficients: $\left(a_n, f_{n}(x)\right)$.}
    Initialization\: \\
    $R_1(x)\,\leftarrow\,\mathcal{Y}(x)$ \\
    $n\,\leftarrow\,1;$\\
    $\mathcal{S} \leftarrow \phi$ \\
    \While{$\|R_n(x)\| < \mathrm{threshold}$}{
        $f_{n}(x) \leftarrow \text{arg} \max_{f_i(x) \in \mathcal{D}} \|R_n(x) - f_i(x)\|$
        $\mathcal{S} = \mathcal{S} \cup f_n(x)$ \\
        $\mathbf{a} \leftarrow \text{arg} \min_{w \in \mathbb{R}^k} \|\mathcal{Y}(x) - \sum_{f_i(x) \in \mathcal{S}} w_i f_i(x)\|\;$ \\
        $R_{n+1}(x) \leftarrow \mathcal{Y}(x) - \sum a_n f_n(x)$ \\
        $n \leftarrow n+1$
    }
    \caption{Orthogonal Matching Pursuit}
    \label{alg:omp}
\end{algorithm}

Often, the recovery problem is often relaxed to an $l_p$ norm optimization problem:
\begin{equation}
	\min_{c} \|c\|_p\text{ such that }y=Ac
	\label{eq:csLpRec}
\end{equation}

A common choice for $p$ in the above is 1, because that convexifies the problem while still promoting sparsity. The optimization problem with $p=1$ is known as basis pursuit.

The noisy case can be handled in a similar manner, by changing the constraint:
\begin{equation}
	\min_{c} \|c\|_p\text{ such that }\|y-Ac\|_2 \leq \epsilon
	\label{eq:csLpRecNoisy}
\end{equation}

\subsection{Theoretical guarantees}

\subsubsection{$l_0$ optimization}
Suppose we found some method of performing the minimization in Eq.~\ref{eq:csL0Rec}. Under what conditions would an $s$-sparse vector $c$ be accurately recovered by solving Eq.~\ref{eq:csL0Rec}?

To answer this, assume that $y = Ac_1$. Now, for any $c_2$ that is $s$-sparse, $c_1 - c_2$ is $2s$-sparse. Therefore, if $y = Ac_2$, we must have $A(c_1 - c_2) = 0$. If $c_1$ is to be the unique solution to Eq.~\ref{eq:csL0Rec}, we must have $c_1 = c_2$, and therefore, cannot have any linearly-dependent subset of $2s$ columns in A. This can be extended to the noisy case \cite{Foucart2013}.

\subsubsection{$l_1$ optimization}
A number of properties of the sensing matrix have been used \cite{Foucart2013} to derive reconstruction error bounds on the matrix $A$. We mention a couple of these that will be useful further.

Let us assume, for the purposes of this section, that the $k \times N$ matrix $A$ has $l_2$-normalized columns. Then, the coherence $\mu$ of the matrix $A$ is defined as
\begin{equation}
	\mu = \min_{1 \leq i \neq j \leq N} \left< a_i, a_j \right>
	\label{eq:cohDefn}
\end{equation}

Further, the $s^\text{th}$ restricted isometry constant $\delta_s$ of the matrix $A$ is defined as 
\begin{equation}
	\delta_s (A) = \max_{S \in \{1, .., N\}, \text{ card}(S) \leq s}\lambda_{max} \left( A_S^T A_S - I \right)
	\label{eq:ricDefn}
\end{equation}
where $A_S$ is the restriction of the columns of the matrix $A$ to the subset $S$ of the set $[N]$ of numbers from $1$ to $N$.

It can be shown \cite{Foucart2013} that if the $2s^\text{th}$ restricted isometry constant $\delta_{2s} \leq 4 / \sqrt{41}$, then the solution $c^*$ of \ref{eq:csLpRec} with $p=1$ approximates the inherent, nearly $s$-sparse $c$ within an error bound determined by $\delta_{2s}$:
\begin{equation}
	\|c - c^*\|_1 \leq L c^\# + M \sqrt{s} \epsilon
	\label{eq:ricBound1}
\end{equation}
\begin{equation}
	\|c - c^*\|_2 \leq \frac{L}{\sqrt{s}} c^\# + M \epsilon
	\label{eq:ricBound2}
\end{equation}
where $c^\#$ is the restriction of $c$ to the largest (in magnitude) $s$ entries of $c$. $L$ and $M$ are increasing functions of the RIC. This points to the fact that one way of minimizing the reconstruction error for $s$-sparse signals is to minimize the $2s^\text{th}$ RIC. The RIC calculation, however, involves a combinatorial optimization over the subset $S$ of the set $[N]$, and cannot be calculated in polynomial time -- and is therefore difficult to optimize.

However, it can be shown that
\begin{equation}
	\delta_{2s}(A) \leq (s-1) \mu(A)
	\label{eq:cohRicBound}
\end{equation}
and therefore, a looser, but easier way to reduce errors is to minimize the coherence $\mu$ of $A$. We will find applications of this later.

\section{Source separation}
Source separation is a classical problem in signal processing. It comes in two flavors: one in which both the nature of the signals and the mixing process is unknown (also referred to as blind source separation), and the easier case where the signals are still unknown but the mixing model is known. In the compressed sensing, we control the sensing framework -- so when (if) we use the source separation approach, the relevant paradigm is the second, easier one.

\subsection{The framework}
We consider the case in which two sources are combined in some (known) model, with the possible addition of bounded noise. In this case, the measurement model \cite{Studer201412} is
\begin{equation}
    z = Ax + Be + n
    \label{eq:sourcesepModel}
\end{equation}
where $A$ and $B$ are general deterministic dictionaries. For convenience, we assume that they are $l_2$-normalized in their columns. The vectors $x$ and $e$ are assumed to be sparse (we have a bit of leeway here: the source $x$ can be approximately sparse as well). The noise $n$ needs no constraint other than $\|n\| \leq \epsilon$, allowing arbitrary bounded noise models.

\subsection{Theoretical guarantees}
Under the assumptions of Eq.~\ref{eq:sourcesepModel}, \cite{Studer201412} proves the following about recovery of the vector $w = [x^T e^T]$: if $\norm{n}{2} \leq \epsilon$, $\mu_b < \mu_a$ and
\begin{equation}
    \norm{w}{0} = \norm{x}{0} + \norm{e}{0} < \max \left\{ \frac{2(1 + \mu_a)}{\mu_a + 2\mu_d + \sqrt{\mu_a^2 + \mu_m^2}}, \frac{1 + \mu_d}{2 \mu_d} \right\}
    \label{eq:sourcesepSparsity}
\end{equation}
where $\mu_a$ and $\mu_b$ are the coherences of the individual dictionaries $A$ and $B$, $\mu_m$ is the cross-coherence of $A$ and $B$ by taking pairs of columns, one from $A$ and one from $B$, and $\mu_d$ is the coherence of the joint dictionary $[A\ B]$, then the solution $w^*$ to the basis pursuit problem formed by this measurement model satisfies, in relation to the true $w$,
\begin{equation}
    \norm{w - w^*}{2} \leq C(\epsilon + \eta) + D \norm{w - w_\mathcal{W}}{1}
    \label{eq:sourcesepGuarantee}
\end{equation}
where $w_\mathcal{W}$ is $w$ restricted to the top $\norm{w}{0}$ elements and $C$ and $D$ are non-negative constants depending on the matrix coherence and signal sparsity. It is this theoretical guarantee that our sensing and recovery framework rely upon for coded source separation.

\section{Optimizing compressed sensing -- previous work}
It can be verified that the constants on the right hand sides of the error bounds in Eq. \ref{eq:ricBound2} and \ref{eq:sourcesepGuarantee} are increasing functions of the RIC. Upper bounds on these constants in terms of the coherence, therefore, are increasing functions of the coherence. A straightforward strategy to optimize matrices, therefore, is to reduce an upper bound on the right hand sides by reducing coherence. 

Given a sparsifying basis $\psi$, then, it is necessary to construct an `optimal' sensing matrix $\phi$ such that the coherence of the effective dictionary $A = \phi \psi$ is low. Most previous work and our first method do this in terms of $\mu(D)$.

\subsection{Coherence minimization via the Gram matrix}
One way to look at the coherence is~\cite{Elad200610} to look at the absolute maximum non-diagonal element of $G = D^T D$.
% Since $D is overcomplete, this is a low-rank matrix.
The goal is to reduce the magnitudes of the non-diagonal elements. \cite{Elad200610} tries to minimize the following function, with a parameter $t$:
\begin{equation}
\mu_t{\left(D\right)} = \frac{\sum_{i \neq j} \left(|g_{ij}| > t\right) |g_{ij}| }{\sum_{i \neq j} \left(|g_{ij}| > t\right)}
\label{eq:tAvgCoh}
\end{equation}
This is an absolute average of off-diagonal Gram matrix entries above $t$. To achieve this, \cite{Elad200610} processes the entries of the Gram matrix with a `shrinking' function Fig.~\ref{fig:eladShrink}, forces the shrunk Gram matrix to be low-rank to get a `new' Gram matrix, and builds the square root of the this matrix to obtain the updated dictionary.

\begin{figure}
    \centering
    \includegraphics[scale=0.6]{pics/shrinkfn.png}
    \caption{Shrinking function in \cite{Elad200610}}
    \label{fig:eladShrink}
\end{figure}

However, this method gives no guarantees on whether the actual maximum value decreases or not (notice the method minimizes the \textit{average} value of off-diagonal elements above $t$). Also, the square-root step involves an assumption that the input matrix is positive semi-definite, which is not always the case. When it is not, one needs to force the offending eigenvalues to zero. Guarantees on whether coherence decreases across these iterations don't exist.

\subsection{Coherence Minimization via rank-1 approximation}
An equivalent way to look at the problem is making the columns of $D$ as `orthogonal' to each other as possible. This implies that the Gram matrix $G$ should be as close to the identity matrix as possible. \cite{Duarte200907} solves the problem of estimating $\phi$ given $\psi$ this way (\cite{Duarte200907} also solves the problem of estimating both jointly from sample signals, but that is not applicable in the general video scenario). Knowing that we need $G = \psi^T \phi^T \phi\psi \approx I$, $\psi \psi^T \phi^T \phi \psi \psi^T \approx \psi\psi^T$. With $\psi\psi^T = V \Lambda V^T$ and $\phi V = \Gamma$, we need $\Lambda \Gamma^T \Gamma \Lambda \approx \Lambda$. So we solve
\begin{equation}
\min_{\Gamma} \left\|\Lambda \Gamma^T \Gamma \Lambda - \Lambda \right\|_F
\label{eq:rankApproxOpt}
\end{equation}
This can be written as 
\begin{equation}
\min_{\Gamma} \left\| \Lambda - \sum_{i} \mathbf{\nu}_i \mathbf{\nu}_i^T \right\|_F = \min_{\Gamma} \left\| \Lambda - \sum_{i, i \neq j} \mathbf{\nu}_i \mathbf{\nu}_i^T - \mathbf{\nu}_j \mathbf{\nu}_j^T \right\|_F
\label{eq:rankApproxSimp}
\end{equation}
where $\nu_i$ is the $i^\text{th}$ column of $\Lambda \Gamma^T$. This, however, is a rank-1 approximation problem which can be solved non-iteratively with the singular value decomposition of $\Lambda - \sum_{i, i \neq j} \nu_i \nu_i^T$. We do this by initializing $\Lambda \Gamma^T$ to a random matrix and successively optimizing for all $j$. This in turn yields $\Gamma$, and therefore $\phi$.

The paper however does not proceed with the SVD of E, which is the natural thing to do as per the Eckart-Young theorem. Instead it proceeds with eigenvalue decomposition, which makes the technique less rigorous since it is no longer guaranteed to be positive semi-definite, and this requires the negative eigenvalues to be set to 0. Also, this method minimizes some appropriate average of the Gram matrix elements and therefore isn't guaranteed to minimize the maximum of off-diagonal entries.

\subsection{Information-theoretic methods}
Some authors have taken an information-theoretic route to the problem \cite{Carson2012,Renna2013,Weiss2007}. These papers design sensing matrices $\phi$ such that the mutual information between a set of small patches $\{x_i\}_{i=1}^n$ and their corresponding projections $\{y_i\}_{i=1}^n$ where $Y_i = \phi x_i$, is maximized. The minimum mean square error (MMSE), it can be shown, is lower bounded by a quantity decreasing in this mutual information. Maximizing this mutual information, therefore, gives the MMSE more leeway to decrease. However, this gives no upper bound on MMSE and nothing prevents it from being much larger. Besides, computing this mutual information first requires estimation of the probability density function of $X$ and hence $Y$ using Gaussian mixture models, for instance. This can be expensive and is an iterative process. Moreover these learned GMMs for a class of patches may not be general enough to ably represent patches of other classes. 

\subsection{Other methods and applications}
There have been an array of other efforts for compressed sensing design recently, all using the coherence as a goodness criterion for sensing matrices. For instance, \cite{Mordechay2014} designs an optimal energy-preserving sensing matrix for Poisson compressed sensing, where the optimizing criterion is the coherence directly. \cite{Abolghasemi10} uses a method similar to \cite{Duarte200907} for optimizing general sensing matrices for coherence with gradient descent. In \cite{Pereira14} is a method to design sensing matrices maximally incoherent with the sparsifying orthogonal basis. \cite{Parada17} applies coherence minimization to design structured matrices for the Coded Aperture Snapshot Spectral Imaging (CASSI) system \cite{Gehm07,Wagadarikar08}. \cite{Bouchhima15} and \cite{Obermeier17} apply coherence-based design to environmental sounds and electromagnetic compressed sensing applications respectively. 
