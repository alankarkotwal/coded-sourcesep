%\subsection{An $l_1$ sparsity level-based error criterion}
%Tang \textit{et al}. \cite{Tang2011} introduce a new sparsity measure, the $l_1$ sparsity level $s(x)$, of a vector $x$, defined as
%\begin{equation}
%    s(x) = \frac{\norm{x}{1}^2}{\norm{x}{2}^2}
%    \label{l1SparsityLevel}
%\end{equation}
%
%It is easy to see that $s(x) \leq \norm{x}{0}$. The nullspace property \cite{Foucart2013} then implies that, in the noiseless case, that if for all $z$ in Ker($A$), $s(z) > 4k$, all $k$-sparse vectors (in the $l_0$ norm) can be recovered noiselessly with basis pursuit.
%
%Next, they define a quantity they call the $l_1$-constrained minimum singular value ($l_1$ CMSV) of A, $\rho_s(A)$
%\begin{equation}
%    \rho_s(A) = \min_{x \neq 0,\ s(x) < s} \frac{\norm{Ax}{2}}{\norm{x}{2}}
%    \label{eq:l1cmsv}
%\end{equation}
%
%With this, it can be proved that if $\norm{w}{2} \leq \epsilon$ and $\norm{x}{0} \leq k$, then the solution $x^*$ to the basis pursuit problem satisfies
%\begin{equation}
%    \norm{x^*-x}{2} \leq \frac{2\epsilon}{\rho_{4k}(A)}
%    \label{eq:l1SparsityGuarantee}
%\end{equation}
%
%This seems to suggest that this error bound can be minimized by maximizing $\rho_{4k}(A)$ over the entries of $A$. Consider, therefore, the process of calculating $\rho_s(A)$. The optimization problem in \ref{eq:l1cmsv} is equivalent to 
%\begin{equation}
%    \rho_s(A) = \min_{\norm{x}{1} \leq \sqrt{s},\ \norm{x}{2}=1} \norm{Ax}{2}
%    \label{eq:l1cmsvOpt}
%\end{equation}
%This, however, is a non-convex optimization problem due to the equality constraint $\norm{x}{2} = 1$. Therefore, it is in general hard to compute the global minimum and hence the $L_1$ CMSV. \cite{Tang2011}, however, give an algorithm to find a lower bound on the $l_1$ CMSV. The value presented by this algorithm can be maximized as a function of $A$. We have not explored this area yet.

%\subsection{Measures of sparsity}
%The two sections above show examples where a sparsity measure other than the $l_0$ norm can give quantities easier to compute and optimize than the RIC. It, therefore, makes sense to look for measures that preserve the intuitive sense of sparsity and follow some axioms that a reasonable sparsity measure should follow.
%
%\cite{Hurley2009} propose six qualities that a sparsity measure should have. These are
%\begin{itemize}
%    \item \textit{Robin Hood}: Stealing from the rich and giving to the poor decreases sparsity.
%    \item \textit{Scaling}: Sparsity is invariant under scaling.
%    \item \textit{Rising Tide}: Adding a constant to all entries decreases sparsity.
%    \item \textit{Cloning}: Sparsity is invariant under cloning.
%    \item \textit{Bill Gates}: Huge entries increase sparsity.
%    \item \textit{Babies}: Zeros increase sparsity.
%\end{itemize}
%
%It can be proved that the first four imply the last two, and are the maximal independent set of properties among these. They further survey some 15-odd measures of sparsity used in practice and determine if they are consistent with these properties. It turns out only two, the $pq$-mean and the Gini index follow all these. We hope to use these quantities to derive tractable upper bounds on reconstruction errors to optimize sensing matrices in a principled manner.

\subsection{Using the $l_\infty$ criterion}
To optimize for an $A$, for a given sparsity level $s$, such that $\omega(A, 2s)$ is maximized, we want
\begin{equation}
    A^* = \arg \max_{\norm{a_j}{2} \leq 1 \forall j} \min_{i \in {1..n}} \min_{\lambda \in \mathbb{R}^{n-1}} \norm{Q_i - Q(:, \sim i) \lambda}{\diamond} \text{ subject to } \norm{\lambda}{1} \leq 2s-1
    \label{eq:omegaMax}
\end{equation}
where the constraint on the $l_2$ norm of the columns of $A$ prevents them from blowing off to $\infty$. For a first attempt we use Matlab's \texttt{fminimax} solver to solve the minimax problem
\begin{equation}
    A^* = \arg \min_{\norm{a_j}{2} \leq 1 \forall j} \max_{i \in {1..n}} F_i(A)
    \label{eq:omegaMinimax}
\end{equation}
where $F_i(A) = -\min_{\lambda \in \mathbb{R}^{n-1}} \norm{A_i - A(:, \sim i) \lambda}{\diamond} \text{ subject to } \norm{\lambda}{1} \leq 2s-1$

More formally, we try to use gradient descent to perform this minimization. This problem is not convex in $A$, so we use a multi-start strategy and an adaptive step size. To calculate gradients, we use the fact that the objective function is a maximum of the $F_i(A)$s over all $i$. This means, $dF/dA = dF_{i^*}/dA$ for that $i^*$ for which $F_{i^*}(A)$ is the maximum at the current estimate of $A$. We have the value of $\lambda^*$ corresponding to the maximizing $i^*$. The problem then boils down to calculating the gradients of $F_{i^*}(A)$ with respect to $A$, given $\lambda^*$.

We consider the matrix $A$ as a collection of vectors $a_j$, set the $\diamond$ to be the $l_2$ norm and optimize $P_{i^*}(A) = F_{i^*}^2(A)$ with respect to $a_j$. Define
\begin{equation}
    g_i(j) = \begin{cases} 
      \ \ \ \ j & j < i \\
      \ \ \ \ j+1 & j \geq i \\
   \end{cases}
    \label{eq:gFuncDefn}
\end{equation}
Then, we the $j^\text{th}$ column of $A(:, \sim i)$ is $a_{g_i(j)}$ because the numbering for $A(:, \sim i)$ skips the $i^\text{th}$ column. The inverse function is
\begin{equation}
    g_i^{-1}(k) = \begin{cases} 
      \ \ \ \ k & k < i \\
      \ \ \ \ \infty & k = i \\
      \ \ \ \ k-1 & k > i
    \end{cases}
    \label{eq:gInvDefn}
\end{equation}
Now,
\begin{equation}
    \begin{split}
        \frac{dP_{i^*}(A)}{da_j} &= 2\ \norm{a_{i^*} - A(:, \sim i^*) \lambda^*}{2}\ \frac{d}{da_j} \left( a_{i^*} - A(:, \sim i^*) \lambda^* \right) \\
        &= 2\ \norm{a_{i^*} - A(:, \sim i^*) \lambda^*}{2}\ \left( \mathbf{1}_{i^* = j} - \mathbf{1}_{i^* \neq j} a_{g_{i^*}^{-1}(j)} \lambda^*(g_{i^*}^{-1}(j)) \right)
    \end{split}
    \label{eq:omegaGrad}
\end{equation}
where $\lambda^*(g_{i^*}^{-1}(j))$ is the $g_{i^*}^{-1}(j)^\text{th}$ element of $\lambda^*$.

Given an initial guess for $A$, we determine the $i^*$ and the $\lambda^*$ corresponding to the $i^*$ that maximize $F_i(A)$ over all $i$. We then take a step in the gradient found above and update $A$ to the guess at the current step and repeat.

\section{Optimizing general sensing matrices}
We use Matlab's \href{https://www.mathworks.com/help/optim/ug/fminimax.html}{\texttt{fminimax}} solver to perform the optimization in \ref{eq:omegaMinimax} for matrices of small sizes. This solver deals with arbitrary objective functions and uses finite differencing to solve for a local minimum. We initialize the solver with a positive random matrix with uniformly random entries in [0, 1]. After optimization, we compare the performance of the initialization and the optimized matrix on a set of synthetic, sparse vectors and show a boxplot of relative reconstruction errors.

