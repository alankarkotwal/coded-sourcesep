\documentclass[11pt,a4paper]{article}
\usepackage{amsmath,amssymb,graphicx,epstopdf,epsfig,url}
\usepackage[margin=0.75in]{geometry}
\usepackage{lettrine}

\title{Optimizing Sensing Matrices for Compressed Sensing}

\author{Alankar Kotwal $\|$ 12D070010 \\
Electrical Engineering, IIT Bombay
}
\date{}

\pagenumbering{gobble}

\begin{document}
\vspace{-2in}
%\thispagestyle{empty}
\renewcommand{\abstractname}{Dual Degree Thesis: Abstract}

\maketitle

\vspace{-20pt} 

\begin{abstract}

The elegant mathematical formalism behind signal recovery using compressed sensing principles provides theoretical guarantees on the error between recovered and ground truth signals. These guarantees are provided in terms of conditions involving signal sparsity and some property of the sensing matrix. An attempt, therefore, can be made to find `good' sensing matrices by optimizing upper bounds on recovery error. This thesis is a story of such attempts, spanning general matrices and matrices tailored to very specific applications. 

We start off by pointing out that there exist several applications in image processing (like video compressed sensing in the compressive camera described in [Hitomi, Y., Gu, J., et al, ``Video from a single coded exposure photograph using a learned overcomplete dictionary''] and color image demosaicing) which require separation of constituent images given measurements in the form of a coded superposition of those images. This is a compressive measurement. Physically practical code patterns in these measurements are non-negative and do not obey the nice coherence properties of other patterns such as Gaussian codes, which can adversely affect reconstruction performance. This is inspiration for us to optimize such patterns for coherence while balancing design complexity and optimality with techniques like patchwise reconstruction and circular shifts. This design rule gets us matrices that perform better than random at high compressions and sparsity.

Inspired by the improvement in reconstruction quality in the coded source separation case, we proceed to designing matrices for another camera. This Coded Aperture Compressive Temporal Imaging system [Llull, P., Liao, X., et al, ``Coded aperture compressive temporal imaging''], or CACTI, generates compressive measurements by mechanical translation of a coded aperture. We optimize coherence, again, for the sensing matrix induced by this coded aperture. This time, however, our design does not perform better than random.

This leads us to explore the empirical nature and `looseness' of compressed sensing bounds by calculating, step by step, the difference induced by the chain of inequalities in proofs of these bounds [Studer, C., Baranuik, R. G., ``Stable restoration and separation of approximately sparse signals''] in these two situations, and in general. The results are surprising, and reveal that much needs to be done for the conventional compressed sensing bounds to be useful for matrix design in general. 

We then deviate from the video compressed sensing scenario and explore another bound on the reconstruction error that is easier to calculate than bounds based on the RIC (restricted isometry constant) whose computation is known to be NP-hard, and tighter than the ubiquitous coherence bound, with an aim to exploit it for optimization. However, we will see that this bound, as well, has its own quirks that make its use in practical compressed sensing matrix optimization intractable.

Moreover, we also perform a bound looseness analysis for the RIC as in [Cai, T. T., Wang, L., Xu, G., ``New Bounds for Restricted Isometry Constants'']. The conclusion of these efforts is that coherence optimization is problematic not only because of the coherence bound on the RIC, but also the RIC bound itself. These negative results imply that despite the success of previous work in designing sensing matrices based on optimization of a matrix quality factor, one needs to exercise caution in using them for practical sensing matrix design.

We then introduce an alternative paradigm for optimizing sensing matrices that overcomes the looseness of compressed sensing upper bounds using an average case error approach. We show a proof-of-concept design using this paradigm that performs convincingly better than coherence-based design in not only the CACTI case, but also for general matrices.

To present a complete story of the design process for these cameras, we note that errors are induced by in coded apertures by the manufacturing process. We show how, by taking into account these errors, we can recover input signals and true coded aperture values.
\end{abstract}

\noindent \textbf{Keywords} -- video compressed sensing, source separation, sensing matrices, coherence, optimization, error bounds, sparsity measures, alternative error bounds, restricted isometry, coded aperture, circular shifts, temporal imaging, CACTI, looseness of bound, matrix perturbation, quantization, support prior

\end{document}
