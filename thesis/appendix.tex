\begin{appendices}

\chapter{Derivation of coherence expressions} \label{App:derCoh}
Recalling our definitions, we call the index varying from $1$ to $T$ as $\mu$ or $\nu$, and the index varying from $1$ to $n$ as $\alpha$, $\beta$ or $\gamma$. The $\mu^\text{th}$ block of $\Phi$ is thus $\phi_\mu$. Let the $\beta^\text{th}$ diagonal element of $\phi_\mu$ be $\phi_{\mu\beta}$. Define the $\alpha^\text{th}$ column of $D^T$ to be $d_\alpha$. Thus, the Gram matrix $\tilde{M} = \Psi^T \Phi^T \Phi \Psi$ has the block structure
\begin{align*}
\tilde{M}_{\mu \nu} &= D^T \phi_\mu^T \phi_\nu D \\
&= D^T \phi_\mu \phi_\nu D \\
&= \begin{pmatrix}
d_1 & d_2 & \hdots & d_n
\end{pmatrix}
\begin{pmatrix}
\phi_{\mu 1} \phi_{\nu 1} & 0 & \hdots & 0 \\
0 & \phi_{\mu 2} \phi_{\nu 2} & \hdots & 0 \\
\vdots & \vdots & \ddots & \vdots \\
0 & 0 & \hdots & \phi_{\mu n} \phi_{\nu n}
\end{pmatrix}
\begin{pmatrix}
d_1^T \\
d_2^T \\
\vdots \\
d_n^T
\end{pmatrix} \\
&= \begin{pmatrix}
d_1 & d_2 & \hdots & d_n
\end{pmatrix}
\begin{pmatrix}
\phi_{\mu 1} \phi_{\nu 1} d_1^T \\
\phi_{\mu 2} \phi_{\nu 2} d_2^T \\
\vdots \\
\phi_{\mu n} \phi_{\nu n} d_n^T
\end{pmatrix} \\
&= \sum_{\alpha = 1}^{n} \phi_{\mu \alpha} \phi_{\nu \alpha} d_\alpha d_\alpha^T
\end{align*}

\noindent The $\beta\gamma^\text{th}$ element of $\tilde{M}_{\mu \nu}$, thus, is
\begin{align}
\tilde{M}_{\mu \nu}(\beta\gamma) &= \sum_{\alpha = 1}^{n} \phi_{\mu \alpha} \phi_{\nu \alpha} d_\alpha (\beta) d_\alpha (\gamma)
\end{align}

\noindent Now we need to normalize the columns of $\Phi \Psi$. Squared column norms are diagonal elements of $\tilde{M}_{\mu \nu}$. So the product of the squared norms of the $\beta^\text{th}$ column of the $\mu^\text{th}$ block and the $\gamma^\text{th}$ column of the $\nu^\text{th}$ block is (call this $\xi_{\mu \nu}^2 (\beta \gamma)$)
\begin{align}
\xi_{\mu \nu}^2 (\beta \gamma) &= \left( \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right) \left( \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) \right)
\end{align}

\noindent Let the normalized Gram matrix be $M$. Thus, following the same conventions as above (define the numerator of the expression to be $\chi_{\mu \nu} (\beta \gamma)$), 
\begin{align}
M_{\mu \nu}(\beta\gamma) &= \frac{\sum_{\alpha = 1}^{n} \phi_{\mu \alpha} \phi_{\nu \alpha} d_\alpha (\beta) d_\alpha (\gamma)}{\sqrt{\left( \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right) \left( \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) \right)}} = \frac{\chi_{\mu \nu} (\beta \gamma)}{\xi_{\mu \nu} (\beta \gamma)}
\end{align}

\noindent Finally, using the square soft-max function to deal with the \texttt{max} in the coherence expression, we get the squared soft coherence $\mathcal{C}$ to be
\begin{align}
\mathcal{C} = \frac{1}{\theta} \log \left[ \sum_{\mu=1}^{T} \sum_{\nu=1}^{\mu-1} \sum_{\beta=1}^{n} \sum_{\gamma=1}^{n} e^{\theta M_{\mu \nu}^2(\beta\gamma)} + \sum_{\mu=1}^{T} \sum_{\beta=1}^{n} \sum_{\gamma=1}^{\beta - 1} e^{\theta M_{\mu \mu}^2(\beta\gamma)} \right]
\end{align}

\noindent In the above, the first term corresponds to all ($\mu > \nu$) blocks that are `below' the block diagonal. Here, we consider all terms in the given block for the maximum. The second term corresponds to ($\mu = \nu$) blocks on the block diagonal. Here, we consider only consider ($\beta > \gamma$) below-diagonal elements for the maximum.

\chapter{Derivation of coherence derivatives} \label{App:derCohDer}
Differentiating the expression for the squared soft coherence above, we get
\begin{equation}
\begin{split}
\frac{d\mathcal{C}(\Phi)}{d\phi_{\delta \epsilon}} = \frac{1}{\theta e^{\theta \mathcal{C}(\Phi)}} \left[ \sum_{\mu=1}^{T} \sum_{\nu=1}^{\mu-1} \sum_{\beta=1}^{n} \sum_{\gamma=1}^{n} 2\theta e^{\theta M_{\mu \nu}^2(\beta\gamma)} M_{\mu \nu}(\beta\gamma) \frac{dM_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} \right. \\
\left. + \sum_{\mu=1}^{T} \sum_{\beta=1}^{n} \sum_{\gamma=1}^{\beta - 1} 2\theta e^{\theta M_{\mu \mu}^2(\beta\gamma)} M_{\mu \mu}(\beta\gamma) \frac{\theta M_{\mu \mu}(\beta\gamma)}{d\phi_{\delta \epsilon}} \right]
\end{split}
\end{equation}

\noindent Next, we calculate the derivatives in the above equation, ${dM_{\mu \nu}(\beta\gamma)}/{d\phi_{\delta \epsilon}}$. Define the numerator of the expression for $M_{\mu \nu}(\beta\gamma)$ as $\chi_{\mu \nu}(\beta\gamma)$, and thus, $M_{\mu \nu}(\beta\gamma) = \chi_{\mu \nu}(\beta\gamma)/\xi_{\mu \nu}(\beta\gamma)$. Clearly,
\begin{align}
\frac{dM_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} = \frac{\xi_{\mu \nu}(\beta\gamma) \frac{d\chi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} - \chi_{\mu \nu}(\beta\gamma)\frac{d\xi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}}}{\xi_{\mu \nu}(\beta\gamma)^2}
\end{align}

\noindent Next,
\begin{align*}
\frac{d\chi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} &= \frac{d}{d\phi_{\delta \epsilon}} \sum_{\alpha = 1}^{n} \phi_{\mu \alpha} \phi_{\nu \alpha} d_\alpha (\beta) d_\alpha (\gamma) \\
&= \sum_{\alpha = 1}^{n} d_\alpha (\beta) d_\alpha (\gamma) \frac{d}{d\phi_{\delta \epsilon}} \left( \phi_{\mu \alpha} \phi_{\nu \alpha} \right)
\end{align*}

\noindent Notice that a term in the above summation can be non-zero only if $\alpha = \epsilon$. Thus,
\begin{align*}
\frac{d\chi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} &= d_\epsilon (\beta) d_\epsilon (\gamma) \frac{d}{d\phi_{\delta \epsilon}} \left( \phi_{\mu \epsilon} \phi_{\nu \epsilon} \right) \\
&= d_\epsilon (\beta) d_\epsilon (\gamma) \left( \phi_{\mu \epsilon} \frac{d\phi_{\nu \epsilon}}{d\phi_{\delta \epsilon}} + \frac{d\phi_{\mu \epsilon}}{d\phi_{\delta \epsilon}} \phi_{\nu \epsilon} \right)
\end{align*}

\noindent Now, notice that ${d\phi_{\mu \epsilon}}/{d\phi_{\delta \epsilon}}$ is non-zero only if $\mu = \epsilon$. Denote by $\uparrow_{\mu \epsilon}$ the Kronecker delta function, which is 1 only if $\mu = \epsilon$, 0 otherwise. Then,
\begin{align}
\frac{d\chi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} &= d_\epsilon (\beta) d_\epsilon (\gamma) \left( \phi_{\mu \epsilon} \uparrow_{\nu \delta} + \uparrow_{\mu \delta} \phi_{\nu \epsilon} \right)
\end{align}

\noindent Next, 
\begin{align*}
\frac{d\xi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} &= \frac{d}{d\phi_{\delta \epsilon}} \sqrt{\left( \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right) \left( \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) \right)} \\
&= \frac{1}{2 \xi_{\mu \nu}(\beta\gamma)} \frac{d}{d\phi_{\delta \epsilon}} \left( \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) \right) \\
&= \frac{1}{2 \xi_{\mu \nu}(\beta\gamma)} \left[ \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \frac{d}{d\phi_{\delta \epsilon}} \left( \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) \right) \right.\\
&\quad \quad \quad \quad \quad \left. + \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) \frac{d}{d\phi_{\delta \epsilon}} \left( \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right) \right]
\end{align*}

\noindent Again, a term in one of the above summations is non-zero only if $\alpha$ or $\tau$ is the same as $\epsilon$. Thus,
\begin{align*}
\frac{d}{d\phi_{\delta \epsilon}} \left( \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right) &= 2 \phi_{\mu \epsilon} d_\epsilon^2(\beta) \uparrow_{\mu \delta}
\end{align*}

\noindent Thus, 
\begin{align}
\frac{d\xi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} &= \frac{1}{\xi_{\mu \nu}(\beta \gamma)} \left[ \phi_{\mu \epsilon} d_\epsilon^2(\beta) \uparrow_{\mu \delta} \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) + \phi_{\nu \epsilon} d_\epsilon^2(\gamma) \uparrow_{\nu \delta} \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right]
\end{align}

\noindent This completes the calculation of derivatives. 

\end{appendices}