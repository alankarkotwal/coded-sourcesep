\chapter{Optimizing Coded Source Separation} \label{sourcesepforcv}

\lettrine[lines=2]{\scalebox{2}{W}}{ e} now look at how compressed sensing principles may be used for video data. In practical situations, it is easier to combine video frames across time than to combine frames across both space and time, which would have been superior. However, we find that the right linear combination of video frames gives good reconstruction results. Our final aim is to develop a framework and a set of codes that provide for optimal reconstruction on video data compressed across time.

\section{Sensing framework}
Linear coded combinations of input frames were exploited in implementation in \cite{Hitomi2011}, where $T$ vectorized input frames $\{x_i\}_{i=1}^{T}$ are sensed so that the vectorized output $y$ appears as a coded combination (dictated by the `sensing matrices' $\phi_i$) of the inputs. The sensing framework (depicted in Fig. \ref{fig:measModel}) is
\begin{equation}
y = \sum_{i=1}^{T} \phi_i x_i
\label{eq:hitSensing}
\end{equation}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{pics/measModel}
    \caption{Measurement Model}
    \label{fig:measModel}
\end{figure}

Since the output of this operation is a coded combination, this sensing framework constrains the $\phi_i$ to be a diagonal matrix, with the code elements on the diagonal.

The sparsifying basis here is a 3D dictionary learned on video patches. Given this dictionary, called $D$, any given signal $x$, and in particular, its frames $\{x_i\}_{i=1}^{T}$ can be approximately reconstructed as a sum of its projections $\alpha_j$ on the $K$ atoms in $D$:
\begin{equation}
x_i = \sum_{j=1}^{K} D_{ji} \alpha_{j}
\label{eq:hitDict}
\end{equation}
where $D_{ji}$ is the $i^\text{th}$ frame in the $j^\text{th}$ 3-D dictionary atom $D_{ji}$. From the measurements and the dictionary, the input images are recovered solving the following optimization problem:
\begin{equation}
\min_{\mathbb{\alpha}} \|\alpha\|_0 \text{ subject to } \left\| y - \sum_{i=1}^{T} \phi_i \sum_{j=1}^{K} D_{ji} \alpha_{j} \right\|_2 \leq \epsilon
\label{eq:hitOpt}
\end{equation}
This problem can be approximately solved with sparse recovery techniques like orthogonal matching pursuit \cite{Cai2011}.

The drawback here, though, is that the 3D dictionary imposes a smoothness assumption on the scene. Since a linear combination of dictionary atoms cannot `speed' an atom up, the typical speeds of objects moving in the video must be roughly the same as the dictionary. Also, because of the nature of the training data, the dictionary fails to sparsely represent sudden scene changes caused by, say, lighting or occlusion. Though it is possible to model a video sequence as a base frame followed by some innovation in the rest of the frames, this representation makes optimizing simpler and performs as well in reconstruction. Also, the remarkable success of sparse recovery makes us safe in modeling consecutive frames independently. Other techniques like \cite{VR201104} exploit additional structure within the signal, like periodicity, rigid motion or analytical motion models and cannot be used in the general video sensing case.

\section{Our approach to reconstruction}
We try relaxing these constraints using a source-separation approach~\cite{Studer201412}, where precise error bounds on the recovery of the images have been derived, with possible improvement using the techniques in~\cite{Cai2010}. 

We, therefore, propose to use a recovery method different from the one used in \cite{Hitomi2011}, within the same acquisition framework. Thus, our signals are still acquired according to Eq.~\ref{eq:hitSensing}. However, the choice of the sparsifying basis is different: we use a DCT basis $D$ to model each frame in the input data. The dictionary $\Psi$ sparsifying the entire video sequence, thus, is a block-diagonal matrix with the $n \times n$ sparsifying basis $D$ on the diagonal. Thus,
\begin{align}
y &= \begin{pmatrix}
\phi_1 & \hdots & \phi_T
\end{pmatrix}
\begin{pmatrix}
D \alpha_1 &
\hdots &
D \alpha_T
\end{pmatrix}^T \\
&= \begin{pmatrix}
\phi_1 D & \hdots & \phi_T D
\end{pmatrix}
\begin{pmatrix}
\alpha_1 &
\hdots &
\alpha_T
\end{pmatrix}^T
\label{eq:sourceSepModel}
\end{align}

\noindent Given a measurement $y$, we recover the input $\{x_i\}_{i=1}^{T}$ through the DCT coefficients $\alpha$ by solving the optimization problem
\begin{equation}
\min_{\alpha} \|\alpha\|_1 \text{ subject to } y = \Phi \Psi \mathbf{\alpha},\ \alpha = \begin{pmatrix}
\alpha_1 &
\alpha_2 &
\hdots &
\alpha_T
\end{pmatrix}^T
\label{eq:sourceSepOpt}
\end{equation}
In our implementation we used the \texttt{CVX}~\cite{cvx} solver for solving the convex optimization problem in Eq.~\ref{eq:sourceSepOpt}.

We experimented with basis pursuit recovery with Gaussian-random sensing matrices, getting excellent results with no visible ghosting for both similar and radically different images. Unfortunately, the more realizable positive sensing matrices do not have the nice incoherence properties of Gaussian-random matrices, which are sufficient conditions for near-accurate recovery as derived in~\cite{Studer201412}. At higher compressions and sparsities in input signals, we find, positive matrices do not work as well, making a case for designing good matrices of this kind.

\section{Optimizing codes for source separation}

\subsection{Track I: Direct coherence minimization}
Our aim here is to optimize the sensing matrices $\phi_i$ directly for minimum coherence with gradient descent. We now calculate gradients of the coherence with respect to the elements of $\phi_i$. As in Eq.~\ref{eq:sourceSepModel}, with an $n \times n$ dictionary $D$, we have the effective dictionary
\begin{align}
\Phi \Psi = \begin{pmatrix}
\phi_1 D & \phi_2 D & \hdots & \phi_T D
\end{pmatrix}
\end{align}
The expression for the coherence of a general dictionary \ref{eq:cohDefn} contains \texttt{max} and \texttt{abs} functions that a gradient-based scheme cannot handle. Instead, we soften the \texttt{max} and convert the \texttt{abs} to a square by using, for large enough $\theta$,
\begin{align}
\max_i \{t_i^2\}_{i=1}^{n} \approx \frac{1}{\theta} \log \sum_{i=1}^{n} e^{\theta t_i^2}
\label{eq:softMax}
\end{align} 

The advantage of using the square soft coherence is that lowering $\beta$ gives us some kind of average coherence, as compared to $\beta = \infty$, where we get the maximum. This enables us to reduce our design method to the methods used by others to minimize some average coherence, as well as gives us the framework to design structured matrices. 

We now need to evaluate the coherence of this dictionary as a function of the elements of $\Phi$. We will call the index varying from $1$ to $T$ as $\mu$ or $\nu$, and the index varying from $1$ to $n$ as $\alpha$, $\beta$ or $\gamma$. The $\mu^\text{th}$ block of $\Phi$ is thus $\phi_\mu$. Let the $\beta^\text{th}$ diagonal element of $\phi_\mu$ be $\phi_{\mu\beta}$. Define the $\alpha^\text{th}$ column of $D^T$ to be $d_\alpha$. Then, it can be shown~[Appendix~\ref{App:derCoh}] that the normalized dot product between the $\beta^\text{th}$ column of the $\mu^\text{th}$ block and the $\gamma^\text{th}$ column of the $\nu^\text{th}$ block is
\begin{align}
M_{\mu \nu}(\beta\gamma) &= \frac{\sum_{\alpha = 1}^{n} \phi_{\mu \alpha} \phi_{\nu \alpha} d_\alpha (\beta) d_\alpha (\gamma)}{\sqrt{\left( \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right) \left( \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) \right)}}
\end{align}

Finally, using the squared soft-max function [Eq.~\ref{eq:softMax}] to deal with the \texttt{max} and the \texttt{abs} in the coherence expression, we get the squared soft coherence $\mathcal{C}$ to be
\begin{align}
\mathcal{C} = \frac{1}{\theta} \log \left[ \sum_{\mu=1}^{T} \sum_{\nu=1}^{\mu-1} \sum_{\beta=1}^{n} \sum_{\gamma=1}^{n} e^{\theta M_{\mu \nu}^2(\beta\gamma)} + \sum_{\mu=1}^{T} \sum_{\beta=1}^{n} \sum_{\gamma=1}^{\beta - 1} e^{\theta M_{\mu \mu}^2(\beta\gamma)} \right]
\end{align}

In the above, the first term corresponds to all ($\mu > \nu$) blocks that are `below' the block diagonal. Here, we consider all terms in the given block for the maximum. The second term corresponds to ($\mu = \nu$) blocks on the block diagonal. Here, we consider only consider ($\beta > \gamma$) below-diagonal elements for the maximum.

\subsubsection{Calculation of coherence derivatives}
We note that the $\mathcal{C}$ computed in the section above is a function of $\Phi$. We differentiate $\mathcal{C}$ with respect to $\phi_{\delta \epsilon}$. For this, we define the numerator of the expression for $M_{\mu \nu}(\beta\gamma)$ as $\chi_{\mu \nu}(\beta\gamma)$ and the denominator as $\xi_{\mu \nu}(\beta\gamma)$. The derivative of the objective function can be found in terms of these quantities. Defining $\uparrow_{\mu \delta}$ to be the Kronecker delta function that is 1 only if $\mu = \delta$, it can be shown~[Appendix~\ref{App:derCohDer}]

\begin{equation}
\frac{d\chi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} = d_\epsilon (\beta) d_\epsilon (\gamma) \left( \phi_{\mu \epsilon} \uparrow_{\nu \delta} + \uparrow_{\mu \delta} \phi_{\nu \epsilon} \right)
\end{equation}
\begin{equation}
\frac{d\xi_{\mu \nu}(\beta\gamma)}{d\phi_{\delta \epsilon}} = \frac{1}{\xi_{\mu \nu}(\beta \gamma)} \left[ \phi_{\mu \epsilon} d_\epsilon^2(\beta) \uparrow_{\mu \delta} \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) + \phi_{\nu \epsilon} d_\epsilon^2(\gamma) \uparrow_{\nu \delta} \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right]
\end{equation}

Using these, we do gradient descent with adaptive step-size and use a multi-start strategy to combat the non-convexity of the problem.

\subsubsection{Time complexity and the need for something more}
The calculation of coherence for a matrix requires us to evaluate normalized dot products between columns of the matrix. In our case, the size of the matrix is $n \times nT$, and each dot product needs \order{n} operations, warranting the calculation of \order{n^3 T^2} quantities. Optimizing this rapidly becomes intractable as $n$ increases. The performance of gradient descent on this non-convex optimization problem also worsens as the dimensionality of the search-space (\order{nT}) increases.

Empirically, we observe that it is intractable to design codes that are more than $20 \times 20$ in size in any reasonable time. This points to the fact that we need something more to make designing effective codes possible.

\subsection{Track II: Including circular shifts}
The computational intractability of optimizing large codes leads us to designing smaller masks and tiling them to fit the image size we're dealing with. A small coherence for the designed patch guarantees good reconstruction for patches exactly aligned with the code block; however, other patches see a code that is a circular shift of the original code. Fig~\ref{fig:circMot} provides a visual explanation. The big outer square denotes the image. On top of the image we show tiled designed codes. Now, the patch in red clearly multiplies with the exact designed code; however the patch in green multiplies with a code shifted in both the coordinates circularly. 
\begin{figure}
\centering
\includegraphics[scale=0.5]{pics/descent-circular/circMot}
\caption{Motivation behind circularly-shifted optimization}
\label{fig:circMot}
\end{figure} 

This points to designing sensing matrices that have small coherence in all their circular permutations (note that these permutations happen in two dimensions and must be handled as such). To this end, we modify the above objective function to minimize the maximum coherence resulting from all circularly-shifted vectorized versions of $\Phi$. We thus have
\begin{align}
\mathcal{C} = \frac{1}{\theta} \log \left[ \sum_{\zeta \in \text{perm}(\Phi)} \left[ \sum_{\mu=1}^{T} \sum_{\nu=1}^{\mu-1} \sum_{\beta=1}^{n} \sum_{\gamma=1}^{n} e^{\theta M_{\mu \nu}^{(\zeta)2}(\beta\gamma)} + \sum_{\mu=1}^{T} \sum_{\beta=1}^{n} \sum_{\gamma=1}^{\beta - 1} e^{\theta M_{\mu \mu}^{(\zeta)2} (\beta\gamma)} \right] \right]
\end{align}
where $M_{\mu \nu}^{(\zeta)}(\beta\gamma)$ represents the normalized dot product between the $\beta^\text{th}$ column of the $\mu^\text{th}$ block and the $\gamma^\text{th}$ column of the $\nu^\text{th}$ block, resulting from the instance of the circular permutation $\zeta$ of $\Phi$. Derivatives of this expression are found exactly like in Appendix~\ref{App:derCohDer}, except that the $\mu$, $\nu$, $\beta$ and $\gamma$ parameters are subjected to the appropriate circular permutation.

The time complexity for determining this maximum coherence among all circular permutations is \order{n^5 T^2}, out of which a \order{n^3 T^2} term arises from the calculation of coherence for each circular permutation, and a \order{n^2} arises from the fact that there are $n^2$ such permutations. The advantage here, though, is that we don't need to optimize masks having very high values of $n$; we can do away with keeping $n$ a small constant because the scheme works for any $n$ such that $n$-sized patches are sparse in the dictionary $D$. This scheme is, thus, more scalable in terms of the size of the input image. Therefore the effective dimension of the optimization problem in such a scheme is, in terms of the variables that matter, \order{T^2}.

It is worth mentioning that this simple idea has been largely ignored in literature concerning sensing matrix optimization. As mentioned in the introduction, previous attempts mostly use an average coherence minimization technique \cite{Duarte200907,Elad200610,Mordechay2014} for full-sized sensing matrices, and are not as scalable as ours is for large images because they involve optimization problems in variables whose dimensions are at least of the order of image size. Sensing matrices can be designed at the patch level as well, for instance using information theoretic techniques as in~\cite{Carson2012,Renna2013,Weiss2007}, but the methods therein are not designed to account for the issue of overlapping reconstruction. To the best of our knowledge, ours is the first piece of work to handle this important issue in a principled manner.

\subsection{Track III: Optimizing bounds tighter than coherence} \label{sec:untractRIC}
The coherence bound mentioned in Eq.~\ref{eq:cohRicBound} is a very pessimistic bound: it arises from applying Ger\v{s}gorin disk theorem -- that bounds the eigenvalues of a matrix in terms of their distance from diagonal elements -- to the definition of the RIC as in Eq.~\ref{eq:ricDefn} and approximating the maximum column sum as $(s-1)$ times the maximum element constituting the sum \cite{Foucart2013}.

\subsubsection{Ger\v{s}gorin radii}
Instead, we can try to minimize the maximum Ger\v{s}gorin radius, achieving a tighter bound than coherence on the RIC. In our framework, then, do the following: given a particular $s$--cardinality subset $S$ of indices from 1 to $nT$, we want to evaluate dot products of (normalized) columns of $\Phi \Psi$. Let us call the sensing matrix with normalized columns $A$. Restricting this to the columns specified by $S$ reduces us to $A_S$. Note that 
\begin{align*}
\left[ A_S^T A_S - I \right]_{ij} &= \left[ A^T A - I \right]_{S_i S_j} \\
&= M_{\mu \nu} \left( \beta \gamma \right) - \mathbf{1}_{S_i = S_j}
\end{align*}
where we calculate the $\mu$, $\nu$, $\beta$, $\gamma$ arguments for the $M$ by the appropriate column number: $\mu^S_{i} = \text{floor}(S_i/n)$ and $\beta^S_{i} = S_i \text{ mod } n$. Call $M_{\mu^S_i \mu^S_j} \left( \beta^S_i \beta^S_j \right)$ as $\omega^S_{ij}$. This is symmetric in the arguments $i$ and $j$.

We now want to calculate row absolute sums for the matrix $A_S^T A_S - I$. Since by definition $M_{\mu \mu} (\beta \beta) = 1$,
\begin{align*}
\sum_{j} |\omega^S_{ij} - \mathbf{1}_{S_i = S_j}| = \sum_{j \neq i} |\omega^S_{ij}|
\end{align*}

Finally, using the square soft-max function, we get the maximum row absolute sum, the Ger\v{s}gorin radius and our objective function $\mathcal{C}$ to be
\begin{align*}
\mathcal{C}\left( \Phi \right) = \frac{1}{\theta} \log \left[ \sum_S \sum_i \exp \left\{ \theta \sum_{j \neq i} |\omega^S_{ij}| \right\} \right]
\end{align*}
Derivatives of this quantity are calculated in a similar way to the coherence function derivatives.

\subsubsection{Brauer ellipse bounds}
A similar bound to the Ger\v{s}gorin bound is the Brauer ellipse bound, which bounds the eigenvalue in an ellipse around diagonal elements, instead of circles. This is provably better than the Ger\v{s}gorin bound, and so can be used to get a tighter bound on the coherence. \\

However, these optimization problems are combinatorial in the size of the matrices involved and the sparsity one needs to optimize for. These are presented here only as attempts to see if they are feasible. It turns out they aren't.

\section{Experiments and results}

\subsection{Validating our framework}
We start with testing the proposed framework visually. In all such results in this chapter, we show successive frames top-to-bottom, and different types of reconstruction left-to-right. Here, for the sake of saving time, all reconstructions are done in a non-overlapping way. We first use two synthetic images that are known to have very low sparsity. These are $20 \times 20$ images, with only 3 out of the 400 DCT coefficients set to non-zero values. The results, with relative root mean errors of the order of $10^{-5}$, for these are shown in Fig.~\ref{fig:syn}. The results are similar for Gaussian sensing matrices and positive random matrices.
\begin{figure}[!h]
\fourTwoAcross{pics/framework_visual/syn1in}{pics/framework_visual/syn2in}{pics/framework_visual/syn1est}{pics/framework_visual/syn2est}{0.75}
\caption{Synthetic image results. Left: input images, Right: reconstructions}
\label{fig:syn}
\end{figure}

Next, we test on two video frames that are very similar, with Gaussian random matrices. The relative root mean square errors are around 0.0019 for each image. The results are shown in Fig.~\ref{fig:realGauss}.
\begin{figure}[!h]
\fourTwoAcross{pics/framework_visual/realGauss1in}{pics/framework_visual/realGauss1est}{pics/framework_visual/realGauss2in}{pics/framework_visual/realGauss2est}{1}
\caption{Real images, Gaussian matrices. Left: input images, right: reconstructions}
\label{fig:realGauss}
\end{figure}

Next, with positive random diagonal matrices, the relative root mean square errors are around 0.0036 for each image. The results are shown in Fig.~\ref{fig:realUnif}.
\begin{figure}[!h]
\fourTwoAcross{pics/framework_visual/realUnif1in}{pics/framework_visual/realUnif2in}{pics/framework_visual/realUnif1est}{pics/framework_visual/realUnif2est}{1}
\caption{Real images, uniform matrices. Left: input images, right: reconstructions}
\label{fig:realUnif}
\end{figure}
Looking at these results, one notices that there is very little to no ghosting, that is, appearance of features from one image into the other, in the output images even when the images are very close to each other. This is a very desirable property in any algorithm that separates images from compressed video.

To evaluate how this works for multiple images, we try separating three images with uniform matrices. See Fig.~\ref{fig:threeUnif}. Here, we notice ghosting happening in the third frame. However, with better-designed sensing matrices, one can think of getting rid of this effect. The relative root mean square errors here are worse, around 0.005 for each image.
\begin{figure}[!h]
\sixThreeAcross{pics/framework_visual/threeImage1in}{pics/framework_visual/threeImage2in}{pics/framework_visual/threeImage3in}{pics/framework_visual/threeImage1est}{pics/framework_visual/threeImage2est}{pics/framework_visual/threeImage3est}{1.75}
\caption{Separating three images, uniform matrices. Up:~input images, down:~reconstructions}
\label{fig:threeUnif}
\end{figure}

To simulate sudden changes, we run the optimization with two very different input images. We can separate these well, as is shown in Fig.~\ref{fig:realSuddenChangeUnif}.
\begin{figure}[!h]
\fourTwoAcross{pics/framework_visual/suddenChange1in}{pics/framework_visual/suddenChange1est}{pics/framework_visual/suddenChange2in}{pics/framework_visual/suddenChange2est}{1}
\caption{Sudden change, uniform matrices. Left: input images, right: reconstructions}
\label{fig:realSuddenChangeUnif}
\end{figure}

We do a numerical comparison between our designed codes and random codes for various values of $s = \|x\|_0/n$ and $T$. We randomly generate $T$ $s$-sparse (in 2D DCT) $8 \times 8$ signals $\{x_i\}_{i=1}^{T}$, combine them using random matrices to get $y$. Average relative root mean square errors on recovering the input signals from $y$ as a function of $s$ and $T$ are shown in Figs.~\ref{fig:frameworkNum} and \ref{fig:errorMapDesc}, and a difference map is shown in in Fig. \ref{fig:diffMapDesc} Errors are near-zero in the region where both $T$ and $s$ are small, and one can expect reasonable quality reconstructions till $T = 4$ from random matrices. To increase $T$ further, we would need to optimize our sensing matrix appropriately, as is shown further in this paper.
\begin{figure}[!h]
\centering
\includegraphics[scale=0.25]{sims/framework-num/errorMap}
\caption{Average relative root mean square errors in our scheme as a function of $s$ and $T$ with positive random matrices}
\label{fig:frameworkNum}
\end{figure}

\subsection{Demosaicing}
To demonstrate the utility of this scheme, we show results on demosaicing RGB images. The general demosaicing problem involves addressing the difficulty that on a camera sensor, a single pixel can sense only one of the three R, G and B channels. Therefore, raw camera data needs to be interpolated to recover all the three channels. Traditional approaches to demosaicing involve the use of the Bayer pattern, which tiles a fixed [B, G; G, R] pattern over the image and use variants of algorithms like edge-directed interpolation which are tuned to the Bayer pattern. The Matlab \texttt{demosaic} function, for instance, uses \cite{Malvar2004}, which takes a gradient-corrected bilinear interpolated approach. However, recently a case has been made for panchromatic demosaicing~\cite{Hirakawa2008}, where we sense a linear combination of the three channels and use techniques from compressive recovery to reconstruct. However, it turns out that the Bayer pattern has very high mutual coherence, so it is unsuitable for compressive recovery. Here, we propose to design the mosaic patterns by minimizing coherence.

We design $8 \times 8$ codes for linearly combining the three channels using our method and visually compare overlapping reconstructions. 
\begin{figure}[!h]
\sixThreeAcross{pics/demosaicing/736503_2161-DemosaicDesignedOvp-in}{pics/demosaicing/736503_2926-DemosiacRandomOvp-out}{pics/demosaicing/736503_2161-DemosaicDesignedOvp-out}{pics/demosaicing/736503_4279-ParrotDemosaicDesignedOvp-in}{pics/demosaicing/736503_4782-ParrotDemosiacRandomOvp-out}{pics/demosaicing/736503_4279-ParrotDemosaicDesignedOvp-out}{1.75}
\caption{Demosaicing. Left:~inputs, middle, right: reconstructions with \{random, non-circularly designed\} matrices}
\label{fig:demos}
\end{figure}
As Figs.~\ref{fig:demos} and \ref{fig:smallScaleDemos} show, results from the designed case are more faithful to the ground-truth than the random reconstructions are. The random reconstructions show (more) color artifacts, especially in areas where the input image varies a lot (car headlights in the top image, around parrot eyes in the bottom). Our designed codes do not show as many color artifacts. The relative root mean square errors don't differ much for these two cases, but subtle details of color are better preserved by our matrices. In Fig.~\ref{fig:smallScaleDemos}, notice in the first case the green artifacts near car headlights and the leftmost cyclist in the random reconstruction that is, while that ares is better-reconstructed with our matrices. The car headlight area on the car at the right is also better-reconstructed by our matrices. In the bottom, notice less color artifacts in the densely-varying area near the eye and on the bottom part of the beak.
\begin{figure}[!h]
\centering
\begin{tabular}{cc}
\includegraphics[scale=0.5]{pics/demosaicing/compDemos-1-in}
\hspace{-10pt}
%\vspace{-1pt}
&
\includegraphics[scale=0.5]{pics/demosaicing/compDemos-1-random}
\hspace{-10pt}
\\
\includegraphics[scale=0.5]{pics/demosaicing/compDemos-1-nonCirc}
\hspace{-10pt}
&
\includegraphics[scale=0.5]{pics/demosaicing/compDemos-1-circ}
\hspace{-10pt}
\\
\begin{tabular}{cc}
\includegraphics[scale=0.5]{pics/demosaicing/compDemos-2-in}
\hspace{-10pt}
&
\includegraphics[scale=0.5]{pics/demosaicing/compDemos-21-in}
\hspace{-10pt}
\end{tabular}
&
\begin{tabular}{cc}
\includegraphics[scale=0.5]{pics/demosaicing/compDemos-2-random}
\hspace{-10pt}
&
\includegraphics[scale=0.5]{pics/demosaicing/compDemos-21-random}
\hspace{-10pt}
\end{tabular}
\\
\begin{tabular}{cc}
\includegraphics[scale=0.5]{pics/demosaicing/compDemos-2-nonCirc}
\hspace{-10pt}
&
\includegraphics[scale=0.5]{pics/demosaicing/compDemos-21-nonCirc}
\hspace{-10pt}
\end{tabular}
&
\begin{tabular}{cc}
\includegraphics[scale=0.5]{pics/demosaicing/compDemos-2-circ}
\hspace{-10pt}
&
\includegraphics[scale=0.5]{pics/demosaicing/compDemos-21-circ}
\hspace{-10pt}
\end{tabular}
\end{tabular}
\caption{Demosaicing close-ups, examples \{1, 2, 3\}. Clockwise:~inputs, reconstructions with \{random, \{circularly, non-circularly\} designed\} matrices}
\label{fig:smallScaleDemos}
\end{figure}

\subsection{Coherence minimization}
The coherence of a uniform random matrix of the type we're interested in has a typical value around 0.8 for $8 \times 8$ codes. The distribution of these values is shown in the boxplot in Fig~\ref{fig:randDistr}.
\begin{figure}[!h]
\centering
\includegraphics[scale=0.25]{sims/descent/cohDist}
\caption{Distribution of coherences for $8 \times 8$ random positive codes as a function of $T$}
\label{fig:randDistr}
\end{figure}
The typical profile of descent on coherence from a random initialization is shown in Fig.~\ref{fig:descPlot}.
\begin{figure}[!h]
\centering
\includegraphics[scale=0.4]{pics/descent/descPlot}
\caption{Typical coherence decrease profile}
\label{fig:descPlot}
\end{figure}

The minimum coherence we have been able to achieve in this scheme has been around 0.27 (for $T = 2$). It is interesting to note that all initialization instances lead to coherences (for $T = 2$) of at the most 0.35, and hence empirically yield nearly as good matrices.

We first visually validate that our matrices perform better than positive random matrices. We design $8 \times 8$ codes and tile them, reconstructing patchwise with overlapping patches. An example of this running on six not necessarily close frames in a video is shown in Fig.~\ref{fig:optSix} (Fig.~\ref{fig:optTwo} shows an example for $T = 2$). Ghosting artifacts marked out in Fig.~\ref{fig:optSix} in white boxes in the random matrix reconstructions are absent or lower in the designed matrix reconstructions. These outputs show that on the large scale, we do as well as random matrices for low $T$ and better for high $T$. For a small scale comparison, see Figs.~\ref{fig:smallScaleComp1} and \ref{fig:smallScaleComp2}.
%736499_4602
\begin{figure}[]
\setlength{\tmpLength}{0.98\textwidth}
\setlength{\tmpLength}{0.12\tmpLength}
\def\arraystretch{1}
\setlength\tabcolsep{2pt}
\centerline{
\begin{tabular}{ccc}
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_0921-DesignedOvp-6-1-in} &
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_4602-RandomOvp-6-1-out} &
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_0921-DesignedOvp-6-1-out} \\
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_0921-DesignedOvp-6-2-in} &
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_4602-RandomOvp-6-2-out} &
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_0921-DesignedOvp-6-2-out} \\
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_0921-DesignedOvp-6-3-in} &
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_4602-RandomOvp-6-3-out} &
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_0921-DesignedOvp-6-3-out} \\
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_0921-DesignedOvp-6-4-in} &
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_4602-RandomOvp-6-4-out} &
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_0921-DesignedOvp-6-4-out} \\
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_0921-DesignedOvp-6-5-in} &
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_4602-RandomOvp-6-5-out} &
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_0921-DesignedOvp-6-5-out} \\
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_0921-DesignedOvp-6-6-in} &
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_4602-RandomOvp-6-6-out} &
\includegraphics[height=1.8\tmpLength]{pics/comparison/6/736499_0921-DesignedOvp-6-6-out} 
\end{tabular}
} % centering
\caption{Optimized output from combining six not necessarily close images. Left: inputs, middle, right: reconstructions with \{random, non-circularly optimized\} matrices}
\label{fig:optSix}
\end{figure}
\begin{figure}[!h]
\sixThreeAcross{pics/descent/736497_3251-RandomOvp-2-1-in}{pics/descent/736497_3251-RandomOvp-2-1-out}{pics/descent/736496_9893-DesignedOvp-2-1-out}{pics/descent/736497_3251-RandomOvp-2-2-in}{pics/descent/736497_3251-RandomOvp-2-2-out}{pics/descent/736496_9893-DesignedOvp-2-2-out}{1.75}
\caption{Optimized output from combining two close images. Left: inputs, middle, right: reconstructions with \{random, non-circularly optimized\} matrices}
\label{fig:optTwo}
\end{figure}

Finally, we do a numerical comparison similar to the one in Fig.~\ref{fig:frameworkNum}. The resulting error map is shown in Fig.~\ref{fig:errorMapDesc}. On an average, we see that we perform better than the random case. To characterize this, we compute the difference between these two error maps (random minus optimized). This is not very significant numerically, though it does produce significant changes in subtle texture as seen in Figs.~\ref{fig:smallScaleComp1} and \ref{fig:smallScaleComp2}.
\begin{figure}[!h]
\centering
\includegraphics[scale=0.25]{sims/descent/errorMap}
\caption{Error map for optimized codes as a function of $s$ and $T$}
\label{fig:errorMapDesc}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.25]{sims/framework-num/diffMap}
\caption{Average RRMSE using random codes minus average RRMSE using optimized codes, as a function of $s$ and $T$}
\label{fig:diffMapDesc}
\end{figure}
Note that though random matrices beat our designed matrices at some low values of $s$ and $T$, our motivation for optimization was to achieve better performance than random at high $s$ and $T$, which we comprehensively achieve.

\subsubsection{Circularly-symmetric coherence minimization}
Again, we design $8 \times 8$ codes for $T = 2$. To show coherence improvement between positive random codes, and codes designed with and without circular permutations, we plot the distribution of coherences of $\Phi^{(\zeta)} D$ in Fig.~\ref{fig:cohHist} for all circular permutations $\zeta$. Note that even though the coherences of non-circularly designed matrices are much lower than positive random matrices, the maximum coherence among all permutations is quite large. The circularly-designed matrices, however, have permuted coherences clustered around a low value. We then expect good reconstruction with all circular permutations, yielding good expected reconstructions for images.

\begin{figure}
\centering
\threeAcross{pics/descent-circular/random-cohHist}{pics/descent-circular/noncircular-cohHist}{pics/descent-circular/circular-cohHist}{1}
\caption{Left to right: Circularly-shifted coherence histograms for \{random, non-circularly optimized, circularly optimized\} matrices}
\label{fig:cohHist}
\end{figure}

Similar to the above section, we validate our matrices visually. Following the same conventions, here is an output for the $T=2$ case [Fig.~\ref{fig:optCircTwo}].
\begin{figure}[!h]
\sixThreeAcross{pics/descent/736497_3251-RandomOvp-2-1-in}{pics/descent-circular/736497_3251-RandomOvp-2-1-out}{pics/descent-circular/736509_9723-DesignedOvp-2-1-out}{pics/descent/736497_3251-RandomOvp-2-2-in}{pics/descent-circular/736497_3251-RandomOvp-2-2-out}{pics/descent-circular/736509_9723-DesignedOvp-2-2-out}{1.75}
\caption{Circularly optimized output from combining two close images. Left to right: reconstructions with \{random, circularly optimized\} matrices}
\label{fig:optCircTwo}
\end{figure}

We now look at reconstructions from random and both classes of our designed matrices on a small scale. As a first example, we show a close-up from the car video sequence shown earlier [Fig.~\ref{fig:smallScaleComp1}]. Note, to start off, that the reconstruction of the numberplate and headlight area is much clearer in our case than the random matrix case. Further, notice the presence of major ghosting in the random case, especially near the rear-view mirrors, bonnet (marked by arrows) and headlights (marked by boxes), while our reconstructions remain free of these artifacts. Adding circular optimization to the picture further improves image quality especially in the bonnet area, where the non-circular reconstruction is slightly splotchy. Next, in Fig.~\ref{fig:smallScaleComp2}, which is a smaller part of the same image, the superiority of our reconstruction is clearer, with the circular optimization smoothing out blotchier parts of the bonnet.
\begin{figure}[!h]
\twoHeight{pics/comparison/comp1-1}{pics/comparison/comp1-2}{80pt}
\caption{Close-ups showing subtle texture preservation with optimized matrices, example 1. Left to right: inputs, reconstructions with \{random, non-circularly optimized, circularly optimized\} matrices}
\label{fig:smallScaleComp1}
\end{figure}
\begin{figure}[!h]
\twoHeight{pics/comparison/comp2-1}{pics/comparison/comp2-2}{80pt}
\caption{Close-ups showing subtle texture preservation with optimized matrices, example 2. Left to right: inputs, reconstructions with \{random, non-circularly optimized, circularly optimized\} matrices}
\label{fig:smallScaleComp2}
\end{figure}
