T = 4;
desPref = ['736493.6297-DesignedPosNoOvp-' num2str(T) '-'];
randPref = ['736493.6308-FullRandomNoOvp-' num2str(T) '-'];

imSize = [113 151];
imSizeFin = [112 144];

origs = zeros([imSize T]);
rands = zeros([imSize T]);
desgs = zeros([imSize T]);

for i = 1:T

	origs(:, :, i) = imread([desPref num2str(i) '-in.png']);
	rands(:, :, i) = imread([randPref num2str(i) '-out.png']);
	desgs(:, :, i) = imread([desPref num2str(i) '-out.png']);

end

origs = origs(1:imSizeFin(1), 1:imSizeFin(2), :);
rands = rands(1:imSizeFin(1), 1:imSizeFin(2), :);
desgs = desgs(1:imSizeFin(1), 1:imSizeFin(2), :);

randErr = sum(sum(sum((rands - origs) .^ 2))) / sum(sum(sum(origs .^ 2)));
desgErr = sum(sum(sum((desgs - origs) .^ 2))) / sum(sum(sum(origs .^ 2)));

disp(['T = ' num2str(T) ', randErr = ' num2str(randErr) ', desgErr = ' num2str(desgErr)]);
