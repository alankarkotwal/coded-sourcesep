clear; close all; clc;

filestr = '736850_6694-64-6';

load([filestr '.mat']);

runName = filestr; 

matrixNoiseSDs = 0.03:0.03:0.24;

scrsz = get(groot, 'ScreenSize');
figure('Position', scrsz);
set(gca, 'FontSize', 50);
hold on;

noisesVec = 1:size(matrixNoiseSDs, 2);

bp1 = boxplot(rrmses, matrixNoiseSDs, 'Positions', noisesVec+0.25, 'symbol', '', 'widths', 0.4);
bp2 = boxplot(rrmsesOrig, matrixNoiseSDs, 'Positions', noisesVec-0.25, 'symbol', '', 'widths', 0.4);
xlim([0 size(noisesVec, 2)+1]);
ylim auto;

color = [repmat('b',1,size(matrixNoiseSDs, 2)), repmat('r', 1, size(matrixNoiseSDs, 2))];
h = findobj(gca,'Tag','Box');
for j=1:length(h)
  patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
end
c = get(gca, 'Children');
hleg1 = legend([c(1) c(size(matrixNoiseSDs, 2)+1)], 'Corrected', 'Original', 'Location', 'NorthWest');

%set(bp1(:,:),'linewidth',5);
%set(bp2(:,:),'linewidth',5);

xlabel('Matrix Noise Standard Deviation')
ylabel('RRMSE');
title(['RRMSE comparison: n = ' num2str(8^2) ', T = ' num2str(t)]);
set(gca, 'FontSize', 40);
savefig([runName '.fig']);


disp([runName '.png']);
export_fig([runName '.png'])

