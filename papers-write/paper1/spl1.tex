\documentclass{IEEEtran}

\usepackage{spconf,amsmath,amssymb,graphicx,formatfig,epstopdf,epsfig,url}
%\usepackage[margin=1in]{geometry}
\usepackage{url,hyperref}
\usepackage[justification=centering]{caption}
\usepackage{formatfig}
\usepackage{lettrine}
\usepackage{color}
\usepackage{float}

\newcommand{\order}[1]{$\mathcal{O}(#1)$}

\title{Optimizing Codes for Source Separation in Compressed Video Recovery and Color Image Demosaicing}
\name{\vspace*{-30pt}}
\address{Alankar Kotwal$^{1}$ (alankar.kotwal@iitb.ac.in) and Ajit Rajwade$^{2} $
%\sthanks{Prof. Rajwade thanks funding via the IIT Bombay Seed Grant \_\_\_\_. This is a space filler for two lines}
(ajitvr@cse.iitb.ac.in) \\ ${^1}$Electrical Engineering, ${^2}$Computer Science and Engineering,
Indian Inst. of Technology Bombay \\ \vspace*{-25pt}}

%\twoauthors
%  {Alankar Kotwal}
%	{Department of Electrical Engineering \\
%	 Indian Institute of Technology Bombay}
%  {Ajit V. Rajwade \sthanks{We thank funding via the IIT
%Bombay Seed Grant \_\_\_\_.}}
%	{Department of Computer Science \& Engineering \\
%	 Indian Institute of Technology Bombay}

\begin{document}
%\ninept

\setlength{\parindent}{0em}

\maketitle

\begin{abstract}
%Recent efforts at applying the principles of compressed sensing to video data involve sensing coded linear combinations of frames and separating them with a 3D over-complete dictionary. This needs a dictionary at the same frame-rate and time-smoothness as the video. We try relaxing this constraint using source-separation. With this, Gaussian-random sensing matrices give excellent results. Unfortunately, the practically more realizable non-negative sensing matrices don't work as well, because they do not have the nice incoherence properties of Gaussian-random matrices. We aim to design such positive sensing matrices with low coherence, making them ideal for compressed video. We also aim to design matrices that when permuted circularly, still have low mutual coherence so that they can be tiled for patchwise reconstruction. 
There exist several applications in image processing (eg: video compressed sensing \cite{Hitomi2011} and color image demosaicing) which require separation of constituent images given measurements in the form of a coded superposition of those images. Physically possible code patterns in these applications are non-negative and do not obey the nice coherence properties of Gaussian codes, which can adversely affect reconstruction performance. The contribution of this paper is to design code patterns for these applications by minimizing the coherence given a fixed dictionary. Our method explicitly takes into account the structure of those code patterns as required by these applications: (1)~non-negativity, (2)~block-diagonal nature, and (3)~circular shifting. In particular, the last property enables accurate patchwise reconstruction.
\end{abstract}

\begin{keywords}
video compressed sensing, color image demosaicing, source separation, coherence, patchwise recovery
\end{keywords}

\section{Introduction and Related Work}
\lettrine[lines=2]{\scalebox{1}{C}}{ompressed} sensing is a fast way of sampling sparse continuous time signals. Its success with images has inspired efforts to apply it to video. Indeed,~\cite{Hitomi2011} achieves compression across time by combining $T$ input frames $\{X_i\}_{i=1}^{T}$ linearly, weighted by the sensing matrices $\phi_i$ into the output $Y$: $Y = \sum_{i=1}^{T} \phi_i X_i$. The sparsifying basis here is a 3D dictionary $D$ learned on video patches. Any set of frames $\{X_i\}_{i=1}^{T}$ can then be represented as a sum of its projections $\alpha_j$ on the $K$ atoms in $D$: $X_i = \sum_{j=1}^{K} D_{ji} \alpha_{j}$, where $D_{ji}$ is the $i^\text{th}$ frame in the $j^\text{th}$ 3-D dictionary atom $D_{ji}$. The input images are recovered by solving the following optimization problem:
$\min_{\mathbb{\alpha}} \|\alpha\|_0 \text{ subject to } \left\| Y - \sum_{i=1}^{T} \phi_i \sum_{j=1}^{K} D_{ji} \alpha_{j} \right\|_2 \leq \epsilon$.
The drawback here is that using the 3D DCT, for example, imposes a smoothness assumption on the scene. Since a linear combination of dictionary atoms cannot `speed' an atom up, the typical speeds of objects moving in the video must be roughly the same as the dictionary. Therefore, the dictionary fails to sparsely represent sudden scene changes caused by, say, lighting or occlusion. Other than that, dictionaries are learned on classes of patches and a dictionary learned on outdoor scenes may not work as well on an indoor scene.

We treat each of the coded snapshots as a coded mixture of sources, each sparse in some basis. We aim to design codes with this structure and low coherence, making them ideal for compressed video. Most current approaches to this problem have their limitations: firstly, they do not account for the special structure (imposed by non-negative linear combinations) of the sensing matrices used in \cite{Hitomi2011} or for demosaicing, a framework which this paper expressly deals with. The method in~\cite{Elad200610} involves a step that requires a Cholesky-type decomposition of a `reduced' Gram matrix, and the non-linear reduction process is not guaranteed to keep the Gram matrix positive-semidefinite. Besides, the methods in~\cite{Duarte200907,Elad200610,Mordechay2014} optimize objective functions that are some average of normalized dot products of dictionary columns, and minimizing averages doesn't guarantee minimizing the maximum (coherence, in this case) of the quantities forming the average. Information-theoretic routes to this problem \cite{Carson2012,Renna2013,Weiss2007} design sensing matrices $\Phi$ such that the mutual information between a set of small patches $\{X_i\}_{i=1}^n$ and their corresponding projections $\{Y_i\}_{i=1}^n$ where $Y_i = \Phi X_i$, is maximized. Computing this mutual information first requires estimation of the probability density function of $X$ and $Y$. This can be expensive computationally, and may not be general enough to work on a different class of patches. Even with this restriction, overlapping patchwise reconstruction with these codes ignores the fact that patches in the input not aligned with the sensing pattern get multiplied by circularly shifted versions of the designed sensing patches, possibly losing out on optimal reconstruction. The procedure introduced in \cite{Arguello2013} for designing codes for a hyperspectral compressed sensing camera (CASSI) imposes successively spatially shifted structure (a restriction we do not assume) on its codes and employs a combinatorial-complexity design scheme. Other techniques \cite{VR201104} exploit structure like periodicity and analytical motion models in their input and cannot be used in general.

There are obvious applications for optimal code design in fast video sensing and the general problem of coded source separation. Besides, this will find applications in improving multi-spectral imaging and image demosaicing, where inputs are coded linear combinations of images sparse in some domain and need to be solved for in a source-separation framework.

\section{Method and Analysis}

\subsection{Our framework}
We propose using a recovery method different from the one used in \cite{Hitomi2011} within the same acquisition framework. We use a basis $D$ to model each frame in the input. The dictionary $\Psi$ sparsifying the video sequence is a block-diagonal matrix with the $n \times n$ sparsifying basis $D$ on the diagonal. Thus,
\begin{align}
Y
%&= 
%\begin{pmatrix}
%\Phi_1 & \hdots & \Phi_T
%\end{pmatrix}
%\begin{pmatrix}
%D \alpha_1 &
%\hdots &
%D \alpha_T
%\end{pmatrix}^T \\
&= \begin{pmatrix}
\phi_1 D & \hdots & \phi_T D
\end{pmatrix}
\begin{pmatrix}
\alpha_1 &
\hdots &
\alpha_T
\end{pmatrix}^T
\label{eq:sourceSepModel}
\end{align}

\noindent Given a $Y$, we recover the input $\{X_i\}_{i=1}^{T}$ through the DCT coefficients $\alpha$ by solving the optimization problem
\begin{equation}
\arg\min_{\alpha} \|\alpha\|_1, Y = \Phi \Psi \mathbf{\alpha},\ \alpha = \begin{pmatrix}
\alpha_1 &
\alpha_2 &
\hdots &
\alpha_T
\end{pmatrix}^T
\label{eq:sourceSepOpt}
\end{equation}

\subsection{Calculation of coherence for our matrices}
Our aim, then, is to optimize the sensing matrices $\phi_i$ for coherence. As in Eq.~\ref{eq:sourceSepModel}, we have the effective dictionary
\begin{align}
\Phi \Psi = \begin{pmatrix}
\phi_1 D & \phi_2 D & \hdots & \phi_T D
\end{pmatrix}.
\end{align}
The coherence of a dictionary $M$, with $i^\text{th}$ column $m_i$, is 
\begin{align}
\mu = \max_{i \neq j} \frac{|\left< m_i, m_j \right>|}{\sqrt{\left< m_i, m_i \right>\left< m_j, m_j \right>}}.
\end{align}
This expression contains \texttt{max} and \texttt{abs} functions that a gradient-based scheme cannot handle. We soften the \texttt{max} and convert the \texttt{abs} to a square using, for large enough $\theta$,
\begin{align}
\max_i \{t_i^2\}_{i=1}^{n} \approx \frac{1}{\theta} \log \sum_{i=1}^{n} e^{\theta t_i^2}.
\label{eq:softMax}
\end{align}

We will call the index varying from $1$ to $T$ as $\mu$ or $\nu$, and the index varying from $1$ to $n$ as $\alpha$, $\beta$ or $\gamma$. The $\mu^\text{th}$ block of $\Phi$ is thus $\phi_\mu$. Let the $\beta^\text{th}$ diagonal element of $\phi_\mu$ be $\phi_{\mu\beta}$. Define the $\alpha^\text{th}$ column of $D^T$ to be $d_\alpha$. Then, it can be shown~\cite{Full} that the dot product between the $\beta^\text{th}$ column of the $\mu^\text{th}$ block and the $\gamma^\text{th}$ column of the $\nu^\text{th}$ block is
\begin{align}
M_{\mu \nu}(\beta\gamma) &= \frac{\sum_{\alpha = 1}^{n} \phi_{\mu \alpha} \phi_{\nu \alpha} d_\alpha (\beta) d_\alpha (\gamma)}{\sqrt{\left( \sum_{\alpha = 1}^{n} \phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right) \left( \sum_{\tau = 1}^{n} \phi_{\nu \tau}^2 d^2_\tau (\gamma) \right)}}
\end{align}

Using Eq.~\ref{eq:softMax}, the squared soft coherence $\mathcal{C}$ is 
\begin{align}
\mathcal{C} = \frac{1}{\theta} \log \sum_{\mu=1}^{T} \sum_{\beta=1}^{n} \left[  \sum_{\nu=1}^{\mu-1} \sum_{\gamma=1}^{n} e^{\theta M_{\mu \nu}^2(\beta\gamma)} + \sum_{\gamma=1}^{\beta - 1} e^{\theta M_{\mu \mu}^2(\beta\gamma)} \right]
\end{align}

The first term above corresponds to all ($\mu > \nu$) blocks that are `below' the block diagonal. The second term corresponds to ($\mu = \nu$) blocks on the block diagonal. Here, we consider only ($\beta > \gamma$) below-diagonal elements for the maximum. Deriving expressions for the gradient of this quantity~\cite{Full}, we perform adaptive projected gradient descent with a multi-start strategy to combat the non-convexity of the problem. 

\subsection{Time complexity and the need for something more}
The size of $\Phi$ to be optimized above is $n \times nT$, and each dot product needs \order{n} operations, warranting the calculation of \order{n^3 T^2} quantities. Optimizing this rapidly becomes intractable as $n$ increases. The performance of gradient descent on this non-convex problem also worsens as the dimensionality of the search-space (\order{nT}) increases. We observe that it is intractable to design codes that are more than $20 \times 20$ in size in any reasonable time. This implies that we need something more to make designing codes possible.

\subsection{Circularly-symmetric coherence minimization}
This leads us to designing smaller masks and tiling them to fit the image size we're dealing with. A small coherence for the designed patch guarantees good reconstruction for patches exactly aligned with the code block; however, other patches see a code that is a circular shift of the original code. Fig.~\ref{fig:circMot} provides a visual explanation. The big outer square denotes the image. On top of the image, in bold lines, we show tiled codes. The lighter lines denote pixel boundaries. The patch in red clearly multiplies with the exact code; however the patch in green multiplies with a code shifted in both the coordinates circularly. 
\begin{figure}
\centering
\includegraphics[scale=0.33]{pics/descent-circular/circMot}
\caption{Motivation behind circularly-shifted optimization}
\label{fig:circMot}
\end{figure} 

This points to designing codes that have small coherence in all their circular permutations (note that these permutations happen in two dimensions and must be handled as such). To this end, we modify the above objective function:
\begin{equation}
\begin{split}
\mathcal{C} = \frac{1}{\theta} \log \sum_{\zeta \in \text{perm}(\Phi)} \sum_{\mu=1}^{T} \sum_{\beta=1}^{n} \left[ \sum_{\nu=1}^{\mu-1} \sum_{\gamma=1}^{n} e^{\theta M_{\mu \nu}^{(\zeta)2}(\beta\gamma)} \right. \\
\left. + \sum_{\gamma=1}^{\beta - 1} e^{\theta M_{\mu \mu}^{(\zeta)2} (\beta\gamma)} \right]
\end{split}
\end{equation}
where $M_{\mu \nu}^{(\zeta)}(\beta\gamma)$ represents the normalized dot product between the $\beta^\text{th}$ column of the $\mu^\text{th}$ block and the $\gamma^\text{th}$ column of the $\nu^\text{th}$ block, resulting from the instance $\zeta$ among the circular permutations of $\Phi$. Derivatives of this expression are found as in the non-circular case, except that the $\mu$, $\nu$, $\beta$ and $\gamma$ parameters are subjected to the appropriate circular permutation.

The time complexity for determining this quantity is \order{n^5 T^2}, with the extra \order{n^2} arising from $n^2$ permutations. However, we don't need to optimize masks having very high values of $n$; we can get away with keeping $n$ a small constant such that $n \times n$ patches are sparse in some chosen basis. Therefore the effective dimension of the optimization problem in such a scheme is, in terms of the variables that matter, \order{T^2} and is more scalable in terms of the size of the input image. 

It is worth mentioning that this simple idea has been largely ignored in literature. Previous attempts \cite{Duarte200907,Elad200610,Mordechay2014} minimize coherence for full-sized matrices, and are not as scalable for large images because they involve optimization problems of dimensions of the order of image size. Sensing matrices can be designed at the patch level as well, for instance using information theoretic techniques as in~\cite{Carson2012,Renna2013,Weiss2007}, but the methods therein are not designed to account for overlapping reconstruction. To the best of our knowledge, ours is the first piece of work to handle this important issue in a principled manner.
%Information-theoretic methods~\cite{Carson2012,Renna2013,Weiss2007} ignore the issue of overlapping reconstruction. As far as we know, we are the first to come up with optimization on circular permutations so that the matrices are amenable to overlapping reconstruction.

\section{Validation and Results}
\subsection{Coherence minimization}
The distribution of coherences of a uniform random matrix of the type we're interested in is shown in Fig~\ref{fig:randDistr}. It is interesting to note that the resulting minimum coherence is close for all random starts, and therefore the corresponding designed matrices are nearly equally good.
\begin{figure}[!h]
\centering
\includegraphics[scale=0.175]{sims/descent/cohDist}
\caption{Distribution of coherences for $8 \times 8$ random positive codes as a function of $T$}
\label{fig:randDistr}
\end{figure}

To show coherence improvement between positive random codes, and codes designed with and without circular permutations, we plot a histogram of coherences of $\Phi^{(\zeta)} D$ in Fig.~\ref{fig:cohHist} for all circular permutations $\zeta$, with $8 \times 8$ codes and $T=2$. Note that though the coherences of non-circularly designed matrices are much lower than positive random matrices, the maximum coherence among all permutations is quite large. The circularly-designed matrices, however, have permuted coherences clustered around a low value. We then expect good reconstruction with all circular permutations.
%, yielding good expected reconstructions for images.
\begin{figure*}
\centering
\begin{tabular}{ccc}
\includegraphics[scale=0.2]{pics/descent-circular/random-cohHist}
&
\includegraphics[scale=0.2]{pics/descent-circular/noncircular-cohHist}
&
\includegraphics[scale=0.2]{pics/descent-circular/circular-cohHist}
\end{tabular}
\caption{Left to right: Circularly permuted coherence histograms, \{random, \{non-circularly, circularly\} optimized\} matrices}
\label{fig:cohHist}
\end{figure*}

\subsection{Demosaicing}
To demonstrate our scheme, we show results on demosaicing RGB images. The traditional demosaicing problem involves interpolating pixel-sensed RGB data acquired from a camera to estimate the original scene using the Bayer pattern. Recovery algorithms, like Matlab's \texttt{demosaic} function, take approaches like \cite{Malvar2004}, a gradient-corrected bilinear interpolation approach. A case has recently been made for panchromatic demosaicing~\cite{Hirakawa2008}, where we sense a coded linear combination of the three channels. However, the Bayer pattern has very high coherence, unsuitable for compressive recovery. Here, we design the mosaic patterns by minimizing coherence.
\newcommand{\scaleUp}{0.35}
\newcommand{\scaleDownA}{0.25}
\newcommand{\scaleDownB}{0.35}
\begin{figure}[!h]
\centering
\begin{tabular}{cc}
\includegraphics[scale=\scaleUp]{pics/demosaicing/compDemos-1-in}
\hspace{-20pt}
&
\includegraphics[scale=\scaleUp]{pics/demosaicing/compDemos-1-random}
\\
\includegraphics[scale=\scaleUp]{pics/demosaicing/compDemos-1-nonCirc}
\hspace{-20pt}
&
\includegraphics[scale=\scaleUp]{pics/demosaicing/compDemos-1-circ}
\\
\begin{tabular}{cc}
\includegraphics[scale=\scaleDownA]{pics/demosaicing/compDemos-2-in}
\hspace{-10pt}
&
\includegraphics[scale=\scaleDownA]{pics/demosaicing/compDemos-2-random}
\hspace{-10pt}
\end{tabular}
&
\begin{tabular}{cc}
\includegraphics[scale=\scaleDownB]{pics/demosaicing/compDemos-21-in}
\hspace{-10pt}
&
\includegraphics[scale=\scaleDownB]{pics/demosaicing/compDemos-21-random}
\hspace{-10pt}
\end{tabular}
\\
\begin{tabular}{cc}
\includegraphics[scale=\scaleDownA]{pics/demosaicing/compDemos-2-nonCirc}
\hspace{-10pt}
&
\includegraphics[scale=\scaleDownA]{pics/demosaicing/compDemos-2-circ}
\hspace{-10pt}
\end{tabular}
&
\begin{tabular}{cc}
\includegraphics[scale=\scaleDownB]{pics/demosaicing/compDemos-21-nonCirc}
\hspace{-10pt}
&
\includegraphics[scale=\scaleDownB]{pics/demosaicing/compDemos-21-circ}
\hspace{-10pt}
\end{tabular}
\end{tabular}
\caption{Demosaicing \{1, \{2, 3\}\}. Clockwise:~inputs, reconstructions with \{random, \{circularly, non-circularly\} designed\} matrices. Zoom in for more clarity.}
\label{fig:demos}
\end{figure}

As Fig. \ref{fig:demos} shows, results from the designed case are more faithful to the ground-truth than the random reconstructions are. The random reconstructions show (more) color artifacts than ours, especially in areas where the input image varies a lot. Notice the artifacts near car headlights (green blotches) and those in the densely-varying area near the eyes of the parrots in Fig.~\ref{fig:demos}. Using our optimized matrices reduces the magnitude of these color artifacts. Better reconstructions are when we use circularly-optimized matrices. Full-scale color image demosaicing results live in section 3.2 of \cite{Full}.

\subsection{Results on video data}
Next, we validate the superiority of our matrices on video data. We design $8 \times 8$ codes (with and without the circular permutations), reconstructing patchwise with overlapping patches. We show close-ups that illustrate the superiority of our matrices. Note, in the car video sequence in Fig.~\ref{fig:smallScaleComp1}, better reconstruction of the numberplate and headlight area in our case. Further, notice the presence of major ghosting and degradation in the random case (arrows and boxes), while our reconstructions remain free of these artifacts. Circular optimization further improves quality in the bonnet area, where the non-circular reconstruction is splotchy. Full-scale results live in sections 3.1 and 3.4 of \cite{Full}.
\begin{figure}[!h]
\twoHeight{pics/comparison/comp1-1}{pics/comparison/comp1-2}{45pt}
\caption{Close-ups showing subtle texture preservation with optimized matrices. Left to right: inputs, reconstructions with \{random, non-circularly optimized, circularly optimized\} matrices. Zoom in for more clarity.}
\label{fig:smallScaleComp1}
\end{figure}

We do a numerical comparison between our designed codes and random codes for various values of $s = \|x\|_0/n$ and $T$. We randomly generate $T$ $s$-sparse (in 2D DCT) $8 \times 8$ signals $\{x_i\}_{i=1}^{T}$, combine them using random matrices to get $y_1$ and using our designed codes to get $y_2$. Average relative root mean square errors on recovering the input signals from $y_1$ as a function of $s$ and $T$ are shown in Fig. \ref{fig:errorMap}(a). On an average, we see that we perform better than the random case. The RRMSE error difference between $y_1$ and $y_2$ recovery is shown in Fig. \ref{fig:errorMap}(b). Note that we do better than random at high $T$ and $s$, which was the motivating factor behind optimizing matrices in the first place.

\section{Conclusion}
We cast the video compressed sensing problem as one of separation of a coded linear combination of sparse signals into its constituent sources. We saw that this scheme works well for low sparsity levels, and yields visually pleasing results. At high $T$, though, we saw that random matrices aren't good enough \cite{Full}--they cause major ghosting in video compressed sensing and color artifacts in color image demosaicing. 

We provided an expression for the coherence of the sensing matrix in our scheme and optimized it. Results showed better quality, less ghosting and less numerical error. However as $n$ increased, the optimization problem rapidly became intractable, so we settled for optimizing small masks such that they have small coherence in all circular permutations, so they can be tiled for overlapping patchwise reconstruction, making optimal recovery possible for all input patches.

Detailed version of this paper lives at arXiv:1609.02135v1~\cite{Full}.

%Most code used in generating results and optimizing matrices in this paper lives in the Bitbucket repository \href{https://bitbucket.org/alankarkotwal/coded-sourcesep}{here}~\cite{Implement}.

\begin{figure}[H]
\begin{tabular}{c}
\hspace{-15pt} \includegraphics[scale=0.18]{sims/framework-num/errorMap}
\\
(a) \\
\hspace{-15pt} \includegraphics[scale=0.15]{sims/diff/diffMap} \\
(b)
\end{tabular}
\caption{(a) Error map for random codes as a function of sparsity  $s$ and $T$, (b) average RRMSE using random codes minus average RRMSE using optimized codes}
\label{fig:errorMap}
\end{figure}

\newpage
%\nocite{*}
\nocite{Implement}
\bibliographystyle{plain}
\bibliography{references}

\end{document}
