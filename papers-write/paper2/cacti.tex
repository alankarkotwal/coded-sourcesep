\section{The CACTI Camera} \label{sec:cacti}
The principal idea behind the design of the CACTI camera, in the words of the authors of \cite{Llull13a} is using ``mechanical translation of a coded aperture for code division multiple access (CDMA) compression of video''. The setup is shown in Fig. 1 of \cite{Llull13a}. The camera achieves compression across time by combining frames into coded snapshots while sensing and separating them during reconstruction. $T$ input frames of size $N_1 \times N_2$ denoted as $\{\boldsymbol{X_i}\}_{i=1}^{T}$ are sensed so that output $\boldsymbol{Y}$ of size $N_1 \times N_2$ appears as a pixel-wise coded superposition (dictated by the sensing matrices $\boldsymbol{\Phi_i}$) of the input frames. The sensing framework is
\begin{equation}
\textrm{vec}(\boldsymbol{Y}) = \sum_{i=1}^{T} \boldsymbol{\Phi_i} \textrm{vec}(\boldsymbol{X_i}) = \boldsymbol{\Phi} \textrm{vec} (\boldsymbol{X}).
\label{eq:cactiSensing}
\end{equation}
Here, each $\boldsymbol{\Phi_i}$ is a $N_1 N_2 \times N_1 N_2$ non-negative (possibly, but not necessarily binary) diagonal sensing matrix with the vectorized code elements on the diagonal, and the overall sensing matrix $\boldsymbol{\Phi} = (\boldsymbol{\Phi_1} | \boldsymbol{\Phi_2} | ... \boldsymbol{\Phi_T})$ has size $N_1 N_2 \times N_1 N_2 T$. The complete $N_1 \times N_2 \times T$ video is represented as $\boldsymbol{X} = (\boldsymbol{X_1} | \boldsymbol{X_2} | ... | \boldsymbol{X_T})$. The sensing matrices $\boldsymbol{\Phi_i}$ are not independent across $i$: the mechanical translation amounts to a fixed circular shift in the diagonal elements of $\boldsymbol{\Phi_i}$, dictated by the set mechanical translations. Because of this relationship, there is a fixed set of $N_1N_2$ values used as code elements. Let us define a vector $\boldsymbol{\Phi}$ of dimension $N_1N_2 \times 1$ to be the vector of the values in this set. Let the $j^\text{th}$ diagonal element of $\boldsymbol{\Phi_i}$ be $\Phi_{ij}$ and that of $\boldsymbol{\Phi}$ be $\Phi^j$. 

It is now of importance to note where each $\Phi_{ij}$ came from. The $i^\text{th}$ mechanical permutation takes $\boldsymbol{\Phi}$ to $\boldsymbol{\Phi_i}$. Clearly, if we apply the permutation $i$ to the vector $(1\ 2\ 3\ \hdots\ n)^T$ and call the resultant vector $\boldsymbol{p_i}$, the $j^\text{th}$ element of $\boldsymbol{p_i}$ will denote which element of $\boldsymbol{\Phi}$ got circularly shifted to $\Phi_{ij}$. Defining $p_{ij}$ to be the $j^\text{th}$ element of $\boldsymbol{p_i}$, we have $\Phi_{ij} = \Phi^{p_{ij}}$. 

We use a 2D-DCT basis $\boldsymbol{D}$ to model each frame in the input data, though our method is general enough to work for any other basis. The dictionary $\boldsymbol{\Psi}$ sparsifying the entire video sequence, thus, is a block-diagonal matrix with the $n \times n$ sparsifying basis $\boldsymbol{D}$ on the diagonal where $n = N_1 N_2$ is the number of pixels per video frame. Thus,
\begin{align}
\textrm{vec}(\boldsymbol{Y}) &= \begin{pmatrix}
\boldsymbol{\Phi_1} & \hdots & \boldsymbol{\Phi_T}
\end{pmatrix}
\begin{pmatrix}
\boldsymbol{D \alpha_1} &
\hdots &
\boldsymbol{D \alpha_T}
\end{pmatrix}^T \\
&= \begin{pmatrix}
\boldsymbol{\Phi_1 D} & \hdots & \boldsymbol{\Phi_T D}
\end{pmatrix}
\begin{pmatrix}
\boldsymbol{\alpha_1} &
\hdots &
\boldsymbol{\alpha_T}
\end{pmatrix}^T
\label{eq:cactiModel}
\end{align}

\noindent Given a measurement $\boldsymbol{Y}$, we recover the input $\{\boldsymbol{X_i}\}_{i=1}^{T}$ through the DCT coefficients $\boldsymbol{\alpha}$ by solving the optimization problem
\begin{equation}
\begin{split}
\min_{\boldsymbol{\alpha}} \|\boldsymbol{\alpha}\|_1 \text{ subject to } \\ \textrm{vec}(\boldsymbol{Y}) = \boldsymbol{\tilde{\Phi} \Psi \alpha},\\ \boldsymbol{\alpha} = \begin{pmatrix}
\boldsymbol{\alpha_1} &
\boldsymbol{\alpha_2} &
\hdots &
\boldsymbol{\alpha_T}
\end{pmatrix}^T \\
\boldsymbol{\tilde{\Phi}} = \begin{pmatrix}
\boldsymbol{\Phi_1} & \hdots & \boldsymbol{\Phi_T}
\end{pmatrix}
\end{split}
\label{eq:cactiRecOpt}
\end{equation}
In our implementation we used the \texttt{CVX}~\cite{cvx} solver for solving the convex optimization problem in Eq.~\ref{eq:cactiRecOpt}.

\subsection{Optimizing for the CACTI camera} \label{subsec:cactiOpt}
We follow an explicit coherence minimization policy similar to \cite{Full} for optimizing codes. To minimize coherence, we write down the expression for the coherence of the joint dictionary $\boldsymbol{\tilde{\Phi} \Psi}$:
\begin{align}
\boldsymbol{\tilde{\Phi} \Psi} = \begin{pmatrix}
\boldsymbol{\Phi_1} \boldsymbol{D} & \boldsymbol{\Phi_2} \boldsymbol{D} & \hdots & \boldsymbol{\Phi_T} \boldsymbol{D}
\end{pmatrix}.
\end{align}

Define the $\mu^\text{th}$ column of $\boldsymbol{D^T}$ to be $\boldsymbol{d_\mu}$, and its $\beta^\text{th}$ element as $d_{\mu}\left(\beta\right)$. Let the variables $\mu$ and $\nu$ go from 1 to $n$, and the variables $\beta$ and $\gamma$ go from 1 to $T$. In a similar way to \cite{Full} (see appendix in \cite{Full}), using the steps outlined in the appendices, we write the normalized dot product between the $\beta^\text{th}$ column of the $\mu^\text{th}$ block of the effective dictionary and the $\gamma^\text{th}$ column of the $\nu^\text{th}$ block of the effective dictionary as
\begin{align}
M_{\mu \nu}(\beta\gamma) &= \frac{\sum_{\alpha = 1}^{n} \Phi_{\mu \alpha} \Phi_{\nu \alpha} d_\alpha (\beta) d_\alpha (\gamma)}{\sqrt{\left( \sum_{\alpha = 1}^{n} \Phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right) \left( \sum_{\tau = 1}^{n} \Phi_{\nu \tau}^2 d^2_\tau (\gamma) \right)}} \\
&= \frac{\sum_{\alpha = 1}^{n} \Phi^{p_{\mu \alpha}} \Phi^{p_{\nu \alpha}} d_\alpha (\beta) d_\alpha (\gamma)}{\sqrt{\left( \sum_{\alpha = 1}^{n} \Phi^{p_{\mu \alpha}2} d^2_\alpha (\beta) \right) \left( \sum_{\tau = 1}^{n} \Phi^{p_{\nu \tau}2} d^2_\tau (\gamma) \right)}}.
\end{align}

With the numerator of the above expression renamed to $\chi_{\mu \nu}(\beta\gamma)$ and the denominator renamed to $\xi_{\mu \nu}(\beta\gamma)$, we write, 
\begin{equation}
\begin{split}
\frac{d\chi_{\mu \nu}(\beta\gamma)}{d\Phi_{\delta \epsilon}} &= d_\epsilon (\beta) d_\epsilon (\gamma) \left( \Phi_{\mu \epsilon} \uparrow_{\nu \delta} + \uparrow_{\mu \delta} \Phi_{\nu \epsilon} \right) \\
\implies \frac{d\chi_{\mu \nu}(\beta\gamma)}{d\Phi^{p_{\delta \epsilon}}} &= d_\epsilon (\beta) d_\epsilon (\gamma) \left( \Phi^{p_{\mu \epsilon}} \uparrow_{\nu \delta} + \uparrow_{\mu \delta} \Phi^{p_{\nu \epsilon}} \right).
\end{split}
\end{equation}

\begin{equation}
\begin{split}
\frac{d\xi_{\mu \nu}(\beta\gamma)}{d\Phi_{\delta \epsilon}} &= \frac{1}{\xi_{\mu \nu}(\beta \gamma)} \left[ \Phi_{\mu \epsilon} d_\epsilon^2(\beta) \uparrow_{\mu \delta} \sum_{\tau = 1}^{n} \Phi_{\nu \tau}^2 d^2_\tau (\gamma) \right. + \\
&\qquad \qquad \left. \Phi_{\nu \epsilon} d_\epsilon^2(\gamma) \uparrow_{\nu \delta} \sum_{\alpha = 1}^{n} \Phi_{\mu \alpha}^2 d^2_\alpha (\beta) \right] \\
\implies \frac{d\xi_{\mu \nu}(\beta\gamma)}{d\Phi^{p_{\delta \epsilon}}} &= \frac{1}{\xi_{\mu \nu}(\beta \gamma)} \left[ \Phi^{p_{\mu \epsilon}} d_\epsilon^2(\beta) \uparrow_{\mu \delta} \sum_{\tau = 1}^{n} \Phi^{p_{\nu \tau}2} d^2_\tau (\gamma) + \right. \\ 
&\qquad \qquad \left. \Phi^{p_{\nu \epsilon}} d_\epsilon^2(\gamma) \uparrow_{\nu \delta} \sum_{\alpha = 1}^{n} \Phi^{p_{\mu \alpha}2} d^2_\alpha (\beta) \right].
\end{split}
\end{equation}

We perform a projected (to maintain non-negativity of $\boldsymbol{\Phi}$) gradient descent with adaptive step-size and use a multi-start strategy to combat the non-convexity of the problem.

\subsection{Results on simulated data} \label{subsec:cactiRes}
Here, we experiment with toy data where we can precisely control the sparsity of the input signals. Specifically, assuming a set of mechanical translations, we randomly generate $T$ $s$-sparse (in 2D DCT) $8 \times 8$ signals $\{\boldsymbol{X_i}\}_{i=1}^{T}$, combine them as per the sensing framework in Eq.~\ref{eq:cactiSensing}, using matrices formed by using random codes and our designed codes as per the structure in Eq.~\ref{eq:cactiModel} and add noise bounded in norm to $\epsilon=10^{-5}$ to get $\boldsymbol{Y}$. Average RRMSE errors on doing this over a set of 100 vectors, as a function of signal sparsity and compression level $T$ are shown in Figs. \ref{fig:cactiRRMSE2}, \ref{fig:cactiRRMSE4}, and \ref{fig:cactiRRMSE6}.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/cacti/736852_6681-64-2-100-fin}
\caption{Average RRMSE as a function of sparsity for $8 \times 8$ signals, sparse in 2D DCT, combined with $T = 2$. Permutations: [5, 3; 6, 8].}
\label{fig:cactiRRMSE2}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/cacti/736852_6689-64-4-100-fin}
\caption{Average RRMSE as a function of sparsity for $8 \times 8$ signals, sparse in 2D DCT, combined with $T = 4$. Permutations: [7, 8; 2, 8; 6, 1; 3, 5]}
\label{fig:cactiRRMSE4}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/cacti/736852_6699-64-6-100-fin}
\caption{Average RRMSE as a function of sparsity for $8 \times 8$ signals, sparse in 2D DCT, combined with $T = 6$. Permutations: [6, 7; 3, 6; 6, 2; 1, 4; 8, 3; 5, 2]}
\label{fig:cactiRRMSE6}
\end{figure}

These figures don't tell a very happy story: the optimization technique completely fails to produce any statistically significant improvement in the error over random matrices. The coherence decreases are significant: in the best case, the coherence for $T = 2$ decreases from 0.7911 to 0.3462, for $T = 4$ decreases from 0.7921 to 0.4952, and for $T = 6$ decreases from 0.9156 to 0.5430. 

What causes the algorithm to fail? The underlying assumption in this method is that the bound that RIC establishes on the maximum recovery error surface plotted against the space of sensing matrices behaves close to the actual maximum error surface (coherence further loosens up the bound). However, this might not be the case: the looser the bound gets, the more freedom the error surface has to deviate from the behavior of the bound. Then, minimizing the maximum in the bound may not correspond to minimizing the maximum in the actual error surface.

To quantify this concept, suppose that some oracle gives us the actual supports of the vectors in the dataset. Then, a pseudoinverse solution is possible at sufficiently small sparsities. Defining $\lambda_k\left(\boldsymbol{M}\right)$ as the $k^\text{th}$ largest absolute eigenvalue of the matrix $M$, note that the $s^\text{th}$ RIC of a matrix $\boldsymbol{A} \triangleq \boldsymbol{\tilde{\Phi} \Psi}$ is the following:
\begin{equation}
\delta_s = \max_{\setS \in \{1\ ...\ n\}} |\lambda_1(\matA_\setS^T \matA_\setS - \matI)|.
\end{equation}
This is the maximum vector-induced 2-norm of the matrix $\matA_\setS^T \matA_\setS - \matI$ over all subsets $\setS$ of $\{1,2,...,n\}$ with size $s$, where $\boldsymbol{A_{\setS}}$ is a submatrix of $\boldsymbol{A}$ with columns from set $\setS$. Each $\matA_\setS^T \matA_\setS - \matI$ appears in the error bound for recovery for a vector with known support $\setS$. We therefore plot, for the entire dataset of vectors we used to make the RRMSE plot, the absolute maximum eigenvalue of $\matA_\setS^T \matA_\setS - \matI$, where $\setS$ is the support of the vector, for both random $\matA$ of the form imposed by CACTI, and designed $\matA$. The error in reconstructing this particular vector is bounded tighter than coherence by this absolute maximum eigenvalue. This gives us a handle on how well minimizing coherence minimizes this eigenvalue across supports, and therefore how much we lose by relaxing the RIC to coherence. The maximum eigenvalues for random CACTI and design CACTI matrices for $T = 2,4,6$ are shown in Figs. \ref{fig:cactiEigen2}, \ref{fig:cactiEigen4} and \ref{fig:cactiEigen6} respectively, for randomly chosen permutations. No significant difference was observed for other sets of permutations.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/cacti/736852_6681-64-2-100-eig-fin}
\caption{Absolute maximum restricted eigenvalue boxplot as a function of sparsity for $8 \times 8$ signals, sparse in 2D DCT, with $T = 2$. Permutations: [5, 3; 6, 8]}
\label{fig:cactiEigen2}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/cacti/736852_6689-64-4-100-eig-fin}
\caption{Absolute maximum restricted eigenvalue boxplot as a function of sparsity for $8 \times 8$ signals, sparse in 2D DCT, with $T = 4$. Permutations: [7, 8; 2, 8; 6, 1; 3, 5]}
\label{fig:cactiEigen4}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/cacti/736852_6699-64-6-100-eig-fin}
\caption{Absolute maximum restricted eigenvalue boxplot as a function of sparsity for $8 \times 8$ signals, sparse in 2D DCT, with $T = 6$. Permutations: [6, 7; 3, 6; 6, 2; 1, 4; 8, 3; 5, 2]}
\label{fig:cactiEigen6}
\end{figure}

The $T = 2$ case is surprising: decreasing coherence starting from random matrices seems to increase the values of the absolute maximum eigenvalues, which goes directly against the assumption involved in minimizing coherence. The $T = 4$ and $T = 6$ cases behave better in terms of eigenvalues, though their performance in terms of RRMSE error isn't very good. These findings point to the fact that the problem lies not only in the relaxation of the RIC to the coherence, but also in the RIC bound itself. 

\textbf{Using Average Coherence:} A tempting thought, at this juncture, is to maximize some average of the squares of the dot products of pairs of (non-identical) columns in $\boldsymbol{A}$, instead of the coherence, which is the maximum of these. This is because the coherence bound on the RIC relaxes the sum of $k-1$ off-diagonal elements in $\boldsymbol{A}^T \boldsymbol{A}$ to $k-1$ times the maximum, which is the coherence. Designing matrices optimizing the square of these off-diagonal elements and performing simulated data experiments similar to the above produce similar RRMSE behavior: the matrices designed this way are no better than matrices designed using just coherence, and certainly no better than random matrices. Besides, this is a heuristic approach because no theoretical reconstruction guarantees have been derived for such a measure of average coherence in the compressed sensing literature yet. 

This warrants a more detailed empirical understanding of error bounds in compressed sensing, focusing on how bounds evolve across inequalities that give rise to them. This will be the subject of the next section.
