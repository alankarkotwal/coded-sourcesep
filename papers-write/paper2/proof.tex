\section{The compressed sensing bound} \label{sec:bound}
The bound we choose to examine is the one quoted in Eq.~\ref{eq:studerBoundMain} for recovery of nearly sparse vectors using basis pursuit. Let the compressed sensing scenario be as in Eq.~\ref{eq:compSensProblem}, with an overcomplete $m \times n$-sized $\matA$, $n \times 1$-sized $\vecx$ and $m \times 1$-sized $\vecy$, recovering $\vecxh$ by solving the basis pursuit problem in Eq.~\ref{eq:basisPursuit}.

We will quote some relevant steps from the proof of the bound in Eq.~\ref{eq:studerBoundMain}, following \cite{Studer201412}. Let $\vech = \vecxh - \vecx$. Construct $\vecho = \vech_\setX$ by setting elements of $\vech$ at indices in $\setX^C$ to zero. Let $\veceo = 2 \|\vecx - \vecx_\setX\|_1$. Then, using the above definitions,
\begin{align}
\|\vecx\|_1 &\geq \|\vecxh\|_1 \\
&= \|\vecxh_\setX\|_1 + \|\vecxh_{\setX^C}\|_1 \\ 
&= \|\vecx_\setX + \vecho\|_1 + \|\vech-\vecho + \vecx_{\setX^C}\|_1 \\
&\geq \|\vecx_\setX\|_1 - \|\vecho\|_1 + \|\vech-\vecho\|_1 - \|\vecx_{\setX^C}\|_1 \\
&\implies \|\vech-\vecho\|_1 \leq 2 \|\vecx_{\setX^C}\|_1 + \|\vecho\|_1 \\ 
&\implies \|\vech-\vecho\|_1 \leq \|\vecho\|_1 + \veceo \label{eq:coneconstraint}\\
&\implies \|\vech\|_1 \leq 2 \|\vecho\|_1 + \veceo 
\end{align}
%
where the last step follows from the reverse triangle inequality. Furthermore, 
\begin{align}
\|\matA \vech\|_2 &= \|\matA \vecxh - \vecy - (\matA \vecx - \vecy)\|_2 \\
&\leq \|\matA \vecxh - \vecy\|_2 + \|\matA \vecx - \vecy\|_2 \\
&\leq \eta + \epsilon \label{eq:tubeconstraint}
\end{align}
%
An application of the Ger\v{s}gorin disc theorem to $\|\matA \vecho\|^2$ gives, since $\vecho$ is perfectly sparse
\begin{align}
(1 - \mu (n_x - 1)) \|\vecho\|_2^2 \leq \|\matA\vecho\|_2^2 \leq (1 + \mu (n_x - 1)) \|\vecho\|_2^2
\label{eq:gersgorindiscthm}
\end{align}
%
Next,
\begin{align}
\abs{\vech^T \matA^T \matA \vecho} & \geq \abs{\vecho^T \matA^T \matA \vecho} - \abs{(\vech-\vecho)^T \matA^T \matA \vecho} \label{eq:diffs4n4} \\
& \geq \left(1-\mu(\nx-1)\right)\normtwo{\vecho}^2 \nonumber \\ & \qquad \qquad - \abs{\sum_{k\in\setX}\sum_{l\in\setX^c} [\vecho^T]_k \boldsymbol{a_k}^T \boldsymbol{a_l} [\vech]_l} \label{eq:errbound0} \\
& \geq \left(1-\mu(\nx-1)\right)\normtwo{\vecho}^2 \nonumber \\ & \qquad \qquad \qquad - \mu\normone{\vecho}\normone{\vecho} \label{eq:errbound1} \\
& \geq \left(1-\mu(\nx-1)\right)\normtwo{\vecho}^2 \nonumber \\ & \qquad \qquad \qquad - \mu\normone{\vecho}\left(\normone{\vecho} + \veceo\right) \label{eq:errbound2}\\
& \geq \left(1-\mu(\nx-1)\right)\normtwo{\vecho}^2 - \mu \nx \normtwo{\vecho}^2 \nonumber \\ & \qquad \qquad \qquad - \mu\sqrt{\nx}\normtwo{\vecho} \veceo \label{eq:errbound3} \\
& = \left(1-\mu(2\nx-1)\right)\normtwo{\vecho}^2 \nonumber \\ & \qquad \qquad \qquad - \mu\sqrt{\nx}\normtwo{\vecho} \veceo, \label{eq:errbound4}
\end{align}
Eq. \ref{eq:errbound0} is a result of the Ger\v{s}gorin bound in Eq. \ref{eq:gersgorindiscthm}, Eq. \ref{eq:errbound1} arises from $\abs{\boldsymbol{a_k}^T\boldsymbol{a_l}} \leq \mu$,  $\forall\ k\neq l$ and Eq. \ref{eq:errbound2} comes from the condition in Eq. \ref{eq:coneconstraint}. Eq. \ref{eq:errbound3} is an application of the Cauchy-Schwarz inequality. Next,
\begin{align}
\normtwo{\vecho} &\leq \frac{\abs{\vech^T\matA^T\matA\vecho} + \mu\sqrt{\nx}\normtwo{\vecho}\veceo}{\left(1-\mu(2\nx-1)\right)\normtwo{\vecho}} \label{eq:errbound5} \\
& \leq \frac{\normtwo{\matA\vech}\normtwo{\matA\vecho}+\mu\sqrt{\nx}\normtwo{\vecho}\veceo}{\left(1-\mu(2\nx-1)\right)\normtwo {\vecho}} \label{eq:errbound6} \\
& \leq \frac{(\epsilon+\eta) \sqrt{1+\mu(\nx-1)}\normtwo{\vecho} + \mu\sqrt{\nx}\normtwo{\vecho}\veceo}{\left(1-\mu(2\nx-1)\right)\normtwo{\vecho}} \label{eq:errbound7} \\
& = \frac{ (\epsilon+\eta) \sqrt{1+\mu(\nx-1)}+\mu\sqrt{\nx}\veceo}{ 1-\mu(2\nx-1)} \label{eq:errbound7b}
\end{align}

The proof further goes on to bound $\normtwo{\vech}$. The rest of the proof, however, is an application of the bound yet derived, and we will study the looseness of just this part of the proof. For completeness, we quote the bound here.
\begin{align}
\|\vech\|_2\ \leq\ &\frac{1 - \mu (2 n_x - 1) + \sqrt{\mu n_x} \sqrt{1 + \mu (n_x - 1)}}{\sqrt{1 + \mu (2 n_x - 1)}}\ (\epsilon + \eta)\ + \label{eq:finalBound} \\
&\frac{2\sqrt{\mu + \mu^2}}{1 - \mu(2 n_x - 1)}\ \|\vecx - \vecx_\setX\|_1 \notag
\end{align}

\subsection{Important steps in the bound} \label{subsec:steps}
We will concentrate on the simplest case, where the vector $\vecx$ is exactly sparse, and set $n_x = \|\vecx\|_0$. $\|\vecx\|_0$ is set as the maximum $l_0$ norm that the bound allows, which is the greatest integer below $0.5(1 + 1/\mu)$. The definitions then reduce $e_0$ to 0. We generate a set of positive sparse vectors of a size suitable to be sensed with a selected sensing matrix, add noise bounded in norm by $\eta = \epsilon = 10^{-5}$, and plot a boxplot of the relative difference between the left and right hand sides in selected inequalities in the proof above. That means, if an inequality such as $a \leq b$ exists in the bound, and we want to show a relative difference with respect to the left hand side, we show a boxplot, computed over a dataset of our randomly drawn vectors $\vecx_i$, the following quantity:
\begin{equation}
\textrm{Relative Difference}(a,b) = \abs{b-a}/\abs{a}.
\end{equation}

The relative differences we choose to show are due to, in order,
\begin{enumerate}
\item The triangle inequality in Eq. \ref{eq:diffs4n4}, with respect to the left hand side of Eq. \ref{eq:diffs4n4}  
\item The Ger\v{s}gorin bound and triangle inequality in Eq. \ref{eq:errbound0}, with respect to the left hand side of Eq. \ref{eq:diffs4n4}
\item Replacing dot products with their maximum, coherence, in Eq. \ref{eq:errbound1}, with respect to the left hand side of Eq. \ref{eq:diffs4n4} \label{chk:step3}
\item The application of Eq. \ref{eq:coneconstraint} in Eq. \ref{eq:errbound2}, with respect to the left hand side of Eq. \ref{eq:diffs4n4} \label{chk:step4}
\item The bound relating the $l_1$ and $l_2$ norms in Eq. \ref{eq:errbound4}, with respect to the left hand side of Eq. \ref{eq:diffs4n4} \label{chk:step5}
\item The rearrangement of Eq. \ref{eq:errbound5}, with respect to the left hand side of Eq. \ref{eq:errbound5} \label{chk:step6}
\item The Cauchy-Swartz inequality in Eq. \ref{eq:errbound6}, with respect to the left hand side of Eq. \ref{eq:errbound4} \label{chk:step7}
\item The application of Eq. \ref{eq:coneconstraint} in Eq. \ref{eq:errbound7b}, with respect to the left hand side of Eq. \ref{eq:errbound4} \label{chk:step8}
\item The leftmost side of the Ger\v{s}gorin bound in Eq. \ref{eq:gersgorindiscthm}, with respect to $\|\matA \vecho\|_2^2$ \label{chk:stepGnG1} 
\item The rightmost side of the Ger\v{s}gorin bound in Eq. \ref{eq:gersgorindiscthm}, with respect to $\|\matA \vecho\|_2^2$ \label{chk:stepGnG2} 
\item The actual RRMSE error for the simulated vector in question, which is the left hand side of Eq. \ref{eq:finalBound}, and the bound predicted by Eq. \ref{eq:finalBound}, with respect to the actual RRMSE.
\end{enumerate}

\subsection{Looseness propagation} \label{subsec:looseness}
\subsubsection{General sensing matrices} \label{subsubsec:generalLooseness}
Here, to allow a high $\|\vecx\|_0$, we first choose a random $499 \times 500$ matrix drawn from a Gaussian distribution. Also as a benchmark for a compressive scenario, we test with $250 \times 500$ and $125 \times 500$ matrices corresponding to 50\% and 25\% measurement fractions. All this is done at a noise level and reconstruction tolerance of $\epsilon = 10^{-5}$.

Figs. \ref{fig:genBoundComp1}, \ref{fig:genBoundComp2} and \ref{fig:genBoundComp4} show the relative differences for these scenarios. As is immediately noticed, the differences in the first two cases are high: the bound is off by two orders of magnitude compared to the actual error surface. The other three are off by a relative error well above 1.  

Consistent among all these figures is, however, the presence of jumps between relative differences across steps. There appear jumps in the transition from step \ref{chk:step3} to step \ref{chk:step4}, and from step \ref{chk:step7} to step \ref{chk:step8} (the baseline for comparison changes between step \ref{chk:step5} and step \ref{chk:step6}, so the jump here isn't significant. Also, these two steps are rearrangements of each other, so there's no loss happening in between).

We will contrast this behavior to what happens with matrices of the kind we encounter in practical signal processing scenarios further.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/proof/736864_0559-499x500-100-randn-mat-vec-fin}
\caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for a $499 \times 500$ matrix drawn from a standard normal distribution. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
\label{fig:genBoundComp1}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/proof/736864_0573-250x500-100-randn-mat-vec-fin}
\caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for a $250 \times 500$ matrix drawn from a standard normal distribution. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
\label{fig:genBoundComp2}
\end{figure}

% \begin{figure}[!h]
% \centering
% \includegraphics[scale=0.2]{pics/proof/736864_0582-167x500-100-randn-mat-vec-fin}
% \caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for a $167 \times 500$ matrix drawn from a standard normal distribution. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
% \label{fig:genBoundComp3}
% \end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/proof/736864_0584-125x500-100-randn-mat-vec-fin}
\caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for a $125 \times 500$ matrix drawn from a standard normal distribution. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
\label{fig:genBoundComp4}
\end{figure}

% \begin{figure}[!h]
% \centering
% \includegraphics[scale=0.2]{pics/proof/736864_0586-50x500-100-randn-mat-vec-fin}
% \caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for a $50 \times 500$ matrix drawn from a standard normal distribution. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
% \label{fig:genBoundComp5}
% \end{figure}


\subsubsection{In the CACTI camera} \label{subsubsec:cactiLooseness}
The maximum sparsity the structure of the CACTI sensing matrices affords, in most cases, is 1, because of the $n \times nT$ size of the matrix. Typical matrices where the mask values are drawn from positive uniform [0, 1] distributions have coherence values around 0.8 to 0.9. Nevertheless, we repeat the same process as above, for $T$ = 2, 3, 4, 5 and 6 (Figs.~\ref{fig:cactiBoundComp2}, \ref{fig:cactiBoundComp3}, \ref{fig:cactiBoundComp4}, \ref{fig:cactiBoundComp5} and \ref{fig:cactiBoundComp6}), expecting the bound to work a little better in light of this low sparsity. Similarly, we also test matrices, for $T$ = 2, 4 and 6 designed for the CACTI camera in subsection \ref{subsec:cactiOpt} using both the coherence (Figs.~\ref{fig:cactiDesBoundComp2}, \ref{fig:cactiDesBoundComp4} and \ref{fig:cactiDesBoundComp6}) and the sum of squares of off-diagonal dot products of columns of the effective dictionary (Figs.~\ref{fig:cactiAvgDesBoundComp2}, \ref{fig:cactiAvgDesBoundComp4} and \ref{fig:cactiAvgDesBoundComp6}). 

It can be noticed that the same steps show significant jumps just as in the case general sensing matrices. The bound again offers enough room for failure.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/proof/736852_8518-64x2-100-cacti}
\caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for $8 \times 8$ random positive codes in the CACTI camera for $T = 2$. Permutations: [3, 1; 5, 5]. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
\label{fig:cactiBoundComp2}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/proof/736852_8518-64x3-100-cacti}
\caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for $8 \times 8$ random positive codes in the CACTI camera for $T = 3$. Permutations: [7, 7; 4, 4; 7, 3]. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
\label{fig:cactiBoundComp3}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/proof/736852_8518-64x4-100-cacti}
\caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for $8 \times 8$ random positive codes in the CACTI camera for $T = 4$. Permutations: [1, 2; 6, 6; 7, 3; 2, 7]. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
\label{fig:cactiBoundComp4}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/proof/736852_8519-64x5-100-cacti}
\caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for $8 \times 8$ random positive codes in the CACTI camera for $T = 5$. Permutations: [7, 6; 4, 8; 4, 2; 6, 5; 2, 3]. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
\label{fig:cactiBoundComp5}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/proof/736852_8519-64x6-100-cacti}
\caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for $8 \times 8$ random positive codes in the CACTI camera for $T = 6$. Permutations: [1, 8; 8, 7; 6, 1; 4, 6; 7, 8; 5, 3]. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
\label{fig:cactiBoundComp6}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/proof/736856_6676-64x2-100-cacti-des}
\caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for $8 \times 8$ designed positive codes in the CACTI camera for $T = 2$. Permutations: [5, 3; 6, 8]. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
\label{fig:cactiDesBoundComp2}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/proof/736856_6682-64x4-100-cacti-des}
\caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for $8 \times 8$ designed positive codes in the CACTI camera for $T = 4$. Permutations: [7, 8; 2, 8; 6, 1; 3, 5]. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
\label{fig:cactiDesBoundComp4}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/proof/736856_6684-64x6-100-cacti-des}
\caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for $8 \times 8$ designed positive codes in the CACTI camera for $T = 6$. Permutations: [6, 7; 3, 6; 6, 2; 1, 4; 8, 3; 5, 2]. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
\label{fig:cactiDesBoundComp6}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/proof/736856_668-64x2-100-cacti-des-avg}
\caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for $8 \times 8$ average-designed positive codes in the CACTI camera for $T = 2$. Permutations: [8, 4; 7, 2]. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
\label{fig:cactiAvgDesBoundComp2}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/proof/736856_6683-64x4-100-cacti-des-avg}
\caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for $8 \times 8$ average-designed positive codes in the CACTI camera for $T = 4$. Permutations: [3, 1; 1, 7; 6, 3; 8, 1]. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
\label{fig:cactiAvgDesBoundComp4}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.2]{pics/proof/736856_6685-64x6-100-cacti-des-avg}
\caption{Relative difference between inequalities in the error bound in Eq.~\ref{eq:studerBoundMain} for $8 \times 8$ average-designed positive codes in the CACTI camera for $T = 6$. Permutations: [7, 3; 5, 6; 8, 8; 5, 2; 2, 3; 7, 3]. Refer to Subsection~\ref{subsec:steps} for index of step descriptions.}
\label{fig:cactiAvgDesBoundComp6}
\end{figure}

\subsection{Discussion} \label{subsec:discussion}
While the figures above do not explain why coherence succeeds in traditional compressed sensing methods but doesn't in the CACTI case, the fact that there are common steps that cause significant looseness of bound is a big takeaway. These steps can be isolated and the precise inequalities causing problems can be pointed to. In our case, these are the ones leading to step \ref{chk:step4} from step \ref{chk:step3}, and from step \ref{chk:step7} to step \ref{chk:step8}. We will examine these inequalities in some detail now.

The transition from step \ref{chk:step3} to step \ref{chk:step4} involves the use of the constraint in Eq. \ref{eq:coneconstraint}. Further, going from step \ref{chk:step7} to step \ref{chk:step8} involves the constraint in Eq. \ref{eq:tubeconstraint}, and the right side of the Ger\v{s}gorin inequality in Eq. \ref{eq:gersgorindiscthm}. The culprits here, therefore, are Eq. \ref{eq:tubeconstraint}, Eq. \ref{eq:coneconstraint} and the right side of Eq. \ref{eq:gersgorindiscthm}. 

However, these are fundamental constraints that come from the problem itself. For instance, the constraint in Eq. \ref{eq:tubeconstraint} comes from the nature of the noise. The constraint in Eq.~\ref{eq:coneconstraint} comes from the fact that we use basis pursuit recovery. However, as we saw empirically in our situation, there are hardly any (in our dataset, none) vectors that meet this bound. It can be inferred, therefore, that at the cost of accommodating a set of rare worst case vectors, optimizing worst case bounds does not do well on the average case signal.

At this point, it is worthwhile to stop to consider the array of design schemes based on coherence. These methods (\cite{Elad200610, Duarte200907, Mordechay2014, Obermeier17, Bouchhima15, Abolghasemi10, Pereira14, Parada17}) attempt to design matrices with an extremely loose bound. The sheer number of these methods very emphatically states the popularity of the coherence as a measure of matrix goodness. However the analysis in our paper suggests that for very fundamental reasons, one needs to be very careful when designing sensing matrices based on coherence, despite earlier instances of success. Better, tighter bounds on sparse recovery will possibly make these methods more effective at optimizing matrices in their particular applications.