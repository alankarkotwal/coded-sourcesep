\section{The average case: A proof of concept} \label{sec:mmse}
Investigating the source of looseness in compressed sensing bounds led us to a dead end: the looseness comes from steps that are at the core of the problem themselves. We saw in Subsection~\ref{subsec:discussion}, for instance, how bounding the measurement noise was a major source of error, but a major step in the proof as well. The conclusion, then, was that while the error bound takes into consideration all vectors in the input space, the fact that the error bound is not met in a large dataset of vectors we simulated makes the consideration seem unnecessary. The bound is loose at the expense of taking into account a low probability set of vectors.

We now note that any compressed sensing bound, being universal to all vectors, characterizes recovery error in terms of sensing matrix properties and signal sparsity. No other property of the input signal is used. Therefore, in general, any approach targeting worst case errors in terms of these quantities will encounter the same worst case low probability vector pitfall as coherence. A possible direction of future work, therefore, is to circumvent these vectors by considering an average case error analysis.

The average case minimum mean square error (MMSE), introduced in \cite{Carson2012} as
\begin{equation}
\mathbb{E}_{\boldsymbol{X}} \left[ tr \left\{ \left( \vecx - \mathbb{E}_{\boldsymbol{X}|\boldsymbol{Y}} [\vecx|\vecy] \right) \left( \vecx - \mathbb{E}_{\boldsymbol{X}|\boldsymbol{Y}} [\vecx|\vecy] \right)^T  \right\} \right] \label{eq:mmse} \\
\end{equation}
is successfully lower-bounded in the same paper by a function of the mutual information between $\boldsymbol{x}$ and $\boldsymbol{y}=\boldsymbol{\Phi x}$. The aim is to design $\boldsymbol{\Phi}$ so as to optimize the mutual information between $\boldsymbol{x}$ and $\boldsymbol{y}=\boldsymbol{\Phi x}$. The mutual information along with an entropy term is proved to be a lower bound, but that certainly may not necessarily decrease the average recovery error.

A full analysis of average case error, however, requires estimating a posterior on the space of input vectors as well as assuming a specific statistical model for noise, and involves the MMSE in Eq.~\ref{eq:mmse}, an intractable quantity to calculate for many commonly occurring priors and likelihoods. Such an analysis is beyond the scope of this paper. A sampling-based approach with an appropriate prior over the space of input signals seems to be a direction in which to proceed for any tractable sensing matrix design considering the average case. This method, however, will require coming up with a prior distribution such that the posterior $\mathbb{E}[\vecx|\vecy]$ can be sampled from. The parametrization can then be used to optimize sensing matrices with respect to the expectation.

For the purposes of providing a proof-of-concept, we circumvent the calculation of the MMSE as follows: we take a dataset of random vectors from the prior distribution we are interested in, use basis pursuit for signal recovery instead of the full Bayesian $\mathbb{E}[\vecx|\vecy]$. The MMSE is then approximated as the mean squared error on this dataset. With this, we perform a random search on the sensing matrix $\boldsymbol{\Phi}$. In the case of the CACTI matrices considered in this paper, we show superior recovery performance with matrices designed in this manner as compared to coherence. In the case of general sensing matrices, where the number of degrees of freedom is larger, the performance is no worse than that of coherence.

\subsection{General sensing matrices} \label{subsec:genMMSEOpt}
We design $m \times 50$ matrices for $m = $ 8 and 25 (Figs.~\ref{fig:genMMSE2} and \ref{fig:genMMSE6} respectively) targeted at perfectly sparse vectors with sparsities of $s = $ 0.2 and 0.08 respectively. We take a set $\{\mathcal{S}_1, \mathcal{S}_2, \cdots \mathcal{S}_z\}$ of $z=3$ randomly generated support sets, and draw random vectors having supports from this set (this is done so that we do not have to explore the entire set of sparse vectors at a given sparsity, limiting the number of sparse recovery problems we have to solve per objective function evaluation, and hence reducing computational complexity). A random search is done on the elements of the matrix to minimize the squared error on this dataset, limiting the number of samples per iteration to 10. We then compare, on an independently generated test set of vectors having supports from these sets, the performance of a matrix designed to minimize coherence and the random matrix we started from with our matrix. It is clear from these boxplots that our method does no worse than coherence-based design in the general sensing matrix case.

\begin{figure}
    \centering
    \includegraphics[scale=0.2]{pics/mmse/gen-mmse-comparison-2-support}
    \caption{Comparison of sum of squared errors on a dataset of 250 sparse vectors, between $8 \times 50$ general sensing matrices optimized with mean square error, optimized with coherence and a general random matrix, targeted at sparsities of $0.08$}
    \label{fig:genMMSE2}
\end{figure}

% \begin{figure}
%     \centering
%     \includegraphics[scale=0.2]{pics/mmse/gen-mmse-comparison-2-support}
%     \caption{Comparison of sum of squared errors on a dataset of 250 sparse vectors, between $12 \times 50$ general sensing matrices optimized with mean square error, optimized with coherence and a general random matrix, targeted at sparsities of $0.12$}
%     \label{fig:genMMSE4}
% \end{figure}

\begin{figure}
    \centering
    \includegraphics[scale=0.2]{pics/mmse/gen-mmse-comparison-2-support}
    \caption{Comparison of sum of squared errors on a dataset of 250 sparse vectors, between $25 \times 50$ general sensing matrices optimized with mean square error, optimized with coherence and a general random matrix, targeted at sparsities of $0.2$}
    \label{fig:genMMSE6}
\end{figure}

\subsection{In the CACTI camera} \label{subsec:CACTIMMSEOpt}
Similarly, we design $8 \times 8$ codes for $T = $ 2, 4 and 6 (Figs.~\ref{fig:cactiMMSE2}, \ref{fig:cactiMMSE4} and \ref{fig:cactiMMSE6} respectively) targeted at perfectly sparse vectors with sparsity of $s = $ 0.2, 0.12 and 0.08 respectively. Optimizing the code using a random dataset of vectors drawn from similarly generated set of support sets, we compare the performance of the designed matrix on an independent test dataset of vectors having these supports.

\begin{figure}
    \centering
    \includegraphics[scale=0.2]{pics/mmse/cacti-mmse-comparison-2-support}
    \caption{Comparison of sum of squared errors on a dataset of 250 sparse vectors, using $8 \times 8$ positive codes with $T=2$, optimized using the mean square error, optimized with coherence and a positive random code, targeted at sparsity of $0.2$}
    \label{fig:cactiMMSE2}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[scale=0.2]{pics/mmse/cacti-mmse-comparison-2-support}
    \caption{Comparison of sum of squared errors on a dataset of 250 sparse vectors, using $8 \times 8$ positive codes with $T=4$, optimized using the mean square error, optimized with coherence and a positive random code, targeted at sparsity of $0.12$}
    \label{fig:cactiMMSE4}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[scale=0.2]{pics/mmse/cacti-mmse-comparison-2-support}
    \caption{Comparison of sum of squared errors on a dataset of 250 sparse vectors, using $8 \times 8$ positive codes with $T=6$, optimized using the mean square error, optimized with coherence and a positive random code, targeted at sparsity of $0.08$}
    \label{fig:cactiMMSE6}
\end{figure}

The fact that mean squared error design does better than coherence design and random codes, where coherence design did worse than random codes, demonstrates clearly the potency of the MSE-based design over coherence-based design. It turns out, interestingly, that if we design matrices without the support set constraint with the same number of train vectors, MSE-based design still beats coherence and random by the same margin as above.