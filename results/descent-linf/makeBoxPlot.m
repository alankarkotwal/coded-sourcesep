clear; close all; clc;

listing = dir('*-fin.mat');
files = {listing(:).name};

for fileID = 1:size(files, 2)
    
    disp(files{fileID});
    filestr = files{fileID};
    load(filestr);

    runName = filestr(1:end-4); 

    scrsz = get(groot, 'ScreenSize');
    figure('Position', scrsz);
    set(gca, 'FontSize', 50);

    noisesVec = 1:size(sparss, 2);

    relDiffs = bsxfun(@minus, errorBounds', rrmses) ./ rrmses;
    bp1 = boxplot(relDiffs, sparss, 'widths', 0.4);
    %xlim([0 size(noisesVec, 2)+1]);
    %ylim([0 2*mean(mean(rrmses(:)))]);
    %ylim([0 1.2*max(max(errorBounds))]);

    % color = [repmat('b',1,size(sparss, 2)), repmat('r', 1, size(sparss, 2))];
    % h = findobj(gca,'Tag','Box');
    % for j=1:length(h)
    %   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
    % end
    % c = get(gca, 'Children');
    % hleg1 = legend([c(1) c(size(sparss, 2)+1)], 'Corrected', 'Original', 'Location', 'NorthWest');

    %set(bp1(:,:),'linewidth',5);
    %set(bp2(:,:),'linewidth',5);

    xlabel('Sparsity')
    ylabel('Relative bound looseness');
    title(['Bound looseness: nMeas = ' num2str(nMeass(nMeasIndex)) ', n = ' num2str(n)]);
    set(gca, 'FontSize', 40);

    disp([runName '.png']);
    export_fig([runName '.png']);

end