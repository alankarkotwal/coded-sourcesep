clear; close all; clc;

listing = dir('*-randn-mat-vec-fin.mat');
files = {listing(:).name};

for fileID = 1:size(files, 2)
    
    disp(files{fileID});
    filestr = files{fileID};
    load(filestr);

    runName = filestr(1:end-4); 


    scrsz = get(groot, 'ScreenSize');
    figure('Position', scrsz);
    set(gca, 'FontSize', 50);
    hold on;

    bp = boxplot([diffs4n4, diffs4n5, diffs4n6, diffs4n7, diffs4n9, diffs10n10, diffs10n11, diffs10n13, diffsGnG1, diffsGnG2, finBoundRel], 'widths', 0.4);
    xlim([0.5 11.5]);
    ylim auto;

    % color = [repmat('b',1,size(spars, 1)), repmat('r', 1, size(spars, 1))];
    % h = findobj(gca,'Tag','Box');
    % for j=1:length(h)
    %    patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
    % end
    % c = get(gca, 'Children');
    % hleg1 = legend([c(1) c(size(spars, 1)+1)], 'Optimized', 'Original', 'Location', 'NorthEast');
    % 
    xlabel('Index of step');
    ylabel('Relative difference');
    title('Evolution of bound looseness across inequalities');
    set(gca, 'FontSize', 40);
    %savefig([runName '.fig']);

    disp([runName '.png']);
    export_fig([runName '.png'])

end
