clear; close all; clc;

listing = dir('*-100.mat');
files = {listing(:).name};

for fileID = 1:size(files, 2)
    
    disp(files{fileID});
    filestr = files{fileID};
    load(filestr);

    runName = filestr(1:end-4); 
    
    sparsVec = (1:size(spars, 1))';

    scrsz = get(groot, 'ScreenSize');
    figure('Position', scrsz);
    set(gca, 'FontSize', 50);
    hold on;

    bp1 = boxplot(rrmse, spars, 'Positions', sparsVec+0.25, 'widths', 0.4);
    bp2 = boxplot(rrmseOrig, spars, 'Positions', sparsVec-0.25, 'widths', 0.4);
    bp3 = boxplot(rrmseAvg, spars, 'Positions', sparsVec, 'widths', 0.4);
    xlim([0 size(spars, 1)+1]);
    ylim auto;

    color = [repmat('b',1,size(spars, 1)), repmat('r', 1, size(spars, 1)), repmat('g', 1, size(spars, 1))];
    h = findobj(gca,'Tag','Box');
    for j=1:length(h)
       patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
    end
    c = get(gca, 'Children');
    legend([c(1) c(size(spars, 1)+1) c(2*size(spars, 1)+1)], 'Max-Optimized', 'Original', 'Avg-Optimized', 'Location', 'NorthEast');

    xlabel('Sparsity');
    ylabel('RRMSE');
    title(['RRMSE comparison: n = ' num2str(n) ', T = ' num2str(T)]);
    set(gca, 'FontSize', 40);
    savefig([runName '-fin.fig']);
    disp([runName '-fin.fig']);

    scrsz = get(groot, 'ScreenSize');
    figure('Position', scrsz);
    set(gca, 'FontSize', 50);
    hold on;

    bp4 = boxplot(eigvals, spars, 'Positions', sparsVec+0.25, 'widths', 0.4);
    bp5 = boxplot(eigvalsOrig, spars, 'Positions', sparsVec-0.25, 'widths', 0.4);
    bp6 = boxplot(eigvalsAvg, spars, 'Positions', sparsVec, 'widths', 0.4);
    xlim([0 size(spars, 1)+1]);
    ylim auto;

    color = [repmat('b',1,size(spars, 1)), repmat('r', 1, size(spars, 1)), repmat('g', 1, size(spars, 1))];
    h = findobj(gca,'Tag','Box');
    for j=1:length(h)
       patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
    end
    c = get(gca, 'Children');
    legend([c(1) c(size(spars, 1)+1) c(2*size(spars, 1)+1)], 'Max-Optimized', 'Original', 'Avg-Optimized', 'Location', 'NorthEast');

    xlabel('Sparsity');
    ylabel('Maximum eigenvalue of AT * A - I');
    title(['Eigenvalue comparison: n = ' num2str(n) ', T = ' num2str(T)]);
    set(gca, 'FontSize', 40);
    savefig([runName '-eig-fin.fig']);
    disp([runName '-eig-fin.fig']);

end
